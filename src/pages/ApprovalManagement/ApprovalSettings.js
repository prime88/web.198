import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, Steps, Radio, Table } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated } from '@/services/approval';
import styles from './ApprovalSettings.less';
import PersonnelsAdd from "./PersonnelsAdd";
import { imgDomainName } from "../../constants";

const RadioGroup = Radio.Group;
const Step = Steps.Step;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, formList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isBrandNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增流程"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="流程名称">
        {form.getFieldDecorator('name', {
          rules: [
            { required: true, message: '请输入流程名称！' },
            // { validator: checkName }
          ],
        })(<Input placeholder="请输入流程名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="关联表单">
        {form.getFieldDecorator('approvalFormId', {
          rules: [
            { required: true, message: '请选择关联表单！' },
          ],
        })(
          <Select placeholder='请选择关联表单' style={{width: '100%'}} >
            {formList.map(item => <Select.Option key={item.approvalFormId} value={item.approvalFormId}>{item.name}</Select.Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="流程描述">
        {form.getFieldDecorator('description')(<Input.TextArea rows={3} placeholder="请输入流程描述" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record, formList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isBrandNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改流程"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="流程名称">
        {form.getFieldDecorator('name', {
          rules: [
            { required: true, message: '请输入流程名称！' },
            // { validator: checkName }
          ],
          initialValue: record.name
        })(<Input placeholder="请输入流程名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="关联表单">
        {form.getFieldDecorator('approvalFormId', {
          rules: [
            { required: true, message: '请选择关联表单！' },
          ],
          initialValue: record.approvalFormId
        })(
          <Select placeholder='请选择关联表单' style={{width: '100%'}} >
            {formList.map(item => <Select.Option key={item.approvalFormId} value={item.approvalFormId}>{item.name}</Select.Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="流程描述">
        {form.getFieldDecorator('description', {
          initialValue: record.description
        })(<Input.TextArea rows={3} placeholder="请输入流程描述" />)}
      </FormItem>
    </Modal>
  );
});

const NodeSettingModal = (props => {
  const { modalVisible, form, handleAdd, handleModalVisible, formList, handleNodeOperationModalVisible, flowNodeList, nodeRemove } = props;

  console.log('flowNodeList: ', flowNodeList);

  return (
    <Modal
      destroyOnClose
      // title={<div>设置流程节点<Button type='primary' style={{float: 'right'}}>新增节点</Button></div>}
      title='设置流程节点'
      visible={modalVisible}
      onCancel={() => handleModalVisible()}
      closable={false}
      width={420}
      footer={[
        <Button onClick={() => handleModalVisible()} type="primary">
          关闭
        </Button>,
      ]}
    >
      <Steps current={flowNodeList.length+1} direction="vertical" style={{paddingLeft: 130}}>
        <Step title="开始" description="审批开始"/>
        {flowNodeList.map(item => (
            <Step
              title={item.name}
              description={
                <Popconfirm
                  title={`确认删除 ${item.name} 节点？`}
                  onConfirm={() => nodeRemove(item.flowNodeId)}
                  okText="确认"
                  cancelText="取消"
                >
                  <a><Icon type="delete" /></a>
                </Popconfirm>
              }
              icon={
                <a onClick={() => handleNodeOperationModalVisible(true, item.flowNodeId)}>
                  <Icon style={{fontSize: 32}} type="form"/>
                </a>
              }
            />
          )
        )}
        <Step title='新增' description="新增节点" icon={<a onClick={() => handleNodeOperationModalVisible(true)}><Icon style={{fontSize: 32}} type="plus-circle"/></a>} />
        <Step title="结束" description="审批结束" icon={<Icon style={{fontSize: 32}} type="check-circle"/>}/>
      </Steps>
    </Modal>
  );
});

@Form.create()
class NodeOperationModal extends PureComponent {

  state = {
    visible: false,
    selectedRows: [],
    type: '',
  }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   const flowNodePersonnelList = nextProps.flowNodePersonnelList;
  //   let newState = null;
  //   if (flowNodePersonnelList.length > 0) {
  //     console.log('prevState.visible:', prevState.visible)
  //     console.log('getDerivedStateFromProps-flowNodePersonnelList')
  //     newState = {
  //       selectedRows: flowNodePersonnelList
  //     }
  //   }
  //   return newState
  // }

  componentWillReceiveProps(nextProps) {
    const flowNodePersonnelList = nextProps.flowNodePersonnelList;
    const { modalVisible, form } = this.props;
    // if (flowNodePersonnelList.length > 0 && modalVisible === false) {
    if (flowNodePersonnelList.length > 0 ) {
      console.log('进来了!!!')

      this.setState(prevState => {
        const { type } = prevState;
        console.log('type: ', type);
        return {
          selectedRows: type === '' ? flowNodePersonnelList : prevState.selectedRows
        }
      })
    }
  }

  handleOk = (e) => {
    console.log(e);
    const { selectedRows } = this.personnelsAdd.state;
    console.log('selectedRows: ', selectedRows);
    this.setState({
      visible: false,
      selectedRows,
      // name: selectedRows.length > 0 ? selectedRows[0].name : ''
    });
  }

  saveFormRef = (formRef) => {
    this.personnelsAdd = formRef;
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  okHandle = () => {
    const { form, handleUpdate, flowNode } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { selectedRows } = this.state;

      fieldsValue.personnelIds = selectedRows.map(item => item.personnelId).join(',')
      handleUpdate(fieldsValue, flowNode);

      form.resetFields();
      this.setState({ selectedRows: [], type: '' })
    });
  };

  render() {
    const { modalVisible, form, handleUpdate, handleUpdateModalVisible, record, formList, flowNode } = this.props;
    const { visible, selectedRows } = this.state;

    console.log('selectedRows: ', selectedRows);

    return (
      <Modal
        destroyOnClose
        title="节点操作"
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          this.setState({ selectedRows: [], type: '' });
          handleUpdateModalVisible();
        }}
        style={{top: 156}}
        mask={false}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="节点名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入节点名称！' },
            ],
            initialValue: flowNode ? flowNode.name : '',
          })(<Input placeholder="请输入节点名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="审批类型">
          {form.getFieldDecorator('type', {
            rules: [
              { required: true, message: '请输入审批类型！' },
            ],
            initialValue: flowNode ? flowNode.type : 1,
          })(
            <RadioGroup onChange={(e) => {
              this.setState({
                // selectedRows: [],
                type: e.target.value
              })
            }}>
              <Radio value={1}>单人审批</Radio>
              <Radio value={0}>多人审批</Radio>
            </RadioGroup>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<span>审批人(<a onClick={() => this.setState({ visible: true })}><Icon type="edit" /></a>)</span>} >
          <Table
            // dataSource={[{ name: '张三', branch: '运营部' }]}
            dataSource={selectedRows}
            size='small'
            pagination={false}
            rowKey='personnelId'
            columns={
              [
                {
                  title: '姓名',
                  dataIndex: 'name',
                  key: 'name',
                  width: '50%',
                },
                {
                  title: '机构',
                  dataIndex: 'branch',
                  key: 'branch',
                  width: '50%',
                }
              ]
            }
          />
        </FormItem>
        <Modal
          style={{top: 50}}
          width={'90%'}
          title="选择审批人"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <PersonnelsAdd selectedRows={selectedRows} tableType={form.getFieldsValue().type === 1 ? 'radio' : 'checkbox'} wrappedComponentRef={this.saveFormRef} />
        </Modal>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ approval, user, loading }) => ({
  approval,
  user,
  loading: loading.models.approval,
}))
@Form.create()
class ApprovalSettings extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    nodeSettingModalVisible: false,
    nodeOperationModalVisible: false,
    selectedRows: [],
    formValues: {},
    record: {},
    flowNode: '',
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '流程名称',
      dataIndex: 'name',
    },
    {
      title: '关联表单名称',
      dataIndex: 'formName',
    },
    {
      title: '流程描述',
      dataIndex: 'description',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    // {
    //   title: '备注',
    //   dataIndex: 'remark',
    // },
    {
      title: '操作',
      width: '20%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.nodeSetting(record)}>设置节点</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>编辑</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.flowId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  nodeSetting = (record) => {
    //打开modal
    this.handleNodeSettingModalVisible(true, record)

    //获取流程节点列表
    this.updateFlowNodeList(record);
  }

  updateFlowNodeList = (record) => {
    //获取流程节点列表
    const { dispatch } = this.props;
    dispatch({
      type: 'approval/fetchFlowNodeList',
      payload: {
        flowId: record.flowId
      }
    });
  }

  recordRemove = flowIds => {
    console.log('flowIds: ', flowIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'approval/removeFlow',
      payload: { flowIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'approval/changeFlowStatus',
      payload: { flowId: record.flowId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    //流程列表
    dispatch({
      type: 'approval/fetchFlowList',
    });

    //关联表单列表
    dispatch({
      type: 'approval/fetchFormList',
    });

  }

  getElementPermissionModule = (path, array) => {
    let result = '';
    for (let item of array) {
      if (item.route && item.route === path) {
        result = item;
        break;
      }
      else{
        // console.log('result-dj: ', result);
        if (item.childList && item.childList.length > 0) {
          result = this.getElementPermissionModule(path, item.childList)
          if (result) break;
        }
      }
    }
    return result;
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'approval/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleNodeSettingModalVisible = (flag, record) => {
    this.setState({
      nodeSettingModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleNodeOperationModalVisible = (flag, flowNodeId) => {
    const { dispatch, approval: { flowNodeList } } = this.props;

    let flowNode = '';
    if (flowNodeId && flag) {
      flowNode = flowNodeList.find(item => item.flowNodeId === flowNodeId);
      //根据flowNode.personnelIds找审批人列表
      dispatch({
        type: 'approval/fetchFlowNodePersonnelList',
        payload: {
          personnelIds: flowNode.personnelIds
        },
      });
    }
    else{
      //清除flowNodePersonnelList,避免影响新增
      dispatch({
        type: 'approval/saveFlowNodePersonnelList',
        payload: []
      });
    }

    this.setState({
      nodeOperationModalVisible: !!flag,
      flowNode: flowNodeId ? flowNode : '',
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'approval/addFlow',
      payload: fields,
      // callback: () => this.handleModalVisible(true)
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleNodeSetting = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'approval/addFlow',
      payload: fields,
      // callback: () => this.handleModalVisible(true)
    });

    // message.success('新增成功');
    this.handleNodeSettingModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'approval/updateFlow',
      payload: {
        ...fields,
        flowId: record.flowId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleNodeOperationUpdate = (fields, flowNode) => {
    const { dispatch } = this.props;
    const { record } = this.state;
    console.log('fields: ', fields);

    if (flowNode){//编辑操作
      dispatch({
        type: 'approval/updateFlowNode',
        payload: {
          ...fields,
          flowNodeId: flowNode.flowNodeId,
          flowId: record.flowId,
        },
      });
    }
    else{//新增操作
      dispatch({
        type: 'approval/addFlowNode',
        payload: {
          ...fields,
          flowId: record.flowId,
        },
      });
    }

    // message.success('配置成功');
    this.handleNodeOperationModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'warehouse/fetchInventoryList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        },
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'warehouse/fetchInventoryList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="流程名称">
              {getFieldDecorator('name')(<Input placeholder="请输入流程名称" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  nodeRemove = (flowNodeId) => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'approval/removeFlowNode',
      payload: {
        flowNodeId,
        flowId: record.flowId,
      }
    })
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.flowId === record.flowId) {
          newSelectedRows = newSelectedRows.filter(item => item.flowId !== record.flowId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  getElementPermissionList = (pathname, permissionList) => {
    let elementPermissionList = [];
    if (permissionList && permissionList.length > 0 && permissionList[0] && permissionList[0].childList) {
      const realPermissionList = permissionList[0].childList;
      if (pathname !== '/') {
        const elementPermissionModule = this.getElementPermissionModule(pathname, realPermissionList);
        // console.log('isExistInPermissionList: ', isExistInPermissionList);
        if (elementPermissionModule && elementPermissionModule.childList) {
          //元素权限列表
          elementPermissionList = elementPermissionModule.childList;
        }
      }
    }
    if (elementPermissionList.length > 0) {
      elementPermissionList = elementPermissionList.map(item => item.name)
    }
    return elementPermissionList;
  }

  render() {
    const {
      approval: { flowList, formList, flowNodeList, flowNodePersonnelList },
      location: { pathname },
      user: { currentUser: { permissionList } },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record, nodeSettingModalVisible, nodeOperationModalVisible, flowNode } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const nodeSettingMethods = {
      handleAdd: this.handleNodeSetting,
      handleModalVisible: this.handleNodeSettingModalVisible,
    };

    const nodeOperationMethods = {
      handleUpdateModalVisible: this.handleNodeOperationModalVisible,
      handleUpdate: this.handleNodeOperationUpdate,
    };

    //该页面拥有的元素权限列表
    const elementPermissionList = this.getElementPermissionList(pathname, permissionList);
    // console.log('elementPermissionList: ', elementPermissionList);

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <div className={styles.tableListOperator}>
              <Button style={{display: elementPermissionList.includes('新增') ? 'inline-block' : 'none'}} icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.flowId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={flowList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="flowId"
              onRow={(record) => {
                return {
                  onClick: () => {// 点击行
                    this.onRowClick(record)
                  },
                };
              }}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} formList={formList} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} formList={formList}/>
        <NodeSettingModal
          {...nodeSettingMethods}
          flowNodeList={flowNodeList}
          modalVisible={nodeSettingModalVisible}
          handleNodeOperationModalVisible={this.handleNodeOperationModalVisible}
          nodeRemove={this.nodeRemove}
        />
        <NodeOperationModal
          {...nodeOperationMethods}
          flowNode={flowNode}
          modalVisible={nodeOperationModalVisible}
          record={record}
          flowNodePersonnelList={flowNodePersonnelList}
        />
      </div>
    );
  }
}

export default ApprovalSettings;
