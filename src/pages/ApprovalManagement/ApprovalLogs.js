import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, DatePicker } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated } from '@/services/brand';
import styles from './ApprovalLogs.less';
import PurchaseingPlanListDetail from "./PurchaseingPlanListDetail";
import ProcurementPlanListDetail from "./ProcurementPlanListDetail";
import ProcurementOrderListDetail from "./ProcurementOrderListDetail";
import ArrivalOrderListDetail from "./ArrivalOrderListDetail";
import WarehousingOrderListDetail from "./WarehousingOrderListDetail";
import DeliveryApplyOrderListDetail from "./DeliveryApplyOrderListDetail";
import DeliveryOrderListDetail from "./DeliveryOrderListDetail";
import DeliveryOutOrderListDetail from "./DeliveryOutOrderListDetail";
import DeliveryInOrderListDetail from "./DeliveryInOrderListDetail";
import OtherInOrderListDetail from "./OtherInOrderListDetail";
import OtherOutOrderListDetail from "./OtherOutOrderListDetail";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const LogDetail = props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let detailComponent = null;
  switch (record.approvalFormId) {
    case 1://门店请购详情
      detailComponent = <PurchaseingPlanListDetail record={record}/>;
      break;
    case 2://采购计划单详情
      detailComponent = <ProcurementPlanListDetail record={record}/>;
      break;
    case 3://采购订单详情
      detailComponent = <ProcurementOrderListDetail record={record}/>;
      break;
    case 4://采购到货单详情
      detailComponent = <ArrivalOrderListDetail record={record}/>;
      break;
    case 5://采购入库单详情
      detailComponent = <WarehousingOrderListDetail record={record}/>;
      break;
    case 6://调货申请单详情
      detailComponent = <DeliveryApplyOrderListDetail record={record}/>;
      break;
    case 7://调货单详情
      detailComponent = <DeliveryOrderListDetail record={record}/>;
      break;
    case 8://调货出库单详情
      detailComponent = <DeliveryOutOrderListDetail record={record}/>;
      break;
    case 9://调货入库单详情
      detailComponent = <DeliveryInOrderListDetail record={record}/>;
      break;
    case 10://其他入库单详情
      detailComponent = <OtherInOrderListDetail record={record}/>;
      break;
    case 11://其他出库单详情
      detailComponent = <OtherOutOrderListDetail record={record}/>;
      break;
  }


  return (
    <Modal
      width='80%'
      title='审批详情'
      visible={detailModalVisible}
      onCancel={() => handleDetailModalVisible()}
      footer={[
        <Button onClick={() => handleDetailModalVisible()} type="primary">
          关闭
        </Button>,
      ]}
    >
      {detailComponent}
    </Modal>
  );
}

/* eslint react/no-multi-comp:0 */
@connect(({ approval, user, loading }) => ({
  approval,
  user,
  loading: loading.models.approval,
}))
@Form.create()
class ApprovalLogs extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    detailModalVisible: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '流程名称',
      dataIndex: 'flowName',
    },
    {
      title: '发起人',
      dataIndex: 'originator',
    },
    {
      title: '发起时间',
      dataIndex: 'createTime',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '运行中';
            break;
          case '1':
            result = '已批准';
            break;
          case '2':
            result = '不批准';
            break;
        }
        return result;
      }
    },
    // {
    //   title: '备注',
    //   dataIndex: 'remark',
    // },
    {
      title: '操作',
      width: '10%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleDetailModalVisible(true, record)}>查看进度</a>
          {/*<a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>*/}
          {/*<Divider type="vertical" />*/}
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.brandId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
          {/*>*/}
            {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  recordRemove = brandIds => {
    console.log('brandIds: ', brandIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'brand/removeBrand',
      payload: { brandIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'approval/changeBrandStatus',
      payload: { brandId: record.brandId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch, user: { currentUser }, isHome } = this.props;
    dispatch({
      type: 'approval/fetchApprovalLogList',
      payload: {
        type: 1,
        personnelId: currentUser.personnelId
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'approval/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);

    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'approval/addBrand',
      payload: fields,
      callback: () => this.handleModalVisible(true)
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'approval/updateBrand',
      payload: {
        ...fields,
        brandId: record.brandId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form, user: { currentUser } } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const startTime = fieldsValue.startTime;
      const endTime = fieldsValue.endTime;
      if (startTime) {
        fieldsValue.startTime = startTime.format('YYYY-MM-DD HH:mm');
      }
      if (endTime) {
        fieldsValue.endTime = endTime.format('YYYY-MM-DD HH:mm');
      }

      dispatch({
        type: 'approval/fetchApprovalLogList',
        payload: {
          ...fieldsValue,
          personnelId: currentUser.personnelId
        }
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch, user: { currentUser } } = this.props;
    form.resetFields();
    dispatch({
      type: 'approval/fetchApprovalLogList',
      payload: {
        personnelId: currentUser.personnelId,
        type: 1
      }
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="流程分类">
              {getFieldDecorator('type', {
                initialValue: 1
              })(
                <Select placeholder='请选择流程分类'>
                  {/*本人创建的单子生成的审批*/}
                  <Select.Option value={0}>我发起的</Select.Option>
                  {/*1、审批的某个节点中的审批人包含自己；2、包含自己的节点处于运行中的状态*/}
                  <Select.Option value={1}>待办审批</Select.Option>
                  {/*1、审批的某个节点中的审批人包含自己；2、包含自己的节点被自己操作过(批准、不批准)*/}
                  <Select.Option value={2}>已办审批</Select.Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="流程名称">
              {getFieldDecorator('flowName')(<Input placeholder="请输入流程名称" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="流程状态">
              {getFieldDecorator('status')(
                <Select allowClear placeholder='请选择流程状态'>
                  <Select.Option value={0}>运行中</Select.Option>
                  <Select.Option value={1}>已批准</Select.Option>
                  <Select.Option value={2}>不批准</Select.Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="开始时间">
              {getFieldDecorator('startTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择开始时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择结束时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    // return;
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetail = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'storePurchaseing/updatePurchase',
      payload: { ...fields, purchaseRequisitionId: record.purchaseRequisitionId },
    });

    this.handleUpdateModalVisible();
  };

  render() {
    const {
      approval: { approvalLogList },
      loading,
      isHome,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record, detailModalVisible } = this.state;

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
      handleDetail: this.handleDetail,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.title} style={{display: isHome ? 'block' : 'none'}}>待办审批</div>
          <div className={styles.tableList}>
            <div style={{display: isHome ? 'none' : 'block'}} className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <StandardTable
              paginationOfTable={{defaultPageSize: isHome ? 5 : 10}}
              // selectedRows={selectedRows}
              loading={loading}
              data={approvalLogList}
              columns={this.columns}
              // onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="approvalLogId"
              footer={() => <div>总计：{approvalLogList.rows.length}</div>
              }
            />
          </div>
        </Card>
        <LogDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
      </div>
    );
  }
}

export default ApprovalLogs;
