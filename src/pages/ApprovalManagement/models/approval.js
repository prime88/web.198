// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchFlowList,
  fetchFlowNodeList,
  fetchFlowNodePersonnelList,
  fetchFormList,
  fetchApprovalLogList,
  fetchPurchaseingPlanApprovalLogDetail,
  fetchPurchaseingInventoryList,
  fetchProcurementInventoryList,
  fetchProcurementOrderInventoryList,
  fetchArrivalOrderInventoryList,
  fetchWarehousingOrderInventoryList,
  fetchDeliveryApplyOrderInventoryList,
  fetchDeliveryOrderInventoryList,
  fetchDeliveryOutOrderInventoryList,
  fetchDeliveryInOrderInventoryList,
  fetchOtherInInventoryList,
  fetchOtherOutOrderInventoryList,
  addFlow,
  addFlowNode,
  updateFlow,
  updateFlowNode,
  approval,
  removeFlow,
  removeFlowNode,
  changeFlowStatus,
} from '@/services/approval';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';
import { fetchBranchList, fetchStaffList } from '@/services/branch';
// import { fetchPurchaseingInventoryList } from '@/services/storeProcurement';

export default {
  namespace: 'approval',

  state: {
    //流程列表
    flowList: {
      rows: [],
      pagination: {},
    },
    //表单列表
    formList: [],
    //部门列表
    branchList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //流程节点列表
    flowNodeList: [],
    //流程节点审批人列表
    flowNodePersonnelList: [],
    //审批日志列表
    approvalLogList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchFlowList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFlowList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFlowList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchFormList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFormList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFormList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchFlowNodeList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFlowNodeList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFlowNodeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchFlowNodePersonnelList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFlowNodePersonnelList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFlowNodePersonnelList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchApprovalLogList({ payload, callback }, { call, put }) {
      const response = yield call(fetchApprovalLogList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveApprovalLogList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *refreshApprovalLogList({ payload, callback }, { call, put, select }) {
      const user = yield select(state => state.user);
      yield put({
        type: 'fetchApprovalLogList',
        payload: {
          type: 1,
          personnelId: user.currentUser.personnelId
        }
      });
    },
    *fetchPurchaseingPlanApprovalLogDetail({ payload, callback }, { call, put }) {
      const response = yield call(fetchPurchaseingPlanApprovalLogDetail, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchPurchaseingInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchPurchaseingInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchProcurementInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProcurementInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchProcurementOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProcurementOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchArrivalOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchArrivalOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchWarehousingOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchWarehousingOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryApplyOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryApplyOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryOutOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryOutOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryInOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryInOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchOtherInInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOtherInInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchOtherOutOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOtherOutOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *addFlow({ payload, callback }, { call, put }) {
      const response = yield call(addFlow, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addFlowNode({ payload, callback }, { call, put }) {
      const response = yield call(addFlowNode, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowNodeList',
          payload: {
            flowId: payload.flowId
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeFlow({ payload, callback }, { call, put }) {
      const response = yield call(removeFlow, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeFlowNode({ payload, callback }, { call, put }) {
      const response = yield call(removeFlowNode, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchFlowNodeList',
          payload: {
            flowId: payload.flowId
          }
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateFlow({ payload, callback }, { call, put }) {
      const response = yield call(updateFlow, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateFlowNode({ payload, callback }, { call, put }) {
      const response = yield call(updateFlowNode, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowNodeList',
          payload: {
            flowId: payload.flowId
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *approval({ payload, callback }, { call, put }) {
      const response = yield call(approval, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeFlowStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeFlowStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveFlowList(state, action) {
      return {
        ...state,
        flowList: action.payload,
      };
    },
    saveFormList(state, action) {
      return {
        ...state,
        formList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveFlowNodeList(state, action) {
      return {
        ...state,
        flowNodeList: action.payload,
      };
    },
    saveFlowNodePersonnelList(state, action) {
      return {
        ...state,
        flowNodePersonnelList: action.payload,
      };
    },
    saveApprovalLogList(state, action) {
      return {
        ...state,
        approvalLogList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //停留在本页面时的订单通知的数据刷新
        if (location.pathname === '/approval-management/approval-logs' && location.query.type === '同页面跳转') {
          console.log('同页面跳转');
          dispatch({
            type: 'refreshApprovalLogList',
          });
        }
        // //品牌管理
        // if (location.pathname === '/basic-data/brand-management') {
        //   dispatch({
        //     type: 'fetchBrandList',
        //   })
        // }
      });
    },
  },
};
