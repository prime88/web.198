import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Checkbox, message } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isStaffManagementNameRepeated } from '@/services/staff';
import styles from './StaffManagement.less';
import ConnectedStaffNameAdd from './ConnectedStaffNameAdd';
import { imgDomainName } from "../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {

  state = {
    visible: false,
    selectedRows: [],
    name: '',
  }

  okHandle = () => {
    const { form, handleAdd } = this.props;
    const { selectedRows } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (selectedRows.length === 0){
        message.warning('请选择关联人员');
        return;
      }

      fieldsValue.personnelId = selectedRows[0].personnelId;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ name: '' })
      handleAdd(fieldsValue);
    });
  };

  handleOk = (e) => {
    console.log(e);
    const { selectedRows } = this.connectedStaffNameAdd.state;
    console.log('selectedRows: ', selectedRows);
    this.setState({
      visible: false,
      selectedRows,
      name: selectedRows.length > 0 ? selectedRows[0].name : ''
    });
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  saveFormRef = (formRef) => {
    this.connectedStaffNameAdd = formRef;
  }

  render() {
    const { modalVisible, form, handleModalVisible } = this.props;
    const { visible, name } = this.state;

    return (
      <Modal
        style={{top: 100}}
        destroyOnClose
        title="新增用户"
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ name: '' })
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="用户账号">
          {form.getFieldDecorator('username', {
            rules: [
              { required: true, message: '请输入用户账号！' },
              // { validator: checkName }
            ],
          })(<Input placeholder="请输入用户账号" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="关联人员">
          <span className="ant-form-text">{name}</span>
          <a style={{float: 'right'}} href="#" onClick={() => this.setState({ visible: true })}>选择</a>
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15, offset: 5 }}>
          {form.getFieldDecorator('isMultiplayer', {
            valuePropName: 'checked',
            initialValue: false,
          })(
            <Checkbox>多用户公用</Checkbox>
          )}
        </FormItem>

        <Modal
          style={{top: 50}}
          width={'90%'}
          title="选择关联人员"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <ConnectedStaffNameAdd wrappedComponentRef={this.saveFormRef} />
        </Modal>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  state = {
    visible: false,
    selectedRows: [],
    name: '',
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record }  = nextProps;
    const { name } = prevState;
    let newState = null;
    if (!name && record.name !== name) {
      newState = {
        ...newState,
        name: record.name
      }
    }

    return newState;
  }

  okHandle = () => {
    const { form, handleUpdate, record } = this.props;
    const { name, selectedRows } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (!name){
        message.warning('请选择关联人员');
        return;
      }
      if (selectedRows.length > 0) {
        fieldsValue.personnelId = selectedRows[0].personnelId;
      }
      else{
        fieldsValue.personnelId = record.personnelId;
      }
      // fieldsValue.personnelId = record.personnelId;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ name: '' });
      handleUpdate(fieldsValue);
    });
  };

  handleOk = (e) => {
    console.log(e);
    const { selectedRows } = this.connectedStaffNameAdd.state;
    console.log('selectedRows: ', selectedRows);
    this.setState({
      visible: false,
      selectedRows,
      name: selectedRows.length > 0 ? selectedRows[0].name : ''
    });
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  saveFormRef = (formRef) => {
    this.connectedStaffNameAdd = formRef;
  }

  render () {
    const { updateModalVisible, form, handleUpdateModalVisible, record } = this.props;
    const { visible, name } = this.state;

    return (
      <Modal
        style={{top: 100}}
        destroyOnClose
        title="修改用户"
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ name: '' });
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="用户账号">
          {/*{form.getFieldDecorator('username', {*/}
            {/*rules: [*/}
              {/*{ required: true, message: '请输入用户账号！' },*/}
              {/*// { validator: checkName }*/}
            {/*],*/}
            {/*initialValue: record.username,*/}
          {/*})(<Input placeholder="请输入用户账号" />)}*/}
          <span>{record.username}</span>
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="关联人员">
          <span className="ant-form-text">{name}</span>
          <a style={{float: 'right'}} href="#" onClick={() => this.setState({ visible: true })}>选择</a>
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15, offset: 5 }}>
          {form.getFieldDecorator('isMultiplayer', {
            valuePropName: 'checked',
            initialValue: record.isMultiplayer,
          })(
            <Checkbox>多用户公用</Checkbox>
          )}
        </FormItem>

        <Modal
          style={{top: 50}}
          width={'90%'}
          title="选择关联人员"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <ConnectedStaffNameAdd wrappedComponentRef={this.saveFormRef} record={record} />
        </Modal>
      </Modal>
    );
  }
}

const SendForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, messageList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="发送消息"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="消息">
        {form.getFieldDecorator('messageTemplateId', {
          rules: [{ required: true, message: '请选择消息！' }],
        })(
          <Select style={{width: '100%'}}>
            {messageList.rows.map(item => <Select.Option key={item.messageTemplateId} value={item.messageTemplateId}>{item.title}</Select.Option>)}
          </Select>
        )}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ staff, role, user, loading }) => ({
  staff,
  role,
  user,
  loading: loading.models.staff,
}))
@Form.create()
class StaffManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    setRolevisible: false,
    sendModalVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'staffId',
    },
    {
      title: '用户账号',
      dataIndex: 'username',
    },
    {
      title: '用户实名',
      dataIndex: 'name',
    },
    {
      title: '多用户公用账号',
      dataIndex: 'isMultiplayer',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '拥有角色',
      dataIndex: 'roles',
      render: text => text ? text : <span style={{color: 'red'}}>请设置角色 -></span>
    },
    {
      title: '操作',
      width: '25%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => {
            console.log('record: ', record);
            this.setState({ setRolevisible: true, record });
          }}>设置角色</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认重置为123456？"
            onConfirm={() => this.resetPassword(record.staffId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">重置密码</a>
          </Popconfirm>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.staffId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  resetPassword = (staffId) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'staff/resetPassword',
      payload: { staffId }
    })
  }

  recordRemove = staffIds => {
    console.log('staffIds: ', staffIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'staff/removeStaff',
      payload: { staffIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'staff/changeStaffManagementStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //员工列表
    dispatch({
      type: 'staff/fetchStaffList',
    });

    //角色列表
    dispatch({
      type: 'role/fetchRoleList',
    });

    //消息列表
    dispatch({
      type: 'staff/fetchMessageList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'staff/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'staff/addStaff',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'staff/updateStaff',
      payload: {
        ...fields,
        staffId: record.staffId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleRoleSettingOk = () => {
    const { form, dispatch } = this.props;
    const { record } = this.state;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      const payload = {
        ...values,
        roleIds: values.roleIds.join(','),
        staffId: record.staffId
      }
      dispatch({
        type: 'staff/setStaffRole',
        payload
      });

      form.resetFields();
      this.setState({ setRolevisible: false });
    });
  }

  onSendToUser = () => {

  }

  handleSend = fields => {
    const { dispatch, user: { currentUser } } = this.props;
    console.log('fields: ', fields);
    console.log('currentUser: ', currentUser);

    // dispatch({
    //   type: 'rule/add',
    //   payload: {
    //     desc: fields.desc,
    //   },
    // });
    //
    // message.success('添加成功');
    this.handleSendModalVisible();
  };

  handleSendModalVisible = flag => {
    this.setState({
      sendModalVisible: !!flag,
    });
  };

  render() {
    const {
      staff: { staffList, messageList },
      role: { roleList },
      loading,
      form,
    } = this.props;
    const { getFieldDecorator } = form;
    const { selectedRows, modalVisible, updateModalVisible, record, setRolevisible, sendModalVisible } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const sendMethods = {
      handleAdd: this.handleSend,
      handleModalVisible: this.handleSendModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  {/*<Button onClick={() => this.handleSendModalVisible(true)}>发送消息</Button>*/}
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.staffId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={staffList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="staffId"
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />
        {/*<SendForm {...sendMethods} modalVisible={sendModalVisible} messageList={messageList} />*/}
        <Modal
          title="设置角色"
          visible={setRolevisible}
          onOk={this.handleRoleSettingOk}
          onCancel={() => this.setState({ setRolevisible: false }) }
        >
          <Form>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="角色">
              {getFieldDecorator('roleIds', {
                rules: [{ required: true, message: '请选择角色!' }],
                initialValue: record.roleIds ? record.roleIds.split(',') : []
              })(
                <Select optionFilterProp='name' placeholder='请选择角色' mode='multiple'>
                  {roleList.rows.map(item => (
                    <Select.Option key={item.roleId} name={item.name}>{item.name}</Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default StaffManagement;
