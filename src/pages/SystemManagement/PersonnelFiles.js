import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, Tree,
  Upload, Avatar, Radio, message, TreeSelect
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBranchNameRepeated } from '@/services/branch';
import styles from './PersonnelFiles.less';
import {apiDomainName, imgDomainName} from "../../constants";

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {

  state = {
    loading: false,
    imageUrl: '',
  };

  okHandle = () => {
    const { form, handleAdd } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.avatarUrl){
        fieldsValue.avatarUrl = fieldsValue.avatarUrl.length >0 ? fieldsValue.avatarUrl[0].response.msg : '';
      }
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ imageUrl: '' })
      handleAdd(fieldsValue);
    });
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  render() {
    const { modalVisible, form, handleModalVisible } = this.props;
    const { imageUrl } = this.state;

    const uploadButton = (
      <Avatar size={86} icon="user" />
      // <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
      //   <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
      //   {/*<div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>*/}
      // </div>
    );

    return (
      <Modal
        destroyOnClose
        title='新增员工'
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ imageUrl: '' })
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="头像">
          {form.getFieldDecorator('avatarUrl', {
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            // rules: [{ required: true, message: '请选择头像！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <Avatar src={imageUrl} size={86} icon="user" />
                // <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="姓名">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入员工姓名！' },
            ],
          })(<Input placeholder="请输入员工姓名" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="性别">
          {form.getFieldDecorator('gender', {
            rules: [{ required: true, message: '请选择员工性别！' }],
            initialValue: 1
          })(
            <RadioGroup>
              <Radio value={1}>男</Radio>
              <Radio value={2}>女</Radio>
            </RadioGroup>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="岗位">
          {form.getFieldDecorator('post', {
            // rules: [{ required: true, message: '请输入岗位！' }],
          })(<Input placeholder="请输入岗位" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="手机号">
          {form.getFieldDecorator('phone', {
            // rules: [{ required: true, message: '请输入手机号！' }],
          })(<Input placeholder="请输入手机号" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="邮箱">
          {form.getFieldDecorator('mailbox', {
            // rules: [{ required: true, message: '请输入邮箱！' }],
          })(<Input placeholder="请输入邮箱" />)}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {

  state = {
    loading: false,
    imageUrl: '',
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record }  = nextProps;
    const { imageUrl } = prevState;
    let newState = null;
    if (record.avatarUrl && !imageUrl) {
      newState = {
        ...newState,
        imageUrl: `${imgDomainName}${record.avatarUrl}`,
      };
    }
    return newState
  }

  okHandle = () => {
    const { form, handleUpdate } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.avatarUrl){
        fieldsValue.avatarUrl = fieldsValue.avatarUrl.length >0 ? fieldsValue.avatarUrl[fieldsValue.avatarUrl.length-1].response.msg : '';
      }
      form.resetFields();
      this.setState({ imageUrl: '' });
      handleUpdate(fieldsValue);
    });
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  beforeUpload = (file) => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  }

  getChildren = childList => {
    return childList.map(item => ({
      title: item.name,
      value: item.branchId,
      key: item.branchId,
      children: item.childList ? this.getChildren(item.childList) : []
    }))
  }

  render() {
    const { updateModalVisible, form, handleUpdateModalVisible, record, branchList } = this.props;
    const { imageUrl } = this.state;

    const uploadButton = (
      <Avatar size={86} icon="user" />
    );

    const treeData = this.getChildren(branchList);

    return (
      <Modal
        destroyOnClose
        title="修改员工"
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ imageUrl: '' })
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="头像">
          {form.getFieldDecorator('avatarUrl', {
            initialValue: record.avatarUrl ? [{ response: { msg: record.avatarUrl } }] : '',
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            // rules: [{ required: true, message: '请选择头像！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <Avatar src={imageUrl} size={86} icon="user" />
                // <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="姓名">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入员工姓名！' },
            ],
            initialValue: record.name,
          })(<Input placeholder="请输入员工姓名" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="性别">
          {form.getFieldDecorator('gender', {
            rules: [{ required: true, message: '请选择员工性别！' }],
            initialValue: record.gender,
          })(
            <RadioGroup>
              <Radio value={1}>男</Radio>
              <Radio value={2}>女</Radio>
            </RadioGroup>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="部门">
          {form.getFieldDecorator('branchId', {
            rules: [{ required: true, message: '请输入部门！' }],
            initialValue: record.branchId,
          })(
            <TreeSelect
              style={{ width: 300 }}
              // value={this.state.value}
              dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
              treeData={treeData}
              placeholder="请选择部门"
              treeDefaultExpandAll
              // onChange={this.onChange}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="岗位">
          {form.getFieldDecorator('post', {
            // rules: [{ required: true, message: '请输入岗位！' }],
            initialValue: record.post,
          })(<Input placeholder="请输入岗位" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="手机号">
          {form.getFieldDecorator('phone', {
            // rules: [{ required: true, message: '请输入手机号！' }],
            initialValue: record.phone,
          })(<Input placeholder="请输入手机号" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="邮箱">
          {form.getFieldDecorator('mailbox', {
            // rules: [{ required: true, message: '请输入邮箱！' }],
            initialValue: record.mailbox,
          })(<Input placeholder="请输入邮箱" />)}
        </FormItem>
      </Modal>
    );
  }
}
/* eslint react/no-multi-comp:0 */
@connect(({ branch, loading }) => ({
  branch,
  loading: loading.models.branch,
}))
@Form.create()
class PersonnelFiles extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    selectedKeys: ['1'],
    expandedKeys: [],//此处设置初始值失效，因为treedata是在后面componentdidmount中获取的
    branchModalAddVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'personnelId',
      width: 70,
      fixed: 'left'
    },
    {
      title: '头像',
      dataIndex: 'avatarUrl',
      width: 100,
      fixed: 'left',
      render: (text) => (text ? <Avatar icon="user" src={`${imgDomainName}${text}`} size={64} /> : ''),
    },
    {
      title: '姓名',
      dataIndex: 'name',
      width: 100,
      fixed: 'left'
    },
    {
      title: '性别',
      dataIndex: 'gender',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '1':
            result = '男';
            break;
          case '2':
            result = '女';
            break;
        }
        return result;
      }
    },
    {
      title: '部门',
      dataIndex: 'branch',
    },
    {
      title: '岗位',
      dataIndex: 'post',
    },
    {
      title: '手机号',
      dataIndex: 'phone',
    },
    {
      title: '办公邮箱',
      dataIndex: 'mailbox',
    },
    {
      title: '操作',
      width: 110,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.personnelId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = personnelIds => {
    console.log('personnelIds: ', personnelIds);
    const { dispatch } = this.props;
    const { selectedKeys, selectedRows } = this.state;
    dispatch({
      type: 'branch/removeStaff',
      payload: { personnelIds },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({
            selectedKeys,
            selectedRows: [],
          });

          // 获取部门员工列表
          dispatch({
            type: 'branch/fetchStaffList',
            payload: {
              // branchId: selectedKeys[0].toString() === '1' ? '' : selectedKeys[0],
              branchId: selectedKeys[0],
              getChild: '',//传此参数时，表示返回branchId以及其下子部门的员工
            },
          });
        }
      }
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'branch/changePersonnelFilesStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //部门列表
    dispatch({
      type: 'branch/fetchBranchList',
    });

    // 获取部门员工列表
    dispatch({
      type: 'branch/fetchStaffList',
    });

    this.setState({ expandedKeys: ['1'] })
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'branch/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { selectedKeys } = this.state;

    dispatch({
      type: 'branch/addBranchStaff',
      payload: {
        ...fields,
        branchId: selectedKeys[0],
      },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({ selectedKeys });

          // 获取部门员工列表
          dispatch({
            type: 'branch/fetchStaffList',
            payload: {
              // branchId: selectedKeys[0].toString() === '1' ? '' : selectedKeys[0],
              branchId: selectedKeys[0],
              getChild: '',//传此参数时，表示返回branchId以及其下子部门的员工
            },
          });
        }
      }
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedKeys } = this.state;
    dispatch({
      type: 'branch/updateBranchStaff',
      payload: {
        ...fields,
        personnelId: record.personnelId,
      },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({ selectedKeys });

          // 获取部门员工列表
          dispatch({
            type: 'branch/fetchStaffList',
            payload: {
              // branchId: selectedKeys[0].toString() === '1' ? '' : selectedKeys[0],
              branchId: selectedKeys[0],
              getChild: '',//传此参数时，表示返回branchId以及其下子部门的员工
            },
          });
        }
      }
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  loadData = (dataTree) => {
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.branchId}>
            {item.childList ? this.loadData(item.childList) : null}
          </TreeNode>
        )
      })
    )
  }

  onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info);
    const { dispatch } = this.props;

    if (selectedKeys.length) {
      // 获取部门员工列表
      dispatch({
        type: 'branch/fetchStaffList',
        payload: {
          // branchId: selectedKeys[0].toString() === '1' ? '' : selectedKeys[0],
          branchId: selectedKeys[0],
          getChild: '',//传此参数时，表示返回branchId以及其下子部门的员工
        },
      });
      this.setState({ selectedKeys });
    }
  };

  onExpand = (expandedKeys) => {
    console.log('expandedKeys: ', expandedKeys);
    this.setState({
      expandedKeys,
    });
  }

  render() {
    const {
      branch: { branchList, staffList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record, selectedKeys, expandedKeys } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    console.log('branchList: ', branchList);

    return (
      <div>
        <Card bordered={false}>
          <Row >
            <Col span={5}>
              <div style={{ overflowX: 'scroll'}}>
                <Tree
                  selectedKeys={selectedKeys}
                  onSelect={this.onSelect}
                  expandedKeys={expandedKeys}
                  onExpand={this.onExpand}
                >
                  {this.loadData(branchList)}
                </Tree>
              </div>
            </Col>
            <Col span={19}>
              <div className={styles.tableList}>
                <div className={styles.tableListOperator}>
                  <Button
                    disabled={selectedKeys.length <= 0}
                    icon="plus"
                    type="primary"
                    onClick={() => {
                      this.handleModalVisible(true);
                    }}>
                    新增
                  </Button>
                  {selectedRows.length > 0 && (
                    <span>
                      <Popconfirm
                        title="确认批量删除？"
                        onConfirm={() =>
                          this.recordRemove(selectedRows.map(item => item.personnelId).join(','))
                        }
                        okText="确认"
                        cancelText="取消"
                      >
                        <Button>批量删除</Button>
                      </Popconfirm>
                    </span>
                  )}
                </div>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={staffList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey="personnelId"
                  scroll={{x: 1200}}
                />
              </div>
            </Col>
          </Row>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} branchList={branchList} />
      </div>
    );
  }
}

export default PersonnelFiles;
