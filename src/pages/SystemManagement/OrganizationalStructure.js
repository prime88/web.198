import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, Tree, Radio, Upload, TimePicker, InputNumber, Tooltip, Popover } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBranchNameRepeated } from '@/services/branch';
import styles from './OrganizationalStructure.less';
import {apiDomainName} from "../../constants";
import { Map, Marker } from 'react-amap';
import { queryGadMapInputtipsList } from '@/services/common-api';
import router from 'umi/router';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    tips: [],
    center: [0, 0],
    storeFormDisplay: 'none',
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    console.log('info: ', info);
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  onPositionChange = value => {
    console.log('onPositionChange-value: ', value);
    const { tips } = this.state;
    this.setState({ position: value });

    const target = tips[value.split(',')[0]];
    if (typeof target.location === 'string') {
      this.setState({ center: target.location.split(',') });
    } else {
      this.setState({ center: [0, 0] });
    }
  };

  okHandle = () => {
    const { form, handleAdd } = this.props;
    const { center } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (fieldsValue.type === 1) {
        fieldsValue.storeUrl = fieldsValue.storeUrl.length >0 ? fieldsValue.storeUrl[0].response.msg : '';
        fieldsValue.startBusinessHours = fieldsValue.startBusinessHours.format('HH:mm');
        fieldsValue.endBusinessHours = fieldsValue.endBusinessHours.format('HH:mm');
        fieldsValue.coordinate = center.join(',');
        const location = fieldsValue.location.split(',');
        fieldsValue.location = location.length > 1 ? location[1] : '';
        this.setState({ imageUrl: '', center: [0, 0], storeFormDisplay: 'none' });
      }
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  handleSearch = value => {
    console.log('handleSearch-value: ', value);
    if (!value) return;
    // 获取位置信息列表
    const response = queryGadMapInputtipsList({ keywords: value.replace(/\s*/g, '') });
    console.log('response: ', response.then());
    response.then(result => {
      console.log('result: ', result);
      this.setState({ tips: result.tips });
    });
  };

  render() {
    const { modalVisible, form, handleModalVisible, addModalTitle, staffList } = this.props;
    const { loading, imageUrl, tips, center, storeFormDisplay } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    const checkName = (rule, value, callback) => {
      if (!value) {
        callback();
        return;
      }

      const response = isBranchNameRepeated({ name: value });
      response.then(result => {
        if (!result.data) {
          callback();
          return;
        }
        callback('存在相同的名称!');
      });
    };

    return (
      <Modal
        destroyOnClose
        title={addModalTitle}
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ storeFormDisplay: 'none' });
        }}
      >
        <FormItem
          labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}
          label="组织类型"
        >
          {form.getFieldDecorator('type', {
            initialValue: 0,
            rules: [
              { required: true, message: '请选择组织类型！' },
            ]
          })(
            <RadioGroup
              onChange={(e) => this.setState({ storeFormDisplay: e.target.value ? 'block' : 'none' })}
            >
              <Radio value={0}>非门店</Radio>
              <Radio value={1}>门店</Radio>
            </RadioGroup>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="组织名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入组织名称！' },
              { validator: checkName }
            ],
          })(<Input placeholder="请输入组织名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 16 }} label="部门负责人1">
          {form.getFieldDecorator('personLiableId')(
            <Select allowClear placeholder="请选择部门负责人" style={{width: '94%'}}>
              { staffList.rows.map(item => <Select.Option key={item.personnelId}>{item.name}</Select.Option>) }
            </Select>
          )}
          <Popover
            content={
              <span>
                <a onClick={() => {
                  router.push({
                    pathname: '/system-management/personnel-files',
                  });
                }}>前往</a>
                人员档案添加
              </span>
            }
            title="友情提示"
          >
            <Icon type="question-circle-o" />
          </Popover>
        </FormItem>
        <FormItem allowClear labelCol={{ span: 5 }} wrapperCol={{ span: 16 }} label="部门负责人2">
          {form.getFieldDecorator('personLiable2Id')(
            <Select placeholder="请选择部门负责人" style={{width: '94%'}}>
              { staffList.rows.map(item => <Select.Option key={item.personnelId}>{item.name}</Select.Option>) }
            </Select>
          )}
          <Popover
            content={
              <span>
                <a onClick={() => {
                  router.push({
                    pathname: '/system-management/personnel-files',
                  });
                }}>前往</a>
                人员档案添加
              </span>
            }
            title="友情提示"
          >
            <Icon type="question-circle-o" />
          </Popover>
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>

        <div style={{display: storeFormDisplay}}>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店图片">
            {form.getFieldDecorator('storeUrl', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
              rules: [{ required: storeFormDisplay !== 'none', message: '请选择门店图片！' }],
            })(
              <Upload
                name="image"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action={`${apiDomainName}/image/upload`}
                // beforeUpload={this.beforeUpload}
                onChange={this.handleChange}
              >
                {imageUrl ? (
                  <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
                ) : (
                  uploadButton
                )}
              </Upload>
            )}
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店地址">
            {form.getFieldDecorator('address', {
              rules: [{ required: storeFormDisplay !== 'none', message: '请输入门店地址！' }],
            })(<Input placeholder="请输入门店地址" />)}
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="定位地址">
            {form.getFieldDecorator('location', {
              rules: [{ required: storeFormDisplay !== 'none', message: '请选择定位地址！' }],
            })(
              <Select
                showSearch
                // value={position}
                placeholder="请输入定位地址"
                style={{ width: '100%' }}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onSearch={this.handleSearch}
                onChange={this.onPositionChange}
                notFoundContent={null}
                allowClear
              >
                {tips.map((item, index) => (
                  <Select.Option key={`${index},${item.name}`}>{item.name}</Select.Option>
                ))}
              </Select>
            )}
            <div style={{ width: '100%', height: 250 }}>
              <Map center={center} amapkey="质数-金汤面-管理端" zoom={16}>
                <Marker position={center} />
              </Map>
            </div>
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店电话">
            {form.getFieldDecorator('phone', {
              rules: [{ required: storeFormDisplay !== 'none', message: '请输入门店电话！' }],
            })(<Input placeholder="请输入门店电话" />)}
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开店时间">
            {form.getFieldDecorator('startBusinessHours', {
              rules: [{ required: storeFormDisplay !== 'none', message: '请选择开店时间！' }],
            })(<TimePicker format="HH:mm" />)}
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="关店时间">
            {form.getFieldDecorator('endBusinessHours', {
              rules: [{ required: storeFormDisplay !== 'none', message: '请选择关店时间！' }],
            })(<TimePicker format="HH:mm" />)}
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="配送费用">
            {form.getFieldDecorator('distributionCost', {
              rules: [{ required: storeFormDisplay !== 'none', message: '请输入配送费用！' }],
            })(
              <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入配送费用" min={0} />
            )}
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="收货人">
            {form.getFieldDecorator('consignee', {
              rules: [{ required: storeFormDisplay !== 'none', message: '请输入收货人！' }],
            })(<Input placeholder="请输入收货人(退货专用)" />)}
          </FormItem>
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店介绍">
            {form.getFieldDecorator('introduce')(<Input.TextArea rows={3} placeholder="请输入门店介绍" />)}
          </FormItem>
        </div>
      </Modal>
    );
  }
}

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record, staffList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      if (!fieldsValue.personLiableId) {
        fieldsValue.personLiableId = undefined;
      }
      if (!fieldsValue.personLiable2Id) {
        fieldsValue.personLiable2Id = undefined;
      }
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isBranchNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改组织"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="组织名称">
        {form.getFieldDecorator('name', {
          rules: [
            { required: true, message: '请输入组织名称！' },
            { validator: checkName }
          ],
          initialValue: record.name,
        })(<Input placeholder="请输入组织名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 16 }} label="部门负责人1">
        {form.getFieldDecorator('personLiableId', {
          initialValue: record.personLiableId,
        })(
          <Select allowClear placeholder="请选择部门负责人" style={{width: '94%'}}>
            { staffList.rows.map(item => <Select.Option key={item.personnelId} value={item.personnelId}>{item.name}</Select.Option>) }
          </Select>
        )}
        <Popover
          content={
            <span>
                <a onClick={() => {
                  router.push({
                    pathname: '/system-management/personnel-files',
                  });
                }}>前往</a>
                人员档案添加
              </span>
          }
          title="友情提示"
        >
          <Icon type="question-circle-o" />
        </Popover>
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 16 }} label="部门负责人2">
        {form.getFieldDecorator('personLiable2Id', {
          initialValue: record.personLiable2Id
        })(
          <Select allowClear placeholder="请选择部门负责人" style={{width: '94%'}}>
            { staffList.rows.map(item => <Select.Option key={item.personnelId} value={item.personnelId}>{item.name}</Select.Option>) }
          </Select>
        )}
        <Popover
          content={
            <span>
                <a onClick={() => {
                  router.push({
                    pathname: '/system-management/personnel-files',
                  });
                }}>前往</a>
                人员档案添加
              </span>
          }
          title="友情提示"
        >
          <Icon type="question-circle-o" />
        </Popover>
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ branch, loading }) => ({
  branch,
  loading: loading.models.branch,
}))
@Form.create()
class OrganizationalStructure extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    selectedKeys: ['1'],
    expandedKeys: [],//此处设置初始值失效，因为treedata是在后面componentdidmount中获取的
    branchModalAddVisible: false,
    addModalTitle: '',
    deletePopconfirmVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'branchId',
    },
    {
      title: '组织名称',
      dataIndex: 'name',
    },
    {
      title: '部门负责人一',
      dataIndex: 'personLiable',
      render: text => !text ? (
        <div>
          <a onClick={() => {
            router.push({
              pathname: '/system-management/personnel-files',
            });
          }}>添加</a>
        </div>
      ) : <span>{text}</span>
    },
    {
      title: '部门负责人二',
      dataIndex: 'personLiable2',
      render: text => !text ? (
        <div>
          <a onClick={() => {
            router.push({
              pathname: '/system-management/personnel-files',
            });
          }}>添加</a>
        </div>
      ) : <span>{text}</span>
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: '15%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.onBranchDelete(record.branchId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
          {/*>*/}
            {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  onBranchDelete = (branchId) => {
    console.log('branchId: ', branchId);
    const { dispatch } = this.props;
    dispatch({
      type: 'branch/removeBranch',
      payload: { branchId },
      callback: () => {
        this.setState({
          selectedRows: [],
          selectedKeys: [],
        });
      },
    });
  }

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'branch/changeOrganizationalStructureStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //部门列表
    dispatch({
      type: 'branch/fetchBranchList',
    });

    // 获取子部门列表
    dispatch({
      type: 'branch/fetchChildBranchList',
      payload: {
        parentId: 1,
      },
    });

    this.setState({ expandedKeys: ['1'] })
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'branch/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    console.log('flag: ', flag);
    this.setState({
      modalVisible: !!flag,
    });
    if (flag) {//获取部门负责人列表
      const { dispatch } = this.props;
      const { selectedKeys } = this.state;
      // 获取部门员工列表
      dispatch({
        type: 'branch/fetchStaffList',
        payload: {
          // branchId: selectedKeys[0].toString() === '1' ? '' : selectedKeys[0],
          branchId: selectedKeys[0],
          getChild: '',//传此参数时，表示返回branchId以及其下子部门的员工
        },
      });
    }
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });

    if (flag) {//获取部门负责人列表
      const { dispatch } = this.props;
      const { selectedKeys } = this.state;
      // 获取部门员工列表
      dispatch({
        type: 'branch/fetchStaffList',
        payload: {
          // branchId: record.parentId === 1 ? '' : record.parentId,
          branchId: record.parentId,
          getChild: '',//传此参数时，表示返回branchId以及其下子部门的员工
        },
      });
    }
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { addModalTitle, selectedKeys } = this.state;

    let parentId = '';
    if (addModalTitle === '新增一级组织') {
      parentId = '-1';
    }
    else if (addModalTitle === '新增组织') {
      parentId = selectedKeys.length > 0 ? selectedKeys[0] : '';
    }

    dispatch({
      type: 'branch/addBranch',
      payload: {
        ...fields,
        parentId: parentId,
      },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({ selectedKeys });

          // 获取子部门列表
          dispatch({
            type: 'branch/fetchChildBranchList',
            payload: {
              parentId: selectedKeys[0],
            },
          });
        }
      }
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedKeys } = this.state;
    dispatch({
      type: 'branch/updateBranch',
      payload: {
        ...fields,
        branchId: record.branchId,
      },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({ selectedKeys });

          // 获取子部门列表
          dispatch({
            type: 'branch/fetchChildBranchList',
            payload: {
              parentId: selectedKeys[0],
            },
          });
        }
      }
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  getTreeNodeTitle = (item) => {
    const { selectedKeys, deletePopconfirmVisible } = this.state;
    let title = item.name;
    if (selectedKeys.includes(item.branchId.toString()) && item.branchId.toString() !== '1') {
      const handleVisibleChange = (visible) => {
        if (!visible) {
          this.setState({ deletePopconfirmVisible: visible });
          return;
        }
        // Determining condition before show the popconfirm.
        if (!item.childList) {
          this.setState({ deletePopconfirmVisible: true })
        }
      }
      // 该项是选中项
      title = (
        <span>
            {title} &nbsp;
          <Button
            onClick={() => {
              this.handleModalVisible(true);
              this.setState({ addModalTitle: '新增组织' })
            }}
            type="dashed"
            size="small"
            shape="circle"
            icon="plus"
          />
          &nbsp;
          <Button
            onClick={() => this.handleUpdateModalVisible(true, item)}
            type="dashed"
            size="small"
            shape="circle"
            icon="edit"
          />
          &nbsp;
          <Popconfirm
            visible={deletePopconfirmVisible}
            onConfirm={() => this.onBranchDelete(item.branchId)}
            title="确认删除该组织？"
            okText="是"
            cancelText="否"
            onVisibleChange={handleVisibleChange}
          >
              <Button disabled={item.childList} type="dashed" size="small" shape="circle" icon="delete" />
            </Popconfirm>
          </span>
      );
    }
    return title;
  }

  loadData = (dataTree) => {
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={this.getTreeNodeTitle(item)} key={item.branchId}>
            {item.childList ? this.loadData(item.childList) : null}
          </TreeNode>
        )
      })
    )
  }

  onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info);
    const { dispatch } = this.props;

    if (selectedKeys.length) {
      this.setState({ selectedKeys });

      // 获取子部门列表
      dispatch({
        type: 'branch/fetchChildBranchList',
        payload: {
          parentId: selectedKeys[0],
        },
      });
    }
  };

  componentWillUnmount () {
    const {
      branch: { childBranchList },
      dispatch
    } = this.props;
    if (childBranchList.rows.length > 0){
      dispatch({
        type: 'branch/saveChildBranchList',
        payload: {
          rows: [],
        },
      });
    }
  }

  onExpand = (expandedKeys) => {
    console.log('expandedKeys: ', expandedKeys);
    this.setState({
      expandedKeys,
    });
  }

  render() {
    const {
      branch: { branchList, childBranchList, staffList },
      loading,
    } = this.props;
    const { modalVisible, updateModalVisible, record, selectedKeys, addModalTitle, expandedKeys } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <Row >
            <Col span={6}>
              {/*<div style={{ marginBottom: 16 }}>*/}
                {/*<Button*/}
                  {/*icon="plus"*/}
                  {/*type="primary"*/}
                  {/*onClick={() => {*/}
                    {/*this.handleModalVisible(true);*/}
                    {/*this.setState({ addModalTitle: '新增一级组织' })*/}
                  {/*}}*/}
                {/*>*/}
                  {/*新增一级组织*/}
                {/*</Button>*/}
              {/*</div>*/}
              <div style={{ overflowX: 'scroll'}}>
                <Tree
                  selectedKeys={selectedKeys}
                  // defaultExpandedKeys={expandedKeys}
                  expandedKeys={expandedKeys}
                  onSelect={this.onSelect}
                  onExpand={this.onExpand}
                >
                  {this.loadData(branchList)}
                </Tree>
              </div>
            </Col>
            <Col span={18}>
              <div className={styles.tableList}>
                <div className={styles.tableListOperator}>
                  <Button
                    disabled={selectedKeys.length <= 0}
                    icon="plus"
                    type="primary"
                    onClick={() => {
                      this.handleModalVisible(true);
                      this.setState({ addModalTitle: '新增组织' })
                    }}>
                    新增
                  </Button>
                  {/*{selectedRows.length > 0 && (*/}
                    {/*<span>*/}
                      {/*<Popconfirm*/}
                        {/*title="确认批量删除？"*/}
                        {/*onConfirm={() =>*/}
                          {/*this.recordRemove(selectedRows.map(item => item.stockClassifyId).join(','))*/}
                        {/*}*/}
                        {/*okText="确认"*/}
                        {/*cancelText="取消"*/}
                      {/*>*/}
                        {/*<Button>批量删除</Button>*/}
                      {/*</Popconfirm>*/}
                    {/*</span>*/}
                  {/*)}*/}
                </div>
                <StandardTable
                  // selectedRows={selectedRows}
                  loading={loading}
                  data={childBranchList}
                  columns={this.columns}
                  // onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey="branchId"
                />
              </div>
            </Col>
          </Row>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} addModalTitle={addModalTitle} staffList={staffList} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} staffList={staffList} />
      </div>
    );
  }
}

export default OrganizationalStructure;
