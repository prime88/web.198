import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, Tree, InputNumber } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBranchNameRepeated } from '@/services/module';
import styles from './ModuleManagement.less';

const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, moduleTypeList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    const response = isBranchNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title='新增模块'
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="模块名称">
        {form.getFieldDecorator('name', {
          rules: [
            { required: true, message: '请输入模块名称！' },
          ],
        })(<Input placeholder="请输入模块名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="模块类型">
        {form.getFieldDecorator('moduleTypeCode', {
          rules: [
            { required: true, message: '请选择模块类型！' },
          ],
          initialValue: 2
        })(
          <Select placeholder='请选择模块类型' style={{width: '100%'}} >
            {moduleTypeList.rows.map(item => (
              <Select.Option key={item.dictDataCode} value={item.dictDataCode}>{item.dictDataName}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem style={{display: form.getFieldsValue().moduleTypeCode === 1 ? 'block' : 'none'}} labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="模块路径">
        {form.getFieldDecorator('route', {
          rules: [
            { required: form.getFieldsValue().moduleTypeCode === 1, message: '请输入模块路径！' },
          ],
        })(<Input placeholder="请输入模块路径" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="排序">
        {form.getFieldDecorator('sort', {
          rules: [
            { required: true, message: '请输入排序数！' },
          ],
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入排序数" min={0} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record, moduleTypeList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  const formModuleTypeCode = form.getFieldsValue().moduleTypeCode;
  // console.log('formModuleTypeCode: ', formModuleTypeCode)
  const recordModuleTypeCode = record.moduleTypeCode;
  // console.log('recordModuleTypeCode: ', recordModuleTypeCode)
  const formRouteBool = !!(formModuleTypeCode && formModuleTypeCode.toString() === '1');
  // console.log('formRouteBool: ', formRouteBool);
  const recordRouteBool = !!(recordModuleTypeCode && recordModuleTypeCode.toString() === '1');
  // console.log('recordRouteBool: ', recordRouteBool);
  let routeDisplay;
  if (formModuleTypeCode) {
    routeDisplay = formRouteBool ? 'block' : 'none';
  }
  else{
    routeDisplay = recordRouteBool ? 'block' : 'none';
  }
  // console.log('routeDisplay: ', routeDisplay);

  return (
    <Modal
      destroyOnClose
      title="修改模块"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="模块名称">
        {form.getFieldDecorator('name', {
          rules: [
            { required: true, message: '请输入模块名称！' },
          ],
          initialValue: record.name,
        })(<Input placeholder="请输入模块名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="模块类型">
        {form.getFieldDecorator('moduleTypeCode', {
          rules: [
            { required: true, message: '请选择模块类型！' },
          ],
          initialValue: record.moduleTypeCode,
        })(
          <Select placeholder='请选择模块类型' style={{width: '100%'}} >
            {moduleTypeList.rows.map(item => (
              <Select.Option key={item.dictDataCode}>{item.dictDataName}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      {/*<FormItem style={{display: formModuleTypeCode && formModuleTypeCode.toString() === 1 ? 'block' : 'none'}} labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="模块路径">*/}
      <FormItem style={{display: routeDisplay}} labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="模块路径">
        {form.getFieldDecorator('route', {
          rules: [
            { required: routeDisplay === 'block', message: '请输入模块路径！' },
          ],
          initialValue: record.route
        })(<Input placeholder="请输入模块路径" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="排序">
        {form.getFieldDecorator('sort', {
          rules: [
            { required: true, message: '请输入排序数！' },
          ],
          initialValue: record.sort,
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入排序数" min={0} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ module, loading }) => ({
  module,
  loading: loading.models.module,
}))
@Form.create()
class ModuleManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    selectedKeys: [],
    moduleModalAddVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'permissionId',
      width: 70,
      fixed: 'left',
    },
    {
      title: '模块名称',
      dataIndex: 'name',
      width: 100,
      fixed: 'left',
    },
    {
      title: '模块类型',
      dataIndex: 'moduleType',
    },
    {
      title: '模块级别',
      dataIndex: 'level',
    },
    {
      title: '上级模块',
      dataIndex: 'parentName',
    },
    {
      title: '创建人',
      dataIndex: 'createBy',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
    },
    {
      title: '排序',
      dataIndex: 'sort',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 120,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.onBranchDelete(record.permissionId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  onBranchDelete = (permissionIds) => {
    console.log('permissionIds: ', permissionIds);
    const { dispatch } = this.props;
    const { selectedKeys } = this.state;
    dispatch({
      type: 'module/removeModule',
      payload: { permissionIds },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({ selectedKeys });

          // 获取子部门列表
          dispatch({
            type: 'module/fetchModuleList',
            payload: {
              parentId: selectedKeys[0],
            },
          });
        }

        this.setState({
          selectedRows: [],
        });
      }
    });
  }

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'module/changeModuleManagementStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    //模块树
    dispatch({
      type: 'module/fetchModuleTree',
      callback: () => {
        this.setState({ selectedKeys: ['1'] })
      }
    });

    //模块列表
    dispatch({
      type: 'module/fetchModuleList',
      payload: {
        parentId: '1'
      }
    });

    //模块类型
    dispatch({
      type: 'module/fetchModuleTypeList',
      payload: {
        dictCode: 'moduleType'
      }
    });

  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'module/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {

    this.setState({
      modalVisible: !!flag,
    });

    if (flag) {//获取部门负责人列表
      const { dispatch } = this.props;
      const { selectedKeys } = this.state;

      // 获取部门员工列表
      dispatch({
        type: 'module/fetchStaffList',
        payload: {
          moduleId: selectedKeys[0],
        },
      });
    }
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });

    if (flag) {//获取部门负责人列表
      const { dispatch } = this.props;
      const { selectedKeys } = this.state;
      // 获取部门员工列表
      dispatch({
        type: 'module/fetchStaffList',
        payload: {
          moduleId: selectedKeys[0].toString() === '1' ? record.moduleId : selectedKeys[0],
        },
      });
    }
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { selectedKeys } = this.state;

    dispatch({
      type: 'module/addModule',
      payload: {
        ...fields,
        parentId: selectedKeys[0],
      },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({ selectedKeys });

          // 获取子部门列表
          dispatch({
            type: 'module/fetchModuleList',
            payload: {
              parentId: selectedKeys[0],
            },
          });
        }
      }
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedKeys } = this.state;

    fields.route = fields.moduleTypeCode.toString() === '2' ? '' : fields.route;

    dispatch({
      type: 'module/updateModule',
      payload: {
        ...fields,
        permissionId: record.permissionId,
      },
      callback: () => {
        if (selectedKeys.length) {
          this.setState({ selectedKeys });

          // 获取子部门列表
          dispatch({
            type: 'module/fetchModuleList',
            payload: {
              parentId: selectedKeys[0],
            },
          });
        }
      }
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  getTreeNodeTitle = (item) => {
    const { selectedKeys } = this.state;
    let title = item.name;
    if (selectedKeys.includes(item.permissionId.toString())) {
      // 该项是选中项
      title = (
        <span>
            {title} &nbsp;
          <Button
            onClick={() => {
              this.handleModalVisible(true);
            }}
            type="dashed"
            size="small"
            shape="circle"
            icon="plus"
          />
          &nbsp;
          <Button
            onClick={() => this.handleUpdateModalVisible(true, item)}
            type="dashed"
            size="small"
            shape="circle"
            icon="edit"
          />
          &nbsp;
          <Popconfirm
            onConfirm={() => this.onBranchDelete(item.permissionId)}
            title="确认删除该组织？"
            okText="是"
            cancelText="否"
          >
              <Button disabled={item.childList} type="dashed" size="small" shape="circle" icon="delete" />
            </Popconfirm>
          </span>
      );
    }
    return title;
  }

  loadData = (dataTree) => {
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.permissionId}>
            {item.childList ? this.loadData(item.childList) : null}
          </TreeNode>
          // <TreeNode title={this.getTreeNodeTitle(item)} key={item.permissionId}>
      )
      })
    )
  }

  onSelect = (selectedKeys, info) => {
    console.log('selectedKeys', selectedKeys);
    const { dispatch } = this.props;
    if (selectedKeys.length > 0){
      // 获取模块列表
      dispatch({
        type: 'module/fetchModuleList',
        payload: {
          parentId: selectedKeys[0],
        },
      });
      this.setState({ selectedKeys });
    }
  };

  render() {
    const {
      module: { moduleTreeList, moduleList, moduleTypeList },
      loading,
    } = this.props;
    const { modalVisible, updateModalVisible, record, selectedKeys, selectedRows } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <Row >
            <Col span={5}>
              <div style={{ overflowX: 'scroll'}}>
                <Tree
                  selectedKeys={selectedKeys}
                  onSelect={this.onSelect}
                >
                  {this.loadData(moduleTreeList)}
                </Tree>
              </div>
            </Col>
            <Col span={19}>
              <div className={styles.tableList}>
                <div className={styles.tableListOperator}>
                  <Button
                    disabled={selectedKeys.length <= 0}
                    icon="plus"
                    type="primary"
                    onClick={() => {
                      this.handleModalVisible(true);
                    }}>
                    新增
                  </Button>
                  {selectedRows.length > 0 && (
                  <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.onBranchDelete(selectedRows.map(item => item.permissionId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                  </span>
                  )}
                </div>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={moduleList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey="permissionId"
                  scroll={{x: 1300}}
                />
              </div>
            </Col>
          </Row>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} moduleTypeList={moduleTypeList} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} moduleTypeList={moduleTypeList} />
      </div>
    );
  }
}

export default ModuleManagement;
