// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchBranchList,
  fetchChildBranchList,
  fetchStaffList,
  addBranch,
  addBranchStaff,
  updateBranch,
  updateBranchStaff,
  removeBranch,
  removeStaff,
} from '@/services/branch';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'branch',

  state: {
    //部门列表
    branchList: [],
    //子部门列表
    childBranchList: {
      rows: [],
      pagination: {},
    },
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchChildBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchChildBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveChildBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addBranch({ payload, callback }, { call, put }) {
      const response = yield call(addBranch, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBranchList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addBranchStaff({ payload, callback }, { call, put }) {
      const response = yield call(addBranchStaff, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchStaffList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeBranch({ payload, callback }, { call, put }) {
      const response = yield call(removeBranch, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchBranchList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeStaff({ payload, callback }, { call, put }) {
      const response = yield call(removeStaff, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchBranchList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateBranch({ payload, callback }, { call, put }) {
      const response = yield call(updateBranch, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBranchList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateBranchStaff({ payload, callback }, { call, put }) {
      const response = yield call(updateBranchStaff, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchBranchList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveChildBranchList(state, action) {
      return {
        ...state,
        childBranchList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
