// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchStaffList,
  addStaff,
  updateInventoryClassification,
  updateStaff,
  setStaffRole,
  resetPassword,
  removeStaff,
  changeInventoryClassificationStatus,
} from '@/services/staff';
import { fetchMessageList } from '@/services/message';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'staff',

  state: {
    //用户列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //消息列表
    messageList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchMessageList({ payload, callback }, { call, put }) {
      const response = yield call(fetchMessageList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveMessageList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addStaff({ payload, selectedMenuItemKey, callback }, { call, put }) {
      const response = yield call(addStaff, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchStaffList',
          payload: {
            stockClassifyId: selectedMenuItemKey
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeStaff({ payload, callback }, { call, put }) {
      const response = yield call(removeStaff, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchStaffList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateStaff({ payload, callback }, { call, put }) {
      const response = yield call(updateStaff, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchStaffList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *setStaffRole({ payload, callback }, { call, put }) {
      const response = yield call(setStaffRole, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchStaffList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *resetPassword({ payload, callback }, { call, put }) {
      const response = yield call(resetPassword, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchStaffList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeInventoryClassificationStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeInventoryClassificationStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveMessageList(state, action) {
      return {
        ...state,
        messageList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
