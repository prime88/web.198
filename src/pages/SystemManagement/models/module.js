// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchModuleTree,
  fetchModuleList,
  addModule,
  updateModule,
  removeModule,
} from '@/services/module';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'module',

  state: {
    //模块树
    moduleTreeList: [],
    //模块列表
    moduleList: {
      rows: [],
      pagination: {},
    },
    //模块类型列表
    moduleTypeList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchModuleTree({ payload, callback }, { call, put }) {
      const response = yield call(fetchModuleTree, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveModuleTreeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchModuleList({ payload, callback }, { call, put }) {
      const response = yield call(fetchModuleList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveModuleList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchModuleTypeList({ payload, callback }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveModuleTypeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addModule({ payload, callback }, { call, put }) {
      const response = yield call(addModule, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchModuleTree',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeModule({ payload, callback }, { call, put }) {
      const response = yield call(removeModule, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchModuleTree',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateModule({ payload, callback }, { call, put }) {
      const response = yield call(updateModule, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchModuleTree',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveModuleTreeList(state, action) {
      return {
        ...state,
        moduleTreeList: action.payload,
      };
    },
    saveModuleList(state, action) {
      return {
        ...state,
        moduleList: action.payload,
      };
    },
    saveModuleTypeList(state, action) {
      return {
        ...state,
        moduleTypeList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
