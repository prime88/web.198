// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchRoleList,
  addRole,
  setRolePermissions,
  updateRole,
  removeRole,
} from '@/services/role';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'role',

  state: {
    //角色列表
    roleList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchRoleList({ payload, callback }, { call, put }) {
      const response = yield call(fetchRoleList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveRoleList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addRole({ payload, callback }, { call, put }) {
      const response = yield call(addRole, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchRoleList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *setRolePermissions({ payload, callback }, { call, put }) {
      const response = yield call(setRolePermissions, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchRoleList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeRole({ payload, callback }, { call, put }) {
      const response = yield call(removeRole, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchRoleList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateRole({ payload, callback }, { call, put }) {
      const response = yield call(updateRole, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchRoleList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveRoleList(state, action) {
      return {
        ...state,
        roleList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       if (location.pathname === '/app-management/vip-management/electronic-wallet') {
  //         dispatch({
  //           type: 'fetchElectronicWalletList',
  //         })
  //       }
  //       if (location.pathname === '/app-management/vip-management/ele-wall-recharge-record') {
  //         dispatch({
  //           type: 'fetchEleWallRechargeRecordList',
  //         })
  //       }
  //       if (location.pathname === '/app-management/vip-management/vip-level-settings') {
  //         dispatch({
  //           type: 'fetchVipLevelList',
  //         })
  //       }
  //     });
  //   },
  // },
};
