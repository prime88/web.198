import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, InputNumber, Tree } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/role';

import styles from './RoleManagement.less';

const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增角色"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="角色名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入角色名称！' }],
        })(
          <Input style={{width: '100%'}} placeholder="请输入角色名称" />
        )}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isInventoryClassificationNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改角色"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="角色名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入角色名称！' }],
          initialValue: record.name,
        })(
          <Input style={{width: '100%'}} placeholder="请输入角色名称" />
        )}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ role, module, loading }) => ({
  role,
  module,
  loading: loading.models.role,
}))
@Form.create()
class RoleManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    roleModalTitle: '',
    roleModalVisible: false,
    roleSeeModalVisible: false,
    checkedKeys: {
      checked: ['1'],
      halfChecked: [],
    },
    rolePermissionList: [],
    expandedKeys: ['1'],
    parentCheckedKeys: [],
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'roleId',
    },
    {
      title: '角色名称',
      dataIndex: 'name',
      render: text => <div>{text}</div>
    },
    {
      title: '拥有的权限',
      dataIndex: 'permissions',
      render: (text, record) => <a href='#' onClick={() => this.seePermissionList(record)}><Icon type="eye" theme="outlined" /></a>
    },
    {
      title: '操作',
      width: '17%',
      render: (text, record) => (
        <Fragment>
          <a href='#' onClick={() => this.setRolePermissions(record)}>设置权限</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.roleId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  seePermissionList = (record) => {
    this.setState({
      roleSeeModalVisible: true,
      rolePermissionList:
      record.permissions, record,
      expandedKeys: this.getExpandedKeys(record.permissions)
    })
  }

  getExpandedKeys = (daraArray) => {
    let result = [];

    for (let item of daraArray) {
      if (item.childList){
        result.push(item.permissionId.toString(), ...this.getExpandedKeys(item.childList));
      }
    }
    return result;
  }

  setRolePermissions = (record) => {
    console.log('record: ', record);
    let selectedKeys = this.getSelectedKeys(record.permissions);
    console.log('selectedKeys: ', selectedKeys);
    let parentIds = this.getParentIds(record.permissions);
    parentIds = Array.from(new Set(parentIds));
    console.log('parentIds: ', parentIds);
    //去掉父选择节点(半选中状态)
    selectedKeys = selectedKeys.filter(item => !parentIds.includes(item));

    this.setState({
      roleModalVisible: true,
      roleModalTitle: '设置权限',
      record,
      //初始化选中项
      // checkedKeys: { checked: this.getSelectedKeys(record.permissions) },
      checkedKeys: { checked: selectedKeys },
      expandedKeys: this.getExpandedKeys(record.permissions)
    })
  }

  onSendAllClick = (record) => {
    console.log('record: ', record);
    const { dispatch } = this.props;
    dispatch({
      type: 'role/sendMessage',
      payload: {
        title: record.title,
        text: record.text
      },
    });
  }

  recordRemove = roleIds => {
    console.log('roleIds: ', roleIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'role/removeRole',
      payload: { roleIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //角色列表
    dispatch({
      type: 'role/fetchRoleList',
    });

    //模块树
    dispatch({
      type: 'module/fetchModuleTree',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'role/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'role/addRole',
      payload: fields,
    });

    // role.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'role/updateRole',
      payload: {
        ...fields,
        roleId: record.roleId,
      },
    });

    // role.success('配置成功');
    this.handleUpdateModalVisible();
  };

  loadData = (dataTree) => {
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.permissionId} parentId={item.parentId}>
            {item.childList ? this.loadData(item.childList) : null}
          </TreeNode>
          // <TreeNode title={this.getTreeNodeTitle(item)} key={item.permissionId}>
        )
      })
    )
  }

  onExpand = (expandedKeys) => {
    this.setState({ expandedKeys })
  }

  onCheck = (checkedKeys, e) => {
    console.log('onCheck', checkedKeys);
    console.log('e: ', e);
    // console.log('checked', checked);
    // console.log('checkedNodes', checkedNodes);
    // console.log('node', node);
    // console.log('event', event);
    // this.setState(prevState => {
    //   const parentId = node.props.parentId;
    //   console.log('parentId: ', parentId);
    //   console.log('prevState.checkedKeys: ', prevState.checkedKeys);
    //   if (checked){//1、未选中父模块时，不能选其子模块；2
    //     if (parentId === -1 || prevState.checkedKeys.checked.includes(parentId.toString())) {
    //       return { checkedKeys }
    //     }
    //   }
    //   else{
    //     const children = node.props.children;
    //     if (!children) return { checkedKeys };
    //
    //     const childrenKeyList = node.props.children.map(item => item.key);//选中项子节点数组
    //     const checked = checkedKeys.checked;//选中后，所有选择项的数组
    //
    //     //融合childrenKeyList+checked，融合后进行去重，若去重前后长度相等，则表示没有重复项，即没有选择该节点的子节点
    //     const oldCount = childrenKeyList.length + checked.length;
    //     const newCount = Array.from(new Set([...childrenKeyList, ...checked])).length;
    //     if (newCount === oldCount) {
    //       return { checkedKeys }
    //     }
    //   }
    // });

    // this.setState({ checkedKeys: [...checkedKeys, ...e.halfCheckedKeys] })
    this.setState({ checkedKeys, parentCheckedKeys: e.halfCheckedKeys })
  }

  handleOk = (e) => {
    const { dispatch } = this.props;
    const { checkedKeys, record, parentCheckedKeys } = this.state;

    if (!Array.isArray(checkedKeys)) {
      this.setState({ roleModalVisible: false, checkedKeys: { checked: ['1'] } })
      return;
    }
    dispatch({
      type: 'role/setRolePermissions',
      payload: {
        roleId: record.roleId,
        // permissionIds: checkedKeys.checked.join(',')
        permissionIds: [...checkedKeys, ...parentCheckedKeys].join(',')
      },
      callback: () => {
        this.setState({ roleModalVisible: false, checkedKeys: { checked: ['1'] } })
      }
    });
  }

  getSelectedKeys = (daraArray) => {
    let result = [];
    for (let item of daraArray) {
      if (item.childList){
        result.push(item.permissionId.toString(), ...this.getSelectedKeys(item.childList));
      }
      else{
        result.push(item.permissionId.toString());
      }
    }
    return result;
  }

  getParentIds = (daraArray) =>  {
    let result = [];
    for (let item of daraArray) {
      if (item.childList){
        result.push(item.parentId.toString(), ...this.getParentIds(item.childList));
      }
      else{
        result.push(item.parentId.toString());
      }
    }
    return result;
  }

  render() {
    const {
      role: { roleList },
      module: { moduleTreeList },
      loading,
    } = this.props;
    const {
      selectedRows,
      modalVisible,
      updateModalVisible,
      record,
      roleModalTitle,
      roleModalVisible,
      checkedKeys,
      roleSeeModalVisible,
      rolePermissionList,
      expandedKeys,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.roleId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={roleList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="roleId"
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />
        <Modal
          title={roleModalTitle}
          visible={roleModalVisible}
          onOk={this.handleOk}
          onCancel={() => {
            this.setState({ roleModalVisible: false, checkedKeys: { checked: [] }, expandedKeys: [] });
          }}
          // okButtonProps={{disabled: checkedKeys.checked.length <= 0}}
          okButtonProps={{disabled: checkedKeys.length <= 0}}
        >
          <div style={{ overflowX: 'scroll'}}>
            <Tree
              expandedKeys={expandedKeys}
              checkable
              onCheck={this.onCheck}
              onExpand={this.onExpand}
              checkedKeys={checkedKeys}
              // checkStrictly
            >
              {this.loadData(moduleTreeList)}
            </Tree>
          </div>
        </Modal>
        <Modal
          title={record.name}
          visible={roleSeeModalVisible}
          // onOk={this.handleRoleSeeOk}
          onCancel={() => {
            this.setState({ roleSeeModalVisible: false, expandedKeys: [] });
          }}
          footer={[
            <Button type='primary' onClick={() => this.setState({ roleSeeModalVisible: false, expandedKeys: [] })}>关闭</Button>
          ]}
          // okButtonProps={{disabled: checkedKeys.length <= 0}}
        >
          <div style={{ overflowX: 'scroll'}}>
            <Tree
              // defaultExpandedKeys={['1']}
              expandedKeys={expandedKeys}
              onExpand={this.onExpand}
            >
              {this.loadData(rolePermissionList)}
            </Tree>
          </div>
        </Modal>
      </div>
    );
  }
}

export default RoleManagement;
