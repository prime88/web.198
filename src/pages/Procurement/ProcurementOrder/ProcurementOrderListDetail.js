import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './ProcurementOrderListDetail.less';

const { Description } = DescriptionList;

@connect(({ procurementPlan,  user, loading }) => ({
  procurementPlan,
  user,
  // loading: loading.effects['profile/fetchBasic'],
}))
class ProcurementOrderListDetail extends Component {
  state = {
    dataSource: [],
    dataPermission: false,
  }

  componentDidMount() {
    const { dispatch, record, user: { currentUser } } = this.props;
    const purchaseOrderId = record.purchaseOrderId;
    if (purchaseOrderId) {
      dispatch({
        type: 'procurementOrder/fetchProcurementInventoryList',
        payload: {
          purchaseOrderIds: purchaseOrderId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }

  }

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    // {
    //   title: '单位',
    //   dataIndex: 'units',
    // },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    {
      title: '含税单价',
      dataIndex: 'referenceCost',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '无税单价',
      dataIndex: 'taxFreeUnitPrice',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '税率(%)',
      dataIndex: 'taxRate',
    },
    {
      title: '含税合计',
      dataIndex: 'taxTotal',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '无税合计',
      dataIndex: 'taxFreeTotal',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="单据编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单时间">{record.orderTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="计划到货日">{record.planToArriveDate}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="定金">{record.deposit}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="运输方式">{record.transportMode}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="收货联系人">{record.consignee}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="供应商">{record.supplier}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="送货地址">{record.address}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="联系人电话">{record.phone}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>订单明细</div>
          <Table
            style={{ marginBottom: 24 }}
            pagination={{defaultPageSize: 9}}
            size='small'
            // loading={loading}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="stockId"
            scroll={{x: 1500}}
          />
        </div>
      </div>
    );
  }
}

export default ProcurementOrderListDetail;
