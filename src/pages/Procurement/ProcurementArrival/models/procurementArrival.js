// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
  fetchOrderNumber,
  fetchPurchaseingPlanList,
  fetchPurchaseingInventoryList,
  addProcurementOrder,
  addArrivalOrder,
  removeInventory,
  removePurchaseOrder,
  removeProcurementOrder,
  removeArrivalOrder,
  updatePurchase,
  fetchProcurementOrderList,
  fetchProcurementInventoryList,
  updateProcurementOrder,
  updateArrivalOrder,
  submitArrivalOrders,
  fetchArrivalOrderList,
  // changeInventoryClassificationStatus,
} from '@/services/procurementArrival';
import { fetchStaffList } from '@/services/branch';
import { fetchProcurementPlanList } from '@/services/procurementPlan';
import { querySupplierList } from '@/services/supplier';
import { queryStoreList } from '@/services/store';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'procurementArrival',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //请购列表
    purchaseingPlanList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //订单编号
    orderNumber: '',
    //请购项存货列表
    purchaseingInventoryList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //供应商列表
    supplierList: {
      rows: [],
      pagination: {},
    },
    //计划单列表
    procurementPlanList: {
      rows: [],
      pagination: {},
    },
    //采购订单列表
    procurementOrderList: {
      rows: [],
      pagination: {},
    },
    //订单明细列表
    procurementInventoryList: [],
    //采购到货列表
    arrivalOrderList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchPurchaseingPlanList({ payload }, { call, put }) {
      const response = yield call(fetchPurchaseingPlanList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePurchaseingPlanList',
        payload: response.data,
      });
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOrderNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOrderNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchPurchaseingInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchPurchaseingInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePurchaseingInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchSupplierList({ payload }, { call, put }) {
      const response = yield call(querySupplierList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSupplierList',
        payload: response.data,
      });
    },
    *fetchProcurementPlanList({ payload }, { call, put }) {
      const response = yield call(fetchProcurementPlanList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProcurementPlanList',
        payload: response.data,
      });
    },
    *fetchProcurementOrderList({ payload }, { call, put }) {
      const response = yield call(fetchProcurementOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProcurementOrderList',
        payload: response.data,
      });
    },
    *fetchProcurementInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProcurementInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveProcurementInventoryList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }else {
        message.error(response.msg);
      }
    },
    *fetchArrivalOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchArrivalOrderList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveArrivalOrderList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }else {
        message.error(response.msg);
      }
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addProcurementOrder({ payload, callback }, { call, put }) {
      const response = yield call(addProcurementOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addArrivalOrder({ payload, callback }, { call, put }) {
      const response = yield call(addArrivalOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeInventory({ payload, callback }, { call, put }) {
      const response = yield call(removeInventory, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removePurchaseOrder({ payload, callback }, { call, put }) {
      const response = yield call(removePurchaseOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeProcurementOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeProcurementOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeArrivalOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeArrivalOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updatePurchase({ payload, callback }, { call, put }) {
      const response = yield call(updatePurchase, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchPurchaseingPlanList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateProcurementOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateProcurementOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProcurementOrderList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateArrivalOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateArrivalOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchProcurementOrderList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitArrivalOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitArrivalOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchProcurementOrderList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    savePurchaseingPlanList(state, action) {
      return {
        ...state,
        purchaseingPlanList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveOrderNumber(state, action) {
      return {
        ...state,
        orderNumber: action.payload,
      };
    },
    savePurchaseingInventoryList(state, action) {
      return {
        ...state,
        purchaseingInventoryList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveSupplierList(state, action) {
      return {
        ...state,
        supplierList: action.payload,
      };
    },
    saveProcurementPlanList(state, action) {
      return {
        ...state,
        procurementPlanList: action.payload,
      };
    },
    saveProcurementOrderList(state, action) {
      return {
        ...state,
        procurementOrderList: action.payload,
      };
    },
    saveProcurementInventoryList(state, action) {
      return {
        ...state,
        procurementInventoryList: action.payload,
      };
    },
    saveArrivalOrderList(state, action) {
      return {
        ...state,
        arrivalOrderList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
