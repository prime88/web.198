import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './PurchaseingPlanListDetail.less';

const { Description } = DescriptionList;

@connect(({ storePurchaseing, loading }) => ({
  storePurchaseing,
  // loading: loading.effects['profile/fetchBasic'],
}))
class PurchaseingPlanListDetail extends Component {
  state = {
    dataSource: []
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const purchaseRequisitionId = record.purchaseRequisitionId;
    if (purchaseRequisitionId) {
      dispatch({
        type: 'storePurchaseing/fetchPurchaseingInventoryList',
        payload: {
          purchaseRequisitionIds: purchaseRequisitionId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }
  }

  columns = [
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '需求日期',
      dataIndex: 'demandTime',
      editable: true,
    },
    {
      title: '建议订货日期',
      dataIndex: 'proposalTime',
      editable: true,
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
          <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单编号">{record.orderNumber}</Description>
          <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="请购机构">{record.store}</Description>
          <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="请购人员">{record.requisitionBy}</Description>
          <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="请购时间">{record.purchaseRequisitionTime}</Description>
          <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
          <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
        </DescriptionList>
        <Divider style={{ marginBottom: 32 }} />
        <div className={styles.title}>请购明细</div>
        <Table
          style={{ marginBottom: 24 }}
          pagination={{defaultPageSize: 9}}
          // loading={loading}
          dataSource={dataSource}
          columns={this.columns}
          rowKey="id"
          size='small'
        />
      </div>
    );
  }
}

export default PurchaseingPlanListDetail;
