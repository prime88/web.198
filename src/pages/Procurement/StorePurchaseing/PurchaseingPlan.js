import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Table,
  Popconfirm,
} from 'antd';
import PurchaseingPlanAdd from './PurchaseingPlanAdd'
import styles from './PurchaseingPlan.less';
import moment from 'moment'
import { fetchStaffList } from '@/services/branch';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  let purchaseingPlanAdd;
  const okHandle = () => {
    const { selectedRows } = purchaseingPlanAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = (ref) => {
    purchaseingPlanAdd = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="新增请购明细"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{top: 20}}
    >
      <PurchaseingPlanAdd wrappedComponentRef={addModalRef} />
    </Modal>
  );
});

// @Form.create()
// class UpdateForm extends PureComponent {
//   render() {
//
//   }
// }
const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.demandTime = fieldsValue.demandTime.format('YYYY-MM-DD');
      const proposalTime = fieldsValue.proposalTime;
      if (proposalTime) {
        fieldsValue.proposalTime = proposalTime.format('YYYY-MM-DD');
      }
      console.log('fieldsValue: ', fieldsValue);

      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().subtract(1, 'days').endOf('day');
  }

  const onNumberChange = (key, value) => {
    console.log('key: ', key);
    console.log('value: ', value);
    console.log('record.unitRatio: ', record.unitRatio);
    console.log('value/record.unitRatio: ', value/record.unitRatio);
    if (isNaN(value)) {
      props.form.setFieldsValue({ number: '', number2: '' });
      return;
    }

    if (key === 'number') {
      props.form.setFieldsValue({ number2: parseInt((value/record.unitRatio)*100)/100 });
    }
    else if (key === 'number2') {
      //保留两位小数
      value = parseInt(value*100)/100
      props.form.setFieldsValue({ number: parseInt(value*record.unitRatio) });
    }
  }

  return (
    <Modal
      destroyOnClose
      title="修改请购明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label={`主数量(${record.units})`}>
        {form.getFieldDecorator('number', {
          rules: [{ required: true, message: '请输入请购主数量！' }],
          initialValue: record.number,
        })(
          <InputNumber
            onChange={(value) => onNumberChange('number', value)}
            precision={0}
            style={{width: '100%'}}
            placeholder="请输入请购主数量"
            min={0}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label={`辅数量(${record.units2})`}>
        {form.getFieldDecorator('number2', {
          rules: [{ required: true, message: '请输入请购辅数量！' }],
          initialValue: record.number2,
        })(
          <InputNumber
            precision={2}
            onChange={(value) => onNumberChange('number2', value)}
            style={{width: '100%'}}
            placeholder="请输入请购辅数量"
            min={0}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="需求日期">
        {form.getFieldDecorator('demandTime', {
          rules: [{ required: true, message: '请选择需求日期！' }],
          initialValue: record.demandTime ? moment(record.demandTime) : '',
        })(<DatePicker disabledDate={disabledDate} style={{width: '100%'}} placeholder="请选择需求日期" />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="建议订货日期">
        {form.getFieldDecorator('proposalTime', {
          // rules: [{ required: true, message: '请选择建议订货日期！' }],
          initialValue: record.proposalTime ? moment(record.proposalTime) : '',
        })(<DatePicker disabledDate={disabledDate} style={{width: '100%'}} placeholder="请选择建议订货日期" />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ storePurchaseing, user, loading }) => ({
  storePurchaseing,
  user,
  loading: loading.models.storePurchaseing,
}))
@Form.create()
class PurchaseingPlan extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    purchasePersonnel: [],
    dataPermission: false,
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    // {
    //   title: '单位',
    //   dataIndex: 'units',
    // },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
      editable: true,
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
      editable: true,
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '需求日期',
      dataIndex: 'demandTime',
      editable: true,
    },
    {
      title: '建议订货日期',
      dataIndex: 'proposalTime',
      editable: true,
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
    },
    {
      title: '操作',
      width: 120,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.stockId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = (stockId) => {
    console.log('stockId: ', stockId);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.stockId !== stockId) })
    message.success('删除成功');
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;
    //机构列表
    dispatch({
      type: 'storePurchaseing/fetchBranchList',
      payload: {
        isWarehouse: true
      }
    });

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'storePurchaseing/fetchOrderNumber',
    });

    //获取当前部门员工列表
    const personnel = currentUser.personnel;
    if (personnel) {
      // 获取子部门列表
      const response = fetchStaffList({ branchId: personnel.branchId });
      response.then(result => {
        console.log('result: ', result);
        this.setState({ purchasePersonnel: result.data.rows })
      });
    }

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ dataSource: [], purchasePersonnel: [] })
  };

  submit = (isSubmit) => {
    const { dispatch, form } = this.props;
    const { dataSource } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增请购明细');
        return;
      }
      else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      }
      else if (dataSource.find(item => !item.demandTime)) {
        message.warning('请设置商品的需求日期');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.purchaseRequisitionTime = fieldsValue.purchaseRequisitionTime.format('YYYY-MM-DD HH:mm');
      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'storePurchaseing/addPurchaseingOrder',
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [], purchasePersonnel: [] })

          //更新订单编号
          dispatch({
            type: 'storePurchaseing/fetchOrderNumber',
          });
        }
      });
    });
  }

  handleSearch = (e) => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0){
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            if (newItem.stockId.toString() === oldItem.stockId.toString()) {
              // oldItem.number++;
              flag = false;
              break;
            }
          }
          if (flag) {
            // oldDataSource.push({ ...newItem, number: 1 })
            oldDataSource.push({ ...newItem })
          }
        }
      }
      else{
        // oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1 }))];
        oldDataSource = [...selectedRows];
      }
      return {
        // dataSource: [...selectedRows.map(item => ({ ...item, number: 1 }))]
        dataSource: oldDataSource
      }
    });

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.stockId === record.stockId);
    if (index > -1){
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });
      this.setState({ dataSource: newData });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    }
    else{
      message.error('修改失败');
    }
  };

  onStoreChange = (storeName, e) => {
    console.log('storeName: ', storeName);
    console.log('e: ', e);
    console.log('branchId: ', e.props.branchid);
    const branchId = e.props.branchid;

    // 获取子部门列表
    const response = fetchStaffList({ branchId });
    response.then(result => {
      console.log('result: ', result);
      this.setState({ purchasePersonnel: result.data.rows })
      this.props.form.setFieldsValue({ requisitionBy: '' });
    });
  }

  renderTopSimpleForm() {
    const {
      storePurchaseing: { branchList, orderNumber, staffList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;
    const { purchasePersonnel, dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;订单编号</span>}>
              {orderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="请购机构">
              {getFieldDecorator('store', {
                rules: [{ required: true, message: '请选择请购机构' }],
                // initialValue: storeList.rows.length > 0 ? storeList.rows[0].name : ''
                initialValue: currentUser.personnel.branch
              })(
                <Select disabled={!dataPermission} onChange={this.onStoreChange} placeholder="请选择请购机构" style={{ width: '100%' }}>
                  {branchList.rows.map(item => <Select.Option branchid={item.branchId} key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="请购人员">
              {getFieldDecorator('requisitionBy', {
                rules: [{ required: true, message: '请选择请购人员' }],
                initialValue: currentUser.personnel.name
              })(
                <Select placeholder="请选择请购人员" style={{ width: '100%' }}>
                  {/*{staffList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}*/}
                  {purchasePersonnel.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="请购时间">
              {getFieldDecorator('purchaseRequisitionTime', {
                rules: [{ required: true, message: '请选择请购时间' }],
                initialValue: moment(new Date(), 'YYYY-MM-DD HH:mm')
              })(
                <DatePicker
                  showTime={{ format: 'HH:mm' }}
                  style={{width: '100%'}}
                  placeholder="请选择请购时间"
                  format="YYYY-MM-DD HH:mm"
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">
              {currentUser.personnel.name}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
              <Button style={{ marginRight: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginRight: 8 }} onClick={() => this.submit(false)}>
                保存
              </Button>
              <Button type="primary" htmlType='submit' >
                提交
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  total = () => {
    const { dataSource } = this.state;
    console.log('total-dataSource: ', dataSource);
    let result = `合计: 主数量0 / 辅数量0 `;
    if (dataSource.length > 0) {
      const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => (a*100+b*100)/100);
      const ping = dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
      result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100}`
    }
    return result
  }

  render() {
    const { modalVisible, dataSource, updateModalVisible, record } = this.state;


    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              请购明细
              <Button
                type="primary"
                style={{ marginLeft: 16 }}
                // icon="plus"
                onClick={() => this.handleModalVisible(true)}
              >
                新增
              </Button>
            </h2>
            <Divider />
            <Table
              // loading={loading}
              // components={components}
              rowKey='stockId'
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              columns={this.columns}
              scroll={{x: 1800}}
              footer={this.total}
              // footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{paddingTop: 24}} className={styles.tableListForm}>{this.renderBottomSimpleForm()}</div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
      </div>
    );
  }
}

export default PurchaseingPlan;
