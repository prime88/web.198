import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './ProcurementPlanListDetail.less';

const { Description } = DescriptionList;

@connect(({ procurementPlan, user, loading }) => ({
  procurementPlan,
  user,
  // loading: loading.effects['profile/fetchBasic'],
}))
class ProcurementPlanListDetail extends Component {
  state = {
    dataSource: [],
    dataPermission: false,
  }

  componentDidMount() {
    const { dispatch, record, user: { currentUser } } = this.props;
    const purchasePlanId = record.purchasePlanId;
    if (purchasePlanId) {
      dispatch({
        type: 'procurementPlan/fetchProcurementInventoryList',
        payload: {
          purchasePlanIds: purchasePlanId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }


    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }
  }

  columns = [
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    //   editable: true,
    // },
    // {
    //   title: '需求日期',
    //   dataIndex: 'demandTime',
    //   editable: true,
    // },
    // {
    //   title: '建议订货日期',
    //   dataIndex: 'proposalTime',
    //   editable: true,
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '最后一次采购价',
      dataIndex: 'referenceCost',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="计划人员">{record.requisitionBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="计划时间">{record.planTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>计划明细</div>
          <Table
            style={{ marginBottom: 24 }}
            pagination={{defaultPageSize: 9}}
            // loading={loading}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="stockId"
            size='small'
          />
        </div>
      </div>
    );
  }
}

export default ProcurementPlanListDetail;
