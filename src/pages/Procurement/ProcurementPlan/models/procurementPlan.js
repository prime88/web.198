// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
  fetchPlanNumber,
  fetchProcurementPlanList,
  fetchProcurementInventoryList,
  fetchPurchaseingInventoryList,
  fetchPurchaseingPlanList,
  addProcurementPlan,
  removeInventory,
  removeProcurementOrder,
  updateProcurement,
  submitProcurementOrders,
  // changeInventoryClassificationStatus,
} from '@/services/procurementPlan';
import { queryDictionary } from '@/services/common-api';
import { fetchStaffList } from '@/services/branch';
import { message } from 'antd';

export default {
  namespace: 'procurementPlan',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //计划列表
    procurementPlanList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //订单编号
    planNumber: '',
    //请购单存货列表
    procurementInventoryList: [],
    //计划单存货列表
    purchaseingInventoryList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //请购单列表
    purchaseingPlanList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchProcurementPlanList({ payload }, { call, put }) {
      const response = yield call(fetchProcurementPlanList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProcurementPlanList',
        payload: response.data,
      });
    },
    *fetchPlanNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchPlanNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePlanNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchProcurementInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProcurementInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProcurementInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    //---------参照----------
    *fetchPurchaseingInventoryList({ payload, callback }, { call, put, select }) {
      const response = yield call(fetchPurchaseingInventoryList, payload);
      const procurementPlan = yield select(state => state.procurementPlan);
      const { purchaseingInventoryList } = procurementPlan;
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePurchaseingInventoryList',
        payload: [...purchaseingInventoryList, ...response.data],
      });
      if (callback) callback(response.data);
    },
    *fetchPurchaseingPlanList({ payload }, { call, put }) {
      const response = yield call(fetchPurchaseingPlanList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePurchaseingPlanList',
        payload: response.data,
      });
    },
    //-------------------
    *addProcurementPlan({ payload, callback }, { call, put }) {
      const response = yield call(addProcurementPlan, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeInventory({ payload, callback }, { call, put }) {
      const response = yield call(removeInventory, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeProcurementOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeProcurementOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateProcurement({ payload, callback }, { call, put }) {
      const response = yield call(updateProcurement, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchProcurementPlanList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitProcurementOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitProcurementOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchProcurementPlanList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    // *changeInventoryClassificationStatus({ payload, callback }, { call, put }) {
    //   const response = yield call(changeInventoryClassificationStatus, payload);
    //   if (response.code === '0') {
    //     message.success(response.msg);
    //     yield put({
    //       type: 'fetchInventoryClassificationList',
    //     });
    //     if (callback) callback();
    //   } else {
    //     message.error(response.msg);
    //   }
    // },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    saveProcurementPlanList(state, action) {
      return {
        ...state,
        procurementPlanList: action.payload,
      };
    },
    savePlanNumber(state, action) {
      return {
        ...state,
        planNumber: action.payload,
      };
    },
    saveProcurementInventoryList(state, action) {
      return {
        ...state,
        procurementInventoryList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    savePurchaseingPlanList(state, action) {
      return {
        ...state,
        purchaseingPlanList: action.payload,
      };
    },
    savePurchaseingInventoryList(state, action) {
      return {
        ...state,
        purchaseingInventoryList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
