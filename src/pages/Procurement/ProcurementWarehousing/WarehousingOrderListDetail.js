import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './WarehousingOrderListDetail.less';

const { Description } = DescriptionList;

@connect(({ procurementWarehousing, user, loading }) => ({
  procurementWarehousing,
  user,
  // loading: loading.effects['profile/fetchBasic'],
}))
class WarehousingOrderListDetail extends Component {
  state = {
    dataSource: [],
    dataPermission: false,
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const purchaseGodownEntryId = record.purchaseGodownEntryId;
    if (purchaseGodownEntryId) {
      dispatch({
        type: 'procurementWarehousing/fetchProcurementInventoryList',
        payload: {
          purchaseGodownEntryIds: purchaseGodownEntryId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }
  }

  columns = [
    {
      title: '序号',
      dataIndex: '',
      // width: 60,
      // fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      // width: 100,
      // fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      // width: 200,
      // fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    // {
    //   title: '分类',
    //   dataIndex: 'stockClassify',
    // },
    // {
    //   title: '品牌',
    //   dataIndex: 'brand',
    // },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    // {
    //   title: '含税单价',
    //   dataIndex: 'referenceCost',
    // },
    // {
    //   title: '无税单价',
    //   dataIndex: 'taxFreeUnitPrice',
    // },
    {
      title: '含税单价(¥)',
      dataIndex: 'referenceCost',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '税率(%)',
      dataIndex: 'taxRate',
    },
    // {
    //   title: '含税合计',
    //   dataIndex: 'taxTotal',
    // },
    // {
    //   title: '无税合计',
    //   dataIndex: 'taxFreeTotal',
    // },
    // {
    //   title: '订单数量',
    //   dataIndex: 'number',
    // },
    // {
    //   title: '到货数量',
    //   dataIndex: 'arrivalNum',
    // },
    // {
    //   title: '备注',
    //   dataIndex: 'remark',
    // },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="单据编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库时间">{record.godownEntryTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库人员">{record.godownEntryBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="供应商">{record.supplier}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="仓库">{record.warehouse}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>订单明细</div>
          <Table
            style={{ marginBottom: 24 }}
            pagination={{defaultPageSize: 9}}
            size='small'
            // loading={loading}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="stockId"
            // scroll={{x: 1500}}
          />
        </div>
      </div>
    );
  }
}

export default WarehousingOrderListDetail;
