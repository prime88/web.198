import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Table,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import router from 'umi/router';
import styles from './WarehousingOrderList.less';
import { fetchStaffList } from '@/services/branch';
import WarehousingOrderListEdit from './WarehousingOrderListEdit'
import WarehousingOrderListDetail from './WarehousingOrderListDetail'
import ReactToPrint from "react-to-print";
import {apiDomainName} from "../../../constants";
import { stringify } from 'qs';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="新建规则"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="描述">
        {form.getFieldDecorator('desc', {
          rules: [{ required: true, message: '请输入至少五个字符的规则描述！', min: 5 }],
        })(<Input placeholder="请输入" />)}
      </FormItem>
    </Modal>
  );
});

const DetailForm = Form.create()(props => {
  const { detailModalVisible, handleDetailModalVisible, record } = props;
  let componentRef;
  return (
    <Modal
      width={'80%'}
      destroyOnClose
      visible={detailModalVisible}
      onCancel={() => handleDetailModalVisible()}
      style={{top: 10}}
      title={
        <div>
          订单详情
          <span
            style={{float: 'right'}}
          >
            <ReactToPrint
              trigger={() => <a href="#"><Icon type="printer" /></a>}
              content={() => {
                console.log('componentRef: ', componentRef);
                return componentRef
              }}
            />
          </span>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
    >
      <WarehousingOrderListDetail record={record} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  let arrivalOrderEdit;
  const okHandle = (isSubmit) => {
    const { form } = arrivalOrderEdit.props;
    const { dataSource, warehouseId, supplierId } = arrivalOrderEdit.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (dataSource.length === 0) {
        message.warning('请新增订单明细');
        return;
      }
      else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      }
      // else if (dataSource.find(item => !item.arrivalNum && item.arrivalNum !== 0)) {
      //   message.warning('请设置商品的到货数量');
      //   return;
      // }


      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.godownEntryTime = fieldsValue.godownEntryTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.warehouseId = warehouseId;

      if (!fieldsValue.supplier) {
        fieldsValue.supplier = '';
      }
      fieldsValue.supplierId = supplierId;
      const supplier = fieldsValue.supplier;
      //没有供应商时需不传
      if (!supplierId || !fieldsValue.supplier) {
        delete fieldsValue.supplierId;
        delete fieldsValue.supplier;
      }

      if (isSubmit) {
        fieldsValue.submit = isSubmit;
      }
      console.log('fieldsValue: ', fieldsValue);
      handleUpdate(fieldsValue);
    });
  };

  const editModalRef = (ref) => {
    arrivalOrderEdit = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      // title="修改订单"
      title={(
        <div>
          <span>修改订单</span>
          <Button style={{float: 'right', marginRight: 50}} type='primary' onClick={() => okHandle(true)}>提交</Button>
          <Button style={{float: 'right', marginRight: 16}} onClick={() => okHandle(false)}>保存</Button>
        </div>
      )}
      visible={updateModalVisible}
      // onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
      style={{top: 10}}
      footer={[
        <Button onClick={() => handleUpdateModalVisible()} >取消</Button>,
        <Button onClick={() => okHandle(false)}>保存</Button>,
        <Button type='primary' onClick={() => okHandle(true)}>提交</Button>
      ]}
    >
      <WarehousingOrderListEdit row={record} wrappedComponentRef={editModalRef} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ procurementWarehousing, user, loading }) => ({
  procurementWarehousing,
  user,
  loading: loading.models.procurementWarehousing,
}))
@Form.create()
class WarehousingOrderList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
    dataPermission: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
      width: 100
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '入库时间',
      dataIndex: 'godownEntryTime',
    },
    {
      title: '入库人员',
      dataIndex: 'godownEntryBy',
    },
    {
      title: '入库批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '是否提交',
      dataIndex: 'isSubmit',
      render: text => {
        let result = '';
        switch (text) {
          case true:
            result = '已提交';
            break;
          case false:
            result = '未提交';
            break;
        }
        return result;
      }
    },
    {
      title: '审批结果',
      dataIndex: 'submitResult',
      render: (text, record) => {
        let result = '';
        if (record.isSubmit) {
          switch (text.toString()) {
            case '0':
              result = '审批中';
              break;
            case '1':
              result = '已批准';
              break;
            case '2':
              result = '未批准';
              break;
          }
        }
        return result;
      }
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 80,
      render: (text, record) => {
        const menu = (
          <Menu onClick={(e) => e.domEvent.stopPropagation()}>
            <Menu.Item disabled={!!record.isSubmit}>
              { !!record.isSubmit ? '修改' : <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a> }
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.handleDetailModalVisible(true, record)}>查看</a>
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement='bottomRight'>
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        )
      },
    },
  ];

  inventoryRecordRemove = ({ ppStockId, purchasePlanId }) => {
    console.log('ppStockId: ', ppStockId);
    const { dispatch } = this.props;
    dispatch({
      type: 'procurementWarehousing/removeInventory',
      payload: { ppStockId },
      callback: () => {
        dispatch({
          type: 'procurementWarehousing/fetchPurchaseingInventoryList',
          payload: {
            purchasePlanId
          }
        })
      },
    });
  }

  recordRemove = purchaseGodownEntryId => {
    console.log('purchaseGodownEntryId: ', purchaseGodownEntryId);
    const { dispatch } = this.props;
    dispatch({//removePurchaseOrder
      type: 'procurementWarehousing/removeWarehousingOrder',
      payload: { purchaseGodownEntryId },
      callback: () => {
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        this.setState({ selectedRows: [] })
      },
    });
  };

  recordSubmit = purchaseGodownEntryIds => {
    console.log('purchaseGodownEntryIds: ', purchaseGodownEntryIds);
    const { dispatch } = this.props;
    dispatch({//removePurchaseOrder
      type: 'procurementWarehousing/submitWarehousingOrders',
      payload: { purchaseGodownEntryIds },
      callback: () => {
        //1、刷新列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、清空selectedRows
        this.setState({ selectedRows: [] })
      },
    });
  };

  expandedRowRender = record => {
    const {
      procurementWarehousing: { procurementInventoryList },
    } = this.props;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        // width: 60,
        // fixed: 'left',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        // width: 100,
        // fixed: 'left',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        // width: 200,
        // fixed: 'left',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      // },
      {
        title: '含税单价(¥)',
        dataIndex: 'referenceCost',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '无税单价(¥)',
        dataIndex: 'taxFreeUnitPrice',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '税率(%)',
        dataIndex: 'taxRate',
      },
      {
        title: '含税合计',
        dataIndex: 'taxTotal',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '无税合计(¥)',
        dataIndex: 'taxFreeTotal',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      // {
      //   title: '订单数量',
      //   dataIndex: 'number',
      // },
      {
        title: '到货主数量',
        dataIndex: 'arrivalNum',
      },
      {
        title: '备注',
        dataIndex: 'remark',
      },
    ];

    const total = () => {
      let result = `合计:  主数量0 / 辅数量0`;
      if (procurementInventoryList.length > 0) {
        const xiang = procurementInventoryList.map(item => item.number2 || 0).reduce((a, b) => (a*100+b*100)/100);
        const ping = procurementInventoryList.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
        result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100}`
      }
      return result
    }

    return <Table
      size='small'
      columns={columns}
      dataSource={procurementInventoryList}
      footer={total}
      // footer={() => `合计: ${procurementInventoryList.length}`}
      rowKey='ppStockId'
    />
  };

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    // 获取供应商列表
    dispatch({
      type: 'procurementWarehousing/fetchSupplierList',
    });

    //数据权限
    //1、判断是否有数据权限
    const personnel = currentUser.personnel;
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      this.fetchWarehouseList({}, (result) => {
        dispatch({
          type: 'procurementWarehousing/fetchWarehousingOrderList',
        });
      });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      if (personnel) {
        this.fetchWarehouseList( {
          branchId: personnel.branchId,
        });

        dispatch({
          type: 'procurementWarehousing/fetchWarehousingOrderList',
          payload: {
            branchId: personnel.branchId
          }
        });
        // this.fetchWarehouseList( {
        //   branchId: personnel.branchId,
        // }, (result) => {
        //   dispatch({
        //     type: 'procurementWarehousing/fetchWarehousingOrderList',
        //     payload: {
        //       warehouse: result.rows[0] ? result.rows[0].name : ''
        //     }
        //   });
        // });
        //
        // dispatch({
        //   type: 'procurementWarehousing/fetchStaffList',
        //   payload: {
        //     branchId: personnel.branchId,
        //   },
        // });
      }
    }


  }

  fetchWarehouseList = (payload = {}, callback) => {
    const { dispatch } = this.props;
    //获取仓库列表
    dispatch({
      type: 'procurementWarehousing/fetchWarehouseList',
      payload,
      callback,
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'procurementWarehousing/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch, user: { currentUser } } = this.props;
    const { dataPermission } = this.state;
    form.resetFields();
    const personnel = currentUser.personnel;
    this.setState({ purchasePersonnel: [] })
    dispatch({
      type: 'procurementWarehousing/fetchWarehousingOrderList',
      payload: {
        ...form.getFieldsValue(),
        branchId: dataPermission ? '' : personnel.branchId
      }
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'procurementWarehousing/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  executeSearch = (fieldsValue) => {
    const { dispatch, user: { currentUser } } = this.props;
    const { dataPermission } = this.state;
    const personnel = currentUser.personnel;

    const godownEntryTime = fieldsValue.godownEntryTime;
    if (godownEntryTime) {
      fieldsValue.godownEntryTime = fieldsValue.godownEntryTime.format('YYYY-MM-DD')
    }
    if (!dataPermission) {
      fieldsValue.branchId = personnel.branchId;
    }

    dispatch({
      type: 'procurementWarehousing/fetchWarehousingOrderList',
      payload: { ...fieldsValue },
    });
  }

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.executeSearch(fieldsValue)
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'procurementWarehousing/add',
      payload: {
        desc: fields.desc,
      },
    });

    message.success('添加成功');
    this.handleModalVisible();
  };

  handleDetail = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'procurementWarehousing/updateProcurement',
      payload: { ...fields, purchasePlanId: record.purchasePlanId },
    });

    this.handleUpdateModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedRows, expandedRowKeys } = this.state;
    dispatch({
      type: 'procurementWarehousing/updateWarehousingOrder',
      payload: { ...fields, purchaseGodownEntryId: record.purchaseGodownEntryId },
      callback: () => {
        //1、刷新入库单列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、刷新入库单明细列表
        if (expandedRowKeys.length > 0){
          this.onExpand(true, record);
        }
      }
    });

    //若该项被选择了，则需要取消对该项的选择
    if (fields.submit) {
      this.setState({ selectedRows: selectedRows.filter(item => item.purchaseGodownEntryId !== record.purchaseGodownEntryId) })
    }

    this.handleUpdateModalVisible();
  };

  onStoreChange = (storeName, e) => {
    console.log('storeName: ', storeName);
    console.log('e: ', e);
    // console.log('branchId: ', e.props.branchid);
    const branchId = e.props.branchid;

    // 获取子部门列表
    const response = fetchStaffList({ branchId });
    response.then(result => {
      console.log('result: ', result);
      this.setState({ purchasePersonnel: result.data.rows })
      this.props.form.setFieldsValue({ requisitionBy: '' });
    });
  }

  onWarehouseChange = (value) => {
    const { dispatch, procurementWarehousing: { warehouseList }, form } = this.props;

    //根据warehouseId找branchId
    const target = warehouseList.rows.find(item => item.name === value);
    if (target) {
      //获取当前部门员工列表
      dispatch({
        type: 'procurementWarehousing/fetchStaffList',
        payload: {
          branchId: target.branchId
        },
      });

      form.setFieldsValue({ godownEntryBy: '' });
    }
  }

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const godownEntryTime = fieldsValue.godownEntryTime;
      if (godownEntryTime) {
        fieldsValue.godownEntryTime = fieldsValue.godownEntryTime.format('YYYY-MM-DD')
      }
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/purchaseGodownEntry/exportList?${stringify(params)}`;
    });
  }

  renderSimpleForm() {
    const {
      procurementWarehousing: { supplierList, warehouseList, staffList },
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    const { purchasePersonnel, dataPermission } = this.state;

    const warehouse = dataPermission ? '' : (warehouseList.rows[0] ? warehouseList.rows[0].name : '')

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="订单编号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单编号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={<span>仓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;库</span>}>
              {getFieldDecorator('warehouse', {
                // initialValue: warehouse
              })(
                <Select onChange={this.onWarehouseChange} placeholder="请选择仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => <Select.Option key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库人员">
              {getFieldDecorator('godownEntryBy')(
                <Select placeholder="请选择入库人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;供&nbsp;应&nbsp;商&nbsp;</span>}>
              {getFieldDecorator('supplier')(
                <Select
                  // onChange={this.onStoreChange}
                  placeholder="请选择供应商"
                  style={{ width: '100%' }}
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {supplierList.rows.map(item => <Select.Option supplierid={item.supplierId} value={item.name} key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库日期">
              {getFieldDecorator('godownEntryTime')(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择入库日期"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="规则名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="使用状态">
              {getFieldDecorator('status')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="0">关闭</Option>
                  <Option value="1">运行中</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="调用次数">
              {getFieldDecorator('number')(<InputNumber style={{ width: '100%' }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="更新日期">
              {getFieldDecorator('date')(
                <DatePicker style={{ width: '100%' }} placeholder="请输入更新日期" />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="使用状态">
              {getFieldDecorator('status3')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="0">关闭</Option>
                  <Option value="1">运行中</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="使用状态">
              {getFieldDecorator('status4')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="0">关闭</Option>
                  <Option value="1">运行中</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              收起 <Icon type="up" />
            </a>
          </div>
        </div>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const purchaseGodownEntryId = record.purchaseGodownEntryId;
      dispatch({
        type: 'procurementWarehousing/fetchProcurementInventoryList',
        payload: {
          purchaseGodownEntryIds: purchaseGodownEntryId
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  onRowClick = record => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (record.isSubmit) return;

    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.purchaseGodownEntryId === record.purchaseGodownEntryId) {
          newSelectedRows = newSelectedRows.filter(item => item.purchaseGodownEntryId !== record.purchaseGodownEntryId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  render() {
    const {
      procurementWarehousing: { warehousingOrderList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, expandedRowKeys, record, detailModalVisible } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
      handleDetail: this.handleDetail,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => router.push('/procurement-management/procurement-warehousing/warehousing-order')}>
                新增
              </Button>
              {/*<Button >审核</Button>*/}
              {selectedRows.length > 0 && (
                <span>
                  <Button
                    onClick={() => this.recordSubmit( selectedRows.map(item => item.purchaseGodownEntryId).join(','))}
                  >批量提交</Button>
                  <Popconfirm
                    title="确认删除？"
                    onConfirm={() => this.recordRemove( selectedRows.map(item => item.purchaseGodownEntryId).join(','))}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={warehousingOrderList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              rowKey='purchaseGodownEntryId'
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
              onRow={(record) => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
              checkboxProps={record => ({
                // disabled: record.disabled,
                disabled: record.isSubmit,
              })}
              // scroll={{x: 1500}}
            />
          </div>
        </Card>
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
        <DetailForm {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
      </div>
    );
  }
}

export default WarehousingOrderList;
