import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Table,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './WarehousingOrderReference.less';
import { fetchStaffList } from '@/services/branch';
import { fetchProcurementInventoryList } from '@/services/procurementArrival';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ procurementWarehousing, user, loading }) => ({
  procurementWarehousing,
  user,
  loading: loading.models.procurementWarehousing,
}))
@Form.create()
class WarehousingOrderReference extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    selectedRowKeys: [],
    expandedRowKeys: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailVisible: false,
    detailModalVisible: false,
    dataPermission: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    // {
    //   title: '采购人员',
    //   dataIndex: 'requisitionBy',
    // },
    {
      title: '到货时间',
      dataIndex: 'arrivalTime',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '收货人',
      dataIndex: 'consignee',
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  onRowSelectionChange = (selectedRowKeys) => {
    console.log('selectedRowKeys: ', selectedRowKeys);
    this.setState({ selectedRowKeys })
  }

  expandedRowRender = record => {
    const {
      procurementPlan: { purchaseingInventoryList },
    } = this.props;
    const { selectedRowKeys } = this.state;

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onRowSelectionChange,
    };

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        width: 40,
        fixed: 'left',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '单位',
        dataIndex: 'units',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      {
        title: '数量',
        dataIndex: 'number',
      },
      {
        title: '需求日期',
        dataIndex: 'demandTime',
      },
      {
        title: '建议订货日期',
        dataIndex: 'proposalTime',
      },
      {
        title: '供应商',
        dataIndex: 'supplier',
      },
      {
        title: '备注',
        dataIndex: 'remark',
      },
    ];

    return <Table
      rowSelection={rowSelection}
      size='small'
      columns={columns}
      dataSource={purchaseingInventoryList}
      footer={() => `合计: ${purchaseingInventoryList.length}`}
      rowKey='prStockId'
    />
  };

  componentDidMount() {
    const { dispatch, supplier, user: { currentUser } } = this.props;
    //到货单列表
    dispatch({
      type: 'procurementWarehousing/fetchArrivalOrderList',
      payload: {
        supplier,
        submitResult: 1//审批=批准，才能参照
      },
    });

    //数据权限
//1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
  };

  handleFormReset = () => {
    const { form, dispatch, supplier } = this.props;
    form.resetFields();
    dispatch({
      type: 'procurementWarehousing/fetchArrivalOrderList',
      payload: { supplier }
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  onSelect = (record, selected, selectedRows, nativeEvent) => {
    console.log('record: ', record);
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    // const { supplierId } = this.props;
    const { supplier } = this.props;
    const purchaseArrivalId = record.purchaseArrivalId;
    if (selected) {
      // const response = fetchProcurementInventoryList({ purchaseArrivalId, supplierId });
      const response = fetchProcurementInventoryList({ purchaseArrivalIds: purchaseArrivalId, supplier });
      response.then(result => {
        console.log('result: ', result);
        this.setState(prevState => {
          return {
            dataSource: [...prevState.dataSource, ...result.data]
          }
        })
      })
    }
    else{//删除
      const { dataSource } = this.state;
      const newDataSource = dataSource.filter(item => item.purchaseArrivalId.toString() !== purchaseArrivalId.toString());
      this.setState({ dataSource: newDataSource })
    }
  }

  handleSelectRows = rows => {
    console.log('rows: ', rows);
    this.setState({
      selectedRows: rows,
    });

    if (rows.length > 0) {
      const { supplier } = this.props;
      const response = fetchProcurementInventoryList({ purchaseArrivalIds: rows.map(item => item.purchaseArrivalId).join(','), supplier });
      response.then(result => {
        console.log('result: ', result);
        this.setState({
          dataSource: result.data
        })
      })
    }
    else{
      this.setState({ dataSource: [] })
    }

  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form, supplier } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const arrivalTime = fieldsValue.arrivalTime;
      if (arrivalTime) {
        fieldsValue.arrivalTime = fieldsValue.arrivalTime.format('YYYY-MM-DD')
      }
      dispatch({
        type: 'procurementWarehousing/fetchArrivalOrderList',
        payload: { ...fieldsValue, supplier },
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="到货日期">
              {getFieldDecorator('arrivalTime')(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="到货日期"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="规则名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="使用状态">
              {getFieldDecorator('status')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="0">关闭</Option>
                  <Option value="1">运行中</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="调用次数">
              {getFieldDecorator('number')(<InputNumber style={{ width: '100%' }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="更新日期">
              {getFieldDecorator('date')(
                <DatePicker style={{ width: '100%' }} placeholder="请输入更新日期" />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="使用状态">
              {getFieldDecorator('status3')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="0">关闭</Option>
                  <Option value="1">运行中</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="使用状态">
              {getFieldDecorator('status4')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="0">关闭</Option>
                  <Option value="1">运行中</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              收起 <Icon type="up" />
            </a>
          </div>
        </div>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const purchaseRequisitionId = record.purchaseRequisitionId;
      dispatch({
        type: 'procurementPlan/fetchPurchaseingInventoryList',
        payload: {
          purchaseRequisitionId
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  handleOk = (e) => {
    console.log(e);
    this.setState({
      detailVisible: false,
    });
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      detailVisible: false,
    });
  }

  render() {
    const {
      procurementWarehousing: { arrivalOrderList },
      loading,
    } = this.props;
    const { selectedRows, expandedRowKeys, selectedRowKeys, dataSource } = this.state;

    console.log('arrivalOrderList: ', arrivalOrderList);

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      // },
      {
        title: '含税单价',
        dataIndex: 'referenceCost',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '无税单价',
        dataIndex: 'taxFreeUnitPrice',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '税率(%)',
        dataIndex: 'taxRate',
      },
      {
        title: '含税合计',
        dataIndex: 'taxTotal',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '无税合计',
        dataIndex: 'taxFreeTotal',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      // {
      //   title: '订单数量',
      //   dataIndex: 'number',
      // },
      {
        title: '到货主数量',
        dataIndex: 'arrivalNum',
      },
      {
        title: '拒收主数量',
        dataIndex: 'rejectionNum',
      },
      {
        title: '备注',
        dataIndex: 'remark',
      },
    ];

    return (
      <div>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          <StandardTable
            selectedRows={selectedRows}
            loading={loading}
            data={arrivalOrderList}
            columns={this.columns}
            onSelectRow={this.handleSelectRows}
            // onSelect={this.onSelect}
            onChange={this.handleStandardTableChange}
            // expandedRowKeys={expandedRowKeys}
            // expandedRowRender={this.expandedRowRender}
            rowKey='purchaseArrivalId'
            paginationOfTable={{pageSize: 5, size: 'small'}}
            size='small'
            title={() => <div style={{textAlign: 'center'}}>采购订单列表</div>}
            // onExpand={this.onExpand}
            // onExpandedRowsChange={this.onExpandedRowsChange}
            onRow={(record) => {
              return {
                onClick: () => {// 点击行
                  console.log('record: ', record);
                  console.log('oldSelectedRows: ', this.state.selectedRows);
                  this.setState(prevState => {
                    let newSelectedRows = [...prevState.selectedRows];
                    let flag = true;
                    for (let item of prevState.selectedRows) {
                      if (item.purchaseArrivalId === record.purchaseArrivalId) {
                        newSelectedRows = newSelectedRows.filter(item => item.purchaseArrivalId !== record.purchaseArrivalId);
                        flag = false;
                        break;
                      }
                    }
                    if (flag){
                      newSelectedRows.push(record)
                    }
                    console.log('newSelectedRows: ', newSelectedRows);
                    //更新计划明细
                    this.onSelect(record, flag);

                    return ({
                      selectedRows: newSelectedRows
                    })
                  })
                },
              };
            }}

          />
          <Table
            loading={loading}
            // dataSource={purchaseingInventoryList}
            dataSource={dataSource}
            columns={columns}
            size='small'
            title={() => <div style={{textAlign: 'center'}}>订单明细</div>}
          />
        </div>
      </div>
    );
  }
}

export default WarehousingOrderReference;
