import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Table,
  Popconfirm,
} from 'antd';
import WarehousingOrderAdd from './WarehousingOrderAdd'
import styles from './WarehousingOrder.less';
import moment from 'moment'
import { fetchStaffList } from '@/services/branch';
import WarehousingOrderReference from "./WarehousingOrderReference";
import EditableCell from '@/customComponents/EditableCell';

//单元格编辑--------------------------------------------
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
//-----------------------------------------------------

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, supplierId } = props;
  let purchaseingPlanAdd;
  const okHandle = () => {
    const { selectedRows } = purchaseingPlanAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = (ref) => {
    purchaseingPlanAdd = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      // title="订单明细"
      title={(
        <div>
          <span>订单明细</span>
          <Button style={{float: 'right', marginRight: 50}} type='primary' onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{top: 20}}
    >
      <WarehousingOrderAdd wrappedComponentRef={addModalRef} supplierId={supplierId} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改订单明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="含税单价">
        {form.getFieldDecorator('referenceCost', {
          rules: [{ required: true, message: '请输入含税单价！' }],
          initialValue: record.referenceCost,
        })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入含税单价" min={0} />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="订单数量">
        {form.getFieldDecorator('number', {
          rules: [{ required: true, message: '请输入订单数量！' }],
          initialValue: record.number,
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入订单数量" min={1} />)}
      </FormItem>
      {/*<FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="到货数量">*/}
        {/*{form.getFieldDecorator('arrivalNum', {*/}
          {/*rules: [{ required: true, message: '请输入到货数量！' }],*/}
          {/*initialValue: record.arrivalNum,*/}
        {/*})(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入到货数量" min={0} />)}*/}
      {/*</FormItem>*/}
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const ReferenceForm = Form.create()(props => {
  const { referenceModalVisible, handleReference, handleReferenceModalVisible, supplier } = props;
  let WarehousingOrderAdd;
  const okHandle = () => {
    const { dataSource } = WarehousingOrderAdd.state;
    handleReference(dataSource);
  };

  const referenceModalRef = (ref) => {
    WarehousingOrderAdd = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="参照"
      visible={referenceModalVisible}
      onOk={okHandle}
      onCancel={() => handleReferenceModalVisible()}
      style={{top: 20}}
    >
      <WarehousingOrderReference wrappedComponentRef={referenceModalRef} supplier={supplier} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ procurementWarehousing, user, loading }) => ({
  procurementWarehousing,
  user,
  loading: loading.models.procurementWarehousing,
}))
@Form.create()
class WarehousingOrder extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    referenceModalVisible: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    supplierId: '',
    warehouseId: '',
    dataPermission: false
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '入库主数量',
      dataIndex: 'number',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 0,
      inputNumberMin: 1,
      width: 120,
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '入库辅数量',
      dataIndex: 'number2',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 2,
      inputNumberMin: 0,
      width: 120,
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    {
      title: '含税单价(¥)',
      dataIndex: 'referenceCost',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 2,
      inputNumberMin: 0,
      width: 120,
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '无税单价(¥)',
      dataIndex: 'taxFreeUnitPrice',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '税率(%)',
      dataIndex: 'taxRate',
    },
    {
      title: '含税合计(¥)',
      dataIndex: 'taxTotal',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '无税合计(¥)',
      dataIndex: 'taxFreeTotal',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    // {
    //   // title: '订单数量',
    //   title: '入库数量',
    //   dataIndex: 'number',
    //   editable: true,
    //   isInputNumber: true,
    //   inputNumberPrecision: 0,
    //   inputNumberMin: 1,
    //   width: 120,
    // },
    {
      title: '到货主数量',
      dataIndex: 'arrivalNum',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
      width: 200
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.stockId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = (stockId) => {
    console.log('stockId: ', stockId);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.stockId !== stockId) })
    message.success('删除成功');
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'procurementWarehousing/fetchOrderNumber',
    });

    // 获取供应商列表
    dispatch({
      type: 'procurementWarehousing/fetchSupplierList',
    });

    //数据权限
    //1、判断是否有数据权限
    const personnel = currentUser.personnel;
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    let warehouseListPayload = {};
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      if (personnel) {
        warehouseListPayload = {
          branchId: personnel.branchId,
        }
      }
    }
    //获取仓库列表
    dispatch({
      type: 'procurementWarehousing/fetchWarehouseList',
      payload: warehouseListPayload,
      callback: data => {
        const firstWarehouse = data.rows.length > 0 ? data.rows[0] : '';
        if (firstWarehouse) {
          this.onWarehouseChange(firstWarehouse.name);
        }
      }
    });
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      dataSource: [],
      supplierId: '',
    })
  };

  submit = (isSubmit) => {
    const { dispatch, form } = this.props;
    const { dataSource, warehouseId, supplierId } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增订单明细');
        return;
      }
      else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      }
      // else if (dataSource.find(item => !item.arrivalNum && item.arrivalNum !== 0)) {
      //   message.warning('请设置商品的到货数量');
      //   return;
      // }

      fieldsValue.godownEntryTime = fieldsValue.godownEntryTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.warehouseId = warehouseId;
      fieldsValue.supplierId = supplierId;
      const supplier = fieldsValue.supplier;
      if (supplier === undefined){
        fieldsValue.supplier = '';
      }
      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'procurementWarehousing/addWarehousingOrder',//addPurchaseingOrder
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [], supplierId: '' })

          //更新订单编号
          dispatch({
            type: 'procurementWarehousing/fetchOrderNumber',
          });
        }
      });
    });
  };

  handleSearch = e => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0){
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            if (newItem.stockId.toString() === oldItem.stockId.toString()) {
              // oldItem.number++;
              flag = false;
              break;
            }
          }
          if (flag) {
            // oldDataSource.push({ ...newItem, number: 1 })
            oldDataSource.push({ ...newItem, number: 1, number2: parseInt((1/newItem.unitRatio)*100)/100 })
          }
        }
      }
      else{
        // oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1 }))];
        oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1, number2: parseInt((1/item.unitRatio)*100)/100 }))];
      }

      //计算无税单价、含税合计、无税合计
      oldDataSource = this.getNewDataSource(oldDataSource);

      return {
        dataSource: oldDataSource
      }
    });

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.stockId === record.stockId);
    if (index > -1){
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });

      //计算无税单价、含税合计、无税合计
      const dataSource = this.getNewDataSource(newData);

      this.setState({ dataSource });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    }
    else{
      message.error('修改失败');
    }
  };

  onStoreChange = (supplierName, e) => {
    console.log('supplierName: ', supplierName);
    if (supplierName) {
      const supplierId = e.props.supplierid;
      console.log('supplierId: ', supplierId);
      this.setState({
        supplierId,
        dataSource: [],
      })
    }
    else{
      this.setState({
        supplierId: '',
        dataSource: []
      })
    }
  };

  onWarehouseChange = (value, e) => {
    console.log('e: ', e);
    const { dispatch, procurementWarehousing: { warehouseList }, form } = this.props;


    //根据warehouseId找branchId
    const target = warehouseList.rows.find(item => item.name === value);
    if (target) {
      //获取当前部门员工列表
      dispatch({
        type: 'procurementWarehousing/fetchStaffList',
        payload: {
          branchId: target.branchId
        },
      });

      this.setState({ warehouseId: target.warehouseId })

      if (e){
        form.setFieldsValue({ godownEntryBy: '' });
      }
    }
  }

  renderTopSimpleForm() {
    const {
      procurementWarehousing: { orderNumber, supplierList, warehouseList, staffList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;
    const { dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;单据编号</span>}>
              {orderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={<span>选择仓库</span>}>
              {getFieldDecorator('warehouse', {
                rules: [{ required: true, message: '请选择仓库' }],
                initialValue: warehouseList.rows.length > 0 ? warehouseList.rows[0].name : ''
              })(
                <Select
                  // onChange={(value, e) => this.setState({ warehouseId: e.props.warehouseId })}
                  placeholder="请选择仓库"
                  style={{ width: '100%' }}
                  onChange={this.onWarehouseChange}
                >
                  {warehouseList.rows.map(item => <Select.Option key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库人员">
              {getFieldDecorator('godownEntryBy', {
                rules: [{ required: true, message: '请选择收货人' }],
                // initialValue: currentUser.personnel.name
                initialValue: staffList.rows.length > 0 ? staffList.rows[0].name : ''
              })(
                <Select placeholder="请选择收货人" style={{ width: '100%' }}>
                  {staffList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;供&nbsp;&nbsp;应&nbsp;&nbsp;商</span>}>
              {getFieldDecorator('supplier')(
                <Select
                  allowClear
                  onChange={this.onStoreChange}
                  placeholder="请选择供应商"
                  style={{ width: '100%' }}
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {/*{storeList.rows.map(item => <Select.Option branchid={item.branchId} key={item.name}>{item.name}</Select.Option>)}*/}
                  {supplierList.rows.map(item => <Select.Option supplierid={item.supplierId} value={item.name} key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库时间">
              {getFieldDecorator('godownEntryTime', {
                rules: [{ required: true, message: '请选择入库时间' }],
                initialValue: moment(new Date(), 'YYYY-MM-DD HH:mm')
              })(
                <DatePicker
                  showTime={{ format: 'HH:mm' }}
                  style={{width: '100%'}}
                  placeholder="请选择入库时间"
                  format="YYYY-MM-DD HH:mm"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库批次">
              {getFieldDecorator('batchNumber', {
                rules: [{ required: true, message: '请输入批次' }],
                initialValue: '9999999999999999'
              })(
                <Input
                  style={{width: '100%'}}
                  placeholder="请输入批次"
                  disabled
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
      loading
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">
              {currentUser.personnel.name}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
            </span>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
              <Button style={{ marginRight: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button loading={loading} disabled={loading} style={{ marginRight: 8 }} onClick={() => this.submit(false)}>
                保存
              </Button>
              <Button loading={loading} disabled={loading} type="primary" htmlType="submit">
                提交
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onAddClick = () => {
    // const { supplierId } = this.state;
    // if (!supplierId) {
    //   message.warning('请先选择供应商!');
    //   return;
    // }
    this.handleModalVisible(true);
  }

  onReferenceClick = () => {
    // const { supplierId } = this.state;
    // if (!supplierId) {
    //   message.warning('请先选择供应商!');
    //   return;
    // }
    this.handleReferenceModalVisible(true)
  }

  handleReference = (dataSource) => {
    console.log('dataSource: ', dataSource);
    //dataSource去重
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      // if (oldDataSource.length > 0){
      for (let newItem of dataSource) {
        let flag = true;
        for (let oldItem of oldDataSource) {
          if (newItem.stockNum.toString() === oldItem.stockNum.toString()) {
            // oldItem.number = oldItem.number + newItem.number;
            flag = false;
            break;
          }
        }
        if (flag) {
          const arrivalNum = parseInt(newItem.arrivalNum) - parseInt(newItem.rejectionNum);
          oldDataSource.push({
            ...newItem,
            arrivalNum,
            number: arrivalNum,
            number2: parseInt((arrivalNum/newItem.unitRatio)*100)/100
          })
        }
      }

      //计算无税单价、含税合计、无税合计
      oldDataSource = this.getNewDataSource(oldDataSource);

      return {
        dataSource: oldDataSource
      }
    });
    this.handleReferenceModalVisible();
  }

  getNewDataSource = (oldDataSource) => {
    return oldDataSource.map(item => {
      let taxFreeUnitPrice = item.referenceCost/(1+item.taxRate/100);
      taxFreeUnitPrice = parseInt(taxFreeUnitPrice*10000)/10000;

      let taxTotal = item.referenceCost * item.number;
      taxTotal = parseInt(taxTotal*10000)/10000;

      let taxFreeTotal = taxFreeUnitPrice * item.number;
      taxFreeTotal = parseInt(taxFreeTotal*10000)/10000;

      return {
        ...item,
        taxFreeUnitPrice,//无税单价
        taxTotal, //含税合计
        taxFreeTotal, //无税合计
      }
    });
  }

  handleReferenceModalVisible = flag => {
    this.setState({
      referenceModalVisible: !!flag,
    });
  };

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.stockId === item.stockId);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  }

  handleInputNumberChange = (record, key, value) => {
    const { form } = this.props;
    console.log('row: ', row);
    console.log('key: ', key);
    console.log('value: ', value);
    let newData = [...this.state.dataSource];

    const row = { ...record };

    //改row
    if (isNaN(value)) {
      row.number = '';
      row.number2 = '';
      return;
    }
    else if (key === 'number') {
      row.number2 = parseInt((value/row.unitRatio)*100)/100;
      row.number = value;
    }
    else if (key === 'number2') {
      //保留两位小数
      value = parseInt(value*100)/100;
      row.number = parseInt(value*row.unitRatio);
    }
    else if (key === 'referenceCost') {
      row.referenceCost = value;
    }
    const index = newData.findIndex(item => row.stockId === item.stockId);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });

    //计算无税单价、含税合计、无税合计
    newData = this.getNewDataSource(newData);

    this.setState({ dataSource: newData });
  }

  total = () => {
    const { dataSource } = this.state;
    let result = `合计: 主数量0 / 辅数量0 `;
    if (dataSource.length > 0) {
      // const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => a+b);
      const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => (a*100+b*100)/100);
      const ping = dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
      result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100}`
    }
    return result
  }

  render() {
    const { modalVisible, dataSource, updateModalVisible, record, supplierId, referenceModalVisible } = this.state;
    const { form } = this.props;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const referenceMethods = {
      handleReference: this.handleReference,
      handleReferenceModalVisible: this.handleReferenceModalVisible,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          isInputNumber: col.isInputNumber,
          inputNumberPrecision: col.inputNumberPrecision,
          inputNumberMin: col.inputNumberMin,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
          inputNumberChange: (value, row) => this.handleInputNumberChange(row, col.dataIndex, value),
        }),
      };
    });

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              订单明细
              <Button
                type="primary"
                style={{ marginLeft: 16 }}
                // icon="plus"
                onClick={this.onAddClick}
              >
                新增
              </Button>
              <Button
                style={{ marginLeft: 16 }}
                onClick={this.onReferenceClick}
              >
                参照
              </Button>
            </h2>
            <Divider />
            <Table
              // loading={loading}
              components={components}
              columns={columns}
              rowKey='stockId'
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              scroll={{x: 2000}}
              footer={this.total}
              // footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{paddingTop: 24}} className={styles.tableListForm}>{this.renderBottomSimpleForm()}</div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} supplierId={supplierId} />
        {/*<ReferenceForm {...referenceMethods} referenceModalVisible={referenceModalVisible} supplierId={supplierId} />*/}
        <ReferenceForm {...referenceMethods} referenceModalVisible={referenceModalVisible} supplier={form.getFieldsValue().supplier} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
      </div>
    );
  }
}

export default WarehousingOrder;
