import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Divider,
  Popconfirm,
  Row,
  Col,
  DatePicker,
  Table,
  Popover,
  InputNumber,
  message,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import moment from 'moment';
import styles from './SaleReturnList.less';
import { apiDomainName } from '../../constants';
import { stringify } from 'qs';
import NP from 'number-precision';
import OffLineRefundOrderDetail from '@/customComponents/OffLineRefundOrderDetail';
import ReactToPrint from 'react-to-print';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const OrderDetail = Form.create()(props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      // width='60%'
      width={500}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={this.handleOk}
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{ marginLeft: 15 }}>
          <Row>
            <Col span={20}>退货</Col>
            <Col span={4}>
              <span style={{ float: 'right' }}>
                <ReactToPrint
                  trigger={() => (
                    <a href="#">
                      <Icon type="printer" />
                    </a>
                  )}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef;
                  }}
                />
              </span>
            </Col>
          </Row>
        </div>
      }
      // title={
      //   <div style={{marginLeft: 15}}>
      //     <Row>
      //       <Col span={20}>
      //         <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{record.storeName}</span>
      //       </Col>
      //       <Col span={4}>
      //         <span
      //           style={{float: 'right'}}
      //         >
      //           <ReactToPrint
      //             trigger={() => <a href="#"><Icon type="printer" /></a>}
      //             content={() => {
      //               console.log('componentRef: ', componentRef);
      //               return componentRef
      //             }}
      //           />
      //         </span>
      //       </Col>
      //     </Row>
      //     <Row>
      //       <Col span={24}>
      //         <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{`地址: ${record.storeAddress}`}</span>
      //       </Col>
      //     </Row>
      //   </div>
      // }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <OffLineRefundOrderDetail record={record} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ saleReturn, user, loading }) => ({
  saleReturn,
  user,
  loading: loading.models.saleReturn,
}))
@Form.create()
class SaleReturnList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    // visible: [false, false, false],
    selectedRows: [],
    expandedRowKeys: [],
    goodsDataSource: [],
    formValues: {},
    record: {},
    dataPermission: false,
    handleDetailModalVisible: false,
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index + 1,
    },
    {
      title: '退款单',
      dataIndex: 'returnNumber',
      render: (text, record) => this.getPayDetail(text, record),
    },
    {
      title: '原订单号',
      dataIndex: 'orderNumber',
    },
    {
      title: '退货仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '退货批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '退款金额(¥)',
      dataIndex: 'refundAmount',
    },
    {
      title: '退款方式',
      dataIndex: 'refundMethod',
      render: text => {
        let result;
        switch (text) {
          case 0:
            result = '现金';
            break;
          case 1:
            result = '支付宝';
            break;
          case 2:
            result = '微信';
            break;
          case 3:
            result = '银行卡';
            break;
        }

        return result;
      },
    },
    {
      title: '退款原因',
      dataIndex: 'reason',
    },
    {
      title: '退款时间',
      dataIndex: 'createTime',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  getPayDetail = (text, record) => {
    // const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = record;
    // let dataSource = [];
    // if (cashPayment) {
    //   dataSource.push({ payMethod: '现金支付', payAmount: cashPayment, key: dataSource.length })
    // }
    // if (alipayPayment) {
    //   dataSource.push({ payMethod: '支付宝支付', payAmount: alipayPayment, key: dataSource.length })
    // }
    // if (weChatPayment) {
    //   dataSource.push({ payMethod: '微信支付', payAmount: weChatPayment, key: dataSource.length })
    // }
    // if (bankCardPayment) {
    //   dataSource.push({ payMethod: '银行卡支付', payAmount: bankCardPayment, key: dataSource.length })
    // }

    // const columns = [
    //   {
    //     title: '支付方式',
    //     dataIndex: 'payMethod'
    //   },
    //   {
    //     title: '支付金额(¥)',
    //     dataIndex: 'payAmount'
    //   },
    // ]
    // const content = (
    //   <Table rowKey='key' columns={columns} dataSource={dataSource} pagination={false} size='small' />
    // );
    return (
      //<Popover content={content} title="支付明细">
      <a onClick={() => this.handleDetailModalVisible(true, record)}>{text}</a>
      // </Popover>
    );
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;

    // if (personnel) {
    //   //获取当前部门员工列表
    //   // dispatch({
    //   //   type: 'saleReturn/fetchStaffList',
    //   //   payload: {
    //   //     branchId: personnel.branchId,
    //   //   },
    //   // });
    //
    //   //获取仓库列表
    //   dispatch({
    //     type: 'saleReturn/fetchWarehouseList',
    //     payload: {
    //       branchId: personnel.branchId,
    //     },
    //   });
    // }

    const personnel = currentUser.personnel;
    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      //获取仓库列表
      dispatch({
        type: 'saleReturn/fetchWarehouseList',
        // payload: {
        //   branchId: personnel.branchId,
        // },
      });

      dispatch({
        type: 'saleReturn/fetchSaleReturnGoodsList',
      });
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      //获取仓库列表
      dispatch({
        type: 'saleReturn/fetchWarehouseList',
        payload: {
          branchId: personnel.branchId,
        },
      });

      dispatch({
        type: 'saleReturn/fetchSaleReturnGoodsList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'saleReturn/fetch',
      payload: params,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const {
      dispatch,
      form,
      user: { currentUser },
    } = this.props;
    const { dataPermission } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (fieldsValue.time) {
        fieldsValue.startTime = moment(fieldsValue.time[0]).format('YYYY-MM-DD HH:mm');
        fieldsValue.endTime = moment(fieldsValue.time[1]).format('YYYY-MM-DD HH:mm');
        delete fieldsValue.time;
      }
      if (!dataPermission) {
        fieldsValue.branchId = currentUser.personnel.branchId;
      }

      dispatch({
        type: 'saleReturn/fetchSaleReturnGoodsList',
        payload: fieldsValue,
      });
    });
  };

  handleFormReset = () => {
    const {
      form,
      dispatch,
      user: { currentUser },
    } = this.props;
    const { dataPermission } = this.state;
    form.resetFields();

    dispatch({
      type: 'saleReturn/fetchSaleReturnGoodsList',
      payload: {
        branchId: dataPermission ? '' : currentUser.personnel.branchId,
      },
    });
  };

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.time) {
        fieldsValue.startTime = moment(fieldsValue.time[0]).format('YYYY-MM-DD HH:mm');
        fieldsValue.endTime = moment(fieldsValue.time[1]).format('YYYY-MM-DD HH:mm');
        delete fieldsValue.time;
      }
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId,
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/salesReturnGoods/exportList?${stringify(
        params
      )}`;
    });
  };

  renderSimpleForm() {
    const {
      saleReturn: { warehouseList },
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="退款单号">
              {getFieldDecorator('returnNumber')(<Input placeholder="请输入退款单号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="原订单号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入原订单号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="退货仓库">
              {getFieldDecorator('warehouseId')(
                <Select placeholder="请选择出库仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => (
                    <Select.Option key={item.warehouseId} value={item.warehouseId}>
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="退款时间">
              {getFieldDecorator('time')(
                <RangePicker
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder={['开始时间', '结束时间']}
                  style={{ width: '100%' }}
                />
              )}
            </FormItem>
          </Col>
          <Col md={16} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  expandedRowRender = order => {
    const { goodsDataSource } = this.state;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        render: (text, record, index) => index + 1,
      },
      {
        title: '商品编号',
        dataIndex: 'stockNum',
      },
      {
        title: '商品名称',
        dataIndex: 'name',
      },
      // {
      //   title: '单位',
      //   dataIndex: 'units',
      // },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '退货主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '退货辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '零售价(¥)',
        dataIndex: 'referencePrice',
      },
      // {
      //   title: '退货数量',
      //   dataIndex: 'number',
      // },
      {
        title: '退货总额(¥)',
        dataIndex: '',
        // render: (text, record) => parseFloat(record.referencePrice) * parseInt(record.number),
        // render: (text, record) => ((record.referencePrice*100) * parseInt(record.number))/100,
        // render: (text, record) => record.totalNumber === record.returnGoodsNumber ? record.totalPrice : NP.times(record.referencePrice, parseInt(record.number)),
        render: (text, record) => this.getReturnTotalPrice(record, order),
      },
    ];

    return (
      <Table
        size="small"
        columns={columns}
        dataSource={goodsDataSource}
        // footer={() => `合计: ${purchaseingInventoryList.length}`}
        rowKey="salesOrderGoodsId"
        title={() => <h2>商品明细</h2>}
      />
    );
  };

  getReturnTotalPrice = (row, order) => {
    let returnTotalPrice = 0;
    const { goodsDataSource } = this.state;
    // 两种情况
    const a = goodsDataSource.map(item => NP.times(item.number, item.referencePrice)).reduce((a, b) => NP.plus(a, b));
    if (order.refundAmount === a) { // 不是最后一次退货,按退货总额按售价*退后数计算
      console.log('a1: ', a);
      console.log('row: ', row);
      console.log('goodsDataSource: ', goodsDataSource);
      returnTotalPrice = NP.times(row.referencePrice, parseInt(row.number))
    }
    else { // 最后一次退完时，按销售额-单价*(总数-退货数)
      console.log('a2: ', a);
      console.log('row: ', row);
      console.log('goodsDataSource: ', goodsDataSource);
      returnTotalPrice = NP.minus(row.totalPrice, NP.times(row.referencePrice, (NP.minus(row.totalNumber, parseInt(row.number)))))
    }
    return returnTotalPrice;
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const salesReturnGoodsId = record.salesReturnGoodsId;
      dispatch({
        type: 'saleReturn/fetchsalesReturnGoodsDetails',
        payload: {
          salesReturnGoodsId,
        },
        callback: data => {
          console.log('data: ', data);
          this.setState({ goodsDataSource: data });
        },
      });
    }
  };

  onExpandedRowsChange = expandedRows => {
    this.setState({
      expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length - 1]] : [],
    });
  };

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  render() {
    const {
      saleReturn: { returnGoodsList },
      loading,
    } = this.props;
    const { expandedRowKeys, detailModalVisible, record } = this.state;

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <StandardTable
              loading={loading}
              data={returnGoodsList}
              columns={this.columns}
              onChange={this.handleStandardTableChange}
              rowKey="salesReturnGoodsId"
              // scroll={{x: 1300}}
              size="small"
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
            />
          </div>
        </Card>
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record} />
      </div>
    );
  }
}

export default SaleReturnList;
