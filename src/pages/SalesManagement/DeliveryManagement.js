import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, DatePicker, Table, Popover, InputNumber, message } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated } from '@/services/brand';
import moment from 'moment'
import styles from './DeliveryManagement.less';
import OffLineSaleOrderDetail from '@/customComponents/OffLineSaleOrderDetail';
import ReactToPrint from "react-to-print";
import NP from 'number-precision';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const DeliveryForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, staffList, record } = props;
  console.log('dj: ', record);
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="配送"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="防伪码">
        {form.getFieldDecorator('securityCode')(
          <Input placeholder="请输入防伪码"/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="配送员">
        {form.getFieldDecorator('distributionId')(
          <Select placeholder="请选择配送员" style={{ width: '100%' }}>
            {staffList.rows.map(item => <Option key={item.personnelId} value={item.personnelId}>{item.name}</Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="配送地址">
        {form.getFieldDecorator('detailedAddress', {
          initialValue: record.detailedAddress ? record.detailedAddress : ''
        })(
          <Input placeholder="请输入配送地址"/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(
          <Input.TextArea rows={3} placeholder="请输入备注"/>
        )}
      </FormItem>
    </Modal>
  );
});

const ReceivableForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = fieldsValue;
      if ( !cashPayment && !alipayPayment && !weChatPayment && !bankCardPayment ){
        message.warning('请输入收款金额');
        return;
      }

      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="签收"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="现金">
        {form.getFieldDecorator('cashPayment')(
          <InputNumber
            precision={2}
            style={{width: '100%'}}
            placeholder="请输入金额"
            min={0}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="支付宝">
        {form.getFieldDecorator('alipayPayment')(
          <InputNumber
            precision={2}
            style={{width: '100%'}}
            placeholder="请输入金额"
            min={0}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="微信">
        {form.getFieldDecorator('weChatPayment')(
          <InputNumber
            precision={2}
            style={{width: '100%'}}
            placeholder="请输入金额"
            min={0}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="银行卡">
        {form.getFieldDecorator('bankCardPayment')(
          <InputNumber
            precision={2}
            style={{width: '100%'}}
            placeholder="请输入金额"
            min={0}
          />
        )}
      </FormItem>
    </Modal>
  );
});

const OrderDetail = Form.create()(props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      width={500}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={this.handleOk}
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{marginLeft: 15}}>
          <Row>
            <Col span={20}>
              <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{record.storeName}</span>
            </Col>
            <Col span={4}>
              <span
                style={{float: 'right'}}
              >
                <ReactToPrint
                  trigger={() => <a href="#"><Icon type="printer" /></a>}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef
                  }}
                />
              </span>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{`地址: ${record.storeAddress}`}</span>
            </Col>
          </Row>
        </div>
      }
      // title={
      //   <div>
      //     {record.storeName}
      //     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      //     {`地址: ${record.storeAddress}`}
      //     <span
      //       style={{float: 'right'}}
      //     >
      //       <ReactToPrint
      //         trigger={() => <a href="#"><Icon type="printer" /></a>}
      //         content={() => {
      //           console.log('componentRef: ', componentRef);
      //           return componentRef
      //         }}
      //       />
      //     </span>
      //   </div>
      // }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <OffLineSaleOrderDetail isDeliveryOrder={true} record={record}/>
    </Modal>
  );
})

/* eslint react/no-multi-comp:0 */
@connect(({ offLineDelivery, user, loading }) => ({
  offLineDelivery,
  user,
  loading: loading.models.offLineDelivery,
}))
@Form.create()
class DeliveryManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    modalReceivableVisible: false,
    detailModalVisible: false,
    expandForm: false,
    // visible: [false, false, false],
    selectedRows: [],
    expandedRowKeys: [],
    goodsDataSource: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
      // fixed: 'left',
      width: 150,
      render: (text, record) => this.getPayDetail(text, record),
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      render: text => {
        let result = '';
        switch (text) {
          case 0:
            result = <span style={{color: '#f5222d', fontWeight: 'bold'}}>未配送</span>;
            break;
          case 1:
            result = <span style={{color: '#1890ff', fontWeight: 'bold'}}>配送中</span>;
            break;
          case 2:
            result = <span style={{color: '#52c41a', fontWeight: 'bold'}}>已签收</span>;
            break;
          case 3:
            result = <span style={{color: '#faad14', fontWeight: 'bold'}}>已取消</span>;
            break;
        }
        return result;
      }
    },
    {
      title: '订购时间',
      dataIndex: 'createTime',
    },
    {
      title: '会员电话',
      dataIndex: 'memberPhone',
    },
    {
      title: '收货人',
      dataIndex: 'consignee',
    },
    {
      title: '收货人电话',
      dataIndex: 'phone',
    },
    {
      title: '应收款(¥)',
      dataIndex: 'receivableAmount',
    },
    {
      title: '实收款(¥)',
      dataIndex: 'receivedAmount',
    },
    {
      title: '防伪码',
      dataIndex: 'securityCode',
    },
    {
      title: '配送员',
      dataIndex: 'distributionName',
    },
    {
      title: '配送地址',
      dataIndex: 'detailedAddress',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  getReceivedAmount = (fields) => {
    const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = fields;
    let receivedAmount = 0;
    if (cashPayment) {
      receivedAmount = NP.plus(receivedAmount, cashPayment);
    }
    if (alipayPayment) {
      receivedAmount = NP.plus(receivedAmount, alipayPayment);
    }
    if (weChatPayment) {
      receivedAmount = NP.plus(receivedAmount, weChatPayment);
    }
    if (bankCardPayment) {
      receivedAmount = NP.plus(receivedAmount, bankCardPayment);
    }
    return receivedAmount;
  }

  getPayDetail = (text, record) => {
    const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = record;
    let dataSource = [];
    if (cashPayment) {
      dataSource.push({ payMethod: '现金支付', payAmount: cashPayment, key: dataSource.length })
    }
    if (alipayPayment) {
      dataSource.push({ payMethod: '支付宝支付', payAmount: alipayPayment, key: dataSource.length })
    }
    if (weChatPayment) {
      dataSource.push({ payMethod: '微信支付', payAmount: weChatPayment, key: dataSource.length })
    }
    if (bankCardPayment) {
      dataSource.push({ payMethod: '银行卡支付', payAmount: bankCardPayment, key: dataSource.length })
    }

    const columns = [
      {
        title: '支付方式',
        dataIndex: 'payMethod'
      },
      {
        title: '支付金额(¥)',
        dataIndex: 'payAmount'
      },
    ]
    const content = (
      <Table rowKey='key' columns={columns} dataSource={dataSource} pagination={false} size='small' />
    );
    return (
      <Popover content={content} title="支付明细">
        <a onClick={() => this.handleDetailModalVisible(true, record)}>{text}</a>
      </Popover>
    )
  }

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;
    dispatch({
      type: 'offLineDelivery/fetchPhoneOrderDeliveryList',
      payload: {
        // status: 0,
        type: 1,
      }
    });

    const personnel = currentUser.personnel;
    if (personnel) {
      //获取当前部门员工列表
      dispatch({
        type: 'offLineDelivery/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });

      //获取仓库列表
      dispatch({
        type: 'offLineDelivery/fetchWarehouseList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'offLineDelivery/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };


  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (fieldsValue.time){
        fieldsValue.startTime = moment(fieldsValue.time[0]).format('YYYY-MM-DD HH:mm');
        fieldsValue.endTime = moment(fieldsValue.time[1]).format('YYYY-MM-DD HH:mm');
        delete fieldsValue.time;
      }

      dispatch({
        type: 'offLineDelivery/fetchPhoneOrderDeliveryList',
        payload: {
          ...fieldsValue,
          type: 1,
        },
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();

    dispatch({
      type: 'offLineDelivery/fetchPhoneOrderDeliveryList',
      payload: {
        // status: 0,
        type: 1,
      },
    });
  };

  renderSimpleForm() {
    const {
      offLineDelivery: { warehouseList },
      form: { getFieldDecorator },
    } = this.props;
    const { selectedRows } = this.state;

    console.log('selectedRows: ', selectedRows);
    const selectedRowStatus = selectedRows.length > 0 ? selectedRows[0].status : '';

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={7} sm={24}>
            <FormItem label="订单编号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单编号" />)}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="配送状态">
              {getFieldDecorator('status', {
                // initialValue: 0
              })(
                <Select placeholder="请选择配送状态" style={{ width: '100%' }}>
                  <Option value={0}>未配送</Option>
                  <Option value={1}>配送中</Option>
                  <Option value={2}>已签收</Option>
                  <Option value={3}>已取消</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={10} sm={24}>
            <FormItem label="下单时间">
              {getFieldDecorator('time')(
                <RangePicker
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder={['开始时间', '结束时间']}
                  style={{width: '100%'}}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={12} sm={24}>
            <Button
              disabled={selectedRowStatus.toString() !== '0'}
              type="primary"
              onClick={() => this.handleModalVisible(true)}
              style={{marginRight: 8}}
            >
              配送
            </Button>
            <Button
              disabled={selectedRowStatus.toString() !== '1'}
              type="primary"
              onClick={() => this.handleReceivableModalVisible(true)}
              style={{marginRight: 8}}
            >
              签收
            </Button>
            <Button
              disabled={!['0', '1'].includes(selectedRowStatus.toString())}
              type="primary"
              onClick={this.cancelOrder}
              style={{marginRight: 8}}
            >
              取消订单
            </Button>
          </Col>
          <Col md={12} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  expandedRowRender = record => {
    const { goodsDataSource } = this.state;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        render: (text, record, index) => index+1
      },
      {
        title: '商品编号',
        dataIndex: 'stockNum',
      },
      {
        title: '商品名称',
        dataIndex: 'name',
      },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '零售价(¥)',
        dataIndex: 'referencePrice',
      },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      // },
      {
        title: '金额(¥)',
        dataIndex: 'totalPrice',
        // render: (text, record) => parseFloat(record.referencePrice) * parseInt(record.number)
        // render: (text, record) => NP.times(record.referencePrice, parseInt(record.number))
      },
      {
        title: '批次',
        dataIndex: 'batchNumber',
      },
    ];

    return <Table
      // size='small'
      columns={columns}
      dataSource={goodsDataSource}
      // footer={() => `合计: ${purchaseingInventoryList.length}`}
      rowKey='salesOrderGoodsId'
      title={() => <h2>商品明细</h2>}
    />
  };

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const salesOrderId = record.salesOrderId;
      dispatch({
        type: 'offLineDelivery/fetchSaleOrderDetails',
        payload: {
          salesOrderId
        },
        callback: (data) => {
          console.log('data: ', data);
          this.setState({ goodsDataSource: data })
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  handleAdd = fields => {
    const { dispatch, form } = this.props;
    const { selectedRows } = this.state;

    dispatch({
      type: 'offLineDelivery/delivery',
      payload: {
        ...fields,
        salesOrderId: selectedRows[0].salesOrderId,
      },
      callback:() => {
        //更新dataSource
        dispatch({
          type: 'offLineDelivery/fetchPhoneOrderDeliveryList',
          payload: {
            ...form.getFieldsValue(),
            type: 1
          },
          callback: this.refreshSelectedRows
        })
      }
    })

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleReceivableAdd = fields => {
    const { dispatch, form } = this.props;
    const { selectedRows } = this.state;

    dispatch({
      type: 'offLineDelivery/receivables',
      payload: {
        ...fields,
        salesOrderId: selectedRows[0].salesOrderId,
        // receivableAmount: record.receivableAmount,
        receivedAmount: this.getReceivedAmount(fields),
        changeAmount: 0
      },
      // callback: this.refreshSelectedRows,
      callback:() => {
        //更新dataSource
        dispatch({
          type: 'offLineDelivery/fetchPhoneOrderDeliveryList',
          payload: {
            ...form.getFieldsValue(),
            type: 1
          },
          callback: this.refreshSelectedRows
        })
      }
    })

    // message.success('添加成功');
    this.handleReceivableModalVisible();
  };

  cancelOrder = () => {
    const { dispatch, form } = this.props;
    const { selectedRows } = this.state;

    dispatch({
      type: 'offLineDelivery/cancelOrder',
      payload: {
        salesOrderId: selectedRows[0].salesOrderId,
      },
      // callback: this.refreshSelectedRows,
      callback:() => {
        //更新dataSource
        dispatch({
          type: 'offLineDelivery/fetchPhoneOrderDeliveryList',
          payload: {
            ...form.getFieldsValue(),
            type: 1
          },
          callback: this.refreshSelectedRows
        })
      }
    })
  }

  refreshSelectedRows = (orderList) => {
    const { selectedRows } = this.state;
    let selectedNewRows = [];
    if (selectedRows.length > 0) {
      const target = orderList.rows.find(item => item.salesOrderId === selectedRows[0].salesOrderId);
      selectedNewRows = target ? [target] : [];
    }
    this.setState({ selectedRows:selectedNewRows })
  }

  handleModalVisible = (flag, record) => {
    this.setState({
      modalVisible: !!flag,
      // record: flag ? record : {},
    });
  };

  handleReceivableModalVisible = (flag, record) => {
    this.setState({
      modalReceivableVisible: !!flag,
      // record: flag ? record : {},
    });
  };

  onRowClick = (record) => {
    console.log('record: ', record);

    this.setState({ selectedRows: [record] })
  }

  render() {
    const {
      offLineDelivery: { saleOrderList, staffList },
      loading,
    } = this.props;
    const { expandedRowKeys, modalVisible, selectedRows, modalReceivableVisible, detailModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const receivableMethods = {
      handleAdd: this.handleReceivableAdd,
      handleModalVisible: this.handleReceivableModalVisible,
    };

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <StandardTable
              selectedRows={selectedRows}
              type='radio'
              loading={loading}
              data={saleOrderList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="salesOrderId"
              // scroll={{x: 1300}}
              size='small'
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
              onRow={(record) => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
            />
          </div>
        </Card>
        <DeliveryForm {...parentMethods} modalVisible={modalVisible} staffList={staffList} record={selectedRows.length > 0 ? selectedRows[0] : {}} />
        <ReceivableForm {...receivableMethods} modalVisible={modalReceivableVisible} />
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
      </div>
    );
  }
}

export default DeliveryManagement;
