import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Popconfirm,
  Row,
  Col,
  InputNumber,
  Radio,
  message,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './SaleReturn.less';
import NP from 'number-precision';
import OffLineRefundOrderDetail from '@/customComponents/OffLineRefundOrderDetail';
import ReactToPrint from 'react-to-print';

const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
  state = {
    editing: false,
    searchBtnLoading: false,
  };

  componentDidMount() {
    if (this.props.editable) {
      document.addEventListener('click', this.handleClickOutside, true);
    }
  }

  componentWillUnmount() {
    if (this.props.editable) {
      document.removeEventListener('click', this.handleClickOutside, true);
    }
  }

  toggleEdit = () => {
    const editing = !this.state.editing;
    this.setState({ editing }, () => {
      if (editing) {
        this.input.focus();
      }
    });
  };

  handleClickOutside = e => {
    const { editing } = this.state;
    if (editing && this.cell !== e.target && !this.cell.contains(e.target)) {
      this.save();
    }
  };

  save = () => {
    const { record, handleSave } = this.props;
    this.form.validateFields((error, values) => {
      if (error) {
        return;
      }
      this.toggleEdit();
      handleSave({ ...record, ...values });
    });
  };

  render() {
    const { editing } = this.state;
    const { editable, dataIndex, title, record, index, handleSave, ...restProps } = this.props;
    return (
      <td ref={node => (this.cell = node)} {...restProps}>
        {editable ? (
          <EditableContext.Consumer>
            {form => {
              this.form = form;
              return editing ? (
                <FormItem style={{ margin: 0 }}>
                  {form.getFieldDecorator(dataIndex, {
                    rules: [
                      {
                        required: true,
                        message: `${title}需填.`,
                      },
                    ],
                    initialValue: record[dataIndex],
                  })(<Input ref={node => (this.input = node)} onPressEnter={this.save} />)}
                </FormItem>
              ) : (
                <div
                  className={styles['editable-cell-value-wrap']}
                  style={{ paddingRight: 24 }}
                  onClick={this.toggleEdit}
                >
                  {restProps.children}
                </div>
              );
            }}
          </EditableContext.Consumer>
        ) : (
          restProps.children
        )}
      </td>
    );
  }
}

const OrderDetail = Form.create()(props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      // width='60%'
      width={500}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={this.handleOk}
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{ marginLeft: 15 }}>
          <Row>
            <Col span={20}>退货</Col>
            <Col span={4}>
              <span style={{ float: 'right' }}>
                <ReactToPrint
                  trigger={() => (
                    <a href="#">
                      <Icon type="printer" />
                    </a>
                  )}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef;
                  }}
                />
              </span>
            </Col>
          </Row>
        </div>
      }
      // title={
      //   <div style={{marginLeft: 15}}>
      //     <Row>
      //       <Col span={20}>
      //         <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{record.storeName}</span>
      //       </Col>
      //       <Col span={4}>
      //         <span
      //           style={{float: 'right'}}
      //         >
      //           <ReactToPrint
      //             trigger={() => <a href="#"><Icon type="printer" /></a>}
      //             content={() => {
      //               console.log('componentRef: ', componentRef);
      //               return componentRef
      //             }}
      //           />
      //         </span>
      //       </Col>
      //     </Row>
      //     <Row>
      //       <Col span={24}>
      //         <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{`地址: ${record.storeAddress}`}</span>
      //       </Col>
      //     </Row>
      //   </div>
      // }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <OffLineRefundOrderDetail record={record} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ saleReturn, user, loading }) => ({
  saleReturn,
  user,
  loading: loading.models.saleReturn,
}))
@Form.create()
class SaleReturn extends PureComponent {
  state = {
    modalVisible: false,
    goodsModalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    record: {},
    refundAmount: 0,
    receivedAmount: '',
    order: {},
    detailModalVisible: false,
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index + 1,
    },
    {
      title: '商品编号',
      dataIndex: 'stockNum',
    },
    {
      title: '商品名称',
      dataIndex: 'name',
    },
    // {
    //   title: '单位',
    //   dataIndex: 'units',
    // },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '零售价(¥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '购买数量',
      dataIndex: 'number',
    },
    {
      title: '购买总额(¥)',
      dataIndex: 'totalPrice',
      // render: (text, record) => parseFloat(record.referencePrice) * parseInt(record.number),
      // render: (text, record) => ((record.referencePrice*100) * parseInt(record.number))/100,
      // render: (text, record) => NP.times(record.referencePrice, parseInt(record.number))
    },
    {
      title: '已退货数量',
      dataIndex: 'returnGoodsNumber',
    },
    {
      title: '退货数量',
      dataIndex: 'returnNumber',
      editable: true, //先退所有，上线后再优化
      width: 100,
    },
    {
      title: '退货总额(¥)',
      dataIndex: 'returnTotalPrice',
      // dataIndex: '',
      // render: (text, record) => NP.minus(record.returnNumber, record.number) === 0  ? record.totalPrice : NP.times(record.referencePrice, parseInt(record.returnNumber)),
      // render: (text, record) => NP.minus(record.returnNumber, record.number) === 0  ? record.totalPrice : NP.times(record.referencePrice, parseInt(record.returnNumber)),
    },
    {
      title: '操作',
      width: '8%',
      render: (text, record) => (
        <Fragment>
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.stockId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = stockId => {
    console.log('stockId: ', stockId);
    const dataSource = [...this.state.dataSource];
    const newDataSource = dataSource.filter(item => item.stockId !== stockId);
    let refundAmount = 0;
    if (newDataSource.length > 0) {
      refundAmount = newDataSource
        // .map(item => NP.times(item.returnNumber, item.referencePrice))
        .map(item => item.returnTotalPrice)
        .reduce((a, b) => NP.plus(a, b)); //初始化应收金额
      // .reduce((a, b) => a + b); //初始化应收金额
    }
    this.setState({ dataSource: newDataSource, refundAmount });
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
      form,
    } = this.props;

    const personnel = currentUser.personnel;
    if (personnel) {
      //获取当前部门员工列表
      dispatch({
        type: 'saleReturn/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });

      //获取仓库列表
      dispatch({
        type: 'saleReturn/fetchWarehouseList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //盘点单列表跳转过来
    console.log('this.props: ', this.props);
    const { query } = this.props.location;
    if (query.orderNumber) {
      form.setFieldsValue({ orderNumber: query.orderNumber });
      this.handleSearch();
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'saleReturn/fetch',
      payload: params,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { dataSource, refundAmount, order } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请先查询商品!');
        return;
      }

      fieldsValue.salesOrderId = order.salesOrderId;
      fieldsValue.goodsList = JSON.stringify([
        ...dataSource.map(item => ({ ...item, number: item.returnNumber, returnAmount: item.returnTotalPrice })),
      ]);
      // fieldsValue.refundMethod = 0;

      console.log('fieldsValue: ', fieldsValue);

      dispatch({
        type: 'saleReturn/returnGoods',
        payload: fieldsValue,
        callback: data => {
          console.log('returnGoods-data: ', data);
          form.resetFields();
          this.setState({ order: {}, dataSource: [], refundAmount: 0 });

          //展示打印详情页
          this.detail(fieldsValue.orderNumber);
        },
      });
    });
  };

  detail = orderNumber => {
    // const orderNumber = data.orderNumber;
    const { dispatch } = this.props;

    dispatch({
      type: 'saleReturn/fetchSaleReturnGoodsList',
      payload: { orderNumber },
      callback: data => {
        console.log('saleReturn-data: ', data);
        if (data.rows.length > 0) {
          const record = data.rows[0];
          this.handleDetailModalVisible(true, record);
        }
      },
    });
  };

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleSearch = e => {
    // e.preventDefault();

    const { form, dispatch } = this.props;
    this.setState({ searchBtnLoading: true });
    const orderNumber = form.getFieldsValue().orderNumber;
    dispatch({
      type: 'saleReturn/fetchSaleOrderList',
      payload: {
        orderNumber,
      },
      callback: data => {
        console.log('data-dj1: ', data);
        if (data.rows.length > 0) {
          this.setState({ order: data.rows[0] });
          const salesOrderId = data.rows[0].salesOrderId;
          dispatch({
            type: 'saleReturn/fetchSaleOrderDetails',
            payload: {
              salesOrderId,
            },
            callback: data => {
              console.log('data-dj2: ', data);

              //二、初始化应收金额
              const refundAmount = data
                .map(item =>
                  this.getReturnTotalPrice({ ...item, returnNumber: NP.minus(item.number, item.returnGoodsNumber)})
                  // item.returnGoodsNumber === 0 ? item.totalPrice : NP.times(item.referencePrice, NP.minus(item.number, item.returnGoodsNumber))
                  // NP.times(item.referencePrice, NP.minus(item.number, item.returnGoodsNumber))
                )
                .reduce((a, b) => NP.plus(a, b));
              // .reduce((a, b) => a + b);

              let returnTotalPrice = 0;

              this.setState({
                // dataSource: data.map(item => ({ ...item, returnNumber: item.number - item.returnGoodsNumber, returnTotalPrice: item.totalPrice })),
                dataSource: data.map(item => ({
                  ...item,
                  returnNumber: NP.minus(item.number, item.returnGoodsNumber),
                  // returnTotalPrice: item.totalPrice,
                  returnTotalPrice: this.getReturnTotalPrice({ ...item, returnNumber: NP.minus(item.number, item.returnGoodsNumber)}),
                  // returnTotalPrice: NP.times(
                  //   item.referencePrice,
                  //   NP.minus(item.number, item.returnGoodsNumber)
                  // ),
                })),
                refundAmount,
              });
            },
          });
        } else {
          message.error('无该订单!');
        }

        this.setState({ searchBtnLoading: false });
      },
    });
  };

  getReturnTotalPrice = (row) => {
    let returnTotalPrice = 0;
    // 三种情况
    if (NP.minus(parseInt(row.returnNumber), row.number) === 0) { // 1、全退，则销售时输入多少就退多少
      returnTotalPrice = row.totalPrice
    }
    else if (parseInt(row.returnNumber) !== NP.minus(row.number, row.returnGoodsNumber)) { // 2、部分退货，但没退完，则按数量*单价来退
      returnTotalPrice = NP.times(row.referencePrice, parseInt(row.returnNumber))
    }
    else { // 3、最后一次退完时，按销售额-单价*(总数-退货数)
      returnTotalPrice = NP.minus(row.totalPrice, NP.times(row.referencePrice, (NP.minus(row.number, parseInt(row.returnNumber)))))
    }
    return returnTotalPrice;
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ order: {}, dataSource: [], refundAmount: 0, receivedAmount: 0 });
  };

  handleFormPhoneReset = () => {
    const { form } = this.props;
    form.setFieldsValue({ orderNumber: '' });
    this.setState({ order: {}, dataSource: [] });
  };

  onKeyup = e => {
    if (e.keyCode === 13) {
      this.handleSearch();
    }
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { order, searchBtnLoading } = this.state;

    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>订单编号</span>}>
              {getFieldDecorator('orderNumber')(
                <Input
                  addonAfter={
                    <a onClick={this.handleFormPhoneReset}>
                      <Icon type="close-circle" />
                    </a>
                  }
                  placeholder="请输入订单编号"
                  onKeyUp={this.onKeyup}
                />
              )}
            </FormItem>
          </Col>
          <Col md={4} sm={24}>
            <span className={styles.submitButtons}>
              <Button
                onClick={this.handleSearch}
                type="primary"
                disabled={!this.props.form.getFieldsValue().orderNumber}
                loading={searchBtnLoading}
              >
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="购买时间">{order.createTime}</FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="会员电话">{order.memberPhone}</FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="退货仓库">{order.warehouse}</FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={<span>退货入库批次</span>}>
              {getFieldDecorator('batchNumber', {
                rules: [{ required: true, message: '请输入退货入库批次' }],
                initialValue: '9999999999999999',
              })(<Input placeholder="请输入退货入库批次" disabled />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="退货原因">
              {getFieldDecorator('reason')(
                <Input style={{ width: '100%' }} placeholder="请输入退货原因" />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  getReceivedAmount = () => {
    const {
      form: { getFieldsValue },
    } = this.props;
    const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = getFieldsValue();
    let receivedAmount = 0;
    if (cashPayment) {
      receivedAmount += cashPayment;
    }
    if (alipayPayment) {
      receivedAmount += alipayPayment;
    }
    if (weChatPayment) {
      receivedAmount += weChatPayment;
    }
    if (bankCardPayment) {
      receivedAmount += bankCardPayment;
    }
    return receivedAmount;
  };

  changeAmount = () => {
    const { refundAmount } = this.state; //应收金额
    const receivedAmount = this.getReceivedAmount(); //实收金额
    let changeAmount = 0;
    if (receivedAmount > refundAmount) {
      changeAmount = NP.minus(receivedAmount, refundAmount);
    }
    console.log('receivedAmount: ', receivedAmount);
    console.log('refundAmount: ', refundAmount);
    console.log('changeAmount: ', changeAmount);
    return changeAmount;
  };

  renderBottomForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { refundAmount, order } = this.state;
    return (
      <Form onSubmit={this.handleSubmit} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={20}>
            <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
              <Col span={12}>
                <FormItem label="退货方式">
                  {getFieldDecorator('refundMethod', {
                    initialValue: 0,
                  })(
                    <RadioGroup>
                      <Radio value={0}>现金</Radio>
                      <Radio value={1}>支付宝</Radio>
                      <Radio value={2}>微信</Radio>
                      <Radio value={3}>银行卡</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>
              {/*<Col md={5} sm={24}>*/}
              {/*<FormItem label="现金">*/}
              {/*{getFieldDecorator('cashPayment')(*/}
              {/*<InputNumber*/}
              {/*precision={2}*/}
              {/*style={{ width: '100%' }}*/}
              {/*placeholder="请输入"*/}
              {/*min={0}*/}
              {/*/>*/}
              {/*)}*/}
              {/*</FormItem>*/}
              {/*</Col>*/}
              {/*<Col md={5} sm={24}>*/}
              {/*<FormItem label="支付宝">*/}
              {/*{getFieldDecorator('alipayPayment')(*/}
              {/*<InputNumber*/}
              {/*precision={2}*/}
              {/*style={{ width: '100%' }}*/}
              {/*placeholder="请输入"*/}
              {/*min={0}*/}
              {/*/>*/}
              {/*)}*/}
              {/*</FormItem>*/}
              {/*</Col>*/}
              <Col md={8} sm={24}>
                <FormItem label="应退金额">¥ {refundAmount}</FormItem>
              </Col>
              {/*<Col md={5} sm={24}>*/}
              {/*<FormItem label="找零">*/}
              {/*{this.changeAmount()}*/}
              {/*</FormItem>*/}
              {/*</Col>*/}
            </Row>
            <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
              <Col md={12} sm={24}>
                <FormItem label="实退金额">
                  {getFieldDecorator('refundAmount', {
                    initialValue: refundAmount,
                  })(
                    <InputNumber
                      precision={2}
                      style={{ width: '100%' }}
                      placeholder="请输入"
                      min={0}
                    />
                  )}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label="备注">
                  {getFieldDecorator('remark')(
                    <Input style={{ width: '100%' }} placeholder="请输入" />
                  )}
                </FormItem>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <Button
              htmlType="submit"
              style={{ height: 89, width: '100%', fontSize: 30 }}
              type="primary"
              disabled={!order.salesOrderId}
            >
              退货
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }

  handleSave = row => {
    const canReturnNumber = row.number - row.returnGoodsNumber;
    if (row.returnNumber > canReturnNumber) {
      message.warning(`退货数量不能大于${canReturnNumber}!`);
      return;
    }

    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.stockId === item.stockId);
    const item = newData[index];

    //改变商品退货数量时，刷新商品退货总额
    console.log('newData: ', newData);
    console.log('row: ', row);
    console.log('item: ', item);
    if (row.returnNumber !== item.returnNumber) {
      let returnTotalPrice = this.getReturnTotalPrice(row);

      // 三种情况
      // if (NP.minus(parseInt(row.returnNumber), row.number) === 0) { // 1、全退，则销售时输入多少就退多少
      //   returnTotalPrice = row.totalPrice
      // }
      // else if (parseInt(row.returnNumber) !== NP.minus(row.number, row.returnGoodsNumber)) { // 2、部分退货，但没退完，则按数量*单价来退
      //   returnTotalPrice = NP.times(row.referencePrice, parseInt(row.returnNumber))
      // }
      // else { // 3、最后一次退完时，按销售额-单价*(总数-退货数)
      //   returnTotalPrice = NP.minus(row.totalPrice, NP.times(row.referencePrice, (NP.minus(row.number, parseInt(row.returnNumber)))))
      //   console.log('returnTotalPrice: ', returnTotalPrice);
      //   return;
      // }
      row = { ...row, returnTotalPrice };
    }

    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    //刷新应退金额
    console.log('newData-25: ', newData);
    const refundAmount = newData
      // .map(item => NP.times(item.returnNumber, item.referencePrice))
      .map(item => item.returnTotalPrice)
      .reduce((a, b) => NP.plus(a, b));
    console.log('refundAmount-25: ', refundAmount);
    this.setState({ dataSource: newData, refundAmount });
  };

  render() {
    const { loading } = this.props;
    const { dataSource, detailModalVisible, record } = this.state;

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    console.log('dataSource-render: ', dataSource);

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.title}>商品明细</div>
            <StandardTable
              components={components}
              loading={loading}
              data={{ rows: dataSource }}
              columns={columns}
              onChange={this.handleStandardTableChange}
              rowKey="stockId"
              rowClassName={() => styles['editable-row']}
              size="small"
            />
            <div className={styles.title} style={{ marginTop: 24 }}>
              支付
            </div>
            <div className={styles.tableListForm} style={{ marginTop: 24 }}>
              {this.renderBottomForm()}
            </div>
          </div>
        </Card>
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record} />
      </div>
    );
  }
}

export default SaleReturn;
