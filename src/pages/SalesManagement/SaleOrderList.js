import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Divider,
  Popconfirm,
  Row,
  Col,
  DatePicker,
  Table,
  Popover,
  InputNumber,
  message,
  Dropdown,
  Menu,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated } from '@/services/brand';
import moment from 'moment';
import styles from './SaleOrderList.less';
import StoreGainDetail from '../AppManagement/OrderManagement/StoreGainDetail';
import OffLineSaleOrderDetail from '@/customComponents/OffLineSaleOrderDetail';
import OffLineSaleOrderDetailAfterRefund from '@/customComponents/OffLineSaleOrderDetailAfterRefund';
import ReactToPrint from 'react-to-print';
import { apiDomainName, imgDomainName } from '../../constants';
import { stringify } from 'qs';
import router from 'umi/router';
import NP from 'number-precision';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const ReceivableForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = fieldsValue;
      if (!cashPayment && !alipayPayment && !weChatPayment && !bankCardPayment) {
        message.warning('请输入收款金额');
        return;
      }

      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="收款"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="现金">
        {form.getFieldDecorator('cashPayment')(
          <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="支付宝">
        {form.getFieldDecorator('alipayPayment')(
          <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="微信">
        {form.getFieldDecorator('weChatPayment')(
          <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="银行卡">
        {form.getFieldDecorator('bankCardPayment')(
          <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
        )}
      </FormItem>
    </Modal>
  );
});

const OrderDetail = Form.create()(props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      // width='60%'
      width={500}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={this.handleOk}
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{ marginLeft: 15 }}>
          <Row>
            <Col span={20}>
              <span style={{ color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900 }}>
                {record.storeName}
              </span>
            </Col>
            <Col span={4}>
              <span style={{ float: 'right' }}>
                <ReactToPrint
                  trigger={() => (
                    <a href="#">
                      <Icon type="printer" />
                    </a>
                  )}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef;
                  }}
                />
              </span>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <span style={{ color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900 }}>{`地址: ${
                record.storeAddress
              }`}</span>
            </Col>
          </Row>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <OffLineSaleOrderDetail record={record} />
    </Modal>
  );
});

const OrderDetailAfterRefund = Form.create()(props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      // width='60%'
      width={500}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={this.handleOk}
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{ marginLeft: 15 }}>
          <Row>
            <Col span={20}>
              <span style={{ color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900 }}>
                {record.storeName}
              </span>
            </Col>
            <Col span={4}>
              <span style={{ float: 'right' }}>
                <ReactToPrint
                  trigger={() => (
                    <a href="#">
                      <Icon type="printer" />
                    </a>
                  )}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef;
                  }}
                />
              </span>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <span style={{ color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900 }}>{`地址: ${
                record.storeAddress
              }`}</span>
            </Col>
          </Row>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <OffLineSaleOrderDetailAfterRefund record={record} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ offLineSaleOrder, user, loading }) => ({
  offLineSaleOrder,
  user,
  loading: loading.models.offLineSaleOrder,
}))
@Form.create()
class SaleOrderList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    detailModalVisible: false,
    detailAfterRefundModalVisible: false,
    // visible: [false, false, false],
    selectedRows: [],
    expandedRowKeys: [],
    goodsDataSource: [],
    formValues: {},
    record: {},
    dataPermission: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
      // fixed: 'left',
      width: 120,
      render: (text, record) => this.getPayDetail(text, record),
    },
    {
      title: '订单类型',
      dataIndex: 'type',
      render: text => {
        let result = '';
        switch (text) {
          case 0:
            result = '门店销售';
            break;
          case 1:
            result = '电话销售';
            break;
          case 2:
            result = '批发销售';
            break;
        }
        return result;
      },
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      render: (text, record) => {
        let result = <span style={{ fontWeight: 'bold' }}>无配送</span>;
        if (record.type === 1) {
          switch (text.toString()) {
            case '0':
              result = <span style={{ color: '#f5222d', fontWeight: 'bold' }}>未配送</span>;
              break;
            case '1':
              // result = '待收货/待自取';
              result = <span style={{ color: '#1890ff', fontWeight: 'bold' }}>配送中</span>;
              break;
            case '2':
              result = <span style={{ color: '#52c41a', fontWeight: 'bold' }}>已签收</span>;
              break;
            case '3':
              result = <span style={{ color: '#faad14', fontWeight: 'bold' }}>已取消</span>;
              break;
          }
        }
        return result;
      },
    },
    {
      title: '退货状态',
      dataIndex: 'isReturnGoods',
      render: text => (
        <span>
          {text ? (
            <span style={{ color: '#f5222d', fontWeight: 'bold' }}>已退货</span>
          ) : (
            <span style={{ fontWeight: 'bold' }}>未退货</span>
          )}
        </span>
      ),
    },
    {
      title: '下单门店',
      dataIndex: 'storeName',
    },
    {
      title: '订购时间',
      dataIndex: 'createTime',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '销售员',
      dataIndex: 'salesmanName',
    },
    {
      title: '会员电话',
      dataIndex: 'memberPhone',
    },
    {
      title: '应收款(¥)',
      dataIndex: 'receivableAmount',
    },
    {
      title: '实收款(¥)',
      dataIndex: 'receivedAmount',
    },
    {
      title: '找零(¥)',
      dataIndex: 'changeAmount',
    },
    {
      title: '收款状态',
      dataIndex: '',
      // render: (text, record) => record.type === 2 && record.receivedAmount === 0 ? '未收' : '已收'
      render: (text, record) => (record.receivedAmount === 0 ? '未收' : '已收'),
    },
    {
      title: '可先提货',
      dataIndex: 'isTakeDeliveryFirst',
      render: (text, record) => {
        if (record.type === 2) {
          return text ? (
            <Icon type="check-circle" theme="twoTone" />
          ) : (
            <img
              style={{ width: 14, height: 14 }}
              src={`${imgDomainName}alcohol/1903131439581480.png`}
            />
          );
        } else {
          return null;
        }
      },
    },
    {
      title: '已退款',
      dataIndex: 'returnAmount',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 60,
      // fixed: 'right',
      // render: (text, record, index) => record.receivedAmount === 0 ? this.operation(text, record, index) : '',
      render: (text, record) => (
        <Dropdown
          overlay={
            <Menu onClick={e => e.domEvent.stopPropagation()}>
              <Menu.Item
              // disabled={!(record.isSubmit && !record.isPutIn)}
              // style={{
              //   textAlign: 'center',
              //   display: elementPermissionList.includes('生成盈亏单') ? 'block' : 'none',
              // }}
              >
                <a
                  onClick={() => {
                    console.log('record: ', record);
                    if (record.isReturnGoods) {
                      message.warning('已退货！');
                      return;
                    }
                    if (record.type === 1 && record.status !== 2) {
                      message.warning('签收后才可退货！');
                      return;
                    }
                    if ((record.type = 2 && record.receivedAmount === 0)) {
                      message.warning('收款后才可退货！');
                      return;
                    }

                    router.push({
                      pathname: '/sales-management/sale-return',
                      query: {
                        orderNumber: record.orderNumber,
                      },
                    });
                  }}
                >
                  退货
                </a>
              </Menu.Item>
              <Menu.Item
                // disabled={!(record.isSubmit && !record.isPutIn)}
                style={{
                  display:
                    record.receivedAmount === 0 && record.type === 2 ? 'inline-block' : 'none',
                }}
              >
                <a onClick={() => this.handleModalVisible(true, record)}>收款</a>
              </Menu.Item>
              <Menu.Item>
                <a onClick={() => this.handleDetailAfterRefundModalVisible(true, record)}>详情(退货后)</a>
              </Menu.Item>
            </Menu>
          }
          placement="bottomRight"
        >
          <a className="ant-dropdown-link" href="#">
            操作 <Icon type="down" />
          </a>
        </Dropdown>

        // <Fragment>
        //   <div style={{display: record.receivedAmount === 0 && record.type === 2 ? 'inline-block' : 'none'}}>
        //     <a onClick={() => this.handleModalVisible(true, record)}>收款</a>
        //     <Divider type="vertical" />
        //   </div>
        //   <a onClick={() => this.handleModalVisible(true, record)}>退货</a>
        // </Fragment>

        // record.receivedAmount === 0 && record.type === 2 ?
        // <Fragment>
        //   <div style={{display: record.receivedAmount === 0 && record.type === 2 ? 'inline-block' : 'none'}}>
        //     <a onClick={() => this.handleModalVisible(true, record)}>收款</a>
        //     <Divider type="vertical" />
        //   </div>
        //   <a onClick={() => this.handleModalVisible(true, record)}>退货</a>
        // </Fragment> : ''
      ),
    },
  ];

  handleSubmit = (record, e) => {
    e.preventDefault();
    const { form, dispatch } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = fieldsValue;
      if (!cashPayment && !alipayPayment && !weChatPayment && !bankCardPayment) {
        message.warning('请输入收款金额');
        return;
      }

      dispatch({
        type: 'offLineSaleOrder/receivables',
        payload: {
          ...fieldsValue,
          salesOrderId: record.salesOrderId,
          receivableAmount: record.receivableAmount,
          receivedAmount: this.getReceivedAmount(),
        },
        // callback: () => form.resetFields()
      });
    });
  };

  operation = (text, record, index) => {
    const { form } = this.props;
    const content = (
      <Form
      // onSubmit={this.handleSubmit.bind(this, record)}
      >
        <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="现金">
          {form.getFieldDecorator('cashPayment')(
            <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="支付宝">
          {form.getFieldDecorator('alipayPayment')(
            <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="微信">
          {form.getFieldDecorator('weChatPayment')(
            <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="银行卡">
          {form.getFieldDecorator('bankCardPayment')(
            <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="111">
          {form.getFieldDecorator('bankCardPayment1')(
            <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入金额" min={0} />
          )}
        </FormItem>
        {/*<Button type="primary" htmlType="submit">*/}
        {/*收款*/}
        {/*</Button>*/}
      </Form>
    );
    return (
      <Fragment>
        <Popover content={content} title="收款" trigger="click" placement="left">
          <a href="#">收款</a>
        </Popover>
      </Fragment>
    );
  };

  getReceivedAmount = fields => {
    const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = fields;
    let receivedAmount = 0;
    if (cashPayment) {
      receivedAmount = NP.plus(receivedAmount, cashPayment);
    }
    if (alipayPayment) {
      receivedAmount = NP.plus(receivedAmount, alipayPayment);
    }
    if (weChatPayment) {
      receivedAmount = NP.plus(receivedAmount, weChatPayment);
    }
    if (bankCardPayment) {
      receivedAmount = NP.plus(receivedAmount, bankCardPayment);
    }
    return receivedAmount;
  };

  getPayDetail = (text, record) => {
    const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = record;
    let dataSource = [];
    if (cashPayment) {
      dataSource.push({ payMethod: '现金支付', payAmount: cashPayment, key: dataSource.length });
    }
    if (alipayPayment) {
      dataSource.push({
        payMethod: '支付宝支付',
        payAmount: alipayPayment,
        key: dataSource.length,
      });
    }
    if (weChatPayment) {
      dataSource.push({ payMethod: '微信支付', payAmount: weChatPayment, key: dataSource.length });
    }
    if (bankCardPayment) {
      dataSource.push({
        payMethod: '银行卡支付',
        payAmount: bankCardPayment,
        key: dataSource.length,
      });
    }

    const columns = [
      {
        title: '支付方式',
        dataIndex: 'payMethod',
      },
      {
        title: '支付金额(¥)',
        dataIndex: 'payAmount',
      },
    ];
    const content = (
      <Table
        rowKey="key"
        columns={columns}
        dataSource={dataSource}
        pagination={false}
        size="small"
      />
    );
    return (
      <Popover content={content} title="支付明细">
        <a onClick={() => this.handleDetailModalVisible(true, record)}>{text}</a>
      </Popover>
    );
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;

    //数据权限:判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });

      //销售订单列表
      dispatch({
        type: 'offLineSaleOrder/fetchSaleOrderList',
        // payload: {
        //   page: 1,
        //   size: 10,
        // }
      });
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      // const storeId =  currentUser.personnel.storeId;
      //2、判断是否是门店员工
      // if (storeId || storeId === 1) {
      //   console.log('是门店员工')
      //
      // }
      // else{
      //   console.log('不是门店员工')
      // }

      const personnel = currentUser.personnel;
      if (personnel) {
        //获取当前部门员工列表
        // dispatch({
        //   type: 'offLineSaleOrder/fetchStaffList',
        //   payload: {
        //     branchId: personnel.branchId,
        //   },
        // });

        //获取仓库列表
        dispatch({
          type: 'offLineSaleOrder/fetchWarehouseList',
          payload: {
            branchId: personnel.branchId,
          },
        });
      }

      //销售订单列表
      dispatch({
        type: 'offLineSaleOrder/fetchSaleOrderList',
        payload: {
          branchId: currentUser.personnel.branchId,
          // page: 1,
          // size: 10,
        },
      });
    }

    //部门列表
    dispatch({
      type: 'offLineSaleOrder/fetchBranchList',
      payload: {
        isWarehouse: true,
        isStore: true,
      },
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'offLineSaleOrder/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (fieldsValue.time) {
        fieldsValue.startTime = moment(fieldsValue.time[0]).format('YYYY-MM-DD HH:mm');
        fieldsValue.endTime = moment(fieldsValue.time[1]).format('YYYY-MM-DD HH:mm');
        delete fieldsValue.time;
      }

      dispatch({
        type: 'offLineSaleOrder/fetchSaleOrderList',
        payload: fieldsValue,
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();

    dispatch({
      type: 'offLineSaleOrder/fetchSaleOrderList',
      payload: {
        ...form.getFieldsValue(),
      },
    });
  };

  onBranchChange = branchId => {
    const { dispatch, form } = this.props;
    console.log('branchId: ', branchId);

    //出库仓库列表
    dispatch({
      type: 'offLineSaleOrder/fetchWarehouseList',
      payload: {
        branchId,
      },
    });

    form.setFieldsValue({ warehouseId: '' });
  };

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (!fieldsValue.time && !fieldsValue.branchId && !fieldsValue.warehouseId) {
        message.warning('请先选择订购时间、下单门店、出库仓库(一个即可)');
        return;
      }

      if (fieldsValue.time) {
        fieldsValue.startTime = moment(fieldsValue.time[0]).format('YYYY-MM-DD HH:mm');
        fieldsValue.endTime = moment(fieldsValue.time[1]).format('YYYY-MM-DD HH:mm');
        delete fieldsValue.time;
      }
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId,
      };
      console.log('fieldsValue: ', fieldsValue);
      // window.location.href = `${apiDomainName}/server/salesOrder/exportList?${stringify(params)}`;
      window.open(`${apiDomainName}/server/salesOrder/exportList?${stringify(params)}`);
    });
  };

  renderSimpleForm() {
    const {
      offLineSaleOrder: { warehouseList, branchList },
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;

    const { dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="订单编号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单编号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="下单门店">
              {getFieldDecorator('branchId', {
                initialValue: dataPermission ? '' : currentUser.personnel.branchId,
              })(
                <Select
                  disabled={!dataPermission}
                  onChange={this.onBranchChange}
                  placeholder="请选择下单门店"
                  style={{ width: '100%' }}
                >
                  {branchList.rows.map(item => (
                    <Option value={item.branchId}>{item.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库仓库">
              {getFieldDecorator('warehouseId')(
                <Select placeholder="请选择出库仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => (
                    <Select.Option key={item.warehouseId} value={item.warehouseId}>
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={10} sm={24}>
            <FormItem label="订购时间">
              {getFieldDecorator('time')(
                <RangePicker
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder={['开始时间', '结束时间']}
                  style={{ width: '100%' }}
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="订单类型">
              {getFieldDecorator('type')(
                <Select placeholder="请选择订单类型" style={{ width: '100%' }}>
                  <Option value={0}>门店销售</Option>
                  <Option value={1}>电话销售</Option>
                  <Option value={2}>批发销售</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  expandedRowRender = record => {
    const { goodsDataSource } = this.state;

    const goodsColumns = [
      {
        title: '序号',
        dataIndex: '',
        render: (text, record, index) => index + 1,
      },
      {
        title: '商品编号',
        dataIndex: 'stockNum',
      },
      {
        title: '商品名称',
        dataIndex: 'name',
      },
      // {
      //   title: '单位',
      //   dataIndex: 'units',
      // },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '零售价(¥)',
        dataIndex: 'referencePrice',
      },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      // },
      {
        title: '金额(¥)',
        dataIndex: 'totalPrice',
        // render: (text, record) => parseFloat(record.referencePrice) * parseInt(record.number)
        // render: (text, record) => ((record.referencePrice*100) * parseInt(record.number))/100
        // render: (text, record) => NP.times(record.referencePrice, parseInt(record.number))
      },
      {
        title: '批次',
        dataIndex: 'batchNumber',
      },
      {
        title: '已退货数量',
        dataIndex: 'returnGoodsNumber',
      },
    ];

    const deliveryColumns = [
      {
        title: '订单编号',
        dataIndex: 'orderNumber',
        // fixed: 'left',
        width: 150,
        render: (text, record) => this.getPayDetail(text, record),
      },
      {
        title: '订单状态',
        dataIndex: 'status',
        render: text => {
          let result = '';
          switch (text) {
            case 0:
              result = '未配送';
              break;
            case 1:
              result = '配送中';
              break;
            case 2:
              result = '已签收';
              break;
            case 3:
              result = '已取消';
              break;
          }
          return result;
        },
      },
      {
        title: '订购时间',
        dataIndex: 'createTime',
      },
      {
        title: '会员电话',
        dataIndex: 'memberPhone',
      },
      {
        title: '收货人',
        dataIndex: 'consignee',
      },
      {
        title: '收货人电话',
        dataIndex: 'phone',
      },
      {
        title: '应收款(¥)',
        dataIndex: 'receivableAmount',
      },
      {
        title: '实收款(¥)',
        dataIndex: 'receivedAmount',
      },
      {
        title: '防伪码',
        dataIndex: 'securityCode',
      },
      {
        title: '配送员',
        dataIndex: 'distributionName',
      },
      {
        title: '配送地址',
        dataIndex: 'detailedAddress',
      },
      {
        title: '备注',
        dataIndex: 'remark',
      },
    ];

    return (
      <div>
        <Table
          size="small"
          columns={goodsColumns}
          dataSource={goodsDataSource}
          // footer={() => `合计: ${purchaseingInventoryList.length}`}
          rowKey="salesOrderGoodsId"
          title={() => <h2>商品明细</h2>}
        />
        <Table
          size="small"
          columns={deliveryColumns}
          dataSource={[record]}
          // footer={() => `合计: ${purchaseingInventoryList.length}`}
          rowKey="salesOrderId"
          title={() => <h2>配送信息</h2>}
          pagination={false}
          style={{ display: record.type.toString() === '1' ? 'block' : 'none' }}
        />
      </div>
    );
  };

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const salesOrderId = record.salesOrderId;
      //商品明细
      dispatch({
        type: 'offLineSaleOrder/fetchSaleOrderDetails',
        payload: {
          salesOrderId,
        },
        callback: data => {
          console.log('data: ', data);
          this.setState({ goodsDataSource: data });
        },
      });

      //配送信息
    }
  };

  onExpandedRowsChange = expandedRows => {
    this.setState({
      expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length - 1]] : [],
    });
  };

  handleAdd = fields => {
    const { dispatch, form } = this.props;
    const { record } = this.state;

    dispatch({
      type: 'offLineSaleOrder/receivables',
      payload: {
        ...fields,
        salesOrderId: record.salesOrderId,
        receivableAmount: record.receivableAmount,
        receivedAmount: this.getReceivedAmount(fields),
      },
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleModalVisible = (flag, record) => {
    this.setState({
      modalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetailAfterRefundModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailAfterRefundModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  render() {
    const {
      offLineSaleOrder: { saleOrderList },
      loading,
    } = this.props;
    const { expandedRowKeys, modalVisible, detailModalVisible, detailAfterRefundModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    const detailAfterRefundMethods = {
      handleDetailModalVisible: this.handleDetailAfterRefundModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <StandardTable
              // selectedRows={selectedRows}
              loading={loading}
              data={saleOrderList}
              columns={this.columns}
              // onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="salesOrderId"
              // scroll={{x: 1300}}
              size="small"
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
              footer={() => `合计: ${saleOrderList.rows.length}`}
              // pagination={{ total:  }}
            />
          </div>
        </Card>
        <ReceivableForm {...parentMethods} modalVisible={modalVisible} />
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record} />
        <OrderDetailAfterRefund {...detailAfterRefundMethods} detailModalVisible={detailAfterRefundModalVisible} record={record} />
      </div>
    );
  }
}

export default SaleOrderList;
