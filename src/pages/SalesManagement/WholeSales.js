import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Popconfirm,
  Row,
  Col,
  InputNumber,
  Radio,
  message,
  Checkbox,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import InventoryList from '@/customComponents/InventoryList';
import styles from './WholeSales.less';
import { isVipNameRepeated, fetchVipList } from '@/services/customerRelations';
import EditableCell from '@/customComponents/EditableCell';
import { fetchWarehouseStockBatchList } from '@/services/common-api';
import NP from 'number-precision';
import OffLineSaleOrderDetail from '@/customComponents/OffLineSaleOrderDetail';
import ReactToPrint from 'react-to-print';
import moment from 'moment';

const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateVipForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, storeList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      fieldsValue.phone = fieldsValue.phone.replace(/\s*/g,"");
      handleAdd(fieldsValue);
    });
  };

  const checkPhone = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    const response = isVipNameRepeated({ phone: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的手机号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增会员"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="姓名">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }],
        })(<Input placeholder="请输入姓名" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="手机号">
        {form.getFieldDecorator('phone', {
          rules: [{ required: true, message: '请输入手机号！' }, { validator: checkPhone }],
        })(<Input placeholder="请输入手机号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="性别">
        {form.getFieldDecorator('gender', {
          rules: [{ required: true, message: '请选择性别！' }],
          initialValue: 1,
        })(
          <RadioGroup>
            <Radio value={1}>男</Radio>
            <Radio value={2}>女</Radio>
          </RadioGroup>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开卡店铺">
        {form.getFieldDecorator('regStore', {
          rules: [{ required: true, message: '请选择开卡店铺！' }],
          initialValue: storeList.rows.length > 0 ? storeList.rows[0].name : '',
        })(
          <Select style={{ width: '100%' }}>
            {storeList.rows.map(item => (
              <Select.Option value={item.name}>{item.name}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="积分余额">
        {form.getFieldDecorator('integral', {
          rules: [{ required: true, message: '请输入积分余额！' }],
          initialValue: 0,
        })(<InputNumber min={0} placeholder="请输入积分余额" style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="住址">
        {form.getFieldDecorator('address', {
          // rules: [
          //   { required: true, message: '请输入住址！' },
          // ],
        })(<Input placeholder="请输入住址" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const CreateGoodsForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  let wholeSalesAdd;
  const okHandle = () => {
    const { selectedRows } = wholeSalesAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = ref => {
    wholeSalesAdd = ref;
  };

  return (
    <Modal
      width={'90%'}
      destroyOnClose
      // title="新增商品"
      title={
        <div>
          <span>新增商品</span>
          <Button style={{ float: 'right', marginRight: 50 }} type="primary" onClick={okHandle}>
            确定
          </Button>
        </div>
      }
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{ top: 20 }}
    >
      <InventoryList wrappedComponentRef={addModalRef} />
    </Modal>
  );
});

const EditableFormRow = Form.create()(EditableRow);

const OrderDetail = Form.create()(props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      // width='60%'
      width={500}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={this.handleOk}
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{ marginLeft: 15 }}>
          <Row>
            <Col span={20}>
              <span style={{ color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900 }}>
                {record.storeName}
              </span>
            </Col>
            <Col span={4}>
              <span style={{ float: 'right' }}>
                <ReactToPrint
                  trigger={() => (
                    <a href="#">
                      <Icon type="printer" />
                    </a>
                  )}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef;
                  }}
                />
              </span>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <span style={{ color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900 }}>{`地址: ${
                record.storeAddress
              }`}</span>
            </Col>
          </Row>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <OffLineSaleOrderDetail record={record} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ wholeSales, storeSales, user, loading }) => ({
  wholeSales,
  storeSales,
  user,
  loading: loading.models.wholeSales,
  submitLoading: loading.effects['wholeSales/addStoreSaleOrder'],
}))
@Form.create()
class WholeSales extends PureComponent {
  state = {
    modalVisible: false,
    goodsModalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    record: {},
    vip: { memberPhone: '' },
    receivableAmount: 0,
    receivedAmount: '',
    warehouseStockBatchList: [],
    warehouseId: '',
    searchBtnLoading: false,
    detailModalVisible: false,
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      fixed: 'left',
      width: 50,
      render: (text, record, index) => index + 1,
    },
    {
      title: '商品编号',
      fixed: 'left',
      width: 90,
      dataIndex: 'stockNum',
    },
    {
      title: '商品名称',
      fixed: 'left',
      width: 150,
      dataIndex: 'name',
    },
    // {
    //   title: '单位',
    //   dataIndex: 'units',
    // },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 0,
      inputNumberMin: 1,
      width: 120,
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 2,
      inputNumberMin: 0,
      width: 120,
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '零售价(¥)',
      dataIndex: 'referencePrice',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 2,
      // inputNumberMin: 1,
      width: 150,
    },
    {
      title: '零售最低价 (￥)',
      dataIndex: 'lowerPrice',
    },
    {
      title: '零售最高价 (￥)',
      dataIndex: 'upperPrice',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    //   editable: true,
    //   width: 100,
    // },
    {
      title: '金额(¥)',
      // dataIndex: '',
      // render: (text, record) => ((record.referencePrice*100) * parseInt(record.number))/100
      // render: (text, record) => NP.times(record.referencePrice, parseInt(record.number))
      dataIndex: 'total',
      // render: (text, record) => NP.times(record.referencePrice, parseInt(record.number)),
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 2,
      // inputNumberMin: 1,
      width: 150,
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
      editable: true,
      isSelect: true,
      width: 200,
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <Popconfirm
            title="确认删除？"
            // onConfirm={() => this.recordRemove(record.stockId)}
            onConfirm={() => this.recordRemove(record.timestamp)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  // recordRemove = stockId => {
  recordRemove = timestamp => {
    // console.log('stockId: ', stockId);
    console.log('timestamp: ', timestamp);
    const dataSource = [...this.state.dataSource];
    const newDataSource = dataSource.filter(item => item.timestamp !== timestamp);
    let receivableAmount = 0;
    if (newDataSource.length > 0) {
      // receivableAmount = newDataSource.map(item => item.number * item.referencePrice).reduce((a, b) => a+b);//初始化应收金额
      receivableAmount = newDataSource
        .map(item => NP.times(item.number, item.referencePrice))
        .reduce((a, b) => NP.plus(a, b)); //初始化应收金额
    }
    this.setState({ dataSource: newDataSource, receivableAmount });
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;
    //获取会员列表
    // dispatch({
    //   type: 'wholeSales/fetchVipList',
    // });

    const personnel = currentUser.personnel;
    if (personnel) {
      //获取当前部门员工列表
      dispatch({
        type: 'wholeSales/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });

      //获取仓库列表
      dispatch({
        type: 'wholeSales/fetchWarehouseList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      //门店列表
      dispatch({
        type: 'storeSales/fetchStoreList',
      });
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      //门店列表
      dispatch({
        type: 'storeSales/fetchStoreList',
        payload: {
          name: currentUser.personnel.branch,
        },
      });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'wholeSales/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleGoodsModalVisible = flag => {
    this.setState({
      goodsModalVisible: !!flag,
    });
  };

  handleAdd = fields => {
    const { dispatch, form } = this.props;
    dispatch({
      type: 'wholeSales/addVip',
      payload: fields,
      // callback: () => this.handleModalVisible(true)
    });

    //用于初始化姓名和手机号
    this.setState({ vip: fields });
    form.setFieldsValue({ memberPhone: fields.phone });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleGoodsAdd = selectedRows => {
    this.setState(prevState => {
      //一、刷新不表格数据源
      let oldDataSource = [...prevState.dataSource];
      //  新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0) {
        for (let newItem of selectedRows) {
          let flag = true;
          //-------------该区域的代码作用是新增相同商品行数不变，列表不发生任何变化-------------
          // for (let oldItem of oldDataSource) {
          //   // if (newItem.stockId.toString() === oldItem.stockId.toString()) {
          //   if (newItem.timestamp.toString() === oldItem.timestamp.toString()) {
          //     // oldItem.number++;
          //     flag = false;
          //     break;
          //   }
          // }
          //-----------------------------------------------------------------------------
          if (flag) {
            // oldDataSource.push({ ...newItem, number: 1, number2: parseInt((1/newItem.unitRatio)*100)/100  })
            oldDataSource.push({
              ...newItem,
              number: 1,
              number2: parseInt((1 / newItem.unitRatio) * 100) / 100,
              timestamp: moment().unix() + newItem.stockId + '',
            });
          }
        }
      } else {
        // oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1, number2: parseInt((1/item.unitRatio)*100)/100  }))];
        oldDataSource = [
          ...selectedRows.map(item => ({
            ...item,
            number: 1,
            number2: parseInt((1 / item.unitRatio) * 100) / 100,
            timestamp: moment().unix() + item.stockId + '',
          })),
        ];
      }

      oldDataSource = oldDataSource.map(item => ({
        ...item,
        batchNumber: '9999999999999999',
        total: item.total ? item.total : NP.times(item.referencePrice, parseInt(item.number)),
      }));

      //二、初始化应收金额
      // const receivableAmount = oldDataSource.map(item => item.number * item.referencePrice).reduce((a, b) => a+b);
      // const receivableAmount = oldDataSource.map(item => NP.times(item.number, item.referencePrice)).reduce((a, b) => NP.plus(a, b));
      const receivableAmount = oldDataSource
        .map(item => item.total)
        .reduce((a, b) => NP.plus(a, b));

      return {
        // dataSource: [...selectedRows.map(item => ({ ...item, number: 1 }))]
        // dataSource: oldDataSource.map(item => ({ ...item, batchNumber: '9999999999999999' })),
        dataSource: oldDataSource,
        receivableAmount,
      };
    });

    // message.success('新增成功');
    this.handleGoodsModalVisible();
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      dispatch,
      form,
      wholeSales: { vipList, staffList, warehouseList },
      user: { currentUser },
    } = this.props;
    const { dataSource, vip, receivableAmount } = this.state;

    form.validateFields((err, fieldsValue) => {
      console.log('fieldsValue: ', fieldsValue);
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请先新增商品!');
        return;
      } else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      } else if (dataSource.find(item => !item.batchNumber)) {
        message.warning('请输入商品的批次');
        return;
      }

      fieldsValue.goodsList = JSON.stringify([
        ...dataSource.map(item => ({ ...item, totalPrice: item.total })),
      ]);
      fieldsValue.consignee = vip.name;
      fieldsValue.salesmanName = staffList.rows.find(
        item => item.personnelId === fieldsValue.salesmanId
      ).name;
      fieldsValue.warehouse = warehouseList.rows.find(
        item => item.warehouseId === fieldsValue.warehouseId
      ).name;
      fieldsValue.receivableAmount = receivableAmount;
      fieldsValue.receivedAmount = this.getReceivedAmount();
      fieldsValue.changeAmount = this.changeAmount();
      fieldsValue.type = 2;
      if (vip.name) {
        fieldsValue.memberId = vip.memberId;
      }
      fieldsValue.branchId = currentUser.personnel.branchId;

      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'wholeSales/addStoreSaleOrder',
        payload: fieldsValue,
        callback: data => {
          form.resetFields();
          this.setState({ vip: {}, dataSource: [], receivableAmount: 0 });

          //展示打印详情页
          this.detail(data);
        },
      });
    });
  };

  detail = data => {
    const orderNumber = data.orderNumber;
    const { dispatch } = this.props;
    dispatch({
      type: 'offLineSaleOrder/fetchSaleOrderList',
      payload: { orderNumber },
      callback: data => {
        console.log('data: ', data);
        if (data.rows.length > 0) {
          const record = data.rows[0];
          this.handleDetailModalVisible(true, record);
        }
      },
    });
  };

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleSearch = e => {
    // e.preventDefault();

    const {
      form,
      wholeSales: { vipList },
    } = this.props;
    const memberPhone = form.getFieldsValue().memberPhone;

    this.setState({ searchBtnLoading: true });
    const response = fetchVipList({ phone: memberPhone.replace(/\s*/g,"") });
    response.then(result => {
      console.log('result: ', result);
      const members = result.data.rows;
      if (members[0]) {
        this.setState(prevState => ({
          vip: {
            ...prevState.vip,
            name: members[0].name,
            memberId: members[0].memberId,
          },
        }));
      } else {
        message.warning('无此会员');
        this.setState({ vip: { memberPhone: '' } });
      }
      this.setState({ searchBtnLoading: false });
    });

    // const vip = vipList.rows.find(vip => vip.phone === memberPhone);
    // if (vip) {
    //   this.setState(prevState => ({
    //     vip: {
    //       ...prevState.vip,
    //       name: vip.name,
    //       memberId: vip.memberId
    //     }
    //   }))
    // }
    // else{
    //   message.warning('无此会员');
    // }
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ vip: {}, dataSource: [], receivableAmount: 0, receivedAmount: 0 });
  };

  handleFormPhoneReset = () => {
    const { form } = this.props;
    form.setFieldsValue({ memberPhone: '' });
    this.setState(prevState => ({
      vip: {
        ...prevState.vip,
        name: '',
      },
    }));
  };

  onWarehouseChange = warehouseId => {
    // //获取仓库批次列表
    // const response = fetchWarehouseStockBatchList({ warehouseId });
    // response.then(result => {
    //   console.log('result: ', result);
    //   this.setState(prevState => ({
    //     warehouseStockBatchList: result.data,
    //     dataSource: prevState.dataSource.map(item => ({ ...item, batchNumber: '' }))
    //   }))
    // });
    this.setState({ warehouseId });
  };

  onKeyup = e => {
    if (e.keyCode === 13) {
      this.handleSearch();
    }
  };

  renderSimpleForm() {
    const {
      wholeSales: { staffList, warehouseList },
      form: { getFieldDecorator },
    } = this.props;
    const { vip, searchBtnLoading } = this.state;
    return (
      <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="提货号码">
              {getFieldDecorator('memberPhone', {
                initialValue: vip.phone,
                rules: [
                  {
                    required: true,
                    message: '请输入手机号',
                  },
                ],
              })(
                <Input
                  addonAfter={
                    <a onClick={this.handleFormPhoneReset}>
                      <Icon type="close-circle" />
                    </a>
                  }
                  placeholder="请输入手机号"
                  onKeyUp={this.onKeyup}
                />
              )}
            </FormItem>
          </Col>
          <Col md={2} sm={24}>
            <span className={styles.submitButtons}>
              <Button
                loading={searchBtnLoading}
                onClick={this.handleSearch}
                type="primary"
                disabled={!this.props.form.getFieldsValue().memberPhone}
              >
                查询
              </Button>
            </span>
          </Col>
          <Col md={4} sm={24}>
            <FormItem label="提货姓名">{vip.name}</FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="销售人员">
              {getFieldDecorator('salesmanId', {
                rules: [
                  {
                    required: true,
                    message: '请选择销售人员',
                  },
                ],
                initialValue: staffList.rows.length > 0 ? staffList.rows[0].personnelId : '',
              })(
                <Select placeholder="请选择销售人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => (
                    <Option key={item.personnelId} value={item.personnelId}>
                      {item.name}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出货仓库">
              {getFieldDecorator('warehouseId', {
                rules: [
                  {
                    required: true,
                    message: '请选择出货仓库',
                  },
                ],
                initialValue:
                  warehouseList.rows.length > 0 ? warehouseList.rows[0].warehouseId : '',
              })(
                <Select
                  onChange={this.onWarehouseChange}
                  placeholder="请选择出货仓库"
                  style={{ width: '100%' }}
                >
                  {warehouseList.rows.map(item => (
                    <Select.Option key={item.warehouseId} value={item.warehouseId}>
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="可先提货">
              {getFieldDecorator('isTakeDeliveryFirst', {
                initialValue: false,
                valuePropName: 'checked',
              })(<Checkbox />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  handleSelectChange = (record, key, value, stockNumber) => {
    console.log('row: ', record);
    console.log('key: ', key);
    console.log('value: ', value);
    const newData = [...this.state.dataSource];
    const row = { ...record };
    if (key === 'batchNumber') {
      row.batchNumber = value;
    }

    // 自定义业务需求
    // 当选择的批次在库存中的数量小于要出库的商品数量时：a、提示用户“该批次最多只能出库n件”；
    // record.number ? stockNumber
    if (stockNumber < record.number) {
      //库存数 < 要出库的数
      message.info(`该批次最多只能出库${stockNumber}件`);
      //row.number = stockNumber;--------------不能改，关联太强、成本太大
    }

    // const index = newData.findIndex(item => row.stockId === item.stockId);
    const index = newData.findIndex(item => row.timestamp === item.timestamp);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  };

  setSelectOptions = (row, callback) => {
    const stockNum = row.stockNum;
    console.log('stockNum: ', stockNum);
    const { warehouseId } = this.state;

    //获取仓库批次列表
    const response = fetchWarehouseStockBatchList({ warehouseId, stockNum });
    response.then(result => {
      console.log('result: ', result);
      if (callback)
        callback(
          result.data.map(item => ({
            number: item.number,
            value: item.batchNumber,
            label: item.batchNumber,
          }))
        );
      // this.setState(prevState => ({
      //   // warehouseStockBatchList: result.data.map(item => item.batchNumber),
      //   // dataSource: prevState.dataSource.map(item => ({ ...item, batchNumber: '' }))
      // }))
    });
  };

  getReceivedAmount = () => {
    const {
      form: { getFieldsValue },
    } = this.props;
    const { cashPayment, alipayPayment, weChatPayment, bankCardPayment } = getFieldsValue();
    let receivedAmount = 0;
    if (cashPayment) {
      receivedAmount = NP.plus(receivedAmount, cashPayment);
    }
    if (alipayPayment) {
      receivedAmount = NP.plus(receivedAmount, alipayPayment);
    }
    if (weChatPayment) {
      receivedAmount = NP.plus(receivedAmount, weChatPayment);
    }
    if (bankCardPayment) {
      receivedAmount = NP.plus(receivedAmount, bankCardPayment);
    }
    return receivedAmount;
  };

  changeAmount = () => {
    const { receivableAmount } = this.state; //应收金额
    const receivedAmount = this.getReceivedAmount(); //实收金额
    let changeAmount = 0;
    if (receivedAmount > receivableAmount) {
      // changeAmount = (receivedAmount*100 - NP.times(receivableAmount, 100))/100;
      changeAmount = NP.minus(receivedAmount, receivableAmount);
    }
    console.log('receivedAmount: ', receivedAmount);
    console.log('receivableAmount: ', receivableAmount);
    console.log('changeAmount: ', changeAmount);
    return changeAmount;
  };

  renderBottomForm() {
    const {
      form: { getFieldDecorator },
      submitLoading,
    } = this.props;
    const { receivableAmount } = this.state;
    return (
      <Form onSubmit={this.handleSubmit} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={20}>
            <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
              <Col md={5} sm={24}>
                <FormItem label="现金">
                  {getFieldDecorator('cashPayment')(
                    <InputNumber
                      precision={2}
                      style={{ width: '100%' }}
                      placeholder="请输入"
                      min={0}
                    />
                  )}
                </FormItem>
              </Col>
              <Col md={5} sm={24}>
                <FormItem label="支付宝">
                  {getFieldDecorator('alipayPayment')(
                    <InputNumber
                      precision={2}
                      style={{ width: '100%' }}
                      placeholder="请输入"
                      min={0}
                    />
                  )}
                </FormItem>
              </Col>
              <Col md={5} sm={24}>
                <FormItem label="应收金额">¥ {receivableAmount}</FormItem>
              </Col>
              <Col md={5} sm={24}>
                <FormItem label="找零">¥ {this.changeAmount()}</FormItem>
              </Col>
            </Row>
            <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
              <Col md={5} sm={24}>
                <FormItem label="微信">
                  {getFieldDecorator('weChatPayment')(
                    <InputNumber
                      precision={2}
                      style={{ width: '100%' }}
                      placeholder="请输入"
                      min={0}
                    />
                  )}
                </FormItem>
              </Col>
              <Col md={5} sm={24}>
                <FormItem label="银行卡">
                  {getFieldDecorator('bankCardPayment')(
                    <InputNumber
                      precision={2}
                      style={{ width: '100%' }}
                      placeholder="请输入"
                      min={0}
                    />
                  )}
                </FormItem>
              </Col>
              <Col md={5} sm={24}>
                <FormItem label="实收金额">¥ {this.getReceivedAmount()}</FormItem>
              </Col>
              <Col md={9} sm={24}>
                <FormItem label="备注">
                  {getFieldDecorator('remark')(
                    <Input style={{ width: '100%' }} placeholder="请输入" />
                  )}
                </FormItem>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <Button
              htmlType="submit"
              style={{ height: 89, width: '100%', fontSize: 30 }}
              type="primary"
              loading={submitLoading}
            >
              下单
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }

  handleInputNumberChange = (record, key, value) => {
    const { form } = this.props;
    console.log('row: ', record);
    console.log('key: ', key);
    console.log('value: ', value);
    const newData = [...this.state.dataSource];

    const row = { ...record };

    //改row
    if (isNaN(value)) {
      row.number = '';
      row.number2 = '';
      return;
    } else if (key === 'number') {
      // row.number2 = parseInt((value/row.unitRatio)*100)/100;

      const number2 = NP.divide(value, row.unitRatio);
      row.number2 = NP.round(number2, 2);
      row.total = NP.times(record.referencePrice, parseInt(value));
    } else if (key === 'number2') {
      //保留两位小数
      // value = parseInt(value*100)/100;
      value = NP.round(value, 2);
      // row.number = parseInt(value*row.unitRatio);
      row.number = parseInt(NP.times(value, row.unitRatio));
      row.total = NP.times(record.referencePrice, parseInt(row.number));
    } else if (key === 'total') {
      //金额变化，零售价改变
      //计算出零售价
      const referencePrice = NP.divide(value, record.number);
      const lowerPrice = record.lowerPrice; //最低零售价
      const upperPrice = record.upperPrice; //最高零售价
      if (referencePrice >= lowerPrice && referencePrice <= upperPrice) {
        //零售价正常
        row.referencePrice = NP.round(referencePrice, 2);
      } else {
        //零售价不正常
        row.total = NP.times(record.referencePrice, parseInt(row.number));
      }
    }

    // const index = newData.findIndex(item => row.stockId === item.stockId);
    const index = newData.findIndex(item => row.timestamp === item.timestamp);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  };

  handleSave = row => {
    const newData = [...this.state.dataSource];
    // const index = newData.findIndex(item => row.stockId === item.stockId);
    const index = newData.findIndex(item => row.timestamp === item.timestamp);
    const item = newData[index];

    //零售价必须在最高零售价和最低零售价之间
    let realRow = {
      ...row,
      // total: NP.times(row.referencePrice, parseInt(row.number))
    };
    //零售价变化，金额不变
    if (row.referencePrice !== item.referencePrice && row.total === item.total) {
      realRow.total = NP.times(row.referencePrice, parseInt(row.number));
      const referencePrice = row.referencePrice; //零售价
      const lowerPrice = row.lowerPrice; //最低零售价
      const upperPrice = row.upperPrice; //最高零售价
      if (!(referencePrice >= lowerPrice && referencePrice <= upperPrice)) {
        realRow = {
          ...realRow,
          referencePrice: lowerPrice,
          total: NP.times(lowerPrice, parseInt(row.number)),
        };
        message.warning('零售价不在限制范围内！');
      }
    }

    //金额变化，零售价不变
    if (row.total !== item.total) {
      const referencePrice = NP.round(NP.divide(row.total, parseInt(row.number)), 2); //零售价
      const lowerPrice = row.lowerPrice; //最低零售价
      const upperPrice = row.upperPrice; //最高零售价
      if (!(referencePrice >= lowerPrice && referencePrice <= upperPrice)) {
        // realRow = {
        //   ...realRow,
        //   referencePrice: lowerPrice,
        //   total: NP.times(lowerPrice, parseInt(row.number))
        // }
        realRow.total = NP.times(row.referencePrice, parseInt(row.number));
        message.warning('零售价不在限制范围内！');
      }
    }

    newData.splice(index, 1, {
      ...item,
      ...realRow,
    });
    //刷新应收金额
    // const receivableAmount = newData.map(item => (item.number*100 * item.referencePrice*100)/10000).reduce((a, b) => (a*100 + b*100)/100);
    // const receivableAmount = newData.map(item => NP.times(item.number, item.referencePrice)).reduce((a, b) => NP.plus(a, b));
    const receivableAmount = newData.map(item => item.total).reduce((a, b) => NP.plus(a, b));
    this.setState({ dataSource: newData, receivableAmount });
  };

  render() {
    const {
      storeSales: { storeList },
      loading,
    } = this.props;
    const {
      modalVisible,
      dataSource,
      goodsModalVisible,
      warehouseStockBatchList,
      detailModalVisible,
      record,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const goodsMethods = {
      handleAdd: this.handleGoodsAdd,
      handleModalVisible: this.handleGoodsModalVisible,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          isInputNumber: col.isInputNumber,
          inputNumberPrecision: col.inputNumberPrecision,
          inputNumberMin: col.inputNumberMin,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
          inputNumberChange: (value, row) =>
            this.handleInputNumberChange(row, col.dataIndex, value),
          isSelect: col.isSelect,
          selectOptions: warehouseStockBatchList,
          // selectChange: (value, row) => this.handleSelectChange(row, col.dataIndex, value),
          selectFocus: (row, callback) => this.setSelectOptions(row, callback),
          selectChange: (value, stockNumber, row) =>
            this.handleSelectChange(row, col.dataIndex, value, stockNumber),
        }),
      };
    });

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增会员
              </Button>
              <Button onClick={this.handleFormReset}>重置</Button>
            </div>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.title}>
              商品明细
              <Button
                style={{ marginLeft: 14 }}
                type="primary"
                onClick={() => this.handleGoodsModalVisible(true)}
              >
                新增商品
              </Button>
            </div>
            <StandardTable
              components={components}
              loading={loading}
              data={{ rows: dataSource }}
              columns={columns}
              onChange={this.handleStandardTableChange}
              // rowKey="stockId"
              rowKey="timestamp"
              rowClassName={() => styles['editable-row']}
              size="small"
              scroll={{ x: 1700 }}
            />
            <div className={styles.title} style={{ marginTop: 24 }}>
              支付
            </div>
            <div className={styles.tableListForm} style={{ marginTop: 24 }}>
              {this.renderBottomForm()}
            </div>
          </div>
        </Card>
        <CreateVipForm {...parentMethods} modalVisible={modalVisible} storeList={storeList} />
        <CreateGoodsForm {...goodsMethods} modalVisible={goodsModalVisible} />
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record} />
      </div>
    );
  }
}

export default WholeSales;
