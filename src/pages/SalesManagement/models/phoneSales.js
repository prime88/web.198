// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  addStoreSaleOrder,
} from '@/services/phoneSales';
import { message } from 'antd';
import { addVip, fetchVipList } from '@/services/customerRelations';
import { fetchStaffList } from '@/services/branch';
import { queryWarehouseList } from '@/services/procurementWarehousing';

export default {
  namespace: 'phoneSales',

  state: {
    //会员列表
    vipList: {
      rows: [],
      pagination: {},
    },
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //仓库列表
    warehouseList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchVipList({ payload, callback }, { call, put }) {
      const response = yield call(fetchVipList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveVipList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehouseList',
        payload: response.data,
      });
    },
    *addStoreSaleOrder({ payload, callback }, { call, put }) {
      const response = yield call(addStoreSaleOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchBrandList',
        // });
        if (callback) callback(response.data);
      } else {
        message.error(response.msg);
      }
    },
    *addVip({ payload, callback }, { call, put }) {
      const response = yield call(addVip, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchVipList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveVipList(state, action) {
      return {
        ...state,
        vipList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //品牌管理
  //       if (location.pathname === '/basic-data/brand-management') {
  //         dispatch({
  //           type: 'fetchBrandList',
  //         })
  //       }
  //     });
  //   },
  // },
};
