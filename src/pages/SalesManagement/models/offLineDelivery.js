// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchPhoneOrderDeliveryList,
  fetchSaleOrderDetails,
  delivery,
  receivables,
  cancelOrder,
} from '@/services/offLineDelivery';
import { message } from 'antd';
import { fetchStaffList } from '@/services/branch';
import { queryWarehouseList } from '@/services/procurementWarehousing';

export default {
  namespace: 'offLineDelivery',

  state: {
    //销售订单列表
    saleOrderList: {
      rows: [],
      pagination: {},
    },
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //仓库列表
    warehouseList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchPhoneOrderDeliveryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchPhoneOrderDeliveryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSaleOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehouseList',
        payload: response.data,
      });
    },
    *fetchSaleOrderDetails({ payload, callback }, { call, put }) {
      const response = yield call(fetchSaleOrderDetails, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *delivery({ payload, callback }, { call, put }) {
      const response = yield call(delivery, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchPhoneOrderDeliveryList',
        //   callback: callback
        // });
        if (callback) callback()
      } else {
        message.error(response.msg);
      }
    },
    *receivables({ payload, callback }, { call, put }) {
      const response = yield call(receivables, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchPhoneOrderDeliveryList',
        //   callback: callback
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *cancelOrder({ payload, callback }, { call, put }) {
      const response = yield call(cancelOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchPhoneOrderDeliveryList',
        //   callback: callback
        // });
        if (callback) callback()
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveSaleOrderList(state, action) {
      return {
        ...state,
        saleOrderList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //品牌管理
  //       if (location.pathname === '/basic-data/brand-management') {
  //         dispatch({
  //           type: 'fetchBrandList',
  //         })
  //       }
  //     });
  //   },
  // },
};
