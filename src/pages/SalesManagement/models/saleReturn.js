// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchSaleOrderList,
  fetchSaleOrderDetails,
  fetchSaleReturnGoodsList,
  fetchsalesReturnGoodsDetails,
  addStoreSaleOrder,
  returnGoods,
} from '@/services/saleReturn';
import { message } from 'antd';
import { addVip, fetchVipList } from '@/services/customerRelations';
import { fetchStaffList } from '@/services/branch';
import { queryWarehouseList } from '@/services/procurementWarehousing';

export default {
  namespace: 'saleReturn',

  state: {
    //会员列表
    vipList: {
      rows: [],
      pagination: {},
    },
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //仓库列表
    warehouseList: {
      rows: [],
      pagination: {},
    },
    //销售退货列表
    returnGoodsList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchVipList({ payload, callback }, { call, put }) {
      const response = yield call(fetchVipList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveVipList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehouseList',
        payload: response.data,
      });
    },
    *fetchSaleOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchSaleOrderList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      } else {
        message.error(response.msg);
      }
    },
    *fetchSaleOrderDetails({ payload, callback }, { call, put }) {
      const response = yield call(fetchSaleOrderDetails, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      } else {
        message.error(response.msg);
      }
    },
    *fetchSaleReturnGoodsList({ payload, callback }, { call, put }) {
      const response = yield call(fetchSaleReturnGoodsList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveSaleReturnGoodsList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      } else {
        message.error(response.msg);
      }
    },
    *fetchsalesReturnGoodsDetails({ payload, callback }, { call, put }) {
      const response = yield call(fetchsalesReturnGoodsDetails, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      } else {
        message.error(response.msg);
      }
    },
    *addStoreSaleOrder({ payload, callback }, { call, put }) {
      const response = yield call(addStoreSaleOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchBrandList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addVip({ payload, callback }, { call, put }) {
      const response = yield call(addVip, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchVipList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *returnGoods({ payload, callback }, { call, put }) {
      const response = yield call(returnGoods, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback(response.data);
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveVipList(state, action) {
      return {
        ...state,
        vipList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
    saveSaleReturnGoodsList(state, action) {
      return {
        ...state,
        returnGoodsList: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //品牌管理
  //       if (location.pathname === '/basic-data/brand-management') {
  //         dispatch({
  //           type: 'fetchBrandList',
  //         })
  //       }
  //     });
  //   },
  // },
};
