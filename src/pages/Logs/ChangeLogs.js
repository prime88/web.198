import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Button,
  Modal,
  Divider,
  Popconfirm,
  Row,
  Col,
  DatePicker,
  Tooltip,
  Timeline,
  Menu,
  Dropdown,
  Icon,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated } from '@/services/brand';
import styles from './ChangeLogs.less';
import { formatMessage, FormattedMessage } from 'umi/locale';
import Trend from '@/components/Trend';
import Yuan from '@/utils/Yuan';
import { ChartCard, Field } from '@/components/Charts';
import numeral from 'numeral';
import moment from 'moment';

/* eslint react/no-multi-comp:0 */
@connect(({ user, loading }) => ({
  user,
  loading: loading.models.homeApproval,
}))
@Form.create()
class ChangeLogs extends PureComponent {
  state = {};

  componentDidMount() {}

  render() {
    const menu = (
      <Menu>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062002.jpg"
          >
            采购入库
          </a>
        </Menu.Item>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2.jpg"
          >
            调货出库
          </a>
        </Menu.Item>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/3.jpg"
          >
            门店销售
          </a>
        </Menu.Item>
        <Menu.Item>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/4.jpg"
          >
            销售退货
          </a>
        </Menu.Item>
      </Menu>
    );

    return (
      <div style={{ padding: '30px 30px', background: '#fff' }}>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2020-05-19</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    取消春节活动 门店销售所有商品零售最低价调至正常。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2020-01-19</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 门店销售的实付金额不能大于应付金额(即找零始终为0)。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2020-01-18</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 门店销售的支付方式只能选择一种。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2020-01-03</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    春节活动 门店销售所有商品零售最低价调至0元。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2020-01-02</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 调货申请单主辅数量一致的bug。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-12-06</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 新增会员时手机号带空格新增失败的问题。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019120601.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 门店自取小票加"取货人"和"取货人手机号"信息。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019120602.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-12-05</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 退货失败的问题。
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 门店销售订单导出Excel中商品的布局。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019120501.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-12-02</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 APP商品可一键导入。
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 限制了所有基础数据页面的删除和修改操作，防止出现因修改基础数据后导致门店无法正常下单的问题。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-11-29</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 调货入库可入库多次的问题。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-11-26</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 退货后容易导致销售金额不准确的问题。
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 点击"收银"按钮可能出现2个相同单子的问题。
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 门店销售可查看退货后的订单详情。
                    <Dropdown
                      overlay={
                        <Menu>
                          <Menu.Item>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019111102.jpg"
                            >
                              图解1
                            </a>
                          </Menu.Item>
                          <Menu.Item>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019111101.jpg"
                            >
                              图解2
                            </a>
                          </Menu.Item>
                        </Menu>
                      }
                    >
                      <a className="ant-dropdown-link" href="#">
                       查看图片 <Icon type="down" />
                      </a>
                    </Dropdown>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-11-01</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 取消所有葡萄酒8.15折活动。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-10-28</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 库存列表增加商品的"出入库记录"查询。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019102805.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 门店员工可查看所有仓库的库存商品信息。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019102801.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 门店销售新增商品时，将"商品名称"和"商品编号"互换位置，编号在前，名称在后。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019102802.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 所有列表页都加上模糊查询(商品编号除外，编号需精确查询)。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019102803.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 供应商列表新增"名称"和"类别"的查询。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019102804.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-09-26</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 盘点出入库审批功能。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019092501.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 存货分类新增成功后加提示"建议在该分类下新增品牌，否则无法增加存货！"。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019092502.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 存货列表导出功能。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019092503.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化
                    线下销售订单列表导出Excel功能：1、修复导出全部列表失败的Bug；2、修复Excel中"退货状态"显示错误的Bug；3、点击"导出"按钮后，会跳转到新的页面进行下载。
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-09-17</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 品牌管理增加"名称、编号、分类"查询。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019091601.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化
                    APP商品列表上架商品时，可同步覆盖商品信息，信息同步流：存货管理->商品列表->商品上架->APP商品。
                  </span>
                  <Dropdown
                    overlay={
                      <Menu>
                        <Menu.Item>
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019091602.jpg"
                          >
                            图解1
                          </a>
                        </Menu.Item>
                        <Menu.Item>
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019091603.jpg"
                          >
                            图解2
                          </a>
                        </Menu.Item>
                      </Menu>
                    }
                  >
                    <a className="ant-dropdown-link" href="#">
                      查看图片 <Icon type="down" />
                    </a>
                  </Dropdown>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-09-10</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 线下销售(门店销售、电话销售、批发销售)可以新增多条相同的商品。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19090301.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 盘点单只能出入库一次。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019091001.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 新品录入页面 物品分类 和 品牌 下拉搜索查询。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019091002.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 APP自取订单增加自取时间限制。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019091003.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 APP门店配送订单起送价。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/2019091004.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>优化 用户在收到货之前都可取消APP订单。</span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>优化 用户在收到货后不可退货。</span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 APP订单通知声音，鸟声改为人声，且每30秒播报一次待处理订单的通知。
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>优化 APP订单小票加防伪码。</span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>优化 给所有APP空白页添加默认文字提示，如"无收货地址，请先添加！"。</span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-09-05</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 线下会员管理增加：1、开卡店铺查询；2、导出excel。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19090501.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-09-02</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 所有葡萄酒8.15折。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19090201.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-08-27</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 APP门店配送订单详情中无法正常显示配送信息的问题。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19072401.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-07-24</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 同一个调拨出库单只能被收货门店参照入库一次。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19072401.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    增加
                    调货出库订单增加"入库单号"信息，调货入库订单增加"出库单号"信息，方便门店相互追踪调拨订单详情。
                  </span>
                  <Dropdown
                    overlay={
                      <Menu>
                        <Menu.Item>
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19072402.jpg"
                          >
                            调货出库
                          </a>
                        </Menu.Item>
                        <Menu.Item>
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19072403.jpg"
                          >
                            调货入库
                          </a>
                        </Menu.Item>
                      </Menu>
                    }
                  >
                    <a className="ant-dropdown-link" href="#">
                      查看图片 <Icon type="down" />
                    </a>
                  </Dropdown>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-07-10</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 门店销售、电话销售和批发销售下单后即可打印小票。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071002.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 缩小小票的行间距。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071003.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)">
                  <span>
                    新增 销售退货添加打印功能。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071001.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 盘点单新增库存商品时可一键全选。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071004.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 "库存查询"可过滤库存为0的商品。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071005.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)">
                  <span>
                    优化 供应商下拉选项可搜索选择。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071006.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 门店销售添加商品时能查看单个商品的库存数。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071007.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)">
                  <span>
                    新增 在若干页面的弹框顶部添加"确定"按钮。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071008.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 销售退货列表中退货方式显示错误问题。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071010.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    修复 销售订单查询页面商品总额(totalPrice)显示不正确的问题。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19071009.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-07-03</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 电话销售和批发销售可对商品总额进行修改，解决整箱销售总额问题。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/09070301.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>2019-07-02</span>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 门店销售可对商品总额进行修改，解决整箱销售总额问题。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/09070201.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
        <Timeline>
          <Timeline.Item>
            <div style={{ paddingLeft: 20 }}>
              <span style={{ color: '0d1a26', fontWeight: 500, fontSize: 20 }}>
                针对2019年6月12日～13日培训过程中员工提出的建议的优化
              </span>
              <div style={{ margin: '20px 0', color: 'rgba(0,0,0,0.65)' }}>2019-06-27</div>
              <Timeline style={{ marginTop: 10 }} className={styles.customTimeline}>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 销售时商品"零售价"可修改，但修改范围介于"最高零售价"和"最低零售价"之间。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062602.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 调货出库单新增显示“入库仓库”、“入库主数量”和“入库辅数量”。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062501.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    新增 调货入库单新增显示“出库仓库”、“出库主数量”和“出库辅数量”。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062502.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30, fontWeight: 900 }}>
                  <span>
                    优化 盘点单在保存之后(提交之前)即可生成盈亏单。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062503.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    优化 采购到货单的"到货数量"和"拒收数量"，改成"到货主数量"和"拒收主数量"。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/1560501901410.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    优化 盘点单的实盘主数量默认与库存的账面主数量一致。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/1560498642828.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    优化 线下销售输入手机号后可按下回车键查询会员。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/1560771492474.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    优化 给所有入库和出库操作的批次设置默认值。
                    <Dropdown overlay={menu}>
                      <a className="ant-dropdown-link" href="#">
                        查看图片 <Icon type="down" />
                      </a>
                    </Dropdown>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    新增 首页新增显示"退款笔数"。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062001.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    新增 库存列表增加Excel导出功能。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062003.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    优化 线下订单的订单状态文案加颜色区分。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062401.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    优化 线下销售退货中"实退金额"默认与"应退金额"相同。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062402.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    修复 线下销售订单部分商品总额出现多位小数的问题。
                    <Dropdown
                      overlay={
                        <Menu>
                          <Menu.Item>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062403.jpg"
                            >
                              问题页面
                            </a>
                          </Menu.Item>
                          <Menu.Item>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062404.jpg"
                            >
                              修正页面
                            </a>
                          </Menu.Item>
                        </Menu>
                      }
                    >
                      <a className="ant-dropdown-link" href="#">
                        查看图片 <Icon type="down" />
                      </a>
                    </Dropdown>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>优化 第一次打开页面加载太慢的问题。</span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    新增 销售订单可以一键退货。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062405.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
                <Timeline.Item color="rgba(0,0,0,0.65)" style={{ minHeight: 30 }}>
                  <span>
                    优化 门店无法进行盘点出入库操作，有数据权限的账号才可以。
                    <a
                      href="https://36519.oss-cn-beijing.aliyuncs.com/logs/19062601.jpg"
                      target="_Blank"
                    >
                      查看图片
                    </a>
                  </span>
                </Timeline.Item>
              </Timeline>
            </div>
          </Timeline.Item>
        </Timeline>
      </div>
    );
  }
}

export default ChangeLogs;
