import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, DatePicker, Tooltip } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated } from '@/services/brand';
import styles from './HomePage.less';
import PurchaseingPlanListDetail from "./PurchaseingPlanListDetail";
import ProcurementPlanListDetail from "./ProcurementPlanListDetail";
import ProcurementOrderListDetail from "./ProcurementOrderListDetail";
import ArrivalOrderListDetail from "./ArrivalOrderListDetail";
import WarehousingOrderListDetail from "./WarehousingOrderListDetail";
import DeliveryApplyOrderListDetail from "./DeliveryApplyOrderListDetail";
import DeliveryOrderListDetail from "./DeliveryOrderListDetail";
import DeliveryOutOrderListDetail from "./DeliveryOutOrderListDetail";
import DeliveryInOrderListDetail from "./DeliveryInOrderListDetail";
import { formatMessage, FormattedMessage } from 'umi/locale';
import Trend from '@/components/Trend';
import Yuan from '@/utils/Yuan';
import {
  ChartCard,
  Field,
} from '@/components/Charts';
import numeral from 'numeral';
import moment from 'moment'

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const LogDetail = props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let detailComponent = null;
  switch (record.approvalFormId) {
    case 1://门店请购详情
      detailComponent = <PurchaseingPlanListDetail record={record}/>;
      break;
    case 2://采购计划单详情
      detailComponent = <ProcurementPlanListDetail record={record}/>;
      break;
    case 3://采购订单详情
      detailComponent = <ProcurementOrderListDetail record={record}/>;
      break;
    case 4://采购到货单详情
      detailComponent = <ArrivalOrderListDetail record={record}/>;
      break;
    case 5://采购入库单详情
      detailComponent = <WarehousingOrderListDetail record={record}/>;
      break;
    case 6://调货申请单详情
      detailComponent = <DeliveryApplyOrderListDetail record={record}/>;
      break;
    case 7://调货单详情
      detailComponent = <DeliveryOrderListDetail record={record}/>;
      break;
    case 8://调货出库单详情
      detailComponent = <DeliveryOutOrderListDetail record={record}/>;
      break;
    case 9://调货入库单详情
      detailComponent = <DeliveryInOrderListDetail record={record}/>;
      break;
  }


  return (
    <Modal
      width='60%'
      title='审批详情'
      visible={detailModalVisible}
      onCancel={() => handleDetailModalVisible()}
      footer={[
        <Button onClick={() => handleDetailModalVisible()} type="primary">
          关闭
        </Button>,
      ]}
    >
      {detailComponent}
    </Modal>
  );
}

/* eslint react/no-multi-comp:0 */
@connect(({ homeApproval, user, loading }) => ({
  homeApproval,
  user,
  loading: loading.models.homeApproval,
}))
@Form.create()
class HomePage extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    detailModalVisible: false,
    selectedRows: [],
    formValues: {},
    record: {},
    dataPermission: false,
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '流程名称',
      dataIndex: 'flowName',
    },
    {
      title: '发起人',
      dataIndex: 'originator',
    },
    {
      title: '发起时间',
      dataIndex: 'createTime',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '运行中';
            break;
          case '1':
            result = '已批准';
            break;
          case '2':
            result = '不批准';
            break;
        }
        return result;
      }
    },
    // {
    //   title: '备注',
    //   dataIndex: 'remark',
    // },
    {
      title: '操作',
      width: '10%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleDetailModalVisible(true, record)}>查看进度</a>
          {/*<a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>*/}
          {/*<Divider type="vertical" />*/}
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
          {/*title="确认删除？"*/}
          {/*onConfirm={() => this.recordRemove(record.brandId)}*/}
          {/*okText="确认"*/}
          {/*cancelText="取消"*/}
          {/*>*/}
          {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  recordRemove = brandIds => {
    console.log('brandIds: ', brandIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'brand/removeBrand',
      payload: { brandIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'homeApproval/changeBrandStatus',
      payload: { brandId: record.brandId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //门店列表
    dispatch({
      type: 'homeApproval/fetchBranchs',
      payload: {
        isStore: true
      }
    });

    //代办审批列表
    dispatch({
      type: 'homeApproval/fetchApprovalLogList',
      payload: {
        type: 1,
        personnelId: currentUser.personnelId
      }
    });

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList-----dj: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });

    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const storeId =  currentUser.personnel.storeId;
      //2、判断是否是门店员工
      if (storeId || storeId === 1) {
        console.log('是门店员工')

      }
      else{
        console.log('不是门店员工')
      }
    }

    //门店销售数据
    const storeId = currentUser.personnel.storeId;
    const branchId = currentUser.personnel.branchId;
    if (storeId || storeId === 1){
      dispatch({
        type: 'homeApproval/fetchSales',
        payload: {
          branchId,
          startTime: moment(new Date).format('YYYY-MM-DD'),
          endTime: moment(new Date).format('YYYY-MM-DD'),
        }
      });
    }
    else{
      if (target) {//非人员成员，有数据权限
        dispatch({
          type: 'homeApproval/fetchSales',
          payload: {
            // branchId,
            startTime: moment(new Date).format('YYYY-MM-DD'),
            endTime: moment(new Date).format('YYYY-MM-DD'),
          }
        });
      }

    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'homeApproval/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);

    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'homeApproval/addBrand',
      payload: fields,
      callback: () => this.handleModalVisible(true)
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'homeApproval/updateBrand',
      payload: {
        ...fields,
        brandId: record.brandId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form, user: { currentUser } } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'homeApproval/fetchApprovalLogList',
        payload: {
          ...fieldsValue,
          personnelId: currentUser.personnelId
        }
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch, user: { currentUser } } = this.props;
    form.resetFields();
    dispatch({
      type: 'homeApproval/fetchApprovalLogList',
      payload: {
        personnelId: currentUser.personnelId,
        type: 1
      }
    });
  };

  onFormChange = (value, key) => {
    const { form, dispatch } = this.props;
    const formData = { ...form.getFieldsValue(), [key]: value };
    formData.startTime = formData.startTime.format('YYYY-MM-DD');
    formData.endTime = formData.endTime.format('YYYY-MM-DD');
    console.log('formData: ', formData);
    dispatch({
      type: 'homeApproval/fetchSales',
      payload: formData
    });
  }

  renderSimpleForm() {
    const {
      homeApproval: { branchs },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;
    const { dataPermission } = this.state;

    const branchId = currentUser.personnel.branchId;
    const storeId = currentUser.personnel.storeId;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="门店">
              {getFieldDecorator('branchId', {
                initialValue: (storeId || storeId === 1) ? branchId : ''
              })(
                <Select onChange={(value) => this.onFormChange(value, 'branchId')} disabled={!dataPermission} placeholder='请选择门店'>
                  <Select.Option key='' value=''>全部门店</Select.Option>
                  {branchs.rows.map(item => <Select.Option key={item.branchId} value={item.branchId}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="开始时间">
              {getFieldDecorator('startTime', {
                initialValue: moment(new Date())
              })(
                <DatePicker
                  style={{width: '100%'}}
                  // showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD"
                  placeholder="请选择开始时间"
                  onChange={(value) => this.onFormChange(value, 'startTime')}
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime', {
                initialValue: moment(new Date())
              })(
                <DatePicker
                  style={{width: '100%'}}
                  // showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD"
                  placeholder="请选择结束时间"
                  onChange={(value) => this.onFormChange(value, 'endTime')}
                />
              )}
            </FormItem>
          </Col>
        </Row>
        {/*<Row gutter={{ md: 8, lg: 24, xl: 48 }}>*/}
          {/*<Col md={8} sm={24}>*/}
            {/*<span className={styles.submitButtons}>*/}
              {/*<Button type="primary" htmlType="submit">*/}
                {/*查询*/}
              {/*</Button>*/}
              {/*<Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>*/}
                {/*重置*/}
              {/*</Button>*/}
              {/*/!*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*!/*/}
              {/*/!*展开 <Icon type="down" />*!/*/}
              {/*/!*</a>*!/*/}
            {/*</span>*/}
          {/*</Col>*/}
        {/*</Row>*/}
      </Form>
    );
  }

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    // return;
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetail = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'storePurchaseing/updatePurchase',
      payload: { ...fields, purchaseRequisitionId: record.purchaseRequisitionId },
    });

    this.handleUpdateModalVisible();
  };

  render() {
    const {
      homeApproval: { approvalLogList, sales },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record, detailModalVisible, dataPermission } = this.state;

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
      handleDetail: this.handleDetail,
    };

    const topColResponsiveProps = {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 12,
      xl: 6,
      style: { marginBottom: 24 },
    };

    return (
      <div>
        <Card bordered={false} >
          <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          <Row gutter={24}>
            <Col {...topColResponsiveProps}>
              <ChartCard
                style={{border: '1px solid #e8e8e8'}}
                bordered={false}
                title='线下销售总额'
                // action={
                //   <Tooltip
                //     title={
                //       <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                //     }
                //   >
                //     <Icon type="info-circle-o" />
                //   </Tooltip>
                // }
                loading={loading}
                total={() => <Yuan>{sales.offlineSales ? (sales.offlineSales.totalSalesAmount || 0) : 0}</Yuan>}
                footer={
                  <div>
                    <Field
                      label='微信总额'
                      value={`￥${sales.offlineSales ? (sales.offlineSales.weChatAmount || 0) : 0}`}
                    />
                    <Field
                      label='支付宝总额'
                      value={`￥${sales.offlineSales ? (sales.offlineSales.alipayAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='现金总额'
                      value={`￥${sales.offlineSales ? (sales.offlineSales.cashAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='银行卡总额'
                      value={`￥${sales.offlineSales ? (sales.offlineSales.bankCardAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='退款总额'
                      value={`￥${sales.offlineSales ? (sales.offlineSales.refundAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='退款笔数'
                      value={`${sales.offlineSales ? (sales.offlineSales.refundCount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='支付笔数'
                      value={`${sales.offlineSales ? sales.offlineSales.payCount : 0}`}
                      style={{marginTop: 4}}
                    />
                  </div>

                }
                contentHeight={46}
              >
              </ChartCard>
            </Col>
            <Col {...topColResponsiveProps}>
              <ChartCard
                style={{border: '1px solid #e8e8e8'}}
                bordered={false}
                title='线上销售总额'
                // action={
                //   <Tooltip
                //     title={
                //       <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                //     }
                //   >
                //     <Icon type="info-circle-o" />
                //   </Tooltip>
                // }
                loading={loading}
                total={() => <Yuan>{sales.onlineSales ? (sales.onlineSales.totalSalesAmount || 0) : 0}</Yuan>}
                footer={
                  <div>
                    <Field
                      label='微信总额'
                      value={`￥${sales.onlineSales ? (sales.onlineSales.weChatAmount || 0) : 0}`}
                    />
                    <Field
                      label='支付宝总额'
                      value={`￥${sales.onlineSales ? (sales.onlineSales.alipayAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='买赠红包总额'
                      value={`￥${sales.onlineSales ? (sales.onlineSales.buyGiveAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='会员支付'
                      value={`￥${sales.onlineSales ? (sales.onlineSales.memberAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='退款总额'
                      value={`￥${sales.onlineSales ? (sales.onlineSales.refundAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='退款笔数'
                      value={`${sales.onlineSales ? (sales.onlineSales.refundCount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='支付笔数'
                      value={`${sales.onlineSales ? sales.onlineSales.payCount : 0}`}
                      style={{marginTop: 4}}
                    />
                  </div>
                }
                contentHeight={46}
              >
              </ChartCard>
            </Col>
            <Col {...topColResponsiveProps}>
              <ChartCard
                style={{border: '1px solid #e8e8e8'}}
                bordered={false}
                title='电子钱包充值总额'
                // action={
                //   <Tooltip
                //     title={
                //       <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                //     }
                //   >
                //     <Icon type="info-circle-o" />
                //   </Tooltip>
                // }
                loading={loading}
                total={() => <Yuan>{sales.ewalletData ? (sales.ewalletData.totalAmount || 0) : 0}</Yuan>}
                footer={
                  <div style={{padding: '39px 0'}}>
                    <Field
                      label='微信总额'
                      value={`￥${sales.ewalletData ? (sales.ewalletData.weChatAmount || 0) : 0}`}
                    />
                    <Field
                      label='支付宝总额'
                      value={`￥${sales.ewalletData ? (sales.ewalletData.alipayAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='支付笔数'
                      value={`${sales.ewalletData ? sales.ewalletData.payCount : 0}`}
                      style={{marginTop: 4}}
                    />
                  </div>
                }
                contentHeight={46}
              >
              </ChartCard>
            </Col>
            <Col {...topColResponsiveProps}>
              <ChartCard
                style={{border: '1px solid #e8e8e8'}}
                bordered={false}
                title='满赠红包充值总额'
                // action={
                //   <Tooltip
                //     title={
                //       <FormattedMessage id="app.analysis.introduce" defaultMessage="introduce" />
                //     }
                //   >
                //     <Icon type="info-circle-o" />
                //   </Tooltip>
                // }
                loading={loading}
                total={() => <Yuan>{sales.buyGiveData ? (sales.buyGiveData.totalAmount || 0) : 0}</Yuan>}
                // total={() => <div><Yuan ></Yuan>0.03</div>}
                footer={
                  <div style={{padding: '39px 0'}}>
                    <Field
                      label='微信总额'
                      value={`￥${sales.buyGiveData ? (sales.buyGiveData.weChatAmount || 0) : 0}`}
                    />
                    <Field
                      label='支付宝总额'
                      value={`￥${sales.buyGiveData ? (sales.buyGiveData.alipayAmount || 0) : 0}`}
                      style={{marginTop: 4}}
                    />
                    <Field
                      label='支付笔数'
                      value={`${sales.buyGiveData ? sales.buyGiveData.payCount : 0}`}
                      style={{marginTop: 4}}
                    />
                  </div>
                }
                contentHeight={46}
              >
              </ChartCard>
            </Col>
          </Row>
          <div className={styles.title} style={{ display: dataPermission ? 'block' : 'none' }}>待办审批</div>
          <div className={styles.tableList} style={{ display: dataPermission ? 'block' : 'none' }}>
            <StandardTable
              paginationOfTable={{defaultPageSize: 5}}
              // selectedRows={selectedRows}
              loading={loading}
              data={approvalLogList}
              columns={this.columns}
              // onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="approvalLogId"
              footer={() => <div>总计：{approvalLogList.rows.length}</div>}
              size='small'
            />
          </div>
        </Card>
        <LogDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
      </div>
    );
  }
}

export default HomePage;
