// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchFlowList,
  fetchFlowNodeList,
  fetchFlowNodePersonnelList,
  fetchFormList,
  fetchApprovalLogList,
  fetchPurchaseingPlanApprovalLogDetail,
  fetchPurchaseingInventoryList,
  fetchProcurementInventoryList,
  fetchProcurementOrderInventoryList,
  fetchArrivalOrderInventoryList,
  fetchWarehousingOrderInventoryList,
  fetchDeliveryApplyOrderInventoryList,
  fetchDeliveryOrderInventoryList,
  fetchDeliveryOutOrderInventoryList,
  fetchDeliveryInOrderInventoryList,
  fetchBranchs,
  fetchSales,
  addFlow,
  addFlowNode,
  updateFlow,
  updateFlowNode,
  approval,
  removeFlow,
  removeFlowNode,
  changeFlowStatus,
} from '@/services/approval';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';
import { fetchBranchList, fetchStaffList } from '@/services/branch';
import { queryStoreList } from '@/services/store';
// import { fetchPurchaseingInventoryList } from '@/services/storeProcurement';

export default {
  namespace: 'homeApproval',

  state: {
    //流程列表
    flowList: {
      rows: [],
      pagination: {},
    },
    //表单列表
    formList: [],
    //部门列表
    branchList: [],
    //部门列表
    branchs: {
      rows: [],
      pagination: {},
    },
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //流程节点列表
    flowNodeList: [],
    //流程节点审批人列表
    flowNodePersonnelList: [],
    //审批日志列表
    approvalLogList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //门店销售数据
    sales: {},
  },

  effects: {
    *fetchFlowList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFlowList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFlowList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchFormList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFormList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFormList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchs({ payload, callback }, { call, put }) {
      const response = yield call(fetchBranchs, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchs',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchFlowNodeList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFlowNodeList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFlowNodeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchFlowNodePersonnelList({ payload, callback }, { call, put }) {
      const response = yield call(fetchFlowNodePersonnelList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveFlowNodePersonnelList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchApprovalLogList({ payload, callback }, { call, put }) {
      const response = yield call(fetchApprovalLogList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveApprovalLogList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchPurchaseingPlanApprovalLogDetail({ payload, callback }, { call, put }) {
      const response = yield call(fetchPurchaseingPlanApprovalLogDetail, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchPurchaseingInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchPurchaseingInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchProcurementInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProcurementInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchProcurementOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProcurementOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchArrivalOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchArrivalOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchWarehousingOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchWarehousingOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryApplyOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryApplyOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryOutOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryOutOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchDeliveryInOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryInOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchSales({ payload, callback }, { call, put }) {
      const response = yield call(fetchSales, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveSales',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *addFlow({ payload, callback }, { call, put }) {
      const response = yield call(addFlow, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addFlowNode({ payload, callback }, { call, put }) {
      const response = yield call(addFlowNode, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowNodeList',
          payload: {
            flowId: payload.flowId
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeFlow({ payload, callback }, { call, put }) {
      const response = yield call(removeFlow, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeFlowNode({ payload, callback }, { call, put }) {
      const response = yield call(removeFlowNode, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchFlowNodeList',
          payload: {
            flowId: payload.flowId
          }
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateFlow({ payload, callback }, { call, put }) {
      const response = yield call(updateFlow, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateFlowNode({ payload, callback }, { call, put }) {
      const response = yield call(updateFlowNode, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowNodeList',
          payload: {
            flowId: payload.flowId
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *approval({ payload, callback }, { call, put }) {
      const response = yield call(approval, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeFlowStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeFlowStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchFlowList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveFlowList(state, action) {
      return {
        ...state,
        flowList: action.payload,
      };
    },
    saveFormList(state, action) {
      return {
        ...state,
        formList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveBranchs(state, action) {
      return {
        ...state,
        branchs: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveFlowNodeList(state, action) {
      return {
        ...state,
        flowNodeList: action.payload,
      };
    },
    saveFlowNodePersonnelList(state, action) {
      return {
        ...state,
        flowNodePersonnelList: action.payload,
      };
    },
    saveApprovalLogList(state, action) {
      return {
        ...state,
        approvalLogList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveSales(state, action) {
      return {
        ...state,
        sales: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //品牌管理
  //       if (location.pathname === '/basic-data/brand-management') {
  //         dispatch({
  //           type: 'fetchBrandList',
  //         })
  //       }
  //     });
  //   },
  // },
};
