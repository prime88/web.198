import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider, Steps, Form, Modal, Icon, Radio, Input, Popover } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './WarehousingOrderListDetail.less';

const { Description } = DescriptionList;
const Step = Steps.Step;
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="审批"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      mask={false}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="审批">
        {form.getFieldDecorator('type', {
          rules: [{ required: true, message: '请选择！' }],
          initialValue: 1
        })(
          <RadioGroup >
            <Radio value={1}>批准</Radio>
            <Radio value={0}>不批准</Radio>
          </RadioGroup>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="批语">
        {form.getFieldDecorator('remarks')(<Input.TextArea rows={3} placeholder="请输入批语" />)}
      </FormItem>
    </Modal>
  );
});


@connect(({ homeApproval, user, loading }) => ({
  homeApproval,
  user,
  // loading: loading.effects['profile/fetchBasic'],
}))
class WarehousingOrderListDetail extends Component {
  state = {
    dataSource: [],
    data: {},
    approvalLogDetail: {
      flowNodeLogList: []
    },
    modalVisible: false,
  }

  componentDidMount() {
    // const { dispatch, record } = this.props;
    // const purchaseGodownEntryId = record.purchaseGodownEntryId;
    // if (purchaseGodownEntryId) {
    //   dispatch({
    //     type: 'approval/fetchProcurementInventoryList',
    //     payload: {
    //       purchaseGodownEntryIds: purchaseGodownEntryId
    //     },
    //     callback: (data) => {
    //       this.setState({ dataSource: data })
    //     }
    //   })
    // }

    const { dispatch, record } = this.props;
    const billId = record.billId;
    const approvalLogId = record.approvalLogId;
    if (billId.toString() && approvalLogId.toString()) {

      //获取门店请购订单详情
      dispatch({
        type: 'homeApproval/fetchWarehousingOrderInventoryList',
        payload: {
          purchaseGodownEntryIds: billId
        },
        callback: (data) => {
          console.log('data-获取门店请购订单详情: ', data);
          this.setState({ dataSource: data.pgeStockList, data })
        }
      })

      //获取门店请购审批日志详情
      this.updateApprovalLogDetail(approvalLogId)
    }

  }

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '单位',
      dataIndex: 'units',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    {
      title: '含税单价',
      dataIndex: 'referenceCost',
    },
    {
      title: '无税单价',
      dataIndex: 'taxFreeUnitPrice',
    },
    {
      title: '税率(%)',
      dataIndex: 'taxRate',
    },
    {
      title: '含税合计',
      dataIndex: 'taxTotal',
    },
    {
      title: '无税合计',
      dataIndex: 'taxFreeTotal',
    },
    {
      title: '订单数量',
      dataIndex: 'number',
    },
    {
      title: '到货主数量',
      dataIndex: 'arrivalNum',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  updateApprovalLogDetail = (approvalLogId) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'homeApproval/fetchPurchaseingPlanApprovalLogDetail',
      payload: {
        approvalLogId
      },
      callback: (data) => {
        console.log('data-获取门店请购审批日志详情: ', data);
        this.setState({ approvalLogDetail: data })
      }
    })
  }


  getSteps = (item) => {
    const { user: { currentUser } } = this.props;

    let defaultProps = {};
    if (item.status === 1) {
      defaultProps = {
        icon: <Icon style={{fontSize: 32}} type={'loading'} />
      };
    }

    const content = (
      <Table
        pagination={false}
        // loading={loading}
        dataSource={item.flowNodeLogDetailsList}
        columns={[
          {
            title: '审批人',
            dataIndex: 'agent',
          },
          {
            title: '审批状态',
            dataIndex: 'status',
            render: text => this.getNodeStatusName(parseInt(text))
          },
          {
            title: '批语',
            dataIndex: 'remarks',
          },
        ]}
        rowKey="id"
        size='small'
      />
    );

    const logDetail = item.flowNodeLogDetailsList.find(item => item.agentId.toString() === currentUser.personnelId.toString() && item.status === 1);
    const isCurrentUserNode = item.status === 1 && logDetail;

    return <Step
      { ...defaultProps }
      title={<span>
              {item.nodeName}
        <Popover placement="right" content={content} title="审批人列表">
                <a><Icon type="eye" /></a>
              </Popover>
        &nbsp;
        {isCurrentUserNode ? <a onClick={() => this.handleModalVisible(true)}><Icon type="edit" /></a> : null}
            </span>}
      description={`${this.getNodeStatusName(item.status)}`}
    />
  }

  getNodeStatusName = (status) => {
    let statusName = '';
    switch (status) {
      case 0:
        statusName = '未运行';//此节点还没有到达开始审批的步骤
        break;
      case 1:
        statusName = '运行中';//此节点正在进行审批，上一个节点已被批准
        break;
      case 2:
        statusName = '批准';//此节点已被批准，下一个节点处于运行中状态
        break;
      case 3:
        statusName = '不批准';//此节点未被批准，下一个节点处于已结束状态
        break;
      case 4:
        statusName = '已结束';//上一个节点未被批准，此结束已结束
        break;
    }
    return statusName
  }

  handleAdd = fields => {
    const { dispatch, record } = this.props;
    const approvalLogId = record.approvalLogId;
    dispatch({
      type: 'homeApproval/approval',
      payload: {
        ...fields,
        approvalLogId
      },
      callback: () => {
        this.updateApprovalLogDetail(approvalLogId)
      }
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };


  render() {
    const { record } = this.props;
    const { dataSource, approvalLogDetail, data, modalVisible } = this.state;
    //获取stemps的current
    //1、获取运行中的flowNodeLog
    let current = 0;
    const currentFlowNodeLog = approvalLogDetail.flowNodeLogList.find(item => item.status === 1);
    if (currentFlowNodeLog) {
      current = approvalLogDetail.flowNodeLogList.indexOf(currentFlowNodeLog);
    }
    else {
      current = approvalLogDetail.flowNodeLogList.length+1;
    }

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="单据编号">{data.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库时间">{data.godownEntryTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库人员">{data.godownEntryBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="供应商">{data.supplier}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="仓库">{data.warehouse}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{data.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{data.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>订单明细</div>
          <Table
            style={{ marginBottom: 24 }}
            pagination={{defaultPageSize: 9}}
            size='small'
            // loading={loading}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="stockId"
            scroll={{x: 1500}}
          />
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>审批进度</div>
          <Steps direction="vertical" current={current} style={{paddingLeft: 365}} initial={-1}>
            <Step title="开始" description="审批开始" />
            {approvalLogDetail.flowNodeLogList.map(item => this.getSteps(item))}
            <Step title="结束" description="审批结束" />
          </Steps>
          <CreateForm {...parentMethods} modalVisible={modalVisible} />
        </div>
      </div>
    );
  }
}

export default WarehousingOrderListDetail;
