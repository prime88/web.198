import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  DatePicker,
  Modal,
  message,
  Table,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import router from 'umi/router';
import styles from './CheckOrderList.less';
import { fetchStaffList } from '@/services/branch';
import CheckOrderListEdit from './CheckOrderListEdit';
import CheckOrderListDetail from './CheckOrderListDetail';
import ReactToPrint from 'react-to-print';
import { apiDomainName } from '../../constants/index';
import { stringify } from 'qs';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const DetailForm = Form.create()(props => {
  const { detailModalVisible, handleDetailModalVisible, record } = props;
  let componentRef;
  return (
    <Modal
      width={'80%'}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={okHandle}
      onCancel={() => handleDetailModalVisible()}
      style={{ top: 10 }}
      title={
        <div>
          盘点单详情
          <span style={{ float: 'right' }}>
            <ReactToPrint
              trigger={() => (
                <a href="#">
                  <Icon type="printer" />
                </a>
              )}
              content={() => {
                console.log('componentRef: ', componentRef);
                return componentRef;
              }}
            />
          </span>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
    >
      <CheckOrderListDetail record={record} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const {
    updateModalVisible,
    form,
    handleUpdate,
    handleUpdateModalVisible,
    record,
    warehouseList,
  } = props;
  let checkOrderListEdit;
  const okHandle = isSubmit => {
    const { form } = checkOrderListEdit.props;
    const { dataSource } = checkOrderListEdit.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (dataSource.length === 0) {
        message.warning('请新增盘点明细');
        return;
      } else if (dataSource.find(item => !item.checkNumber && isNaN(item.checkNumber))) {
        message.warning('请设置实盘主数量');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.checkTime = fieldsValue.checkTime.format('YYYY-MM-DD');
      fieldsValue.warehouse = warehouseList.rows.find(
        item => item.warehouseId === fieldsValue.warehouseId
      ).name;
      if (isSubmit) {
        fieldsValue.submit = isSubmit;
      }
      console.log('fieldsValue: ', fieldsValue);
      handleUpdate(fieldsValue);
    });
  };

  const editModalRef = ref => {
    checkOrderListEdit = ref;
  };

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      // title="修改盘点单"
      title={
        <div>
          <span>修改盘点单</span>
          {/*<Button style={{float: 'right', marginRight: 50}} type='primary' onClick={() => okHandle(true)}>提交</Button>*/}
          <Button
            type="primary"
            style={{ float: 'right', marginRight: 50 }}
            onClick={() => okHandle(false)}
          >
            保存
          </Button>
        </div>
      }
      visible={updateModalVisible}
      // onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
      style={{ top: 10 }}
      footer={[
        <Button onClick={() => handleUpdateModalVisible()}>取消</Button>,
        <Button type="primary" onClick={() => okHandle(false)}>
          保存
        </Button>,
        //<Button type="primary" onClick={() => okHandle(true)}>
        //  提交
        //</Button>,
      ]}
    >
      <CheckOrderListEdit row={record} wrappedComponentRef={editModalRef} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ checkOrder, user, loading }) => ({
  checkOrder,
  user,
  loading: loading.models.checkOrder,
}))
@Form.create()
class CheckOrderList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
    dataPermission: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    {
      title: '盘点仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '盘点时间',
      dataIndex: 'checkTime',
      // render: text => text.split(' ').length > 0 ? text.split(' ')[0] : text
    },
    {
      title: '盘点人员',
      dataIndex: 'checkBy',
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '是否提交',
      dataIndex: 'isSubmit',
      render: text => {
        let result = '';
        switch (text) {
          case true:
            result = '已提交';
            break;
          case false:
            result = '未提交';
            break;
        }
        return result;
      },
    },
    // {
    //   title: '审批结果',
    //   dataIndex: 'submitResult',
    //   render: (text, record) => {
    //     let result = '';
    //     if (record.isSubmit) {
    //       switch (text.toString()) {
    //         case '0':
    //           result = '审批中';
    //           break;
    //         case '1':
    //           result = '已批准';
    //           break;
    //         case '2':
    //           result = '未批准';
    //           break;
    //       }
    //     }
    //     return result;
    //   }
    // },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      render: (text, record) => {
        //该页面拥有的元素权限列表
        const {
          user: {
            currentUser: { permissionList },
          },
          location: { pathname },
        } = this.props;
        const elementPermissionList = this.getElementPermissionList(pathname, permissionList);
        // console.log('elementPermissionList: ', elementPermissionList);
        const menu = (
          <Menu onClick={e => e.domEvent.stopPropagation()}>
            <Menu.Item
              disabled={!!record.isSubmit}
              style={{
                textAlign: 'center',
                display: elementPermissionList.includes('修改') ? 'block' : 'none',
              }}
            >
              {!!record.isSubmit ? (
                '修改'
              ) : (
                <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
              )}
            </Menu.Item>
            <Menu.Item style={{ textAlign: 'center' }}>
              <a onClick={() => this.handleDetailModalVisible(true, record, this)}>查看</a>
            </Menu.Item>
            <Menu.Item
              style={{
                textAlign: 'center',
                display: elementPermissionList.includes('导出') ? 'block' : 'none',
              }}
            >
              <a onClick={() => this.export(record)}>导出</a>
            </Menu.Item>
            {/*<Menu.Item disabled={record.isPutIn || !record.isSubmit ? true : false } style={{textAlign: 'center'}}>*/}
            <Menu.Item
              disabled={!(record.isSubmit && !record.isPutIn)}
              style={{
                textAlign: 'center',
                display: elementPermissionList.includes('生成盈亏单') ? 'block' : 'none',
              }}
            >
              {/*{!(record.isSubmit && !record.isPutIn) ? (*/}
              {record.isSubmit ? (
                '生成盈亏单'
              ) : (
                <a onClick={() => this.recordPutIn(record)}>生成盈亏单</a>
              )}
              {/*<a onClick={() => this.recordPutIn(record)}>生成盈亏单</a>*/}
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement="bottomRight">
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        );
      },
    },
  ];

  getElementPermissionModule = (path, array) => {
    let result = '';
    for (let item of array) {
      if (item.route && item.route === path) {
        result = item;
        break;
      } else {
        // console.log('result-dj: ', result);
        if (item.childList && item.childList.length > 0) {
          result = this.getElementPermissionModule(path, item.childList);
          if (result) break;
        }
      }
    }
    return result;
  };

  getElementPermissionList = (pathname, permissionList) => {
    let elementPermissionList = [];
    if (
      permissionList &&
      permissionList.length > 0 &&
      permissionList[0] &&
      permissionList[0].childList
    ) {
      const realPermissionList = permissionList[0].childList;
      if (pathname !== '/') {
        const elementPermissionModule = this.getElementPermissionModule(
          pathname,
          realPermissionList
        );
        // console.log('isExistInPermissionList: ', isExistInPermissionList);
        if (elementPermissionModule && elementPermissionModule.childList) {
          //元素权限列表
          elementPermissionList = elementPermissionModule.childList;
        }
      }
    }
    if (elementPermissionList.length > 0) {
      elementPermissionList = elementPermissionList.map(item => item.name);
    }
    return elementPermissionList;
  };

  export = record => {
    const {
      user: { currentUser },
    } = this.props;
    window.location.href = `${apiDomainName}/server/checkList/export?checkListId=${
      record.checkListId
    }&personnelId=${currentUser.personnelId}`;
  };

  recordPutIn = record => {
    const { dispatch } = this.props;
    const { expandedRowKeys } = this.state;

    const checkListId = record.checkListId;
    dispatch({
      type: 'checkOrder/putInCheckOrder',
      payload: {
        checkListId,
      },
      callback: () => {
        //1、刷新申请单列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、刷新申请单明细列表
        if (expandedRowKeys.length > 0) {
          this.onExpand(true, record);
        }
      },
    });
  };

  recordSubmit = checkListIds => {
    console.log('checkListIds: ', checkListIds);
    const { dispatch } = this.props;
    const { record, expandedRowKeys } = this.state;
    dispatch({
      type: 'checkOrder/submitCheckOrders',
      payload: { checkListIds },
      callback: () => {
        //1、刷新列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、清空selectedRows
        this.setState({ selectedRows: [] });

        //3、刷新申请单明细列表
        if (expandedRowKeys.length > 0) {
          this.onExpand(true, record);
        }
      },
    });
  };

  recordRemove = checkListId => {
    console.log('checkListId: ', checkListId);
    const { dispatch } = this.props;
    dispatch({
      type: 'checkOrder/removeCheckOrder',
      payload: { checkListId },
      callback: () => {
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        this.setState({ selectedRows: [] });
      },
    });
  };

  expandedRowColumnRender = (text, record) => {
    console.log('text: ', text);
    console.log('record: ', record);
    let result = text;
    switch (record.profitAndLossStatus) {
      case '盘亏':
        result = <span style={{ color: 'red' }}>{text}</span>;
        break;
      case '盘盈':
        result = <span style={{ color: 'blue' }}>{text}</span>;
        break;
    }
    return result;
  };

  expandedRowRender = row => {
    console.log('单据的record: ', row);
    const {
      checkOrder: { checkInventoryList },
    } = this.props;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        // width: 60,
        // fixed: 'left',
        render: (text, record, index) => (
          <span
            style={{ color: record.profitAndLossStatus === '盘亏' ? 'red' : 'rgba(0, 0, 0, 0.65)' }}
          >
            {index + 1}
          </span>
        ),
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        // width: 100,
        // fixed: 'left',
        render: this.expandedRowColumnRender,
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        // width: 150,
        // fixed: 'left',
        render: this.expandedRowColumnRender,
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
        render: this.expandedRowColumnRender,
      },
      // {
      //   title: '单位',
      //   dataIndex: 'units',
      //   render: (text, record) => <span style={{color: record.profitAndLossStatus === '盘亏' ? 'red' : 'rgba(0, 0, 0, 0.65)'}}>{text}</span>
      // },
      {
        title: '主单位',
        dataIndex: 'units',
        render: this.expandedRowColumnRender,
      },
      {
        title: '账面主数量',
        dataIndex: 'number',
        render: this.expandedRowColumnRender,
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
        render: this.expandedRowColumnRender,
      },
      {
        title: '账面辅数量',
        dataIndex: 'number2',
        render: this.expandedRowColumnRender,
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
        render: this.expandedRowColumnRender,
      },
      {
        title: '实盘主数量',
        dataIndex: 'checkNumber',
        render: this.expandedRowColumnRender,
      },

      {
        title: '分类',
        dataIndex: 'stockClassify',
        render: this.expandedRowColumnRender,
      },
      {
        title: '品牌',
        dataIndex: 'brand',
        render: this.expandedRowColumnRender,
      },
      {
        title: '供应商',
        dataIndex: 'supplier',
        render: this.expandedRowColumnRender,
      },
      {
        title: '盈亏数量',
        dataIndex: 'profitAndLoss',
        render: this.expandedRowColumnRender,
      },
      {
        title: '盈亏状态',
        dataIndex: 'profitAndLossStatus',
        render: this.expandedRowColumnRender,
      },
      {
        title: '批次',
        dataIndex: 'batchNumber',
        render: this.expandedRowColumnRender,
      },
      {
        title: '出入库单号',
        dataIndex: 'otherPutOrderNumber',
        render: (text, record) => (
          <span>
            {
              { 盘盈: `入库：${text ? text : '未入库'}`, 盘亏: `出库：${text ? text : '未出库'}` }[
                record.profitAndLossStatus
              ]
            }
          </span>
        ),
      },
      {
        title: '备注',
        dataIndex: 'remark',
        editable: true,
        render: this.expandedRowColumnRender,
      },
      {
        title: '操作',
        key: 'operation',
        width: 70,
        render: (text, record) => {
          const { dataPermission } = this.state;
          let result = null;
          let pathname = '';
          switch (record.profitAndLossStatus) {
            case '盘盈':
              result = '入库';
              pathname = '/stock-management/other-out-in/other-in';
              break;
            case '盘亏':
              result = '出库';
              pathname = '/stock-management/other-out-in/other-out';
              break;
          }
          return dataPermission && row.isSubmit && !record.otherPutOrderNumber ? (
            <a
              href="javascript:;"
              onClick={() => {
                router.push({
                  pathname,
                  query: {
                    source: 'CheckOrderList',
                    record: {
                      ...record,
                      branchId: row.branchId,
                      checkOrderNumber: row.orderNumber,
                    },
                  },
                });
              }}
            >
              {result}
            </a>
          ) : (
            ''
          );
        },
      },
    ];

    const total = () => {
      let result = `合计: 实盘主数量0`;
      if (checkInventoryList.length > 0) {
        const ping = checkInventoryList
          .map(item => item.checkNumber || 0)
          .reduce((a, b) => parseInt(a) + parseInt(b));
        result = `合计: 实盘主数量${ping}`;
      }
      return result;
    };

    return (
      <Table
        size="small"
        columns={columns}
        dataSource={checkInventoryList}
        footer={total}
        // footer={() => `合计: ${checkInventoryList.length}`}
        rowKey="warehouseStockId"
      />
    );
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;

    //盘点单列表
    this.fetchCheckOrderList();

    //门店列表
    dispatch({
      type: 'checkOrder/fetchStoreList',
    });

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      this.fetchWarehouseList();
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId = currentUser.personnel.branchId;
      if (branchId || branchId === 1) {
        this.fetchWarehouseList({
          branchId,
        });
      }
    }
  }

  fetchCheckOrderList = (payload = {}) => {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      //盘点单列表
      dispatch({
        type: 'checkOrder/fetchCheckOrderList',
        payload,
      });
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId = currentUser.personnel.branchId;
      if (branchId || branchId === 1) {
        dispatch({
          type: 'checkOrder/fetchCheckOrderList',
          payload: {
            ...payload,
            branchId,
          },
        });

        //仓库列表
        // dispatch({
        //   type: 'checkOrder/fetchWarehouseList',
        //   payload,
        //   callback: warehouseList => {
        //     const defaultWarehouse = warehouseList.rows.find(
        //       item => item.branchId === currentUser.personnel.branchId
        //     );
        //     if (defaultWarehouse) {
        //       //盘点单列表
        //       dispatch({
        //         type: 'checkOrder/fetchCheckOrderList',
        //         payload: {
        //           ...payload,
        //           warehouseId: defaultWarehouse.warehouseId,
        //         },
        //       });
        //     }
        //   },
        // });

        //获取当前部门的人员列表
        // dispatch({
        //   type: 'checkOrder/fetchStaffList',
        //   payload: {
        //     branchId,
        //   },
        // });
      }
    }
  };

  fetchWarehouseList = (payload = {}) => {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;
    //仓库列表
    dispatch({
      type: 'checkOrder/fetchWarehouseList',
      payload,
      // callback: warehouseList => {
      //   const defaultWarehouse = warehouseList.rows.find(
      //     item => item.branchId === currentUser.personnel.branchId
      //   );
      //   if (defaultWarehouse) {
      //     this.setState({ warehouseId: defaultWarehouse.warehouseId });
      //   }
      // },
    });
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'checkOrder/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.fetchCheckOrderList({ ...form.getFieldsValue() });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.executeSearch(fieldsValue);
    });
  };

  executeSearch = fieldsValue => {
    const { dispatch } = this.props;
    const checkTime = fieldsValue.checkTime;
    if (checkTime) {
      fieldsValue.checkTime = checkTime.format('YYYY-MM-DD');
    }

    this.fetchCheckOrderList({ ...fieldsValue });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetail = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'checkOrder/updatePurchase',
      payload: { ...fields, purchaseRequisitionId: record.purchaseRequisitionId },
    });

    this.handleUpdateModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedRows, expandedRowKeys } = this.state;
    dispatch({
      // type: 'checkOrder/updateApplyOrder',
      type: 'checkOrder/updateCheckOrder',
      payload: { ...fields, checkListId: record.checkListId },
      callback: () => {
        //1、刷新申请单列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、刷新申请单明细列表
        if (expandedRowKeys.length > 0) {
          this.onExpand(true, record);
        }

        message.success('生成盈亏单成功！');
      },
    });

    //若该项被选择了，则需要取消对该项的选择
    if (fields.submit) {
      this.setState({
        selectedRows: selectedRows.filter(item => item.checkListId !== record.checkListId),
      });
    }

    this.handleUpdateModalVisible();
  };

  onWarehouseChange = warehouseId => {
    //根据warehouseId找branchId
    const {
      dispatch,
      checkOrder: { warehouseList },
      form,
    } = this.props;
    const target = warehouseList.rows.find(item => item.warehouseId === warehouseId);
    if (target) {
      //获取对应机构的员工列表
      dispatch({
        type: 'checkOrder/fetchStaffList',
        payload: {
          branchId: target.branchId,
        },
      });

      form.setFieldsValue({ checkBy: '' });
    }
  };

  exportList = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const checkTime = fieldsValue.checkTime;
      if (checkTime) {
        fieldsValue.checkTime = fieldsValue.checkTime.format('YYYY-MM-DD');
      }
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId,
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/checkList/exportList?${stringify(params)}`;
    });
  };

  renderSimpleForm() {
    const {
      checkOrder: { storeList, staffList, warehouseList },
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    const { purchasePersonnel, dataPermission } = this.state;

    const currentWarehouse = warehouseList.rows.find(
      item => item.branchId === currentUser.personnel.branchId
    );

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="订单编号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单编号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="盘点仓库">
              {getFieldDecorator('warehouseId', {
                // initialValue: dataPermission
                //   ? ''
                //   : currentWarehouse
                //     ? currentWarehouse.warehouseId
                //     : '',
              })(
                <Select
                  onChange={this.onWarehouseChange}
                  placeholder="请选择盘点仓库"
                  style={{ width: '100%' }}
                >
                  {warehouseList.rows.map(item => (
                    <Select.Option key={item.warehouseId} value={item.warehouseId}>
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="盘点人员">
              {getFieldDecorator('checkBy')(
                <Select placeholder="请选择盘点人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => (
                    <Option key={item.name} value={item.name}>
                      {item.name}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="盘点日期">
              {getFieldDecorator('checkTime')(
                <DatePicker style={{ width: '100%' }} placeholder="请选择盘点日期" />
              )}
            </FormItem>
          </Col>
          <Col md={16} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.exportList}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const checkListId = record.checkListId;
      dispatch({
        type: 'checkOrder/fetchCheckInventoryList',
        payload: {
          checkListIds: checkListId,
        },
      });

      this.setState({ record });
    }
  };

  onExpandedRowsChange = expandedRows => {
    this.setState({
      expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length - 1]] : [],
    });
  };

  onRowClick = record => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (record.isSubmit) return;

    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.purchaseRequisitionId === record.purchaseRequisitionId) {
          newSelectedRows = newSelectedRows.filter(
            item => item.purchaseRequisitionId !== record.purchaseRequisitionId
          );
          flag = false;
          break;
        }
      }
      if (flag) {
        newSelectedRows.push(record);
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return {
        selectedRows: newSelectedRows,
      };
    });
  };

  render() {
    const {
      checkOrder: { checkOrderList, warehouseList },
      loading,
    } = this.props;
    const {
      selectedRows,
      updateModalVisible,
      expandedRowKeys,
      record,
      detailModalVisible,
    } = this.state;

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
      handleDetail: this.handleDetail,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button
                icon="plus"
                type="primary"
                onClick={() => router.push('/stock-management/check-order')}
              >
                新增
              </Button>
              {/*<Button >审核</Button>*/}
              {selectedRows.length > 0 && (
                <span>
                  {/*<Button*/}
                  {/*onClick={() => this.recordPutIn( selectedRows.map(item => item.checkListId).join(','))}*/}
                  {/*>生成盈亏单</Button>*/}
                  <Button
                    onClick={() =>
                      this.recordSubmit(selectedRows.map(item => item.checkListId).join(','))
                    }
                  >
                    批量提交
                  </Button>
                  <Popconfirm
                    title="确认删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.checkListId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={checkOrderList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              rowKey="checkListId"
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
              onRow={record => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
              checkboxProps={record => ({
                disabled: record.isSubmit,
              })}
            />
          </div>
        </Card>
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          record={record}
          warehouseList={warehouseList}
        />
        <DetailForm {...detailMethods} detailModalVisible={detailModalVisible} record={record} />
      </div>
    );
  }
}

export default CheckOrderList;
