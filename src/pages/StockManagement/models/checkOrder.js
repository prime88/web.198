// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
  fetchOrderNumber,
  fetchCheckInventoryList,
  fetchDeliveryApplyOrderList,
  fetchCheckOrderList,
  fetchStockList,
  addCheckOrder,
  putInCheckOrder,
  removeCheckOrder,
  updateCheckOrder,
  submitCheckOrders,
  fetchLogList,
} from '@/services/checkOrder';
import { fetchChildBranchList, fetchStaffList } from '@/services/branch';
import { queryStoreList } from '@/services/store';
import { queryWarehouseList } from '@/services/warehouse';

import { message } from 'antd';

export default {
  namespace: 'checkOrder',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //订单编号
    orderNumber: '',
    //盘点单存货列表
    checkInventoryList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //部门列表
    branchList: {
      rows: [],
      pagination: {},
    },
    //调货申请单列表
    deliveryApplyOrderList: {
      rows: [],
      pagination: {},
    },
    //盘点仓库的默认值：当中登录员工所在门店部门所关联的仓库之一
    warehouseList: {
      rows: [],
      pagination: {},
    },
    //库存列表
    stockList: {
      rows: [],
      pagination: {},
    },
    //盘点单列表
    checkOrderList: {
      rows: [],
      pagination: {},
    },
    logList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOrderNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOrderNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchCheckInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchCheckInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveCheckInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchChildBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryApplyOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryApplyOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryApplyOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload, callback }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response-dj: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehouseList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStockList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStockList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStockList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchCheckOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchCheckOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveCheckOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchLogList({ payload, callback }, { call, put }) {
      const response = yield call(fetchLogList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveLogList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addCheckOrder({ payload, callback }, { call, put }) {
      const response = yield call(addCheckOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *putInCheckOrder({ payload, callback }, { call, put }) {
      const response = yield call(putInCheckOrder, payload);
      if (response.code === '0') {
        // message.success(response.msg);
        message.success('生成成功！');
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeCheckOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeCheckOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateCheckOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateCheckOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitCheckOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitCheckOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveOrderNumber(state, action) {
      return {
        ...state,
        orderNumber: action.payload,
      };
    },
    saveCheckInventoryList(state, action) {
      return {
        ...state,
        checkInventoryList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveDeliveryApplyOrderList(state, action) {
      return {
        ...state,
        deliveryApplyOrderList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
    saveStockList(state, action) {
      return {
        ...state,
        stockList: action.payload,
      };
    },
    saveCheckOrderList(state, action) {
      return {
        ...state,
        checkOrderList: action.payload,
      };
    },
    saveLogList(state, action) {
      return {
        ...state,
        logList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
