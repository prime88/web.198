// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
  fetchInOrderNumber,
  fetchOutOrderNumber,
  fetchPurchaseingPlanList,
  fetchPurchaseingInventoryList,
  fetchOtherOutOrderInventoryList,
  fetchOtherInOrderList,
  fetchOtherOutOrderList,
  addProcurementOrder,
  addArrivalOrder,
  removeInventory,
  removePurchaseOrder,
  removeProcurementOrder,
  removeArrivalOrder,
  removeOtherInOrder,
  removeOtherOutOrder,
  updatePurchase,
  fetchProcurementOrderList,
  fetchOtherInInventoryList,
  updateProcurementOrder,
  updateArrivalOrder,
  updateOtherInOrder,
  updateOtherOutOrder,
  fetchArrivalOrderList,
  addOtherInOrder,
  addOtherOutOrder,
  queryWarehouseList,
  submitOtherInOrders,
  submitOtherOutOrders,
  // changeInventoryClassificationStatus,
} from '@/services/otherOutIn';
import { fetchProcurementPlanList } from '@/services/procurementPlan';
import { querySupplierList } from '@/services/supplier';
import { queryStoreList } from '@/services/store';
import { queryDictionary } from '@/services/common-api';
import { fetchChildBranchList, fetchStaffList } from '@/services/branch';
import { fetchStockList } from '@/services/checkOrder'
import { message } from 'antd';

export default {
  namespace: 'otherOutIn',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //请购列表
    purchaseingPlanList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //入库订单编号
    inOrderNumber: '',
    //出库订单编号
    outOrderNumber: '',
    //请购项存货列表
    purchaseingInventoryList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //供应商列表
    supplierList: {
      rows: [],
      pagination: {},
    },
    //计划单列表
    procurementPlanList: {
      rows: [],
      pagination: {},
    },
    //采购订单列表
    procurementOrderList: {
      rows: [],
      pagination: {},
    },
    //订单明细列表
    procurementInventoryList: [],
    //采购到货列表
    arrivalOrderList: {
      rows: [],
      pagination: {},
    },
    //仓库列表
    warehouseList: {
      rows: [],
      pagination: {},
    },
    //入库单列表
    warehousingOrderList: {
      rows: [],
      pagination: {},
    },
    inTypeList: {
      rows: [],
      pagination: {},
    },
    outTypeList: {
      rows: [],
      pagination: {},
    },
    //部门列表
    branchList: {
      rows: [],
      pagination: {},
    },
    //库存列表
    stockList: {
      rows: [],
      pagination: {},
    },
    //其他出库单列表
    otherOutOrderList: {
      rows: [],
      pagination: {},
    },
    //其他出库单存货列表
    otherOutOrderInventoryList: [],
  },

  effects: {
    *fetchOtherOutOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOtherOutOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOtherOutOrderInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOtherOutOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOtherOutOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOtherOutOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchPurchaseingPlanList({ payload }, { call, put }) {
      const response = yield call(fetchPurchaseingPlanList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePurchaseingPlanList',
        payload: response.data,
      });
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInOrderNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchInOrderNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInOrderNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOutOrderNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchOutOrderNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOutOrderNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchPurchaseingInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchPurchaseingInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePurchaseingInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchSupplierList({ payload }, { call, put }) {
      const response = yield call(querySupplierList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSupplierList',
        payload: response.data,
      });
    },
    *fetchProcurementPlanList({ payload }, { call, put }) {
      const response = yield call(fetchProcurementPlanList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProcurementPlanList',
        payload: response.data,
      });
    },
    *fetchProcurementOrderList({ payload }, { call, put }) {
      const response = yield call(fetchProcurementOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProcurementOrderList',
        payload: response.data,
      });
    },
    *fetchOtherInInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOtherInInventoryList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveProcurementInventoryList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }else {
        message.error(response.msg);
      }
    },
    *fetchArrivalOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchArrivalOrderList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveArrivalOrderList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }else {
        message.error(response.msg);
      }
    },
    *fetchWarehouseList({ payload, callback }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehouseList',
        payload: response.data,
      });
      if (callback) callback(response.data)
    },
    *fetchOtherInOrderList({ payload }, { call, put }) {
      const response = yield call(fetchOtherInOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehousingOrderList',
        payload: response.data,
      });
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    // *fetchTagTypeList({ payload, callback }, { call, put }) {
    *fetchInTypeList({ payload, callback }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInTypeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOutTypeList({ payload, callback }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOutTypeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchChildBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStockList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStockList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStockList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addProcurementOrder({ payload, callback }, { call, put }) {
      const response = yield call(addProcurementOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addArrivalOrder({ payload, callback }, { call, put }) {
      const response = yield call(addArrivalOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addOtherInOrder({ payload, callback }, { call, put }) {
      const response = yield call(addOtherInOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addOtherOutOrder({ payload, callback }, { call, put }) {
      const response = yield call(addOtherOutOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeInventory({ payload, callback }, { call, put }) {
      const response = yield call(removeInventory, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removePurchaseOrder({ payload, callback }, { call, put }) {
      const response = yield call(removePurchaseOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeProcurementOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeProcurementOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeArrivalOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeArrivalOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeOtherInOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeOtherInOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeOtherOutOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeOtherOutOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateOtherOutOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateOtherOutOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updatePurchase({ payload, callback }, { call, put }) {
      const response = yield call(updatePurchase, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchPurchaseingPlanList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateProcurementOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateProcurementOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProcurementOrderList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateArrivalOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateArrivalOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchProcurementOrderList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateOtherInOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateOtherInOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchProcurementOrderList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitOtherInOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitOtherInOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchProcurementOrderList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitOtherOutOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitOtherOutOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    savePurchaseingPlanList(state, action) {
      return {
        ...state,
        purchaseingPlanList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveInOrderNumber(state, action) {
      return {
        ...state,
        inOrderNumber: action.payload,
      };
    },
    saveOutOrderNumber(state, action) {
      return {
        ...state,
        outOrderNumber: action.payload,
      };
    },
    savePurchaseingInventoryList(state, action) {
      return {
        ...state,
        purchaseingInventoryList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveSupplierList(state, action) {
      return {
        ...state,
        supplierList: action.payload,
      };
    },
    saveProcurementPlanList(state, action) {
      return {
        ...state,
        procurementPlanList: action.payload,
      };
    },
    saveProcurementOrderList(state, action) {
      return {
        ...state,
        procurementOrderList: action.payload,
      };
    },
    saveProcurementInventoryList(state, action) {
      return {
        ...state,
        procurementInventoryList: action.payload,
      };
    },
    saveArrivalOrderList(state, action) {
      return {
        ...state,
        arrivalOrderList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
    saveWarehousingOrderList(state, action) {
      return {
        ...state,
        warehousingOrderList: action.payload,
      };
    },
    saveInTypeList(state, action) {
      return {
        ...state,
        inTypeList: action.payload,
      };
    },
    saveOutTypeList(state, action) {
      return {
        ...state,
        outTypeList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveStockList(state, action) {
      return {
        ...state,
        stockList: action.payload,
      };
    },
    saveOtherOutOrderList(state, action) {
      return {
        ...state,
        otherOutOrderList: action.payload,
      };
    },
    saveOtherOutOrderInventoryList(state, action) {
      return {
        ...state,
        otherOutOrderInventoryList: action.payload,
      };
    },

  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
