import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  Table,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import router from 'umi/router';
import styles from './CheckOrderAdd.less';
import { fetchStaffList } from '@/services/branch';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ checkOrder, user, loading }) => ({
  checkOrder,
  user,
  loading: loading.models.checkOrder,
}))
@Form.create()
class CheckOrderAdd extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'warehouseStockChangeId',
      width: 100,
      fixed: 'left'
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 150,
      fixed: 'left'
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left'
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    // {
    //   title: '单位',
    //   dataIndex: 'units',
    // },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  expandedRowRender = record => {
    const {
      checkOrder: { applyInventoryList },
    } = this.props;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        width: 60,
        fixed: 'left',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        width: 100,
        fixed: 'left',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        width: 200,
        fixed: 'left',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '单位',
        dataIndex: 'units',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      {
        title: '数量',
        dataIndex: 'number',
        editable: true,
      },
      {
        title: '需求日期',
        dataIndex: 'demandTime',
        editable: true,
      },
      {
        title: '供应商',
        dataIndex: 'supplier',
      },
      {
        title: '备注',
        dataIndex: 'remark',
        editable: true,
      },
    ];

    return <Table
      size='small'
      columns={columns}
      dataSource={applyInventoryList}
      footer={() => `合计: ${applyInventoryList.length}`}
      rowKey='daStockId'
    />
  };

  componentDidMount() {
    const { dispatch, user: { currentUser }, warehouseId } = this.props;
    //库存列表
    dispatch({
      type: 'checkOrder/fetchStockList',
      payload: {
        warehouseId
      }
    });

    //门店列表
    dispatch({
      type: 'checkOrder/fetchStoreList',
    });

    //获取当前部门请购人员列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'checkOrder/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //仓库列表
    // dispatch({
    //   type: 'checkOrder/fetchWarehouseList',
    // })
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'checkOrder/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch, warehouseId } = this.props;
    form.resetFields();
    this.setState({ purchasePersonnel: [] })
    dispatch({
      type: 'checkOrder/fetchStockList',
      payload: {
        warehouseId
      }
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.executeSearch(fieldsValue)
    });
  };

  executeSearch = (fieldsValue) => {
    const { dispatch, warehouseId } = this.props;
    dispatch({
      type: 'checkOrder/fetchStockList',
      payload: { ...fieldsValue, warehouseId },
    });
  }

  renderSimpleForm() {
    const {
      checkOrder: { storeList, staffList, warehouseList },
      form: { getFieldDecorator },
    } = this.props;
    const { purchasePersonnel } = this.state;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="存货编号">
              {getFieldDecorator('stockNum')(<Input placeholder="请输入存货编号" />)}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="存货名称">
              {getFieldDecorator('name')(<Input placeholder="请输入存货名称" />)}
            </FormItem>
          </Col>
          {/*<Col md={7} sm={24}>*/}
            {/*<FormItem label="仓库">*/}
              {/*{getFieldDecorator('warehouseId')(*/}
                {/*<Select placeholder="请选择盘点仓库" style={{ width: '100%' }}>*/}
                  {/*{warehouseList.rows.map(item => <Option value={item.warehouseId}>{item.name}</Option>)}*/}
                {/*</Select>*/}
              {/*)}*/}
            {/*</FormItem>*/}
          {/*</Col>*/}
          <Col md={7} sm={24}>
            <FormItem label={<span>批&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 次</span>}>
              {getFieldDecorator('batchNumber')(<Input placeholder="请输入批次" />)}
            </FormItem>
          </Col>
          <Col md={4} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const distributionApplyId = record.distributionApplyId;
      dispatch({
        type: 'checkOrder/fetchApplyInventoryList',
        payload: {
          distributionApplyIds: distributionApplyId
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (record.isSubmit) return;

    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.warehouseStockChangeId === record.warehouseStockChangeId) {
          newSelectedRows = newSelectedRows.filter(item => item.warehouseStockChangeId !== record.warehouseStockChangeId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  onSelectAll = (selected, selectedRows, changeRows, dataSource) => {
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    console.log('changeRows: ', changeRows);
    console.log('dataSource: ', dataSource);
    // const warehouseStockChangeIds = dataSource.filter(item => item.number !== 0).map(item => item.warehouseStockChangeId)
    //去掉库存为0的库存
    // const newSelectedRows = dataSource.filter(item => item.number !== 0)
    console.log('dataSource: ', dataSource);
    let newSelectedRows = [];
    if (selected) {
      newSelectedRows = dataSource;
    }
    this.setState({ selectedRows: newSelectedRows })
  }

  render() {
    const {
      checkOrder: { stockList },
      loading,
    } = this.props;
    const { selectedRows, expandedRowKeys } = this.state;

    const total = () => {
      let result = `合计: 主数量0 / 辅数量0 / 总计0条，已选中0条`;
      if (stockList.rows.length > 0) {
        const xiang = stockList.rows.map(item => item.number2 || 0).reduce((a, b) => a+b);
        const ping = stockList.rows.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
        result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100} / 总计${stockList.rows.length}条，已选中${selectedRows.length}条`
      }
      return result
    }

    return (
      <div>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          <StandardTable
            selectedRows={selectedRows}
            loading={loading}
            data={stockList}
            columns={this.columns}
            onSelectRow={this.handleSelectRows}
            onChange={this.handleStandardTableChange}
            // expandedRowKeys={expandedRowKeys}
            // expandedRowRender={this.expandedRowRender}
            rowKey='warehouseStockChangeId'
            // onExpand={this.onExpand}
            // onExpandedRowsChange={this.onExpandedRowsChange}
            onRow={(record) => {
              return {
                onClick: () => this.onRowClick(record),
              };
            }}
            scroll={{x: 1800}}
            // checkboxProps={record => ({
            //   disabled: record.isSubmit,
            // })}
            size='small'
            footer={total}
            // footer={() => `合计: ${stockList.rows.length}`}
            onSelectAll={this.onSelectAll}
          />
        </div>
      </div>
    );
  }
}

export default CheckOrderAdd;
