import React, { Component } from 'react';
import { connect } from 'dva';
import { Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import styles from './CheckOrderListDetail.less';

const { Description } = DescriptionList;

@connect(({ checkOrder, loading }) => ({
  checkOrder,
  // loading: loading.effects['profile/fetchBasic'],
}))
class CheckOrderListDetail extends Component {
  state = {
    dataSource: []
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const checkListId = record.checkListId;
    if (checkListId) {
      dispatch({
        type: 'checkOrder/fetchCheckInventoryList',
        payload: {
          checkListIds: checkListId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }
  }

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },

    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '账面主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '账面辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '实盘主数量',
      dataIndex: 'checkNumber',
    },

    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    // {
    //   title: '账面数量',
    //   dataIndex: 'number',
    // },
    // {
    //   title: '实盘数量',
    //   dataIndex: 'checkNumber',
    // },
    {
      title: '盈亏数量',
      dataIndex: 'profitAndLoss',
    },
    {
      title: '盈亏状态',
      dataIndex: 'profitAndLossStatus',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse'
    },
    // {
    //   title: '价格',
    //   // dataIndex: 'referenceCost',
    //   dataIndex: 'warehousePrice',
    // },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="盘点人员">{record.checkBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="盘点仓库">{record.warehouse}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="盘点日期">{record.checkTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>盘点明细</div>
          <Table
            style={{ marginBottom: 24 }}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="warehouseStockId"
            scroll={{x: 1500}}
            pagination={{defaultPageSize: 9}}
            size='small'
          />
        </div>
      </div>
    );
  }
}

export default CheckOrderListDetail;
