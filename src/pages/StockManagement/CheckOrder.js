import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Divider,
  Table,
  Popconfirm,
} from 'antd';
import CheckOrderAdd from './CheckOrderAdd'
import styles from './CheckOrder.less';
import moment from 'moment'
import { fetchStaffList } from '@/services/branch';
import EditableCell from '@/customComponents/EditableCell';

//单元格编辑--------------------------------------------
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
//-----------------------------------------------------

const FormItem = Form.Item;
const { Option } = Select;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, warehouseId } = props;
  let checkOrderAdd;
  const okHandle = () => {
    const { selectedRows } = checkOrderAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = (ref) => {
    checkOrderAdd = ref;
  }

  return (
    <Modal
      width={'90%'}
      destroyOnClose
      // title="新增盘点明细"
      title={(
        <div>
          <span>新增盘点明细</span>
          <Button style={{float: 'right', marginRight: 50}} type='primary' onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{top: 20}}
    >
      <CheckOrderAdd wrappedComponentRef={addModalRef} warehouseId={warehouseId} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改盘点明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="实盘数量">
        {form.getFieldDecorator('checkNumber', {
          // rules: [{ required: true, message: '请输入实盘数量！' }],
          initialValue: record.checkNumber,
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入实盘数量" min={0} />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ checkOrder, user, loading }) => ({
  checkOrder,
  user,
  loading: loading.models.checkOrder,
}))
@Form.create()
class CheckOrder extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    warehouseId: '',
  };

  columns = [
    // {
    //   title: '序号',
    //   dataIndex: '',
    //   width: 70,
    //   fixed: 'left',
    //   render: (text, record, index) => index+1
    // },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 120,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '账面主数量',
      dataIndex: 'number',
      // editable: true,
      // isInputNumber: true,
      // inputNumberPrecision: 0,
      // inputNumberMin: 1,
      // width: 120,
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '账面辅数量',
      dataIndex: 'number2',
      // editable: true,
      // isInputNumber: true,
      // inputNumberPrecision: 2,
      // inputNumberMin: 0,
      // width: 120,
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    // {
    //   title: '账面数量',
    //   dataIndex: 'number',
    // },
    {
      title: '实盘主数量',
      dataIndex: 'checkNumber',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 0,
      inputNumberMin: 0,
      width: 120,
      render: text => <span style={{color: 'red'}}>{text}</span>
      // render: (text, record) => <span style={{color: 'red'}}>{record.number}</span>
    },
    // {
    //   title: '价格',
    //   // dataIndex: 'referenceCost',
    //   dataIndex: 'warehousePrice',
    // },
    {
      title: '批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
      width: 200
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.warehouseStockChangeId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = (warehouseStockChangeId) => {
    console.log('warehouseStockChangeId: ', warehouseStockChangeId);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.warehouseStockChangeId !== warehouseStockChangeId) })
    message.success('删除成功');
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'checkOrder/fetchOrderNumber',
    });


    // const personnel = currentUser.personnel;
    // if (personnel) {
    //   //获取当前部门员工列表
    //   dispatch({
    //     type: 'checkOrder/fetchStaffList',
    //     payload: {
    //       branchId: personnel.branchId,
    //     },
    //   });
    // }

    //部门列表
    dispatch({
      type: 'checkOrder/fetchBranchList',
      payload: {
        isWarehouse: true
      }
    });

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      this.fetchWarehouseList();
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId =  currentUser.personnel.branchId;
      if (branchId || branchId === 1) {
        this.fetchWarehouseList({
          branchId
        });
      }
    }
  }

  fetchWarehouseList = (payload = {}) => {
    const { dispatch, user: { currentUser } } = this.props;
    //仓库列表
    dispatch({
      type: 'checkOrder/fetchWarehouseList',
      payload,
      callback: data => {
        const warehouseList = data.rows;
        if (warehouseList.length > 0) {
          const firstWarehouse = warehouseList[0];
          this.onWarehouseChange(firstWarehouse.warehouseId);
        }
      }
    })
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ dataSource: [] })
  };

  submit = (isSubmit) => {
    const { dispatch, form, user: { currentUser }, checkOrder: { warehouseList } } = this.props;
    const { dataSource } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增盘点明细');
        return;
      }
      else if (dataSource.find(item => !item.checkNumber && isNaN(item.checkNumber))) {
        message.warning('请设置实盘主数量');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.checkTime = fieldsValue.checkTime.format('YYYY-MM-DD');
      // fieldsValue.checkBy = currentUser.personnel.name;
      fieldsValue.warehouse = warehouseList.rows.find(item => item.warehouseId === fieldsValue.warehouseId).name;
      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'checkOrder/addCheckOrder',
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [] })

          //更新订单编号
          dispatch({
            type: 'checkOrder/fetchOrderNumber',
          });
        }
      });
    });
  }

  handleSearch = (e) => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //1.同一个库存商品，直接覆盖；2.不同库存，push
      if (oldDataSource.length > 0){
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            // if (newItem.stockNum.toString() === oldItem.stockNum.toString() && newItem.warehousePrice.toString() === oldItem.warehousePrice.toString()) {
            if (newItem.stockNum.toString() === oldItem.stockNum.toString() && newItem.batchNumber.toString() === oldItem.batchNumber.toString()) {
              // oldItem.number++;
              // const index = oldDataSource.findIndex(item => item.warehouseStockChangeId === oldItem.warehouseStockChangeId);
              // if (index > -1) {
              //   oldDataSource[index] = newItem;
              // }

              flag = false;
              break;
            }
          }
          if (flag) {
            // oldDataSource.push({ ...newItem, number: 1 })
            oldDataSource.push(newItem)
          }
        }
      }
      else{
        // oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1 }))];
        // for (let newItem of selectedRows) {
        //   oldDataSource.push(newItem)
        // }
        oldDataSource = [...selectedRows]
      }

      // let oldDataSource = [...prevState.dataSource];
      // for (let newItem of selectedRows) {
      //   oldDataSource.push(newItem)
      // }
      return {
        // dataSource: [...selectedRows.map(item => ({ ...item, number: 1 }))]
        dataSource: oldDataSource.map(item => ({ ...item, checkNumber: item.checkNumber ? item.checkNumber : item.number }))
      }
    });

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.warehouseStockChangeId === record.warehouseStockChangeId);
    if (index > -1){
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });
      this.setState({ dataSource: newData });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    }
    else{
      message.error('修改失败');
    }
  };

  onWarehouseChange = (warehouseId, e) => {
    this.setState({ dataSource: [], warehouseId });

    //根据warehouseId找branchId
    const { dispatch, checkOrder: { warehouseList }, form } = this.props;
    const target = warehouseList.rows.find(item => item.warehouseId === warehouseId);
    if (target) {
      //获取对应机构的员工列表
      dispatch({
        type: 'checkOrder/fetchStaffList',
        payload: {
          branchId: target.branchId,
        },
      });

      if (e) {
        form.setFieldsValue({ checkBy: '' });
      }
    }
  }

  renderTopSimpleForm() {
    const {
      checkOrder: { orderNumber, staffList, branchList, warehouseList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;

    //盘点仓库的默认值：当中登录员工所在门店部门所关联的仓库之一
    // const defaultWarehouse = warehouseList.rows.find(item => item.branchId === currentUser.personnel.branchId);

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;订单编号</span>}>
              {orderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="盘点仓库">
              {getFieldDecorator('warehouseId', {
                rules: [{ required: true, message: '请选择盘点仓库' }],
                // initialValue: defaultWarehouse ? defaultWarehouse.warehouseId : ''
                initialValue: warehouseList.rows.length > 0 ? warehouseList.rows[0].warehouseId : ''
              })(
                <Select onChange={this.onWarehouseChange} placeholder="请选择盘点仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => <Option value={item.warehouseId}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          {/*<Col md={6} sm={24}>*/}
            {/*<FormItem label="盘点人员">*/}
              {/*{currentUser.personnel.name}*/}
            {/*</FormItem>*/}
          {/*</Col>*/}
          <Col md={6} sm={24}>
            <FormItem label="盘点人员">
              {getFieldDecorator('checkBy', {
                rules: [{ required: true, message: '请选择盘点人员' }],
                // initialValue: currentUser.personnel.name
                initialValue: staffList.rows.length > 0 ? staffList.rows[0].name : ''
              })(
                <Select placeholder="请选择盘点人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="盘点日期">
              {getFieldDecorator('checkTime', {
                rules: [{ required: true, message: '请选择盘点日期' }],
                initialValue: moment(new Date(), 'YYYY-MM-DD')
              })(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择盘点日期"
                  format="YYYY-MM-DD"
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
      loading
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">
              {currentUser.personnel.name}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
              <Button style={{ marginRight: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button disabled={loading} loading={loading} style={{ marginRight: 8 }} onClick={() => this.submit(false)}>
                保存
              </Button>
              {/*<Button type="primary" htmlType='submit' >*/}
                {/*提交*/}
              {/*</Button>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.warehouseStockChangeId === item.warehouseStockChangeId);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  }

  total = () => {
    const { dataSource } = this.state;
    let result = `合计: 实盘主数量0 / 总计0条`;
    if (dataSource.length > 0) {
      const ping = dataSource.map(item => item.checkNumber || 0).reduce((a, b) => parseInt(a)+parseInt(b));
      result = `合计: 实盘主数量${ping} / 总计${dataSource.length}条`
    }
    return result
  }

  onAddClick = () => {
    const { warehouseId } = this.state;
    if (!warehouseId) {
      message.warning('请先选择盘点仓库!');
      return;
    }
    this.handleModalVisible(true)
  }

  render() {
    const { modalVisible, dataSource, updateModalVisible, record, warehouseId } = this.state;
    console.log('dataSource: ', dataSource);

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          isInputNumber: col.isInputNumber,
          inputNumberPrecision: col.inputNumberPrecision,
          inputNumberMin: col.inputNumberMin,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              盘点明细
              <Button
                type="primary"
                style={{ marginLeft: 16 }}
                // icon="plus"
                // onClick={() => this.handleModalVisible(true)}
                onClick={this.onAddClick}
              >
                新增
              </Button>
            </h2>
            <Divider />
            <Table
              components={components}
              columns={columns}
              rowKey='warehouseStockChangeId'
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              scroll={{x: 2000}}
              footer={this.total}
              // footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{paddingTop: 24}} className={styles.tableListForm}>{this.renderBottomSimpleForm()}</div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} warehouseId={warehouseId} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
      </div>
    );
  }
}

export default CheckOrder;
