import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  Upload,
  Icon,
  message,
  Spin,
  Checkbox,
  Popover,
  Modal,
  Table,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import router from 'umi/router';
import styles from './StockList.less';
import { fetchStaffList } from '@/services/branch';
import {apiDomainName} from "../../constants";
import { stringify } from 'qs';


const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ checkOrder, user, loading }) => ({
  checkOrder,
  user,
  loading: loading.models.checkOrder,
}))
@Form.create()
class StockList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
    importSpin: false,
    logVisible: false
  };

  columns = [
    // {
    //   title: '编号',
    //   // dataIndex: 'warehouseStockId',
    //   dataIndex: 'warehouseStockChangeId',
    //   width: 100,
    //   fixed: 'left',
    // },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
      render: (text, record) => <a onClick={() => this.onNameClick(record)}>{text}</a>
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
      render: text => parseInt(text*100)/100
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  logColumns = [
    {
      title: '类型',
      dataIndex: 'type',
      key: 'type',
      render: (text, record) => (
        {
          0: '采购入库',
          1: '调货入库',
          2: '调货出库',
          3: '门店销售退货入库',
          4: '门店销售取消入库',
          5: '门店销售出库',
          6: 'APP订单退货入库',
          7: 'APP销售出库',
          8: `其他入库${{1: '(盘盈入库)', 2: '(赠品入库)', 3: ''}[record.otherTypeCode]}`,
          9: `其他出库${{1: '(盘亏出库)', 2: '(赠品出库)', 3: ''}[record.otherTypeCode]}`,
          10: 'excel导入入库',
          11: 'APP订单取消后自动入库',
        }[text]
      )
    },
    {
      title: '主数量',
      dataIndex: 'number',
      key: 'number',
    },
    {
      title: '操作人',
      dataIndex: 'operationBy',
      key: 'operationBy',
    },
    {
      title: '操作时间',
      dataIndex: 'createTime',
      key: 'createTime',
    },
  ];

  onNameClick = (record) => {
    console.log('record: ', record);
    this.setState({ logVisible: true, record })
    // 获取商品出入库记录
    const { dispatch } = this.props;
    dispatch({
      type: 'checkOrder/fetchLogList',
      payload: {
        stockNum: record.stockNum,
        warehouseId: record.warehouseId
      }
    });
  }

  fetchStockList = (payload = {}) => {
    const { dispatch, user: { currentUser } } = this.props;

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    // const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : []; // 该页面存在数据权限时
    const target = true; // 该页面不存在数据权限时
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      dispatch({
        type: 'checkOrder/fetchStockList',
        payload
      });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId =  currentUser.personnel.branchId;
      dispatch({
        type: 'checkOrder/fetchStockList',
        payload: { ...payload, branchId }
      });
    }
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;
    //库存列表
    this.fetchStockList();

    //门店列表
    dispatch({
      type: 'checkOrder/fetchStoreList',
    });

    //获取当前部门请购人员列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'checkOrder/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //存货分类列表
    dispatch({
      type: 'checkOrder/fetchInventoryClassificationList',
    });

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    // const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : []; // 该页面存在数据权限时
    const target = true; // 该页面不存在数据权限时
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      this.fetchWarehouseList();
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId =  currentUser.personnel.branchId;
      if (branchId || branchId === 1) {
        this.fetchWarehouseList({
          branchId
        });
      }
    }
  }

  fetchWarehouseList = (payload = {}) => {
    const { dispatch, user: { currentUser } } = this.props;
    //仓库列表
    dispatch({
      type: 'checkOrder/fetchWarehouseList',
      payload,
      // callback: warehouseList => {
      //   const defaultWarehouse = warehouseList.rows.find(item => item.branchId === currentUser.personnel.branchId);
      //   if (defaultWarehouse) {
      //     this.setState({ warehouseId: defaultWarehouse.warehouseId })
      //   }
      // }
    })
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'checkOrder/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({ purchasePersonnel: [] })
    this.fetchStockList();
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!fieldsValue.isNotZero) {
        delete fieldsValue.isNotZero
      }
      this.executeSearch(fieldsValue)
    });
  };

  executeSearch = (fieldsValue) => {
    const { dispatch } = this.props;
    this.fetchStockList({ ...fieldsValue });
  }

  export = () => {
    const {
      form,
      user: { currentUser },
      dispatch,
    } = this.props;
    const { dataPermission } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      let isNotZeroObj= {};
      if (fieldsValue.isNotZero) {
        isNotZeroObj = { isNotZero: true }
      }
      else{
        delete fieldsValue.isNotZero
      }
      if (dataPermission) {//有数据权限
        const params = {
          ...fieldsValue,
          ...isNotZeroObj,
          personnelId: currentUser.personnelId,
        }
        // window.location.href = `${apiDomainName}/server/warehouseStock/exportList?${stringify(params)}`;
        window.open(`${apiDomainName}/server/warehouseStock/exportList?${stringify(params)}`);
        // dispatch({
        //   type: 'checkOrder/fetchStockList',
        //   payload: { ...fieldsValue, personnelId: currentUser.personnelId }
        // });
      }
      else {//没有数据权限
        const branchId = currentUser.personnel.branchId;
        const params = {
          ...fieldsValue,
          ...isNotZeroObj,
          branchId,
          personnelId: currentUser.personnelId,
        }
        // window.location.href = `${apiDomainName}/server/warehouseStock/exportList?${stringify(params)}`;
        window.open(`${apiDomainName}/server/warehouseStock/exportList?${stringify(params)}`);
      }
    });
  }

  renderSimpleForm() {
    const {
      checkOrder: { storeList, staffList, warehouseList, inventoryClassificationList },
      form: { getFieldDecorator },
    } = this.props;
    const that = this;

    const props = {
      name: 'excelFile',
      action: `${apiDomainName}/server/warehouseStock/readExcel`,
      headers: {
        token: localStorage.getItem('token'),
      },
      showUploadList: false,
      onChange(info) {
        if (info.file.status === 'uploading') {
          console.log(info.file, info.fileList);
          that.setState({ importSpin: true });
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} 导入成功,数据获取中...`);
          that.setState({ importSpin: false });
          //获取存货列表
          that.handleFormReset();

        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} 导入失败.`);
          that.setState({ importSpin: false });
        }
      },
    };

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="存货编号">
              {getFieldDecorator('stockNum')(<Input placeholder="请输入存货编号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="存货名称">
              {getFieldDecorator('name')(<Input placeholder="请输入存货名称" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="存货仓库">
              {getFieldDecorator('warehouseId')(
                <Select allowClear placeholder="请选择存货仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => <Option value={item.warehouseId}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="存货分类">
              {getFieldDecorator('stockClassify')(
                <Select allowClear placeholder="请选择存货分类" style={{ width: '100%' }}>
                  {/*<Option value={'白酒'}>白酒</Option>*/}
                  {inventoryClassificationList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={<span>批&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 次</span>}>
              {getFieldDecorator('batchNumber')(<Input placeholder="请输入批次" />)}
            </FormItem>
          </Col>
          <Col md={2} sm={24} style={{paddingRight: 0}}>
            <Form.Item
              label={(
                <span>过滤&nbsp;
                  <Popover content={<div>过滤库存为0的商品</div>}>
                    <Icon type="question-circle" />
                  </Popover>
                </span>
              )}
            >
              {getFieldDecorator('isNotZero', {
                valuePropName: 'checked',
                initialValue: false
              })(
                <Checkbox />
              )}
            </Form.Item>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ float: 'right', marginRight: 0, marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
              {/*<Upload {...props}>*/}
                    {/*<Button style={{ marginLeft: 8 }}>*/}
                      {/*<Icon type="upload" />Excel导入*/}
                    {/*</Button>*/}
              {/*</Upload>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleOk = e => {
    console.log(e);
    this.setState({
      logVisible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      logVisible: false,
    });
  };

  render() {
    const {
      checkOrder: { stockList, logList },
      loading,
    } = this.props;

    const { importSpin, logVisible } = this.state;

    const total = () => {
      let result = `合计: 主数量0 / 辅数量0 `;
      if (stockList.rows.length > 0) {
        const xiang = stockList.rows.map(item => item.number2 || 0).reduce((a, b) => a+b);
        const ping = stockList.rows.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
        // result = `合计: 主数量${ping} / 辅数量${xiang}`;
        result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100} / 记录数${stockList.rows.length}`
      }
      return result
    }

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <Spin size="large" spinning={importSpin} tip="数据导入中，请稍后...">
              <StandardTable
                loading={loading}
                data={stockList}
                columns={this.columns}
                onChange={this.handleStandardTableChange}
                rowKey='warehouseStockId'
                scroll={{x: 1800}}
                footer={total}
                // footer={() => `合计: ${stockList.rows.length}`}
                size='small'
              />
            </Spin>
          </div>
        </Card>
        <Modal
          title="操作日志"
          visible={logVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          mask={false}
          footer={[
            <Button type='primary' onClick={this.handleCancel}>关闭</Button>
          ]}
          width={700}
        >
          <Table size="small" dataSource={logList.rows} columns={this.logColumns} />
        </Modal>
      </div>
    );
  }
}

export default StockList;
