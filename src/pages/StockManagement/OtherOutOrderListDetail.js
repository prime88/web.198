import React, { Component } from 'react';
import { connect } from 'dva';
import { Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import styles from './OtherOutOrderListDetail.less';

const { Description } = DescriptionList;

@connect(({ otherOutIn, loading }) => ({
  otherOutIn,
  // loading: loading.effects['profile/fetchBasic'],
}))
class OtherOutOrderListDetail extends Component {
  state = {
    dataSource: [],
  };

  componentDidMount() {
    const { dispatch, record } = this.props;
    const otherPutOutId = record.otherPutOutId;
    if (otherPutOutId) {
      dispatch({
        type: 'otherOutIn/fetchOtherOutOrderInventoryList',
        payload: {
          otherPutOutIds: otherPutOutId,
        },
        callback: data => {
          this.setState({ dataSource: data });
        },
      });
    }
  }

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index + 1,
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    // {
    //   title: '单位',
    //   dataIndex: 'units',
    // },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },

    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    // {
    //   title: '含税单价',
    //   dataIndex: 'referenceCost',
    //   // dataIndex: 'warehousePrice',
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="订单编号"
            >
              {record.orderNumber}
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="出库时间"
            >
              {record.putOutTime}
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="出库类型"
            >
              {record.type}
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="出库机构"
            >
              {record.sendOutInstitutions}
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="出库仓库"
            >
              {record.warehouse}
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="出库人员"
            >
              {record.putOutBy}
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="制单人员"
            >
              {record.createBy}
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }}
              term="备注"
            >
              {record.remark}
            </Description>
            <Description
              style={{
                display: record.checkOrderNumber ? 'block' : 'none',
                boxSizing: 'border-box',
                width: '33.33333333%',
                fontWeight: 'bold',
              }}
              term="盘点单号"
            >
              {record.checkOrderNumber}
            </Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>订单明细</div>
          <Table
            style={{ marginBottom: 24 }}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="dpoStockId"
            pagination={{ defaultPageSize: 9 }}
            size="small"
          />
        </div>
      </div>
    );
  }
}

export default OtherOutOrderListDetail;
