import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Divider,
  Table,
  Popconfirm,
  TreeSelect,
} from 'antd';
import OtherOutOrderAdd from './OtherOutOrderAdd';
import styles from './OtherOut.less';
import moment from 'moment';
import { fetchStaffList } from '@/services/branch';
// import DeliveryOutOrderReference from "../../Delivery/DeliveryOut/DeliveryOutOrderReference";
import EditableCell from '@/customComponents/EditableCell';

//单元格编辑--------------------------------------------
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
//-----------------------------------------------------

const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item;
const { Option } = Select;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, warehouseId } = props;
  let deliveryOutOrderAdd;
  const okHandle = () => {
    const { selectedRows } = deliveryOutOrderAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = ref => {
    deliveryOutOrderAdd = ref;
  };

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      // title="新增出库明细"
      title={
        <div>
          <span>新增出库明细</span>
          <Button style={{ float: 'right', marginRight: 50 }} type="primary" onClick={okHandle}>
            确定
          </Button>
        </div>
      }
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{ top: 20 }}
    >
      <OtherOutOrderAdd wrappedComponentRef={addModalRef} warehouseId={warehouseId} />
    </Modal>
  );
});

const ReferenceForm = Form.create()(props => {
  const { referenceModalVisible, handleReference, handleReferenceModalVisible } = props;
  let deliveryApplyOrderAdd;
  const okHandle = () => {
    const { dataSource } = deliveryApplyOrderAdd.state;
    handleReference(dataSource);
  };

  const referenceModalRef = ref => {
    deliveryApplyOrderAdd = ref;
  };

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="参照"
      visible={referenceModalVisible}
      onOk={okHandle}
      onCancel={() => handleReferenceModalVisible()}
      style={{ top: 20 }}
    >
      <DeliveryOutOrderReference wrappedComponentRef={referenceModalRef} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);

      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改调货明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="数量">
        {form.getFieldDecorator('number', {
          rules: [{ required: true, message: '请输入数量！' }],
          initialValue: record.number,
        })(
          <InputNumber precision={0} style={{ width: '100%' }} placeholder="请输入数量" min={1} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ otherOutIn, user, loading }) => ({
  otherOutIn,
  user,
  loading: loading.models.otherOutIn,
}))
@Form.create()
class OtherOut extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    referenceModalVisible: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    warehouseId: '',
    dataPermission: false,
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index + 1,
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
      editable: this.props.location.query.source === 'CheckOrderList' ? false : true, //盘点出入库跳转过来的不让编辑，目前如果可编辑，系统无法多次跟踪出入库记录
      // editable: true,
      isInputNumber: true,
      inputNumberPrecision: 0,
      inputNumberMin: 1,
      width: 120,
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
      editable: this.props.location.query.source === 'CheckOrderList' ? false : true, //盘点出入库跳转过来的不让编辑，目前如果可编辑，系统无法多次跟踪出入库记录
      // editable: true,
      isInputNumber: true,
      inputNumberPrecision: 2,
      inputNumberMin: 0,
      width: 120,
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    //   editable: true,
    //   isInputNumber: true,
    //   inputNumberPrecision: 0,
    //   inputNumberMin: 1,
    //   width: 120,
    // },
    // {
    //   title: '含税单价(¥)',
    //   // dataIndex: 'referenceCost',
    //   dataIndex: 'warehousePrice',
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '盘点单号',
      dataIndex: 'checkListOrderNumber',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
      width: 200,
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.ID)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = ID => {
    console.log('ID: ', ID);
    const dataSource = [...this.state.dataSource];
    this.setState({
      dataSource: dataSource
        .filter(item => item.ID !== ID)
        .map((item, index) => ({ ...item, ID: index })),
    });
    message.success('删除成功');
  };

  onSendOutInstitutionsChange = (value, e, callback) => {
    const {
      dispatch,
      user: { currentUser },
      form,
    } = this.props;
    const branchId = e.props.branchId;
    console.log('branchId: ', branchId);

    //出库仓库列表
    dispatch({
      type: 'otherOutIn/fetchWarehouseList',
      payload: {
        branchId: branchId,
      },
    });

    //出库人员列表
    dispatch({
      type: 'otherOutIn/fetchStaffList',
      payload: {
        branchId: branchId,
      },
    });

    form.setFieldsValue({
      warehouse: '',
      putOutBy: '',
    });

    if (callback) callback();
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'otherOutIn/fetchOutOrderNumber',
    });

    //出库类型列表
    dispatch({
      type: 'otherOutIn/fetchOutTypeList',
      payload: { dictCode: 'otherPutOutType' },
    });

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }
    this.onSendOutInstitutionsChange('', { props: { branchId: currentUser.personnel.branchId } });

    //部门列表
    dispatch({
      type: 'otherOutIn/fetchBranchList',
      payload: {
        isWarehouse: true,
      },
      callback: data => {
        const branchList = data.rows;
        const { query } = this.props.location;
        if (query.source === 'CheckOrderList') {
          let branchName = '';
          console.log('branchList: ', data);
          console.log('record: ', query.record);
          const branchTarget = branchList.find(item => item.branchId === query.record.branchId); //出库机构;
          if (branchTarget) branchName = branchTarget.name;
          console.log('branchName: ', branchName);
          this.props.form.setFieldsValue({
            sendOutInstitutions: branchName,
            // warehouse: query.record.warehouse,
          });
          this.onSendOutInstitutionsChange(
            '',
            { props: { branchId: query.record.branchId } },
            () => {
              this.props.form.setFieldsValue({
                // sendOutInstitutions: branchName,
                warehouse: query.record.warehouse,
                putOutBy: currentUser.personnel.name,
              });

              //出库人员列表
              dispatch({
                type: 'otherOutIn/fetchStaffList',
                payload: {
                  branchId: currentUser.personnel.branchId,
                },
              });
            }
          );
        }
      },
    });

    //盘点单列表跳转过来
    console.log('this.props: ', this.props);
    const { query } = this.props.location;
    if (query.source === 'CheckOrderList') {
      this.props.form.setFieldsValue({
        typeCode: 1,
        // supplier: query.record.supplier,
        // warehouse: query.record.warehouse,
        // batchNumber: query.record.batchNumber,
      });
      // this.onWarehouseChange(query.record.warehouse);
      this.setState({
        dataSource: [query.record].map(item => {
          const newNumber = parseInt(item.number) - parseInt(item.checkNumber);
          return {
            ...item,
            number: newNumber,
            number2: parseInt((newNumber / item.unitRatio) * 100) / 100,
          };
        }),
        warehouseId: query.record.warehouseId,
      });

      //通过warehouseId找branchId
      // const warehouseId = query.record.warehouseId;
    }
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ dataSource: [], warehouseId: '' });
  };

  submit = isSubmit => {
    const {
      dispatch,
      form,
      user: { currentUser },
      location,
    } = this.props;
    const { dataSource, warehouseId } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增出库明细');
        return;
      } else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的出库主数量或出库辅数量');
        return;
      }

      //含税单价：warehousePrice -> referenceCost
      const newDataSource = dataSource.map(item => ({
        ...item,
        referenceCost: item.warehousePrice,
      }));
      fieldsValue.stockJson = JSON.stringify(newDataSource);
      fieldsValue.putOutTime = fieldsValue.putOutTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.warehouseId = warehouseId;

      if (isSubmit) {
        const record = location.query.record;
        console.log('record: ', record);
        if (record && record.checkOrderNumber && record.clStockId) {
          fieldsValue.checkOrderNumber = record.checkOrderNumber;
          fieldsValue.clStockId = record.clStockId;
        }
      }

      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'otherOutIn/addOtherOutOrder',
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [], warehouseId: '' });

          //更新订单编号
          dispatch({
            type: 'otherOutIn/fetchOutOrderNumber',
          });
        },
      });
    });
  };

  handleSearch = e => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0) {
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            // if (newItem.stockNum.toString() === oldItem.stockNum.toString() && newItem.warehousePrice.toString() === oldItem.warehousePrice.toString()) {
            if (
              newItem.stockNum.toString() === oldItem.stockNum.toString() &&
              newItem.batchNumber.toString() === oldItem.batchNumber.toString()
            ) {
              // oldItem.number++;
              flag = false;
              break;
            }
          }
          if (flag) {
            // oldDataSource.push({ ...newItem, number: 1 })
            oldDataSource.push({ ...newItem });
          }
        }
      } else {
        // oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1 }))];
        oldDataSource = [...selectedRows];
      }
      return {
        // dataSource: [...selectedRows.map(item => ({ ...item, number: 1 }))]
        dataSource: oldDataSource.map((item, index) => ({ ...item, ID: index })),
      };
    });

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.ID === record.ID);
    if (index > -1) {
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });
      this.setState({ dataSource: newData.map((item, index) => ({ ...item, ID: index })) });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    } else {
      message.error('修改失败');
    }
  };

  loadData = dataTree => {
    return dataTree.map(item => {
      return (
        <TreeNode title={item.name} key={item.name} value={item.name}>
          {item.childList ? this.loadData(item.childList) : null}
        </TreeNode>
      );
    });
  };

  onWarehouseChange = warehouse => {
    const {
      otherOutIn: { warehouseList },
    } = this.props;
    const warehouseId = warehouseList.rows.find(item => item.name === warehouse).warehouseId;
    if (warehouseId) {
      this.setState({ dataSource: [], warehouseId });
    }
  };

  renderTopSimpleForm() {
    const {
      otherOutIn: { outOrderNumber, staffList, branchList, warehouseList, outTypeList },
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    const { dataPermission } = this.state;
    // console.log('currentUser.personnel.name: ', currentUser.personnel.name);
    // console.log('staffList.rows: ', staffList.rows);
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;订单编号</span>}>
              {outOrderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库时间">
              {getFieldDecorator('putOutTime', {
                rules: [{ required: true, message: '请选择出库时间' }],
                initialValue: moment(new Date(), 'YYYY-MM-DD HH:mm'),
              })(
                <DatePicker
                  style={{ width: '100%' }}
                  placeholder="请选择出库时间"
                  format="YYYY-MM-DD HH:mm"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库类型">
              {getFieldDecorator('typeCode', {
                rules: [{ required: true, message: '请选择出库类型' }],
                // initialValue: currentUser.personnel.name
              })(
                <Select placeholder="请选择出库类型" style={{ width: '100%' }}>
                  {outTypeList.rows.map(item => (
                    <Select.Option key={item.dictDataCode} value={item.dictDataCode}>
                      {item.dictDataName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="出库机构">
              {getFieldDecorator('sendOutInstitutions', {
                rules: [{ required: true, message: '请选择出库机构' }],
                // initialValue: dataPermission ? '' : currentUser.personnel.branch
                initialValue: currentUser.personnel.branch,
              })(
                <Select
                  onChange={this.onSendOutInstitutionsChange}
                  disabled={!dataPermission}
                  placeholder="请选择出库机构"
                  style={{ width: '100%' }}
                >
                  {branchList.rows.map(item => (
                    <Option branchId={item.branchId} value={item.name}>
                      {item.name}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库仓库">
              {getFieldDecorator('warehouse', {
                rules: [{ required: true, message: '请选择出库仓库' }],
                // initialValue: currentUser.personnel.branch
              })(
                <Select
                  onChange={this.onWarehouseChange}
                  placeholder="请选择出库仓库"
                  style={{ width: '100%' }}
                >
                  {warehouseList.rows.map(item => (
                    <Option value={item.name}>{item.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库人员">
              {getFieldDecorator('putOutBy', {
                rules: [{ required: true, message: '请选择出库人员' }],
                // initialValue: staffList.rows.length > 0 ? staffList.rows[0].name : ''
                // initialValue: currentUser.personnel.name
              })(
                <Select placeholder="请选择出库人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => (
                    <Option value={item.name} key={item.name}>
                      {item.name}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">{currentUser.personnel.name}</FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{ float: 'right' }}>
              <Button
                style={{
                  marginRight: 8,
                  display:
                    this.props.location.query.source === 'CheckOrderList' ? 'none' : 'inline-block',
                }}
                onClick={this.handleFormReset}
              >
                重置
              </Button>
              <Button
                style={{
                  marginRight: 8,
                  display:
                    this.props.location.query.source === 'CheckOrderList' ? 'none' : 'inline-block',
                }}
                onClick={() => this.submit(false)}
              >
                保存
              </Button>
              <Button type="primary" htmlType="submit">
                提交
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleReferenceModalVisible = flag => {
    this.setState({
      referenceModalVisible: !!flag,
    });
  };

  handleReference = dataSource => {
    console.log('dataSource: ', dataSource);
    //一、newDataSource中：referenceCost、warehousePrice必须都存在且一样
    dataSource = dataSource.map(item => {
      let result = '';
      if (item.referenceCost) {
        result = { ...item, warehousePrice: item.referenceCost };
      } else if (item.warehousePrice) {
        result = { ...item, referenceCost: item.warehousePrice };
      }
      return result;
    });
    //dataSource去重
    this.setState(prevState => {
      //二、oldDataSource中：referenceCost、warehousePrice必须都存在且一样
      let oldDataSource = [
        ...prevState.dataSource.map(item => {
          let result = '';
          if (item.referenceCost) {
            result = { ...item, warehousePrice: item.referenceCost };
          } else if (item.warehousePrice) {
            result = { ...item, referenceCost: item.warehousePrice };
          }
          return result;
        }),
      ];
      //新存货跟旧存货对比: 1.相同存货，数量+；2.新增不同存货，push
      // if (oldDataSource.length > 0){
      for (let newItem of dataSource) {
        let flag = true;
        for (let oldItem of oldDataSource) {
          if (
            newItem.stockNum.toString() === oldItem.stockNum.toString() &&
            newItem.warehousePrice.toString() === oldItem.warehousePrice.toString()
          ) {
            // oldItem.number = oldItem.number + newItem.number;
            flag = false;
            break;
          }
        }
        if (flag) {
          oldDataSource.push(newItem);
        }
      }

      return {
        dataSource: oldDataSource.map((item, index) => ({ ...item, ID: index })),
      };
    });
    this.handleReferenceModalVisible();
  };

  onAddClick = () => {
    const { warehouseId } = this.state;
    if (!warehouseId) {
      message.warning('请先选择出库仓库!');
      return;
    }
    this.handleModalVisible(true);
  };

  onReferenceClick = () => {
    // const { warehouseId } = this.state;
    // if (!warehouseId) {
    //   message.warning('请先选择出库仓库!');
    //   return;
    // }
    this.handleReferenceModalVisible(true);
  };

  handleSave = row => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.ID === item.ID);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  };

  handleInputNumberChange = (record, key, value) => {
    const { form } = this.props;
    console.log('row: ', record);
    console.log('key: ', key);
    console.log('value: ', value);
    const newData = [...this.state.dataSource];

    const row = { ...record };

    //改row
    if (isNaN(value)) {
      row.number = '';
      row.number2 = '';
      return;
    } else if (key === 'number') {
      row.number2 = parseInt((value / row.unitRatio) * 100) / 100;
    } else if (key === 'number2') {
      //保留两位小数
      value = parseInt(value * 100) / 100;
      row.number = parseInt(value * row.unitRatio);
    }

    const index = newData.findIndex(item => row.ID === item.ID);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  };

  total = () => {
    const { dataSource } = this.state;
    let result = `合计: 主数量0 / 辅数量0 `;
    if (dataSource.length > 0) {
      const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => a + b);
      const ping = dataSource
        .map(item => item.number || 0)
        .reduce((a, b) => parseInt(a) + parseInt(b));
      result = `合计: 主数量${ping} / 辅数量${parseInt(xiang * 100) / 100}`;
    }
    return result;
  };

  render() {
    const {
      modalVisible,
      dataSource,
      updateModalVisible,
      record,
      referenceModalVisible,
      warehouseId,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const referenceMethods = {
      handleReference: this.handleReference,
      handleReferenceModalVisible: this.handleReferenceModalVisible,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          isInputNumber: col.isInputNumber,
          inputNumberPrecision: col.inputNumberPrecision,
          inputNumberMin: col.inputNumberMin,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
          inputNumberChange: (value, row) =>
            this.handleInputNumberChange(row, col.dataIndex, value),
        }),
      };
    });

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              出库明细
              <Button
                type="primary"
                style={{
                  marginLeft: 16,
                  display:
                    this.props.location.query.source === 'CheckOrderList' ? 'none' : 'inline-block',
                }}
                // icon="plus"
                // onClick={() => this.handleModalVisible(true)}
                onClick={this.onAddClick}
              >
                新增
              </Button>
              {/*<Button*/}
              {/*style={{ marginLeft: 16 }}*/}
              {/*// onClick={() => this.handleReferenceModalVisible(true)}*/}
              {/*onClick={this.onReferenceClick}*/}
              {/*>*/}
              {/*参照*/}
              {/*</Button>*/}
            </h2>
            <Divider />
            <Table
              // loading={loading}
              rowKey="ID"
              components={components}
              columns={columns}
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              scroll={{ x: 2000 }}
              footer={this.total}
              // footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{ paddingTop: 24 }} className={styles.tableListForm}>
              {this.renderBottomSimpleForm()}
            </div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} warehouseId={warehouseId} />
        {/*<ReferenceForm {...referenceMethods} referenceModalVisible={referenceModalVisible} />*/}
        {/*<UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>*/}
      </div>
    );
  }
}

export default OtherOut;
