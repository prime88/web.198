import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, Radio } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated } from '@/services/customerRelations';
import styles from './ConsumptionDetails.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ customerRelations, loading }) => ({
  customerRelations,
  loading: loading.models.customerRelations,
}))
@Form.create()
class ConsumptionDetails extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '序号',
      dataIndex: 'brandId',
      render: (text, record, index) => index+1,
    },
    {
      title: '姓名',
      dataIndex: 'name',
    },
    {
      title: '手机号',
      dataIndex: 'phone',
    },
    {
      title: '订单号',
      dataIndex: 'orderNumber',
    },
    {
      title: '消费金额(¥)',
      dataIndex: 'consumptionAmount',
    },
    {
      title: '消费时间',
      dataIndex: 'createTime',
    },
    {
      title: '消费类型',
      dataIndex: 'type',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '门店销售';
            break;
          case '1':
            result = '电话销售';
            break;
          case '2':
            result = '批发销售';
            break;
        }
        return result;
      }
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'customerRelations/fetchConsumptionDetails',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'customerRelations/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'customerRelations/fetchConsumptionDetails',
        // payload: fieldsValue,
        payload: {
          ...fieldsValue,
          phone: fieldsValue.phone.replace(/\s*/g,"")
        },
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();

    dispatch({
      type: 'customerRelations/fetchConsumptionDetails',
    });
  };


  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="手机号">
              {getFieldDecorator('phone')(<Input placeholder="请输入手机号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      customerRelations: { consumptionDetails },
      loading,
    } = this.props;
    const { selectedRows } = this.state;

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={consumptionDetails}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="memberConsumptionId"
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default ConsumptionDetails;
