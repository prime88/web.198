import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Divider,
  Popconfirm,
  Row,
  Col,
  Radio,
  Upload,
  message,
  InputNumber,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isVipNameRepeated } from '@/services/customerRelations';
import styles from './VipManagement.less';
import { apiDomainName } from '../../constants';
import { stringify } from 'qs';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, storeList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      fieldsValue.phone = fieldsValue.phone.replace(/\s*/g,"");
      handleAdd(fieldsValue);
    });
  };

  const checkPhone = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    const response = isVipNameRepeated({ phone: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的手机号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增会员"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="姓名">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }],
        })(<Input placeholder="请输入姓名" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="手机号">
        {form.getFieldDecorator('phone', {
          rules: [{ required: true, message: '请输入手机号！' }, { validator: checkPhone }],
        })(<Input placeholder="请输入手机号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="性别">
        {form.getFieldDecorator('gender', {
          rules: [{ required: true, message: '请选择性别！' }],
          initialValue: 1,
        })(
          <RadioGroup>
            <Radio value={1}>男</Radio>
            <Radio value={2}>女</Radio>
          </RadioGroup>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开卡店铺">
        {form.getFieldDecorator('regStore', {
          rules: [{ required: true, message: '请选择开卡店铺！' }],
        })(
          <Select style={{ width: '100%' }}>
            {storeList.rows.map(item => (
              <Select.Option value={item.name}>{item.name}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="积分余额">
        {form.getFieldDecorator('integral', {
          rules: [{ required: true, message: '请输入积分余额！' }],
          initialValue: 0,
        })(<InputNumber min={0} placeholder="请输入积分余额" style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="住址">
        {form.getFieldDecorator('address', {
          // rules: [
          //   { required: true, message: '请输入住址！' },
          // ],
        })(<Input placeholder="请输入住址" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const {
    updateModalVisible,
    form,
    handleUpdate,
    handleUpdateModalVisible,
    record,
    storeList,
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      fieldsValue.phone = fieldsValue.phone.replace(/\s*/g,"");
      handleUpdate(fieldsValue);
    });
  };

  const checkPhone = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value === record.phone) {
      callback();
      return;
    }

    const response = isVipNameRepeated({ phone: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的手机号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改会员"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="姓名">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }],
          initialValue: record.name,
        })(<Input placeholder="请输入姓名" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="手机号">
        {form.getFieldDecorator('phone', {
          rules: [{ required: true, message: '请输入手机号！' }, { validator: checkPhone }],
          initialValue: record.phone,
        })(<Input placeholder="请输入手机号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="性别">
        {form.getFieldDecorator('gender', {
          rules: [{ required: true, message: '请选择性别！' }],
          initialValue: record.gender,
        })(
          <RadioGroup>
            <Radio value={1}>男</Radio>
            <Radio value={2}>女</Radio>
          </RadioGroup>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开卡店铺">
        {form.getFieldDecorator('regStore', {
          rules: [{ required: true, message: '请输入住址！' }],
          initialValue: record.regStore,
        })(
          <Select style={{ width: '100%' }}>
            {storeList.rows.map(item => (
              <Select.Option value={item.name}>{item.name}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="积分余额">
        {form.getFieldDecorator('integral', {
          rules: [{ required: true, message: '请输入积分余额！' }],
          initialValue: record.integral,
        })(<InputNumber min={0} placeholder="请输入积分余额" style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="住址">
        {form.getFieldDecorator('address', {
          initialValue: record.address,
        })(<Input placeholder="请输入住址" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ customerRelations, user, loading }) => ({
  customerRelations,
  user,
  loading: loading.models.customerRelations,
}))
@Form.create()
class VipManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    dataPermission: false,
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: '10%',
      render: (text, record, index) => index + 1,
    },
    {
      title: '姓名',
      dataIndex: 'name',
    },
    {
      title: '手机号',
      dataIndex: 'phone',
    },
    {
      title: '性别',
      dataIndex: 'gender',
      render: text => (text === 1 ? '男' : '女'),
    },
    {
      title: '开卡店铺',
      dataIndex: 'regStore',
    },
    {
      title: '积分余额',
      dataIndex: 'integral',
    },
    {
      title: '住址',
      dataIndex: 'address',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: '15%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.memberId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = memberIds => {
    console.log('memberIds: ', memberIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'customerRelations/removeVip',
      payload: { memberIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;
    console.log('currentUser: ', currentUser);

    dispatch({
      type: 'customerRelations/fetchVipList',
    });

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      //门店列表
      dispatch({
        type: 'customerRelations/fetchStoreList',
      });
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      //门店列表
      dispatch({
        type: 'customerRelations/fetchStoreList',
        payload: {
          name: currentUser.personnel.branch,
        },
      });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'customerRelations/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'customerRelations/addVip',
      payload: fields,
      // callback: () => this.handleModalVisible(true)
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'customerRelations/updateVip',
      payload: {
        ...fields,
        memberId: record.memberId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'customerRelations/fetchVipList',
        payload: {
          ...fieldsValue,
          phone: fieldsValue.phone.replace(/\s*/g,"")
        },
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'customerRelations/fetchVipList',
      // payload: {},
    });
  };

  export = () => {
    const {
      user: { currentUser },
      form,
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId,
      };
      window.location.href = `${apiDomainName}/server/member/exportList?${stringify(params)}`;
    });
  };

  renderSimpleForm() {
    const that = this;
    const {
      form: { getFieldDecorator },
    } = this.props;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="姓名">
              {getFieldDecorator('name')(<Input placeholder="请输入姓名" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="手机号">
              {getFieldDecorator('phone')(<Input placeholder="请输入手机号" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="开卡店铺">
              {getFieldDecorator('regStore')(<Input placeholder="请输入开卡店铺" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginRight: 0, marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
              {/*<Upload {...props}>*/}
              {/*<Button style={{ marginLeft: 8 }}>*/}
              {/*<Icon type="upload" />Excel导入*/}
              {/*</Button>*/}
              {/*</Upload>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      customerRelations: { vipList, storeList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.memberId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={vipList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="memberId"
              footer={() => `总计：${vipList.rows.length} 名`}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} storeList={storeList} />
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          record={record}
          storeList={storeList}
        />
      </div>
    );
  }
}

export default VipManagement;
