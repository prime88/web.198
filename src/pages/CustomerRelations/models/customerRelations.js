// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchVipList,
  fetchConsumptionDetails,
  addVip,
  updateVip,
  removeVip,
  changeBrandStatus,
} from '@/services/customerRelations';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';
import { queryStoreList } from '@/services/store';

export default {
  namespace: 'customerRelations',

  state: {
    //会员列表
    vipList: {
      rows: [],
      pagination: {},
    },
    //会员消费明细
    consumptionDetails: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchVipList({ payload, callback }, { call, put }) {
      const response = yield call(fetchVipList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveVipList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchConsumptionDetails({ payload, callback }, { call, put }) {
      const response = yield call(fetchConsumptionDetails, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveConsumptionDetails',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addVip({ payload, callback }, { call, put }) {
      const response = yield call(addVip, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchVipList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeVip({ payload, callback }, { call, put }) {
      const response = yield call(removeVip, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchVipList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateVip({ payload, callback }, { call, put }) {
      const response = yield call(updateVip, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchVipList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeBrandStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeBrandStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBrandList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveVipList(state, action) {
      return {
        ...state,
        vipList: action.payload,
      };
    },
    saveConsumptionDetails(state, action) {
      return {
        ...state,
        consumptionDetails: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //品牌管理
  //       if (location.pathname === '/basic-data/brand-management') {
  //         dispatch({
  //           type: 'fetchBrandList',
  //         })
  //       }
  //     });
  //   },
  // },
};
