import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Divider,
  Table,
  Popconfirm,
  TreeSelect,
} from 'antd';
import DeliveryApplyOrderAdd from './DeliveryApplyOrderAdd'
import styles from './DeliveryApplyOrder.less';
import moment from 'moment'
import { fetchStaffList } from '@/services/branch';

const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item;
const { Option } = Select;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  let purchaseingPlanAdd;
  const okHandle = () => {
    const { selectedRows } = purchaseingPlanAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = (ref) => {
    purchaseingPlanAdd = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="新增调货明细"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{top: 20}}
    >
      <DeliveryApplyOrderAdd wrappedComponentRef={addModalRef} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.demandTime = fieldsValue.demandTime.format('YYYY-MM-DD');
      console.log('fieldsValue: ', fieldsValue);

      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().subtract(1, 'days').endOf('day');
  }

  const onNumberChange = (key, value) => {
    console.log('key: ', key);
    console.log('value: ', value);
    console.log('record.unitRatio: ', record.unitRatio);
    console.log('value/record.unitRatio: ', value/record.unitRatio);
    if (isNaN(value)) {
      props.form.setFieldsValue({ number: '', number2: '' });
      return;
    }

    if (key === 'number') {
      props.form.setFieldsValue({ number2: parseInt((value/record.unitRatio)*100)/100 });
    }
    else if (key === 'number2') {
      //保留两位小数
      value = parseInt(value*100)/100
      props.form.setFieldsValue({ number: parseInt(value*record.unitRatio) });
    }
  }

  return (
    <Modal
      destroyOnClose
      title="修改调货申请明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label={`主数量(${record.units})`}>
        {form.getFieldDecorator('number', {
          rules: [{ required: true, message: '请输入申请主数量！' }],
          initialValue: record.number,
        })(
          <InputNumber
            onChange={(value) => onNumberChange('number', value)}
            precision={0}
            style={{width: '100%'}}
            placeholder="请输入申请主数量"
            min={0}
          />
          // <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入申请数量" min={1} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label={`辅数量(${record.units2})`}>
        {form.getFieldDecorator('number2', {
          rules: [{ required: true, message: '请输入申请辅数量！' }],
          initialValue: record.number2,
        })(
          <InputNumber
            onChange={(value) => onNumberChange('number2', value)}
            precision={2}
            style={{width: '100%'}}
            placeholder="请输入申请辅数量"
            min={0}
          />
          // <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入申请数量" min={1} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="需求日期">
        {form.getFieldDecorator('demandTime', {
          rules: [{ required: true, message: '请选择需求日期！' }],
          initialValue: record.demandTime ? moment(record.demandTime) : '',
        })(<DatePicker disabledDate={disabledDate} style={{width: '100%'}} placeholder="请选择需求日期" />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ deliveryApply, user, loading }) => ({
  deliveryApply,
  user,
  loading: loading.models.deliveryApply,
}))
@Form.create()
class DeliveryApplyOrder extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    applyInstitutionsId: '',
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
      editable: true,
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
      editable: true,
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    //   editable: true,
    // },
    {
      title: '需求日期',
      dataIndex: 'demandTime',
      editable: true,
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
    },
    {
      title: '操作',
      width: 120,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.stockId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = (stockId) => {
    console.log('stockId: ', stockId);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.stockId !== stockId) })
    message.success('删除成功');
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'deliveryApply/fetchOrderNumber',
    });

    //获取当前部门员工列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'deliveryApply/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //部门列表
    dispatch({
      type: 'deliveryApply/fetchBranchList',
      payload: {
        isWarehouse: true
      }
    });
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ dataSource: [] })
  };

  submit = (isSubmit) => {
    const { dispatch, form, user: { currentUser } } = this.props;
    const { dataSource, applyInstitutionsId } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增调货明细');
        return;
      }
      else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      }
      else if (dataSource.find(item => !item.demandTime)) {
        message.warning('请设置需求日期');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.demandTime = fieldsValue.demandTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.currentInstitutions = currentUser.personnel.branch;
      fieldsValue.applyInstitutionsId = applyInstitutionsId;
      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'deliveryApply/addDeliveryApplyOrder',
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [] })

          //更新订单编号
          dispatch({
            type: 'deliveryApply/fetchOrderNumber',
          });
        }
      });
    });
  }

  handleSearch = (e) => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0){
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            if (newItem.stockId.toString() === oldItem.stockId.toString()) {
              // oldItem.number++;
              flag = false;
              break;
            }
          }
          if (flag) {
            // oldDataSource.push({ ...newItem, number: 1, demandTime: moment().format('YYYY-MM-DD') })
            oldDataSource.push({ ...newItem, demandTime: moment().format('YYYY-MM-DD') })
          }
        }
      }
      else{
        // oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1, demandTime: moment().format('YYYY-MM-DD') }))];
        oldDataSource = [...selectedRows.map(item => ({ ...item, demandTime: moment().format('YYYY-MM-DD') }))];
      }
      return {
        // dataSource: [...selectedRows.map(item => ({ ...item, number: 1 }))]
        dataSource: oldDataSource
      }
    });

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.stockId === record.stockId);
    if (index > -1){
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });
      this.setState({ dataSource: newData });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    }
    else{
      message.error('修改失败');
    }
  };

  loadData = (dataTree) => {
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.name} value={item.name}>
            {item.childList ? this.loadData(item.childList) : null}
          </TreeNode>
      )
      })
    )
  }

  renderTopSimpleForm() {
    const {
      deliveryApply: { orderNumber, staffList, branchList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;

    const disabledDate = (current) => {
      // Can not select days before today and today
      return current && current < moment().subtract(1, 'days').endOf('day');
    }

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;订单编号</span>}>
              {orderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;当前机构</span>}>
              {currentUser.personnel.branch}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="申请人员">
              {getFieldDecorator('applyBy', {
                rules: [{ required: true, message: '请选择申请人员' }],
                initialValue: currentUser.personnel.name
              })(
                <Select placeholder="请选择申请人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="需求日期">
              {getFieldDecorator('demandTime', {
                rules: [{ required: true, message: '请选择需求日期' }],
                initialValue: moment(new Date(), 'YYYY-MM-DD')
              })(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择请购时间"
                  format="YYYY-MM-DD"
                  disabledDate={disabledDate}
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="调货机构">
              {getFieldDecorator('applyInstitutions', {
                rules: [{ required: true, message: '请选择调货机构' }],
                // initialValue: currentUser.personnel.branch
              })(
                <Select
                  onChange={(value, e) => {
                    console.log('e: ', e);
                    this.setState({ applyInstitutionsId: e.props.branchId })
                  }}
                  placeholder="请选择调货机构"
                  style={{ width: '100%' }}
                >
                  {branchList.rows.map(item => <Option branchId={item.branchId} value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">
              {currentUser.personnel.name}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
              <Button style={{ marginRight: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginRight: 8 }} onClick={() => this.submit(false)}>
                保存
              </Button>
              <Button type="primary" htmlType='submit' >
                提交
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  total = () => {
    const { dataSource } = this.state;
    let result = `合计: 主数量0 / 辅数量0 `;
    if (dataSource.length > 0) {
      // const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => a+b);
      const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => (a*100+b*100)/100);
      const ping = dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
      result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100}`
    }
    return result
  }

  render() {
    const { modalVisible, dataSource, updateModalVisible, record } = this.state;


    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              调货明细
              <Button
                type="primary"
                style={{ marginLeft: 16 }}
                // icon="plus"
                onClick={() => this.handleModalVisible(true)}
              >
                新增
              </Button>
            </h2>
            <Divider />
            <Table
              // loading={loading}
              // components={components}
              rowKey='daStockId'
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              columns={this.columns}
              scroll={{x: 1700}}
              footer={this.total}
              // footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{paddingTop: 24}} className={styles.tableListForm}>{this.renderBottomSimpleForm()}</div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
      </div>
    );
  }
}

export default DeliveryApplyOrder;
