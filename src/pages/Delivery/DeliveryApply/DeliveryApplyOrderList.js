import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  DatePicker,
  Modal,
  message,
  Table,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import router from 'umi/router';
import styles from './DeliveryApplyOrderList.less';
import { fetchStaffList } from '@/services/branch';
import DeliveryApplyOrderListEdit from './DeliveryApplyOrderListEdit'
import DeliveryApplyOrderListDetail from './DeliveryApplyOrderListDetail'
import ReactToPrint from "react-to-print";
import {apiDomainName} from "../../../constants";
import { stringify } from 'qs';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const DetailForm = Form.create()(props => {
  const { detailModalVisible, handleDetailModalVisible, record } = props;
  let componentRef;
  return (
    <Modal
      width={'80%'}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={okHandle}
      onCancel={() => handleDetailModalVisible()}
      style={{top: 10}}
      title={
        <div>
          申请单详情
          <span
            style={{float: 'right'}}
          >
      <ReactToPrint
        trigger={() => <a href="#"><Icon type="printer" /></a>}
        content={() => {
          console.log('componentRef: ', componentRef);
          return componentRef
        }}
      />
    </span>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
    >
      <DeliveryApplyOrderListDetail record={record} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  let purchaseingPlanEdit;
  const okHandle = (isSubmit) => {
    const { form } = purchaseingPlanEdit.props;
    const { dataSource, applyInstitutionsId } = purchaseingPlanEdit.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (dataSource.length === 0) {
        message.warning('请新增请购明细');
        return;
      }
      else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      }
      else if (dataSource.find(item => !item.demandTime)) {
        message.warning('请设置商品的需求日期');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.demandTime = fieldsValue.demandTime.format('YYYY-MM-DD');
      fieldsValue.applyInstitutionsId = applyInstitutionsId;
      if (isSubmit) {
        fieldsValue.submit = isSubmit;
      }
      console.log('fieldsValue: ', fieldsValue);
      handleUpdate(fieldsValue);
    });
  };

  const editModalRef = (ref) => {
    purchaseingPlanEdit = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="修改申请单"
      visible={updateModalVisible}
      // onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
      style={{top: 10}}
      footer={[
        <Button onClick={() => handleUpdateModalVisible()} >取消</Button>,
        <Button onClick={() => okHandle(false)}>保存</Button>,
        <Button type='primary' onClick={() => okHandle(true)}>提交</Button>
      ]}
    >
      <DeliveryApplyOrderListEdit row={record} wrappedComponentRef={editModalRef} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ deliveryApply, user, loading }) => ({
  deliveryApply,
  user,
  loading: loading.models.deliveryApply,
}))
@Form.create()
class DeliveryApplyOrderList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    {
      title: '申请机构',
      dataIndex: 'currentInstitutions',
    },
    {
      title: '调货机构',
      dataIndex: 'applyInstitutions',
    },
    {
      title: '需求日期',
      dataIndex: 'demandTime',
      render: text => text.split(' ').length > 0 ? text.split(' ')[0] : text
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '是否提交',
      dataIndex: 'isSubmit',
      render: text => {
        let result = '';
        switch (text) {
          case true:
            result = '已提交';
            break;
          case false:
            result = '未提交';
            break;
        }
        return result;
      }
    },
    {
      title: '审批结果',
      dataIndex: 'submitResult',
      render: (text, record) => {
        let result = '';
        if (record.isSubmit) {
          switch (text.toString()) {
            case '0':
              result = '审批中';
              break;
            case '1':
              result = '已批准';
              break;
            case '2':
              result = '未批准';
              break;
          }
        }
        return result;
      }
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      render: (text, record) => {
        const menu = (
          <Menu onClick={(e) => e.domEvent.stopPropagation()}>
            <Menu.Item disabled={!!record.isSubmit} >
              { !!record.isSubmit ? '修改' : <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a> }
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.handleDetailModalVisible(true, record, this)}>查看</a>
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement='bottomRight'>
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        )
      },
    },
  ];

  recordSubmit = (distributionApplyIds) => {
    console.log('distributionApplyIds: ', distributionApplyIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'deliveryApply/submitApplyOrders',
      payload: { distributionApplyIds },
      callback: () => {
        //1、刷新列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、清空selectedRows
        this.setState({ selectedRows: [] })
      },
    });
  }

  recordRemove = distributionApplyId => {
    console.log('distributionApplyId: ', distributionApplyId);
    const { dispatch } = this.props;
    dispatch({
      type: 'deliveryApply/removeApplyOrder',
      payload: { distributionApplyId },
      callback: () => {
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        this.setState({ selectedRows: [] })
      },
    });
  };

  expandedRowRender = record => {
    const {
      deliveryApply: { applyInventoryList },
    } = this.props;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        // width: 60,
        // fixed: 'left',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        // width: 100,
        // fixed: 'left',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        // width: 200,
        // fixed: 'left',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      //   editable: true,
      // },
      {
        title: '需求日期',
        dataIndex: 'demandTime',
        editable: true,
      },
      {
        title: '供应商',
        dataIndex: 'supplier',
      },
      {
        title: '备注',
        dataIndex: 'remark',
        editable: true,
      },
    ];

    const total = () => {
      let result = `合计:  主数量0 / 辅数量0`;
      if (applyInventoryList.length > 0) {
        const xiang = applyInventoryList.map(item => item.number2 || 0).reduce((a, b) => (a*100+b*100)/100);
        const ping = applyInventoryList.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
        result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100}`
      }
      return result
    }

    return <Table
      size='small'
      columns={columns}
      dataSource={applyInventoryList}
      footer={total}
      // footer={() => `合计: ${applyInventoryList.length}`}
      rowKey='daStockId'
    />
  };

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;
    //申请列表
    dispatch({
      type: 'deliveryApply/fetchDeliveryApplyOrderList',
    });

    //部门列表
    dispatch({
      type: 'deliveryApply/fetchBranchList',
      payload: {
        isWarehouse: true
      }
    });

    //获取当前部门请购人员列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'deliveryApply/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'deliveryApply/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({ purchasePersonnel: [] })
    dispatch({
      type: 'deliveryApply/fetchDeliveryApplyOrderList',
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.executeSearch(fieldsValue)
    });
  };

  executeSearch = (fieldsValue) => {
    const { dispatch } = this.props;
    const demandTime = fieldsValue.demandTime;
    if (demandTime) {
      fieldsValue.demandTime = demandTime.format('YYYY-MM-DD')
    }
    dispatch({
      type: 'deliveryApply/fetchDeliveryApplyOrderList',
      payload: { ...fieldsValue },
    });
  }

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    })
  }

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetail = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'deliveryApply/updatePurchase',
      payload: { ...fields, purchaseRequisitionId: record.purchaseRequisitionId },
    });

    this.handleUpdateModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedRows, expandedRowKeys } = this.state;
    dispatch({
      type: 'deliveryApply/updateApplyOrder',
      payload: { ...fields, distributionApplyId: record.distributionApplyId },
      callback: () => {
        //1、刷新申请单列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、刷新申请单明细列表
        if (expandedRowKeys.length > 0){
          this.onExpand(true, record);
        }
      }
    });

    //若该项被选择了，则需要取消对该项的选择
    if (fields.submit) {
      this.setState({ selectedRows: selectedRows.filter(item => item.distributionApplyId !== record.distributionApplyId) })
    }

    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      deliveryApply: { branchList, staffList },
      form: { getFieldDecorator },
    } = this.props;
    const { purchasePersonnel } = this.state;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={7} sm={24}>
            <FormItem label="订单编号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单编号" />)}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="申请机构">
              {/*{getFieldDecorator('applyInstitutions')(*/}
              {getFieldDecorator('currentInstitutions')(
                <Select placeholder="请选择申请机构" style={{ width: '100%' }}>
                  {branchList.rows.map(item => <Select.Option key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="需求日期">
              {getFieldDecorator('demandTime')(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择需求日期"
                />
              )}
            </FormItem>
          </Col>
          <Col md={4} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const distributionApplyId = record.distributionApplyId;
      dispatch({
        type: 'deliveryApply/fetchApplyInventoryList',
        payload: {
          distributionApplyIds: distributionApplyId
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (record.isSubmit) return;

    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.purchaseRequisitionId === record.purchaseRequisitionId) {
          newSelectedRows = newSelectedRows.filter(item => item.purchaseRequisitionId !== record.purchaseRequisitionId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const demandTime = fieldsValue.demandTime;
      if (demandTime) {
        fieldsValue.demandTime = fieldsValue.demandTime.format('YYYY-MM-DD')
      }
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/distributionApply/exportList?${stringify(params)}`;
    });
  }

  render() {
    const {
      deliveryApply: { deliveryApplyOrderList },
      loading,
    } = this.props;
    const { selectedRows, updateModalVisible, expandedRowKeys, record, detailModalVisible } = this.state;

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
      handleDetail: this.handleDetail,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => router.push('/delivery-management/delivery-apply/delivery-apply-order')}>
                新增
              </Button>
              {/*<Button >审核</Button>*/}
              {selectedRows.length > 0 && (
                <span>
                  <Button
                    onClick={() => this.recordSubmit( selectedRows.map(item => item.distributionApplyId).join(','))}
                  >批量提交</Button>
                  <Popconfirm
                    title="确认删除？"
                    onConfirm={() => this.recordRemove( selectedRows.map(item => item.distributionApplyId).join(','))}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
              <Button style={{ float: 'right', marginRight: 0 }} onClick={this.export}>
                导出
              </Button>
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={deliveryApplyOrderList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              rowKey='distributionApplyId'
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
              onRow={(record) => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
              checkboxProps={record => ({
                disabled: record.isSubmit,
              })}
            />
          </div>
        </Card>
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
        <DetailForm {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
      </div>
    );
  }
}

export default DeliveryApplyOrderList;
