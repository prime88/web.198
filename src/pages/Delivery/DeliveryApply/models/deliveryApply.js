// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
  fetchOrderNumber,
  fetchApplyInventoryList,
  fetchDeliveryApplyOrderList,
  addDeliveryApplyOrder,
  removeApplyOrder,
  updateApplyOrder,
  submitApplyOrders,
} from '@/services/deliveryApply';
import { fetchChildBranchList, fetchStaffList } from '@/services/branch';
import { queryStoreList } from '@/services/store';
import { message } from 'antd';

export default {
  namespace: 'deliveryApply',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //订单编号
    orderNumber: '',
    //调货申请单存货列表
    applyInventoryList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //部门列表
    branchList: {
      rows: [],
      pagination: {},
    },
    //调货申请单列表
    deliveryApplyOrderList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOrderNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOrderNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchApplyInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchApplyInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveApplyInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchChildBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryApplyOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryApplyOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryApplyOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addDeliveryApplyOrder({ payload, callback }, { call, put }) {
      const response = yield call(addDeliveryApplyOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeApplyOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeApplyOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateApplyOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateApplyOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitApplyOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitApplyOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveOrderNumber(state, action) {
      return {
        ...state,
        orderNumber: action.payload,
      };
    },
    saveApplyInventoryList(state, action) {
      return {
        ...state,
        applyInventoryList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveDeliveryApplyOrderList(state, action) {
      return {
        ...state,
        deliveryApplyOrderList: action.payload,
      };
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //停留在本页面时的订单通知的数据刷新
        if (location.pathname === '/delivery-management/delivery-apply/delivery-apply-order-list' && location.query.type === '同页面跳转') {
          console.log('同页面跳转');
          dispatch({
            type: 'fetchDeliveryApplyOrderList',
          });
        }
        // //存货分类
        // if (location.pathname === '/basic-data/inventory-classification') {
        //   dispatch({
        //     type: 'fetchInventoryClassificationList',
        //   })
        // }
        // //存货管理
        // if (location.pathname === '/basic-data/inventory-management') {
        //   dispatch({
        //     type: 'fetchInventoryList',
        //   })
        // }
        // //仓库管理
        // if (location.pathname === '/basic-data/Warehouse-management') {
        //   dispatch({
        //     type: 'fetchWarehouseList',
        //   })
        // }
      });
    },
  },
};
