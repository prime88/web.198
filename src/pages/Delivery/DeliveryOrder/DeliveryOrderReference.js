import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  DatePicker,
  Table,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './DeliveryOrderReference.less';
import { fetchStaffList } from '@/services/branch';
import { fetchApplyInventoryList } from '@/services/deliveryApply';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ deliveryOrder, loading }) => ({
  deliveryOrder,
  loading: loading.models.deliveryOrder,
}))
@Form.create()
class DeliveryOrderReference extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    selectedRowKeys: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailVisible: false,
    detailModalVisible: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    {
      title: '申请机构',
      dataIndex: 'currentInstitutions',
    },
    {
      title: '调货机构',
      dataIndex: 'applyInstitutions',
    },
    {
      title: '需求日期',
      dataIndex: 'demandTime',
      render: text => text.split(' ').length > 0 ? text.split(' ')[0] : text
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  onRowSelectionChange = (selectedRowKeys) => {
    console.log('selectedRowKeys: ', selectedRowKeys);
    this.setState({ selectedRowKeys })
  }

  componentDidMount() {
    const { dispatch, deliveryInstitutions, currentInstitutions } = this.props;
    //调货申请列表
    dispatch({
      type: 'deliveryOrder/fetchDeliveryApplyOrderList',
      payload: {
        submitResult: 1,//审批=批准，才能参照
        // applyInstitutions: deliveryInstitutions,
        currentInstitutions: currentInstitutions,
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
  };

  handleFormReset = () => {
    const { form, dispatch, deliveryInstitutions, currentInstitutions } = this.props;
    form.resetFields();
    dispatch({
      type: 'deliveryOrder/fetchDeliveryApplyOrderList',
      payload: {
        // applyInstitutions: deliveryInstitutions,
        currentInstitutions: currentInstitutions,
      }
    });
  };

  onSelect = (record, selected, selectedRows, nativeEvent) => {
    console.log('record: ', record);
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    const { dispatch, deliveryOrder } = this.props;
    const distributionApplyId = record.distributionApplyId;
    if (selected) {
      const response = fetchApplyInventoryList({ distributionApplyIds: distributionApplyId });
      response.then(result => {
        console.log('result: ', result);
        this.setState(prevState => {
          return {
            dataSource: [...prevState.dataSource, ...result.data]
          }
        })
      })
    }
    else{//删除
      const { dataSource } = this.state;
      const newDataSource = dataSource.filter(item => item.distributionApplyId.toString() !== distributionApplyId.toString());
      this.setState({ dataSource: newDataSource })
    }
  }

  handleSelectRows = rows => {
    console.log('rows: ', rows);
    this.setState({
      selectedRows: rows,
    });

    if (rows.length > 0) {
      const response = fetchApplyInventoryList({ distributionApplyIds: rows.map(item => item.distributionApplyId).join(',') });
      response.then(result => {
        console.log('result: ', result);
        this.setState({
          dataSource: result.data
        })
      })
    }
    else{
      this.setState({ dataSource: [] })
    }

  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form, deliveryInstitutions, currentInstitutions } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const demandTime = fieldsValue.demandTime;
      if (demandTime) {
        fieldsValue.demandTime = fieldsValue.demandTime.format('YYYY-MM-DD')
      }
      dispatch({
        type: 'deliveryOrder/fetchDeliveryApplyOrderList',
        payload: {
          ...fieldsValue,
          // applyInstitutions: deliveryInstitutions,
          currentInstitutions: currentInstitutions,
        },
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="需求日期">
              {getFieldDecorator('demandTime')(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择需求日期"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      detailVisible: false,
    });
  }

  render() {
    const {
      deliveryOrder: { deliveryApplyOrderList },
      loading,
    } = this.props;
    const { selectedRows, dataSource } = this.state;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        width: 60,
        fixed: 'left',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        width: 100,
        fixed: 'left',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        width: 200,
        fixed: 'left',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      //   editable: true,
      // },
      {
        title: '需求日期',
        dataIndex: 'demandTime',
        editable: true,
      },
      {
        title: '供应商',
        dataIndex: 'supplier',
      },
      {
        title: '备注',
        dataIndex: 'remark',
        editable: true,
      },
    ];

    return (
      <div>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          <StandardTable
            hideDefaultSelections={true}
            selectedRows={selectedRows}
            // loading={loading}
            data={deliveryApplyOrderList}
            columns={this.columns}
            onSelectRow={this.handleSelectRows}
            // onSelect={this.onSelect}
            onChange={this.handleStandardTableChange}
            rowKey='distributionApplyId'
            paginationOfTable={{pageSize: 5, size: 'small'}}
            size='small'
            title={() => <div style={{textAlign: 'center'}}>调货申请单列表</div>}
            onRow={(record) => {
              return {
                onClick: () => {// 点击行
                  console.log('record: ', record);
                  console.log('oldSelectedRows: ', this.state.selectedRows);
                  //选择效果
                  this.setState(prevState => {
                    let newSelectedRows = [...prevState.selectedRows];
                    let flag = true;
                    for (let item of prevState.selectedRows) {
                      if (item.distributionApplyId === record.distributionApplyId) {
                        newSelectedRows = newSelectedRows.filter(item => item.distributionApplyId !== record.distributionApplyId);
                        flag = false;
                        break;
                      }
                    }
                    if (flag){
                      newSelectedRows.push(record);
                    }
                    console.log('newSelectedRows: ', newSelectedRows);
                    //更新调货明细
                    this.onSelect(record, flag);

                    return ({
                      selectedRows: newSelectedRows
                    })
                  });
                },
              };
            }}

          />
          <Table
            loading={loading}
            dataSource={dataSource}
            columns={columns}
            size='small'
            title={() => <div style={{textAlign: 'center'}}>调货明细</div>}
          />
        </div>
      </div>
    );
  }
}

export default DeliveryOrderReference;
