import React, { Component } from 'react';
import { connect } from 'dva';
import { Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import styles from './DeliveryOrderListDetail.less';

const { Description } = DescriptionList;

@connect(({ deliveryOrder, loading }) => ({
  deliveryOrder,
  // loading: loading.effects['profile/fetchBasic'],
}))
class DeliveryOrderListDetail extends Component {
  state = {
    dataSource: []
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const distributionOrderId = record.distributionOrderId;
    if (distributionOrderId) {
      dispatch({
        type: 'deliveryOrder/fetchDeliveryOrderInventoryList',
        payload: {
          distributionOrderIds: distributionOrderId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }
  }

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    //   editable: true,
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="当前机构">{record.currentInstitutions}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="送达机构">{record.deliveryInstitutions}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="预计送达">{record.estimateTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>订单明细</div>
          <Table
            style={{ marginBottom: 24 }}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="daStockId"
            pagination={{defaultPageSize: 9}}
            size='small'
          />
        </div>
      </div>
    );
  }
}

export default DeliveryOrderListDetail;
