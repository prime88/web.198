import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Divider,
  Table,
  Popconfirm,
  TreeSelect,
} from 'antd';
import DeliveryOrderAdd from './DeliveryOrderAdd'
import styles from './DeliveryOrder.less';
import moment from 'moment'
import { fetchStaffList } from '@/services/branch';
import DeliveryOrderReference from "../../Delivery/DeliveryOrder/DeliveryOrderReference";
import EditableCell from '@/customComponents/EditableCell';

//单元格编辑--------------------------------------------
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
//-----------------------------------------------------

const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item;
const { Option } = Select;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  let purchaseingPlanAdd;
  const okHandle = () => {
    const { selectedRows } = purchaseingPlanAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = (ref) => {
    purchaseingPlanAdd = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="新增调货明细"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{top: 20}}
    >
      <DeliveryOrderAdd wrappedComponentRef={addModalRef} />
    </Modal>
  );
});

const ReferenceForm = Form.create()(props => {
  const { referenceModalVisible, handleReference, handleReferenceModalVisible, deliveryInstitutions, currentInstitutions } = props;
  let deliveryApplyOrderAdd;
  const okHandle = () => {
    const { dataSource } = deliveryApplyOrderAdd.state;
    handleReference(dataSource);
  };

  const referenceModalRef = (ref) => {
    deliveryApplyOrderAdd = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="参照"
      visible={referenceModalVisible}
      onOk={okHandle}
      onCancel={() => handleReferenceModalVisible()}
      style={{top: 20}}
    >
      <DeliveryOrderReference
        wrappedComponentRef={referenceModalRef}
        // deliveryInstitutions={deliveryInstitutions}
        currentInstitutions={currentInstitutions}
      />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);

      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改调货明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="调货数量">
        {form.getFieldDecorator('number', {
          rules: [{ required: true, message: '请输入调货数量！' }],
          initialValue: record.number,
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入调货数量" min={1} />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ deliveryOrder, user, loading }) => ({
  deliveryOrder,
  user,
  loading: loading.models.deliveryOrder,
}))
@Form.create()
class DeliveryOrder extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    referenceModalVisible: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '主数量',
      dataIndex: 'number',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 0,
      inputNumberMin: 1,
      width: 120,
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '辅数量',
      dataIndex: 'number2',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 2,
      inputNumberMin: 0,
      width: 120,
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    //   editable: true,
    //   isInputNumber: true,
    //   inputNumberPrecision: 0,
    //   inputNumberMin: 1,
    //   width: 120,
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
      width: 200
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.stockId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = (stockId) => {
    console.log('stockId: ', stockId);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.stockId !== stockId) })
    message.success('删除成功');
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'deliveryOrder/fetchOrderNumber',
    });

    //获取当前部门员工列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'deliveryOrder/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //部门列表
    dispatch({
      type: 'deliveryOrder/fetchBranchList',
      payload: {
        isWarehouse: true
      }
    });
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ dataSource: [] })
  };

  submit = (isSubmit) => {
    const { dispatch, form, user: { currentUser } } = this.props;
    const { dataSource } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增调货明细');
        return;
      }
      else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.estimateTime = fieldsValue.estimateTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.currentInstitutions = currentUser.personnel.branch;
      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'deliveryOrder/addDeliveryOrder',
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [] })

          //更新订单编号
          dispatch({
            type: 'deliveryOrder/fetchOrderNumber',
          });
        }
      });
    });
  }

  handleSearch = (e) => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0){
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            if (newItem.stockId.toString() === oldItem.stockId.toString()) {
              // oldItem.number++;
              flag = false;
              break;
            }
          }
          if (flag) {
            // oldDataSource.push({ ...newItem, number: 1 })
            oldDataSource.push({ ...newItem })
          }
        }
      }
      else{
        // oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1 }))];
        oldDataSource = [...selectedRows];
      }
      return {
        // dataSource: [...selectedRows.map(item => ({ ...item, number: 1 }))]
        dataSource: oldDataSource
      }
    });

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.stockId === record.stockId);
    if (index > -1){
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });
      this.setState({ dataSource: newData });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    }
    else{
      message.error('修改失败');
    }
  };

  loadData = (dataTree) => {
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.name} value={item.name}>
            {item.childList ? this.loadData(item.childList) : null}
          </TreeNode>
        )
      })
    )
  }

  renderTopSimpleForm() {
    const {
      deliveryOrder: { orderNumber, staffList, branchList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;订单编号</span>}>
              {orderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;当前机构</span>}>
              {currentUser.personnel.branch}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="送达机构">
              {getFieldDecorator('deliveryInstitutions', {
                rules: [{ required: true, message: '请选择送达机构' }],
                // initialValue: currentUser.personnel.branch
              })(
                <Select placeholder="请选择送达机构" style={{ width: '100%' }}>
                  {branchList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="预计送达">
              {getFieldDecorator('estimateTime', {
                rules: [{ required: true, message: '请选择预计送达日期' }],
                initialValue: moment(new Date(), 'YYYY-MM-DD')
              })(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择预计送达日期"
                  format="YYYY-MM-DD"
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">
              {currentUser.personnel.name}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
              <Button style={{ marginRight: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginRight: 8 }} onClick={() => this.submit(false)}>
                保存
              </Button>
              <Button type="primary" htmlType='submit' >
                提交
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onReferenceClick = () => {
    const { form } = this.props;
    const { deliveryInstitutions } = form.getFieldsValue();
    if (!deliveryInstitutions) {
      message.warning('请先选择送达机构!');
      return;
    }
    this.handleReferenceModalVisible(true)
  }

  handleReferenceModalVisible = flag => {
    this.setState({
      referenceModalVisible: !!flag,
    });
  };

  handleReference = (dataSource) => {
    console.log('dataSource: ', dataSource);
    //dataSource去重
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      // if (oldDataSource.length > 0){
      for (let newItem of dataSource) {
        let flag = true;
        for (let oldItem of oldDataSource) {
          if (newItem.stockNum.toString() === oldItem.stockNum.toString()) {
            // oldItem.number = oldItem.number + newItem.number;
            flag = false;
            break;
          }
        }
        if (flag) {
          oldDataSource.push(newItem)
        }
      }
      return {
        dataSource: oldDataSource
      }
    });
    this.handleReferenceModalVisible();
  }

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.stockId === item.stockId);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  }

  handleInputNumberChange = (record, key, value) => {
    const { form } = this.props;
    console.log('row: ', record);
    console.log('key: ', key);
    console.log('value: ', value);
    const newData = [...this.state.dataSource];

    const row = { ...record };

    //改row
    if (isNaN(value)) {
      row.number = '';
      row.number2 = '';
      return;
    }
    else if (key === 'number') {
      row.number2 = parseInt((value/row.unitRatio)*100)/100;
    }
    else if (key === 'number2') {
      //保留两位小数
      value = parseInt(value*100)/100;
      row.number = parseInt(value*row.unitRatio);
    }
    else if (key === 'referenceCost') {
      row.referenceCost = value;
    }
    const index = newData.findIndex(item => row.stockId === item.stockId);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  }

  total = () => {
    const { dataSource } = this.state;
    let result = `合计: 主数量0 / 辅数量0 `;
    if (dataSource.length > 0) {
      // const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => a+b);
      const xiang = dataSource.map(item => item.number2 || 0).reduce((a, b) => (a*100+b*100)/100);
      const ping = dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
      result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100}`
    }
    return result
  }

  render() {
    const { form } = this.props;

    const { modalVisible, dataSource, updateModalVisible, record, referenceModalVisible } = this.state;


    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const referenceMethods = {
      handleReference: this.handleReference,
      handleReferenceModalVisible: this.handleReferenceModalVisible,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          isInputNumber: col.isInputNumber,
          inputNumberPrecision: col.inputNumberPrecision,
          inputNumberMin: col.inputNumberMin,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
          inputNumberChange: (value, row) => this.handleInputNumberChange(row, col.dataIndex, value),
        }),
      };
    });

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              调货明细
              <Button
                type="primary"
                style={{ marginLeft: 16 }}
                // icon="plus"
                onClick={() => this.handleModalVisible(true)}
              >
                新增
              </Button>
              <Button
                style={{ marginLeft: 16 }}
                // onClick={() => this.handleReferenceModalVisible(true)}
                onClick={this.onReferenceClick}
              >
                参照
              </Button>
            </h2>
            <Divider />
            <Table
              // loading={loading}
              components={components}
              columns={columns}
              rowKey='stockId'
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              scroll={{x: 1700}}
              footer={this.total}
              // footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{paddingTop: 24}} className={styles.tableListForm}>{this.renderBottomSimpleForm()}</div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <ReferenceForm
          {...referenceMethods}
          referenceModalVisible={referenceModalVisible}
          // deliveryInstitutions={form.getFieldsValue().deliveryInstitutions}
          currentInstitutions={form.getFieldsValue().currentInstitutions}
        />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
      </div>
    );
  }
}

export default DeliveryOrder;
