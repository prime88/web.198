// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
  fetchOrderNumber,
  fetchDeliveryInOrderInventoryList,
  fetchDeliveryApplyOrderList,//参照用，不能删除
  fetchDeliveryOrderList,
  fetchDeliveryInOrderList,
  addDeliveryInOrder,
  removeDeliveryInOrder,
  updateDeliveryInOrder,
  submitDeliveryInOrders,
} from '@/services/deliveryIn';
import { fetchDeliveryOutOrderList } from '@/services/deliveryOut';
import { fetchChildBranchList, fetchStaffList } from '@/services/branch';
import { queryStoreList } from '@/services/store';
import { queryWarehouseList } from '@/services/warehouse';
import { fetchStockList } from '@/services/checkOrder'
import { message } from 'antd';

export default {
  namespace: 'deliveryIn',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //订单编号
    orderNumber: '',
    //入库单存货列表
    deliveryInOrderInventoryList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //部门列表
    branchList: {
      rows: [],
      pagination: {},
    },
    //调货申请单列表-参照用
    deliveryApplyOrderList: {
      rows: [],
      pagination: {},
    },
    //出库单列表
    deliveryOutOrderList: {
      rows: [],
      pagination: {},
    },
    //仓库列表
    warehouseList: {
      rows: [],
      pagination: {},
    },
    //库存列表
    stockList: {
      rows: [],
      pagination: {},
    },
    //入库单列表
    deliveryInOrderList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOrderNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOrderNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryInOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryInOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryInOrderInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchChildBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryApplyOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryApplyOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryApplyOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryOutOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryOutOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryOutOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload, callback }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveWarehouseList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchStockList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStockList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStockList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryInOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryInOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryInOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addDeliveryInOrder({ payload, callback }, { call, put }) {
      const response = yield call(addDeliveryInOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeDeliveryInOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeDeliveryInOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateDeliveryInOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateDeliveryInOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitDeliveryInOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitDeliveryInOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveOrderNumber(state, action) {
      return {
        ...state,
        orderNumber: action.payload,
      };
    },
    saveDeliveryInOrderInventoryList(state, action) {
      return {
        ...state,
        deliveryInOrderInventoryList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveDeliveryApplyOrderList(state, action) {
      return {
        ...state,
        deliveryApplyOrderList: action.payload,
      };
    },
    saveDeliveryOutOrderList(state, action) {
      return {
        ...state,
        deliveryOutOrderList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
    saveStockList(state, action) {
      return {
        ...state,
        stockList: action.payload,
      };
    },
    saveDeliveryInOrderList(state, action) {
      return {
        ...state,
        deliveryInOrderList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
