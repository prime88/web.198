import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  DatePicker,
  Modal,
  message,
  Table,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import router from 'umi/router';
import styles from './DeliveryInOrderList.less';
import { fetchStaffList } from '@/services/branch';
import DeliveryInOrderListEdit from './DeliveryInOrderListEdit'
import DeliveryInOrderListDetail from './DeliveryInOrderListDetail'
import ReactToPrint from "react-to-print";
import {apiDomainName} from "../../../constants";
import { stringify } from 'qs';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const DetailForm = Form.create()(props => {
  const { detailModalVisible, handleDetailModalVisible, record } = props;
  let componentRef;
  return (
    <Modal
      width={'90%'}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={okHandle}
      onCancel={() => handleDetailModalVisible()}
      style={{top: 10}}
      title={
        <div>
          入库订单详情
          <span
            style={{float: 'right'}}
          >
            <ReactToPrint
              trigger={() => <a href="#"><Icon type="printer" /></a>}
              content={() => {
                console.log('componentRef: ', componentRef);
                return componentRef
              }}
            />
          </span>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}

    >
      <DeliveryInOrderListDetail record={record} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  let deliveryInEdit;
  const okHandle = (isSubmit) => {
    const { form } = deliveryInEdit.props;
    const { dataSource, warehouseId } = deliveryInEdit.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (dataSource.length === 0) {
        message.warning('请新增入库单明细');
        return;
      }
      else if (dataSource.find(item => !item.number || !item.number2)) {
        message.warning('请设置商品的主数量或辅数量');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.deliveryTime = fieldsValue.deliveryTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.putInTime = fieldsValue.putInTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.warehouseId = warehouseId;
      fieldsValue.distributionPutOutId = record.distributionPutOutId;
      if (isSubmit) {
        fieldsValue.submit = isSubmit;
      }
      console.log('fieldsValue: ', fieldsValue);
      handleUpdate(fieldsValue);
    });
  };

  const editModalRef = (ref) => {
    deliveryInEdit = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      // title="修改入库订单"
      title={(
        <div>
          <span>修改入库订单</span>
          <Button style={{float: 'right', marginRight: 50}} type='primary' onClick={() => okHandle(true)}>提交</Button>
          <Button style={{float: 'right', marginRight: 16}} onClick={() => okHandle(false)}>保存</Button>
        </div>
      )}
      visible={updateModalVisible}
      // onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
      style={{top: 10}}
      footer={[
        <Button onClick={() => handleUpdateModalVisible()} >取消</Button>,
        <Button onClick={() => okHandle(false)}>保存</Button>,
        <Button type='primary' onClick={() => okHandle(true)}>提交</Button>
      ]}
    >
      <DeliveryInOrderListEdit row={record} wrappedComponentRef={editModalRef} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ deliveryIn, user, loading }) => ({
  deliveryIn,
  user,
  loading: loading.models.deliveryIn,
}))
@Form.create()
class DeliveryInOrderList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
    dataPermission: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    {
      title: '入库机构',
      dataIndex: 'putInInstitutions',
    },
    {
      title: '入库仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '入库时间',
      dataIndex: 'putInTime',
      // render: text => text.split(' ').length > 0 ? text.split(' ')[0] : text
    },
    {
      title: '入库批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '出库仓库',
      dataIndex: 'outWarehouse',
    },
    {
      title: '出库单号',
      dataIndex: 'putOutOrderNumber',
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '是否提交',
      dataIndex: 'isSubmit',
      render: text => {
        let result = '';
        switch (text) {
          case true:
            result = '已提交';
            break;
          case false:
            result = '未提交';
            break;
        }
        return result;
      }
    },
    {
      title: '审批结果',
      dataIndex: 'submitResult',
      render: (text, record) => {
        let result = '';
        if (record.isSubmit) {
          switch (text.toString()) {
            case '0':
              result = '审批中';
              break;
            case '1':
              result = '已批准';
              break;
            case '2':
              result = '未批准';
              break;
          }
        }
        return result;
      }
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 80,
      render: (text, record) => {
        const menu = (
          <Menu onClick={(e) => e.domEvent.stopPropagation()}>
            <Menu.Item disabled={!!record.isSubmit} >
              { !!record.isSubmit ? '修改' : <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a> }
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.handleDetailModalVisible(true, record, this)}>查看</a>
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement='bottomRight'>
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        )
      },
    },
  ];

  recordSubmit = (distributionPutInIds) => {
    console.log('distributionPutInIds: ', distributionPutInIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'deliveryIn/submitDeliveryInOrders',
      payload: { distributionPutInIds },
      callback: () => {
        //1、刷新列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、清空selectedRows
        this.setState({ selectedRows: [] })
      },
    });
  }

  recordRemove = distributionPutInId => {
    console.log('distributionPutInId: ', distributionPutInId);
    const { dispatch } = this.props;
    dispatch({
      type: 'deliveryIn/removeDeliveryInOrder',
      payload: { distributionPutInId },
      callback: () => {
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        this.setState({ selectedRows: [] })
      },
    });
  };

  expandedRowRender = record => {
    const {
      deliveryIn: { deliveryInOrderInventoryList },
    } = this.props;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        // width: 60,
        // fixed: 'left',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        // width: 100,
        // fixed: 'left',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        // width: 200,
        // fixed: 'left',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '入库主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '入库辅数量',
        dataIndex: 'number2',
      },
      {
        title: '出库主数量',
        dataIndex: 'outNumber',
      },
      {
        title: '出库辅数量',
        dataIndex: 'outNumber2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      //   editable: true,
      // },
      {
        title: '供应商',
        dataIndex: 'supplier',
      },
      {
        title: '含税单价(¥)',
        dataIndex: 'referenceCost',
        // dataIndex: 'warehousePrice',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '税率(%)',
        dataIndex: 'taxRate',
      },
      {
        title: '备注',
        dataIndex: 'remark',
        editable: true,
      },
    ];

    const total = () => {
      let result = `合计:  主数量0 / 辅数量0`;
      if (deliveryInOrderInventoryList.length > 0) {
        const xiang = deliveryInOrderInventoryList.map(item => item.number2 || 0).reduce((a, b) => (a*100+b*100)/100);
        const ping = deliveryInOrderInventoryList.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b));
        result = `合计: 主数量${ping} / 辅数量${parseInt(xiang*100)/100}`
      }
      return result
    }

    return <Table
      size='small'
      columns={columns}
      dataSource={deliveryInOrderInventoryList}
      footer={total}
      // footer={() => `合计: ${deliveryInOrderInventoryList.length}`}
      rowKey='dpiStockId'
    />
  };

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //部门列表
    dispatch({
      type: 'deliveryIn/fetchBranchList',
      payload: {
        isWarehouse: true
      }
    });

    //获取当前部门请购人员列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'deliveryIn/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      //入库单列表
      dispatch({
        type: 'deliveryIn/fetchDeliveryInOrderList',
      });

      //仓库列表
      // dispatch({
      //   type: 'deliveryIn/fetchWarehouseList',
      // });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });

      //入库仓库列表
      //仓库列表
      dispatch({
        type: 'deliveryIn/fetchWarehouseList',
        payload: {
          branchId: currentUser.personnel.branchId
        }
      });

      //入库单列表
      dispatch({
        type: 'deliveryIn/fetchDeliveryInOrderList',
        payload: {
          putInInstitutions: currentUser.personnel.branch
        }
      });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'deliveryIn/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({ purchasePersonnel: [] })
    dispatch({
      type: 'deliveryIn/fetchDeliveryInOrderList',
      payload: {
        ...form.getFieldsValue()
      }
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.executeSearch(fieldsValue)
    });
  };

  executeSearch = (fieldsValue) => {
    const { dispatch } = this.props;
    const putInTime = fieldsValue.putInTime;
    if (putInTime) {
      fieldsValue.putInTime = putInTime.format('YYYY-MM-DD')
    }
    dispatch({
      type: 'deliveryIn/fetchDeliveryInOrderList',
      payload: { ...fieldsValue },
    });
  }

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    })
  }

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetail = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'deliveryIn/updatePurchase',
      payload: { ...fields, purchaseRequisitionId: record.purchaseRequisitionId },
    });

    this.handleUpdateModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedRows, expandedRowKeys } = this.state;
    dispatch({
      type: 'deliveryIn/updateDeliveryInOrder',
      payload: { ...fields, distributionPutInId: record.distributionPutInId,  },
      callback: () => {
        //1、刷新申请单列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、刷新申请单明细列表
        if (expandedRowKeys.length > 0){
          this.onExpand(true, record);
        }
      }
    });

    //若该项被选择了，则需要取消对该项的选择
    if (fields.submit) {
      this.setState({ selectedRows: selectedRows.filter(item => item.distributionPutInId !== record.distributionPutInId) })
    }

    this.handleUpdateModalVisible();
  };

  onSendOutInstitutionsChange = (value, e) => {
    const { dispatch, user: { currentUser }, form } = this.props;
    const branchId = e.props.branchId;
    console.log('branchId: ', branchId);

    //出库仓库列表
    dispatch({
      type: 'deliveryIn/fetchWarehouseList',
      payload: {
        branchId: branchId,
      },
    });

    form.setFieldsValue({ warehouse: '' });
  }

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const putInTime = fieldsValue.putInTime;
      if (putInTime) {
        fieldsValue.putInTime = fieldsValue.putInTime.format('YYYY-MM-DD')
      }
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/distributionPutIn/exportList?${stringify(params)}`;
    });
  }

  renderSimpleForm() {
    const {
      deliveryIn: { branchList, staffList, warehouseList },
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    const { purchasePersonnel, dataPermission } = this.state;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="订单编号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单编号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库机构">
              {getFieldDecorator('putInInstitutions', {
                initialValue: dataPermission ? '' : currentUser.personnel.branch
              })(
                <Select
                  disabled={!dataPermission}
                  placeholder="请选择入库机构"
                  style={{ width: '100%' }}
                  onChange={this.onSendOutInstitutionsChange}
                >
                  {branchList.rows.map(item => <Select.Option branchId={item.branchId} key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库仓库">
              {getFieldDecorator('warehouse')(
                <Select placeholder="请选择入库仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="入库日期">
              {getFieldDecorator('putInTime')(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="入库日期"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库单号">
              {getFieldDecorator('putOutOrderNumber')(<Input placeholder="请输入调拨出库单号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const distributionPutInId = record.distributionPutInId;
      dispatch({
        type: 'deliveryIn/fetchDeliveryInOrderInventoryList',
        payload: {
          distributionPutInIds: distributionPutInId
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (record.isSubmit) return;

    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.purchaseRequisitionId === record.purchaseRequisitionId) {
          newSelectedRows = newSelectedRows.filter(item => item.purchaseRequisitionId !== record.purchaseRequisitionId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  render() {
    const {
      deliveryIn: { deliveryInOrderList },
      loading,
    } = this.props;
    const { selectedRows, updateModalVisible, expandedRowKeys, record, detailModalVisible } = this.state;

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
      handleDetail: this.handleDetail,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => router.push('/delivery-management/delivery-in/delivery-in-order')}>
                新增
              </Button>
              {/*<Button >审核</Button>*/}
              {selectedRows.length > 0 && (
                <span>
                  <Button
                    onClick={() => this.recordSubmit( selectedRows.map(item => item.distributionPutInId).join(','))}
                  >批量提交</Button>
                  <Popconfirm
                    title="确认删除？"
                    onConfirm={() => this.recordRemove( selectedRows.map(item => item.distributionPutInId).join(','))}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={deliveryInOrderList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              rowKey='distributionPutInId'
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
              onRow={(record) => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
              checkboxProps={record => ({
                disabled: record.isSubmit,
              })}
            />
          </div>
        </Card>
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
        <DetailForm {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
      </div>
    );
  }
}

export default DeliveryInOrderList;
