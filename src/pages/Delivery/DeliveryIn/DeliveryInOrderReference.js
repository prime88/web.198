import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Select,
  Button,
  DatePicker,
  Table,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './DeliveryInOrderReference.less';
import { fetchStaffList } from '@/services/branch';
import { fetchDeliveryOutOrderInventoryList } from '@/services/deliveryOut';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ deliveryIn, user, loading }) => ({
  deliveryIn,
  user,
  loading: loading.models.deliveryIn,
}))
@Form.create()
class DeliveryInOrderReference extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    selectedRowKeys: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailVisible: false,
    detailModalVisible: false,
    dataPermission: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    {
      title: '送达机构',
      dataIndex: 'deliveryInstitutions',
    },
    {
      title: '送出机构',
      dataIndex: 'sendOutInstitutions',
    },
    {
      title: '出库时间',
      dataIndex: 'putOutTime',
      // render: text => text.split(' ').length > 0 ? text.split(' ')[0] : text
    },
    {
      title: '预计送达时间',
      dataIndex: 'estimateTime'
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  onRowSelectionChange = (selectedRowKeys) => {
    console.log('selectedRowKeys: ', selectedRowKeys);
    this.setState({ selectedRowKeys })
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;
    //出库单列表
    dispatch({
      type: 'deliveryIn/fetchDeliveryOutOrderList',
      payload: {
        submitResult: 1, //审批=批准，才能参照
        isReference: false, //只能参照没有被参照的
        deliveryInstitutions: currentUser.personnel.branch,
      }
    });

    //门店列表
    dispatch({
      type: 'deliveryIn/fetchStoreList',
    });

    //数据权限
//1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
  };

  handleFormReset = () => {
    const { form, dispatch, user: { currentUser } } = this.props;
    form.resetFields();
    dispatch({
      type: 'deliveryIn/fetchDeliveryOutOrderList',
      payload: {
        submitResult: 1, //审批=批准，才能参照
        isReference: false, //只能参照没有被参照的
        deliveryInstitutions: currentUser.personnel.branch,
      }
    });
  };

  onSelect = (record, selected, selectedRows, nativeEvent) => {
    console.log('record: ', record);
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    const { dispatch, deliveryIn } = this.props;
    const distributionPutOutId = record.distributionPutOutId;
    if (selected) {
      const response = fetchDeliveryOutOrderInventoryList({ distributionPutOutIds: distributionPutOutId });
      response.then(result => {
        console.log('result: ', result);
        this.setState(prevState => {
          return {
            // dataSource: [...prevState.dataSource, ...result.data]
            dataSource: [...result.data]
          }
        })
      })
    }
    else{//删除
      const { dataSource } = this.state;
      const newDataSource = dataSource.filter(item => item.distributionPutOutId.toString() !== distributionPutOutId.toString());
      this.setState({ dataSource: newDataSource })
    }
  }

  handleSelectRows = rows => {
    console.log('rows: ', rows);
    this.setState({
      selectedRows: rows,
    });

    if (rows.length > 0) {
      const response = fetchDeliveryOutOrderInventoryList({ distributionPutOutIds: rows.map(item => item.distributionPutOutId).join(',') });
      response.then(result => {
        console.log('result: ', result);
        this.setState({
          dataSource: result.data
        })
      })
    }
    else{
      this.setState({ dataSource: [] })
    }

  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form, user: { currentUser } } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const estimateTime = fieldsValue.estimateTime;
      if (estimateTime) {
        fieldsValue.estimateTime = fieldsValue.estimateTime.format('YYYY-MM-DD')
      }
      dispatch({
        type: 'deliveryIn/fetchDeliveryOutOrderList',
        payload: {
          ...fieldsValue,
          submitResult: 1, //审批=批准，才能参照
          isReference: false, //只能参照没有被参照的
          deliveryInstitutions: currentUser.personnel.branch,
        },
      });
    });
  };

  renderSimpleForm() {
    const {
      deliveryIn: { storeList },
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="送达机构">
              {getFieldDecorator('deliveryInstitutions', {
                initialValue: currentUser.personnel.branch
              })(
                <Select disabled placeholder="请选择送达机构" style={{ width: '100%' }}>
                  {storeList.rows.map(item => <Select.Option key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="预计送达">
              {getFieldDecorator('estimateTime')(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择预计送达日期"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      detailVisible: false,
    });
  }

  render() {
    const {
      deliveryIn: { deliveryOutOrderList },
      loading,
    } = this.props;
    const { selectedRows, dataSource } = this.state;

    const columns = [
      {
        title: '序号',
        dataIndex: '',
        width: 60,
        fixed: 'left',
        render: (text, record, index) => index+1
      },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        width: 100,
        fixed: 'left',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        width: 200,
        fixed: 'left',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '主单位',
        dataIndex: 'units',
      },
      {
        title: '主数量',
        dataIndex: 'number',
      },
      {
        title: '辅单位',
        dataIndex: 'units2',
      },
      {
        title: '辅数量',
        dataIndex: 'number2',
      },
      {
        title: '单位转换率',
        dataIndex: 'unitRatio',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      // {
      //   title: '仓库',
      //   dataIndex: 'warehouse',
      // },
      // {
      //   title: '数量',
      //   dataIndex: 'number',
      // },
      {
        title: '含税单价',
        dataIndex: 'referenceCost',
        // dataIndex: 'warehousePrice',
        render: text => this.state.dataPermission ? <span>{text}</span> : ''
      },
      {
        title: '供应商',
        dataIndex: 'supplier',
      },
      {
        title: '备注',
        dataIndex: 'remark',
      },
    ];

    return (
      <div>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          <StandardTable
            hideDefaultSelections={true}
            selectedRows={selectedRows}
            // loading={loading}
            data={deliveryOutOrderList}
            columns={this.columns}
            onSelectRow={this.handleSelectRows}
            // onSelect={this.onSelect}
            onChange={this.handleStandardTableChange}
            rowKey='distributionPutOutId'
            paginationOfTable={{pageSize: 5, size: 'small'}}
            size='small'
            type='radio'
            title={() => <div style={{textAlign: 'center'}}>出库单列表</div>}
            onRow={(record) => {
              return {
                onClick: () => {// 点击行
                  console.log('record: ', record);
                  // console.log('oldSelectedRows: ', this.state.selectedRows);
                  //选择效果
                  this.setState(prevState => {
                    let newSelectedRows = [record];

                    //更新调货明细
                    this.onSelect(record, true);

                    //多选代码
                    // let newSelectedRows = [...prevState.selectedRows];
                    // let flag = true;
                    // for (let item of prevState.selectedRows) {
                    //   if (item.distributionPutOutId === record.distributionPutOutId) {
                    //     newSelectedRows = newSelectedRows.filter(item => item.distributionPutOutId !== record.distributionPutOutId);
                    //     flag = false;
                    //     break;
                    //   }
                    // }
                    // if (flag){
                    //   newSelectedRows.push(record);
                    // }
                    // console.log('newSelectedRows: ', newSelectedRows);

                    //更新调货明细
                    // this.onSelect(record, flag);

                    return ({
                      selectedRows: newSelectedRows
                    })
                  });
                },
              };
            }}

          />
          <Table
            loading={loading}
            dataSource={dataSource}
            columns={columns}
            size='small'
            title={() => <div style={{textAlign: 'center'}}>出库明细</div>}
          />
        </div>
      </div>
    );
  }
}

export default DeliveryInOrderReference;
