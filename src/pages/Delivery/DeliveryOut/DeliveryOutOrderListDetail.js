import React, { Component } from 'react';
import { connect } from 'dva';
import { Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import styles from './DeliveryOutOrderListDetail.less';

const { Description } = DescriptionList;

@connect(({ deliveryOut, user, loading }) => ({
  deliveryOut,
  user,
  // loading: loading.effects['profile/fetchBasic'],
}))
class DeliveryOutOrderListDetail extends Component {
  state = {
    dataSource: [],
    dataPermission: false,
  }

  componentDidMount() {
    const { dispatch, record, user: { currentUser } } = this.props;
    const distributionPutOutId = record.distributionPutOutId;
    if (distributionPutOutId) {
      dispatch({
        type: 'deliveryOut/fetchDeliveryOutOrderInventoryList',
        payload: {
          distributionPutOutIds: distributionPutOutId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }


//数据权限
//1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }

  }

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '出库主数量',
      dataIndex: 'number',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '出库辅数量',
      dataIndex: 'number2',
    },
    {
      title: '入库主数量',
      dataIndex: 'inNumber',
    },
    {
      title: '入库辅数量',
      dataIndex: 'inNumber2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    // {
    //   title: '数量',
    //   dataIndex: 'number',
    // },
    {
      title: '含税单价',
      dataIndex: 'referenceCost',
      // dataIndex: 'warehousePrice',
      render: text => this.state.dataPermission ? <span>{text}</span> : ''
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="送出机构">{record.sendOutInstitutions}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="出库仓库">{record.warehouse}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="预计送达">{record.estimateTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="出库人员">{record.putOutBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="出库时间">{record.putOutTime}</Description>

            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="送达机构">{record.deliveryInstitutions}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库仓库">{record.inWarehouse}</Description>


            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>订单明细</div>
          <Table
            style={{ marginBottom: 24 }}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="dpoStockId"
            pagination={{defaultPageSize: 9}}
            size='small'
          />
        </div>
      </div>
    );
  }
}

export default DeliveryOutOrderListDetail;
