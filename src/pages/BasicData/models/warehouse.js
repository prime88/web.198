// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryWarehouseList,
  queryInventoryList,
  addInventoryClassification,
  addWarehouse,
  addInventory,
  updateInventoryClassification,
  updateWarehouse,
  updateInventory,
  removeInventoryClassification,
  removeWarehouse,
  removeInventory,
  changeInventoryClassificationStatus,
  changeWarehouseStatus,
  changeInventory,
} from '@/services/warehouse';
import { fetchBranchList } from '@/services/branch';
import { queryDictionary } from '@/services/common-api';
import { message, Modal } from 'antd';

export default {
  namespace: 'warehouse',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //仓库列表
    warehouseList: {
      rows: [],
      pagination: {},
    },
    //仓库分类列表
    warehouseClassificationList: {
      rows: [],
      pagination: {},
    },
    //部门列表
    branchList: [],
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehouseList',
        payload: response.data,
      });
    },
    *fetchWarehouseClassificationList({ payload }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveWarehouseClassificationList',
        payload: response.data,
      });
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addInventoryClassification({ payload, callback }, { call, put }) {
      const response = yield call(addInventoryClassification, payload);
      if (response.code === '0') {
        // message.success(response.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback(response);
      } else {
        message.error(response.msg);
      }
    },
    *addWarehouse({ payload, callback }, { call, put }) {
      const response = yield call(addWarehouse, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchWarehouseList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addInventory({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(addInventory, payload);
      if (response.code === '0') {
        Modal.success({
          title: response.msg,
          content: (
            <div>
              新增存货需
              <span style={{ color: 'red' }}>开启</span>
              后才能正常使用！
            </div>
          ),
        });
        // message.success(response.msg);
        yield put({
          type: 'fetchInventoryList',
          payload: {
            ...fetchForm,
          },
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeInventoryClassification({ payload, callback }, { call, put }) {
      const response = yield call(removeInventoryClassification, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeWarehouse({ payload, callback }, { call, put }) {
      const response = yield call(removeWarehouse, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchWarehouseList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeInventory({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(removeInventory, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchInventoryList',
          payload: {
            ...fetchForm,
          },
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateInventoryClassification({ payload, callback }, { call, put }) {
      const response = yield call(updateInventoryClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateWarehouse({ payload, callback }, { call, put }) {
      const response = yield call(updateWarehouse, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchWarehouseList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateInventory({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(updateInventory, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryList',
          payload: {
            ...fetchForm,
          },
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeInventoryClassificationStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeInventoryClassificationStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeWarehouseStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeWarehouseStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchWarehouseList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeInventory({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(changeInventory, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryList',
          payload: {
            ...fetchForm,
          },
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
    saveWarehouseClassificationList(state, action) {
      return {
        ...state,
        warehouseClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
