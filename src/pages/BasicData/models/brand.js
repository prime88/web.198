// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryBrandList,
  addBrand,
  updateBrand,
  removeBrand,
  changeBrandStatus,
} from '@/services/brand';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'brand',

  state: {
    //品牌列表
    brandList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchBrandList({ payload, callback }, { call, put }) {
      const response = yield call(queryBrandList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBrandList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addBrand({ payload, callback }, { call, put }) {
      const response = yield call(addBrand, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBrandList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeBrand({ payload, callback }, { call, put }) {
      const response = yield call(removeBrand, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchBrandList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateBrand({ payload, callback }, { call, put }) {
      const response = yield call(updateBrand, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBrandList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeBrandStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeBrandStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBrandList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveBrandList(state, action) {
      return {
        ...state,
        brandList: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //品牌管理
  //       if (location.pathname === '/basic-data/brand-management') {
  //         dispatch({
  //           type: 'fetchBrandList',
  //         })
  //       }
  //     });
  //   },
  // },
};
