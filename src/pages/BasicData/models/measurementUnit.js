// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryMeasurementUnitClassificationList,
  queryMeasurementUnitList,
  addMeasurementUnitClassification,
  addMeasurementUnit,
  updateMeasurementUnitClassification,
  updateMeasurementUnit,
  removeMeasurementUnitClassification,
  removeMeasurementUnit,
  changeMeasurementUnitClassificationStatus,
  changeMeasurementUnitStatus,
} from '@/services/measurementUnit';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'measurementUnit',

  state: {
    //计量单位分类列表
    measurementUnitClassificationList: {
      rows: [],
      pagination: {},
    },
    //计量单位列表
    measurementUnitList: {
      rows: [],
      pagination: {},
    },
    defaultFieldList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchMeasurementUnitClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryMeasurementUnitClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveMeasurementUnitClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchMeasurementUnitList({ payload }, { call, put }) {
      const response = yield call(queryMeasurementUnitList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveMeasurementUnitList',
        payload: response.data,
      });
    },
    *fetchDefaultFieldList({ payload }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDefaultFieldList',
        payload: response.data,
      });
    },
    *addMeasurementUnitClassification({ payload, callback }, { call, put }) {
      const response = yield call(addMeasurementUnitClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMeasurementUnitClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addMeasurementUnit({ payload, selectedKey, callback }, { call, put }) {
      const response = yield call(addMeasurementUnit, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMeasurementUnitList',
          payload: {
            unitsClassifyId: selectedKey === '-1' ? '' : selectedKey
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeMeasurementUnitClassification({ payload, callback }, { call, put }) {
      const response = yield call(removeMeasurementUnitClassification, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchMeasurementUnitClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeMeasurementUnit({ payload, selectedKey, callback }, { call, put }) {
      const response = yield call(removeMeasurementUnit, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchMeasurementUnitList',
          payload: {
            unitsClassifyId: selectedKey === '-1' ? '' : selectedKey
          }
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateMeasurementUnitClassification({ payload, callback }, { call, put }) {
      const response = yield call(updateMeasurementUnitClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMeasurementUnitClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateMeasurementUnit({ payload, selectedKey, callback }, { call, put }) {
      const response = yield call(updateMeasurementUnit, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMeasurementUnitList',
          payload: {
            unitsClassifyId: selectedKey === '-1' ? '' : selectedKey
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeMeasurementUnitClassificationStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeMeasurementUnitClassificationStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMeasurementUnitClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeMeasurementUnitStatus({ payload, selectedKey, callback }, { call, put }) {
      const response = yield call(changeMeasurementUnitStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMeasurementUnitList',
          payload: {
            unitsClassifyId: selectedKey === '-1' ? '' : selectedKey
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveMeasurementUnitClassificationList(state, action) {
      return {
        ...state,
        measurementUnitClassificationList: action.payload,
      };
    },
    saveMeasurementUnitList(state, action) {
      return {
        ...state,
        measurementUnitList: action.payload,
      };
    },
    saveDefaultFieldList(state, action) {
      return {
        ...state,
        defaultFieldList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //计量单位分类
  //       if (location.pathname === '/basic-data/measurement-unit-classification') {
  //         dispatch({
  //           type: 'fetchMeasurementUnitClassificationList',
  //         })
  //       }
  //       //计量单位
  //       if (location.pathname === '/basic-data/measurement-unit') {
  //         dispatch({
  //           type: 'fetchMeasurementUnitList',
  //         })
  //       }
  //     });
  //   },
  // },
};
