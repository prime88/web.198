// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  querySupplierClassificationList,
  querySupplierList,
  addSupplierClassification,
  addSupplier,
  updateSupplierClassification,
  updateSupplier,
  removeSupplierClassification,
  removeSupplier,
  changeSupplierClassificationStatus,
  changeSupplierStatus,
} from '@/services/supplier';
import { message } from 'antd';

export default {
  namespace: 'supplier',

  state: {
    //供应商分类列表
    supplierClassificationList: {
      rows: [],
      pagination: {},
    },
    //供应商列表
    supplierList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchSupplierClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(querySupplierClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSupplierClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchSupplierList({ payload }, { call, put }) {
      const response = yield call(querySupplierList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSupplierList',
        payload: response.data,
      });
    },
    *addSupplierClassification({ payload, callback }, { call, put }) {
      const response = yield call(addSupplierClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSupplierClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addSupplier({ payload, callback }, { call, put }) {
      const response = yield call(addSupplier, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSupplierList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeSupplierClassification({ payload, callback }, { call, put }) {
      const response = yield call(removeSupplierClassification, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchSupplierClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeSupplier({ payload, callback }, { call, put }) {
      const response = yield call(removeSupplier, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchSupplierList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateSupplierClassification({ payload, callback }, { call, put }) {
      const response = yield call(updateSupplierClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSupplierClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateSupplier({ payload, callback }, { call, put }) {
      const response = yield call(updateSupplier, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSupplierList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeSupplierClassificationStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeSupplierClassificationStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSupplierClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeSupplierStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeSupplierStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSupplierList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveSupplierClassificationList(state, action) {
      return {
        ...state,
        supplierClassificationList: action.payload,
      };
    },
    saveSupplierList(state, action) {
      return {
        ...state,
        supplierList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //供应商分类
  //       if (location.pathname === '/basic-data/supplier-classification') {
  //         dispatch({
  //           type: 'fetchSupplierClassificationList',
  //         })
  //       }
  //       //供应商分类
  //       if (location.pathname === '/basic-data/supplier') {
  //         dispatch({
  //           type: 'fetchSupplierList',
  //         })
  //       }
  //     });
  //   },
  // },
};
