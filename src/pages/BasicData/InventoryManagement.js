import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Cascader,
  Popconfirm,
  Upload,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { querySupplierList } from '@/services/supplier';
import { isInventoryNameRepeated } from '@/services/warehouse';
import { isInventoryNumRepeated } from '@/services/warehouse';
import { queryBrandList } from '@/services/brand';
import styles from './InventoryManagement.less';
import { apiDomainName, imgDomainName } from '../../constants';
import { stringify } from 'qs';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {
  state = {
    //主计量单位列表
    primaryMeasurementUnitList: {
      rows: [],
      pagination: {},
    },
    //辅计量单位列表
    assistantMeasurementUnitList: {
      rows: [],
      pagination: {},
    },
    //供应商列表
    supplierList: {
      rows: [],
      pagination: {},
    },
    //品牌列表
    brandList: {
      rows: [],
      pagination: {},
    },
    supplierClassifyId: '',
  };

  componentWillReceiveProps(nextProps) {
    const { modalVisible, selectedMenuItemKey } = this.props;
    if (!modalVisible && nextProps.modalVisible && selectedMenuItemKey !== '-1') {
      this.onStockClassifyIdChange(selectedMenuItemKey);
    }
  }

  handlePrimaryMeasurementUnitClassificationChange = (value, e) => {
    const { form } = this.props;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: value });
    response.then(result => {
      this.setState({ primaryMeasurementUnitList: result.data });
      if (e !== undefined) {
        form.setFieldsValue({ unitsId: '' });
      }
    });
  };

  handleAssistantMeasurementUnitClassificationChange = (value, e) => {
    const { form } = this.props;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: value });
    response.then(result => {
      this.setState({ assistantMeasurementUnitList: result.data });
      if (e !== undefined) {
        form.setFieldsValue({ unitsId2: '' });
      }
    });
  };

  handleSupplierClassificationChange = (value, e) => {
    const { form } = this.props;

    this.setState({ supplierClassifyId: value });

    // load options lazily
    const response = querySupplierList({ supplierClassifyId: value });
    response.then(result => {
      this.setState({ supplierList: result.data });
      if (e !== undefined) {
        form.setFieldsValue({ supplierId: '' });
      }
    });
  };

  onStockClassifyIdChange = stockClassifyId => {
    const response = queryBrandList({ stockClassifyId });
    response.then(result => {
      console.log('result: ', result);
      this.setState({ brandList: result.data });

      this.props.form.setFieldsValue({ brandId: '' });
    });
  };

  clearStateCache = () => {
    this.setState({
      //主计量单位列表
      primaryMeasurementUnitList: {
        rows: [],
        pagination: {},
      },
      //辅计量单位列表
      assistantMeasurementUnitList: {
        rows: [],
        pagination: {},
      },
      //供应商列表
      supplierList: {
        rows: [],
        pagination: {},
      },
      //品牌列表
      brandList: {
        rows: [],
        pagination: {},
      },
    });
  };

  onCancel = () => {
    const { handleModalVisible } = this.props;
    handleModalVisible();
    this.clearStateCache();
  };

  render() {
    const {
      modalVisible,
      form,
      handleAdd,
      inventoryClassificationList,
      selectedMenuItemKey,
      measurementUnitClassificationList,
      supplierClassificationList,
    } = this.props;
    const {
      primaryMeasurementUnitList,
      assistantMeasurementUnitList,
      supplierList,
      brandList,
      supplierClassifyId,
    } = this.state;

    const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleAdd({ ...fieldsValue, supplierClassifyId });
        this.clearStateCache();
      });
    };

    const checkName = (rule, value, callback) => {
      if (!value) {
        callback();
        return;
      }
      const response = isInventoryNameRepeated({ name: value });
      response.then(result => {
        if (!result.data) {
          callback();
          return;
        }
        callback('存在相同的名称!');
      });
    };

    const checkNum = (rule, value, callback) => {
      if (!value) {
        callback();
        return;
      }
      const response = isInventoryNumRepeated({ stockNum: value });
      response.then(result => {
        if (!result.data) {
          callback();
          return;
        }
        callback('存在相同的编号!');
      });
    };

    return (
      <Modal
        destroyOnClose
        title="新增存货"
        visible={modalVisible}
        onOk={okHandle}
        onCancel={this.onCancel}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="存货编号">
          {form.getFieldDecorator('stockNum', {
            rules: [{ required: true, message: '请输入编号！' }, { validator: checkNum }],
          })(<Input placeholder="请输入编号" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="存货名称">
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          })(<Input placeholder="请输入名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="主计量单位">
          <Select
            style={{ width: '49%' }}
            onChange={this.handlePrimaryMeasurementUnitClassificationChange}
            placeholder="单位分类"
          >
            {measurementUnitClassificationList.rows.map(item => (
              <Option key={item.unitsClassifyId} value={item.unitsClassifyId}>
                {item.name}
              </Option>
            ))}
          </Select>
          &nbsp;
          {form.getFieldDecorator('unitsId', {
            rules: [{ required: true, message: '请选择主计量单位！' }],
          })(
            <Select style={{ width: '49%' }} placeholder="主计量单位">
              {primaryMeasurementUnitList.rows.map(item => (
                <Option key={item.unitsId} value={item.unitsId}>
                  {item.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="辅计量单位">
          <Select
            style={{ width: '49%' }}
            onChange={this.handleAssistantMeasurementUnitClassificationChange}
            placeholder="单位分类"
          >
            {measurementUnitClassificationList.rows.map(item => (
              <Option key={item.unitsClassifyId} value={item.unitsClassifyId}>
                {item.name}
              </Option>
            ))}
          </Select>
          &nbsp;
          {form.getFieldDecorator('unitsId2', {
            rules: [{ required: true, message: '请选择辅计量单位！' }],
          })(
            <Select style={{ width: '49%' }} placeholder="辅计量单位">
              {assistantMeasurementUnitList.rows.map(item => (
                <Option key={item.unitsId} value={item.unitsId}>
                  {item.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="单位转换率">
          {form.getFieldDecorator('unitRatio', {
            rules: [{ required: true, message: '请输入单位转换率！' }],
          })(
            <InputNumber
              precision={0}
              style={{ width: '100%' }}
              placeholder="请输入单位转换率"
              min={1}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="物品分类">
          {form.getFieldDecorator('stockClassifyId', {
            rules: [{ required: true, message: '请选择物品分类！' }],
            initialValue: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
          })(
            <Select
              showSearch
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              onChange={this.onStockClassifyIdChange}
              style={{ width: '100%' }}
              placeholder="请选择物品分类"
            >
              {inventoryClassificationList.rows.map(item => (
                <Select.Option key={item.stockClassifyId}>{item.name}</Select.Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="品牌">
          {form.getFieldDecorator('brandId', {
            rules: [{ required: true, message: '请选择品牌！' }],
          })(
            <Select
              showSearch
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              style={{ width: '100%' }}
              placeholder="请选择品牌"
            >
              {brandList.rows.map(item => (
                <Select.Option key={item.brandId}>{item.name}</Select.Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="参考成本">
          {form.getFieldDecorator('referenceCost', {
            rules: [{ required: true, message: '请输入参考成本！' }],
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入参考成本"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售参考价">
          {form.getFieldDecorator('referencePrice', {
            rules: [{ required: true, message: '请输入零售参考价！' }],
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入零售参考价"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售最低价">
          {form.getFieldDecorator('lowerPrice', {
            rules: [{ required: true, message: '请输入零售最低价！' }],
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入零售最低价"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售最高价">
          {form.getFieldDecorator('upperPrice', {
            rules: [{ required: true, message: '请输入零售最高价！' }],
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入零售最高价"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="规格型号">
          {form.getFieldDecorator('specification', {
            rules: [{ required: true, message: '请输入规格型号！' }],
          })(<Input placeholder="请输入规格型号" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="供应商">
          <Select
            style={{ width: '49%' }}
            onChange={this.handleSupplierClassificationChange}
            placeholder="供应商分类"
          >
            {supplierClassificationList.rows.map(item => (
              <Option key={item.supplierClassifyId} value={item.supplierClassifyId}>
                {item.name}
              </Option>
            ))}
          </Select>
          &nbsp;
          {form.getFieldDecorator('supplierId', {
            rules: [{ required: true, message: '请选择供应商！' }],
          })(
            <Select
              style={{ width: '49%' }}
              placeholder="供应商"
              dropdownMatchSelectWidth={false}
              showSearch
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {supplierList.rows.map(item => (
                <Option key={item.supplierId} value={item.supplierId}>
                  {item.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="税率(%)">
          {form.getFieldDecorator('taxRate', {
            rules: [{ required: true, message: '请输入税率！' }],
          })(
            <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入税率" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  state = {
    //主计量单位列表
    primaryMeasurementUnitList: {
      rows: [],
      pagination: {},
    },
    //辅计量单位列表
    assistantMeasurementUnitList: {
      rows: [],
      pagination: {},
    },
    //供应商列表
    supplierList: {
      rows: [],
      pagination: {},
    },
    //品牌列表
    brandList: {
      rows: [],
      pagination: {},
    },
    supplierClassifyId: '',
  };

  componentWillReceiveProps(nextProps) {
    const { updateModalVisible } = this.props;
    if (!updateModalVisible && nextProps.updateModalVisible) {
      this.handlePrimaryMeasurementUnitClassificationChange(nextProps.record.unitsClassifyId);

      this.handleAssistantMeasurementUnitClassificationChange(nextProps.record.unitsClassifyId2);

      this.handleSupplierClassificationChange(nextProps.record.supplierClassifyId);

      this.onStockClassifyIdChange(nextProps.record.stockClassifyId);
    }
  }

  handlePrimaryMeasurementUnitClassificationChange = (value, e) => {
    const { form } = this.props;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: value });
    response.then(result => {
      this.setState({ primaryMeasurementUnitList: result.data });
      if (e !== undefined) {
        form.setFieldsValue({ unitsId: '' });
      }
    });
  };

  handleAssistantMeasurementUnitClassificationChange = (value, e) => {
    const { form } = this.props;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: value });
    response.then(result => {
      this.setState({ assistantMeasurementUnitList: result.data });
      if (e !== undefined) {
        form.setFieldsValue({ unitsId2: '' });
      }
    });
  };

  handleSupplierClassificationChange = (value, e) => {
    const { form } = this.props;

    this.setState({ supplierClassifyId: value });

    // load options lazily
    const response = querySupplierList({ supplierClassifyId: value });
    response.then(result => {
      this.setState({ supplierList: result.data });
      if (e !== undefined) {
        form.setFieldsValue({ supplierId: '' });
      }
    });
  };

  checkName = (rule, value, callback) => {
    const { record } = this.props;

    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isInventoryNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  checkNum = (rule, value, callback) => {
    const { record } = this.props;
    if (!value) {
      callback();
      return;
    }

    if (value === record.stockNum) {
      callback();
      return;
    }

    const response = isInventoryNumRepeated({ stockNum: value, stockId: record.stockId });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的编号!');
    });
  };

  okHandle = () => {
    const { form, handleUpdate } = this.props;
    const { supplierClassifyId } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate({ ...fieldsValue, supplierClassifyId });
    });
  };

  onStockClassifyIdChange = (stockClassifyId, callback) => {
    console.log('callback: ', callback);
    console.log('stockClassifyId: ', stockClassifyId);
    const response = queryBrandList({ stockClassifyId });
    response.then(result => {
      console.log('result: ', result);
      this.setState({ brandList: result.data });

      if (callback) {
        callback();
        this.props.form.setFieldsValue({ brandId: '' });
      }
    });
  };

  render() {
    const {
      updateModalVisible,
      form,
      handleUpdateModalVisible,
      inventoryClassificationList,
      record,
      measurementUnitClassificationList,
      supplierClassificationList,
    } = this.props;
    const {
      primaryMeasurementUnitList,
      assistantMeasurementUnitList,
      supplierList,
      brandList,
    } = this.state;

    return (
      <Modal
        destroyOnClose
        title="修改存货"
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => handleUpdateModalVisible()}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="存货编号">
          {form.getFieldDecorator('stockNum', {
            rules: [{ required: true, message: '请输入编号！' }, { validator: this.checkNum }],
            initialValue: record.stockNum,
          })(<Input disabled placeholder="请输入编号" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: '请输入名称！' }, { validator: this.checkName }],
            initialValue: record.name,
          })(<Input disabled placeholder="请输入名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="主计量单位">
          <Select
            defaultValue={record.unitsClassifyId}
            style={{ width: '49%' }}
            onChange={this.handlePrimaryMeasurementUnitClassificationChange}
            placeholder="单位分类"
          >
            {measurementUnitClassificationList.rows.map(item => (
              <Option key={item.unitsClassifyId} value={item.unitsClassifyId}>
                {item.name}
              </Option>
            ))}
          </Select>
          &nbsp;
          {form.getFieldDecorator('unitsId', {
            rules: [{ required: true, message: '请选择主计量单位！' }],
            initialValue: record.unitsId,
          })(
            <Select style={{ width: '49%' }} placeholder="主计量单位">
              {primaryMeasurementUnitList.rows.map(item => (
                <Option key={item.unitsId} value={item.unitsId}>
                  {item.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="辅计量单位">
          <Select
            defaultValue={record.unitsClassifyId2}
            style={{ width: '49%' }}
            onChange={this.handleAssistantMeasurementUnitClassificationChange}
            placeholder="辅计量单位"
          >
            {measurementUnitClassificationList.rows.map(item => (
              <Option key={item.unitsClassifyId} value={item.unitsClassifyId}>
                {item.name}
              </Option>
            ))}
          </Select>
          &nbsp;
          {form.getFieldDecorator('unitsId2', {
            rules: [{ required: true, message: '请选择辅计量单位！' }],
            initialValue: record.unitsId2,
          })(
            <Select style={{ width: '49%' }} placeholder="辅计量单位">
              {assistantMeasurementUnitList.rows.map(item => (
                <Option key={item.unitsId} value={item.unitsId}>
                  {item.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="单位转换率">
          {form.getFieldDecorator('unitRatio', {
            rules: [{ required: true, message: '请输入单位转换率！' }],
            initialValue: record.unitRatio,
          })(
            <InputNumber
              precision={0}
              style={{ width: '100%' }}
              placeholder="请输入单位转换率"
              min={1}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="物品分类">
          {form.getFieldDecorator('stockClassifyId', {
            rules: [{ required: true, message: '请选择物品分类！' }],
            initialValue: record.stockClassifyId,
          })(
            <Select
              onChange={stockClassifyId =>
                this.onStockClassifyIdChange(stockClassifyId, () => {
                  form.setFieldsValue({ brandId: '' });
                })
              }
              style={{ width: '100%' }}
              placeholder="请选择物品分类"
              showSearch
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {inventoryClassificationList.rows.map(item => (
                <Select.Option key={item.stockClassifyId} value={item.stockClassifyId}>
                  {item.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="品牌">
          {form.getFieldDecorator('brandId', {
            rules: [{ required: true, message: '请选择品牌！' }],
            initialValue: record.brandId,
          })(
            <Select
              style={{ width: '100%' }}
              placeholder="请选择品牌"
              showSearch
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {brandList.rows.map(item => (
                <Select.Option key={item.brandId} value={item.brandId}>
                  {item.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="参考成本">
          {form.getFieldDecorator('referenceCost', {
            rules: [{ required: true, message: '请输入参考成本！' }],
            initialValue: record.referenceCost,
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入参考成本"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售参考价">
          {form.getFieldDecorator('referencePrice', {
            rules: [{ required: true, message: '请输入零售参考价！' }],
            initialValue: record.referencePrice,
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入零售参考价"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售最低价">
          {form.getFieldDecorator('lowerPrice', {
            rules: [{ required: true, message: '请输入零售最低价！' }],
            initialValue: record.lowerPrice,
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入零售最低价"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售最高价">
          {form.getFieldDecorator('upperPrice', {
            rules: [{ required: true, message: '请输入零售最高价！' }],
            initialValue: record.upperPrice,
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入零售最高价"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="规格型号">
          {form.getFieldDecorator('specification', {
            rules: [{ required: true, message: '请输入规格型号！' }],
            initialValue: record.specification,
          })(<Input placeholder="请输入规格型号" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="供应商">
          <Select
            defaultValue={record.supplierClassifyId}
            style={{ width: '49%' }}
            onChange={this.handleSupplierClassificationChange}
          >
            {supplierClassificationList.rows.map(item => (
              <Option key={item.supplierClassifyId} value={item.supplierClassifyId}>
                {item.name}
              </Option>
            ))}
          </Select>
          &nbsp;
          {form.getFieldDecorator('supplierId', {
            rules: [{ required: true, message: '请选择供应商！' }],
            initialValue: record.supplierId,
          })(
            <Select
              style={{ width: '49%' }}
              dropdownMatchSelectWidth={false}
              showSearch
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {supplierList.rows.map(item => (
                <Option key={item.supplierId} value={item.supplierId}>
                  {item.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="税率(%)">
          {form.getFieldDecorator('taxRate', {
            rules: [{ required: true, message: '请输入税率！' }],
            initialValue: record.taxRate,
          })(
            <InputNumber precision={2} style={{ width: '100%' }} placeholder="请输入税率" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark', { initialValue: record.remark })(
            <Input.TextArea rows={3} placeholder="请输入备注" />
          )}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ warehouse, brand, measurementUnit, supplier, user, loading }) => ({
  warehouse,
  brand,
  measurementUnit,
  supplier,
  user,
  loading: loading.models.warehouse,
}))
@Form.create()
class InventoryManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    addModalSupplierClassificationOptions: [],
    record: {},
    supplierClassifyId: '',
  };

  columns = [
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      // width: 150,
      // fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      // width: 200,
      // fixed: 'left',
    },
    {
      title: '物品分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '主计量单位',
      dataIndex: 'units',
    },
    {
      title: '辅计量单位',
      dataIndex: 'units2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '最后一次采购价 (￥)',
      dataIndex: 'referenceCost',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '零售最低价 (￥)',
      dataIndex: 'lowerPrice',
    },
    {
      title: '零售最高价 (￥)',
      dataIndex: 'upperPrice',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '税率(%)',
      dataIndex: 'taxRate',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img
            style={{ width: 14, height: 14 }}
            src={`${imgDomainName}alcohol/1903131439581480.png`}
          />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 80,
      fixed: 'right',
      render: (text, record) => {
        const menu = (
          <Menu>
            <Menu.Item>
              <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
            </Menu.Item>
            {/*<Menu.Item>*/}
            {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.stockId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
            {/*>*/}
            {/*<a href="#">删除</a>*/}
            {/*</Popconfirm>*/}
            {/*</Menu.Item>*/}
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement="bottomRight">
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        );
      },
    },
  ];

  componentWillUnmount() {
    console.log('componentWillUnmount-dj');
  }

  changeStatus = record => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'warehouse/changeInventory',
      payload: { stockId: record.stockId, isEnable: !record.isEnable },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      },
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });
  };

  recordRemove = stockIds => {
    console.log('stockIds: ', stockIds);
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'warehouse/removeInventory',
      payload: { stockIds },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      },
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };

  componentDidMount() {
    const { dispatch } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'warehouse/fetchInventoryClassificationList',
    });

    //获取存货列表
    dispatch({
      type: 'warehouse/fetchInventoryList',
    });

    //获取计量单位分类列表
    dispatch({
      type: 'measurementUnit/fetchMeasurementUnitClassificationList',
      // callback: (data) => {
      //   this.setState({ addModalMeasurementUnitOptions: data.rows.map(item => (
      //       {
      //         value: item.unitsClassifyId,
      //         label: item.name,
      //         isLeaf: false,
      //       }
      //     ))
      //   })
      // }
    });

    //获取供应商分类列表
    dispatch({
      type: 'supplier/fetchSupplierClassificationList',
      // callback: (data) => {
      //   console.log('dj-data: ', data);
      //   this.setState({ addModalSupplierClassificationOptions: data.rows.map(item => (
      //       {
      //         value: item.supplierClassifyId,
      //         label: item.name,
      //         isLeaf: false,
      //       }
      //     ))
      //   })
      // }
    });

    // 获取供应商列表
    // dispatch({
    //   type: 'supplier/fetchSupplierList',
    // });

    //获取品牌列表
    dispatch({
      type: 'brand/fetchBrandList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'warehouse/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'warehouse/fetchInventoryList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      },
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'warehouse/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'warehouse/fetchInventoryList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        },
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'warehouse/addInventory',
      payload: fields,
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      },
    });

    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch, form } = this.props;
    const { record, selectedMenuItemKey } = this.state;
    // fields.unitsId = fields.unitsId[fields.unitsId.length-1]
    dispatch({
      type: 'warehouse/updateInventory',
      payload: {
        ...fields,
        stockId: record.stockId,
      },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      brand: { brandList },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={7} sm={24}>
            <FormItem label="存货名称">
              {getFieldDecorator('name')(<Input placeholder="请输入存货名称" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="存货编号">
              {getFieldDecorator('stockNum')(<Input placeholder="请输入存货编号" />)}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="存货品牌">
              {getFieldDecorator('brandId')(
                <Select
                  placeholder="请选择存货品牌"
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  allowClear
                >
                  {brandList.rows.map(item => (
                    <Select.Option value={item.brandId}>{item.name}</Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={4} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'warehouse/fetchInventoryList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        ...form.getFieldsValue(),
      },
    });

    this.setState({ selectedMenuItemKey: key });
  };

  loadNewOptions = newOptions => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  };

  loadSupplierClassificationOptions = newOptions => {
    this.setState({ addModalSupplierClassificationOptions: newOptions });
  };

  onRowClick = record => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.stockId === record.stockId) {
          newSelectedRows = newSelectedRows.filter(item => item.stockId !== record.stockId);
          flag = false;
          break;
        }
      }
      if (flag) {
        newSelectedRows.push(record);
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return {
        selectedRows: newSelectedRows,
      };
    });
  };

  export = () => {
    const {
      user: { currentUser },
      form
    } = this.props;//
    const params = {
      ...form.getFieldsValue(),
      personnelId: currentUser.personnelId,
    };
    // window.location.href = `${apiDomainName}/server/stock/exportList?${stringify(params)}`;
    window.open(`${apiDomainName}/server/stock/exportList?${stringify(params)}`);
  };

  render() {
    const {
      warehouse: { inventoryClassificationList, inventoryList },
      brand: { brandList },
      measurementUnit: { measurementUnitClassificationList },
      supplier: { supplierClassificationList },
      user: { currentUser },
      loading,
    } = this.props;

    const {
      selectedRows,
      modalVisible,
      updateModalVisible,
      stepFormValues,
      selectedMenuItemKey,
      addModalMeasurementUnitOptions,
      addModalSupplierClassificationOptions,
      record,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const refreshDataSource = () => {
      const { dispatch } = this.props;
      dispatch({
        type: 'warehouse/fetchInventoryList',
      });
    };

    const props = {
      name: 'excelFile',
      action: `${apiDomainName}/server/stock/readExcel`,
      headers: {
        token: localStorage.getItem('token'),
      },
      showUploadList: false,
      onChange(info) {
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} 导入成功`);
          //获取存货列表
          refreshDataSource();
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} 导入失败.`);
        }
      },
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Row gutter={16}>
                <Col span={10} push={4}>
                  <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                    新增
                  </Button>
                  <Upload {...props}>
                    <Button>
                      <Icon type="upload" />
                      Excel导入
                    </Button>
                  </Upload>
                  <Button
                    onClick={this.export}
                    // style={{
                    //   display: currentUser.personnel.name === '武金勇' ? 'inline-block' : 'none',
                    // }}
                  >
                    <Icon type="download" />
                    导出
                  </Button>
                  {selectedRows.length > 0 && (
                    <span>
                      <Popconfirm
                        title="确认批量删除？"
                        onConfirm={() =>
                          this.recordRemove(selectedRows.map(item => item.stockId).join(','))
                        }
                        okText="确认"
                        cancelText="取消"
                      >
                        <Button>批量删除</Button>
                      </Popconfirm>
                    </span>
                  )}
                </Col>
              </Row>
            </div>
            <Row gutter={16}>
              <Col span={4}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>
                    全部分类
                  </Menu.Item>
                  {inventoryClassificationList.rows.map(item => (
                    <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>
                      {item.name}
                    </Menu.Item>
                  ))}
                </Menu>
              </Col>
              <Col span={20}>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={inventoryList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey="stockId"
                  scroll={{ x: 2000 }}
                  size="small"
                  footer={() => `总计：${inventoryList.rows.length}`}
                  onRow={record => {
                    return {
                      onClick: () => this.onRowClick(record),
                    };
                  }}
                />
              </Col>
            </Row>
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
          inventoryClassificationList={inventoryClassificationList}
          // brandList={brandList}
          selectedMenuItemKey={selectedMenuItemKey}
          measurementUnitClassificationList={measurementUnitClassificationList}
          supplierClassificationList={supplierClassificationList}
        />
        <UpdateForm
          {...updateMethods}
          record={record}
          inventoryClassificationList={inventoryClassificationList}
          // brandList={brandList}
          updateModalVisible={updateModalVisible}
          measurementUnitClassificationList={measurementUnitClassificationList}
          supplierClassificationList={supplierClassificationList}
        />
      </div>
    );
  }
}

export default InventoryManagement;
