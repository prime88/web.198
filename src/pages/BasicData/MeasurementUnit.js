import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isMeasurementUnitNameRepeated, isMeasurementUnitCodeRepeated } from '@/services/measurementUnit';
import styles from './MeasurementUnit.less';
import { imgDomainName } from "../../constants";

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const {
    modalVisible,
    form,
    handleAdd,
    handleModalVisible,
    measurementUnitClassificationList,
    selectedKeys,
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isMeasurementUnitNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };
  const unitsClassifyId = selectedKeys.length>0 ? selectedKeys[0] : '';

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isMeasurementUnitCodeRepeated({ code: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的计量单位编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增计量单位"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入计量单位编号！' }, { validator: checkCode }],
        })(<Input placeholder="请输入计量单位编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！'}, { validator: checkName }],
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
        {form.getFieldDecorator('unitsClassifyId', {
          rules: [{ required: true, message: '请选择类别！'}],
          initialValue: unitsClassifyId === '-1' ? '' : unitsClassifyId
        })(
          <Select style={{width: '100%'}} placeholder="请选择类别" >
            {measurementUnitClassificationList.rows.map(item => (
              <Select.Option key={item.unitsClassifyId}>{item.name}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="默认">
        {form.getFieldDecorator('isDefault', {
          rules: [{ required: true, message: '请选择默认字段！'}],
        })(
          <Select style={{width: '100%'}} placeholder="请选择默认字段" >
            <Select.Option key={true}>是</Select.Option>
            <Select.Option key={false}>不是</Select.Option>
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const {
    updateModalVisible,
    form,
    handleUpdate,
    handleUpdateModalVisible,
    record,
    measurementUnitClassificationList,
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isMeasurementUnitNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value === record.code) {
      callback();
      return;
    }

    const response = isMeasurementUnitCodeRepeated({ code: value, unitsId: record.unitsId });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的计量单位编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改计量单位"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入计量单位编号！' }, { validator: checkCode }],
          initialValue: record.code
        })(<Input placeholder="请输入计量单位编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！'}, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
        {form.getFieldDecorator('unitsClassifyId', {
          rules: [{ required: true, message: '请选择类别！'}],
          initialValue: record.unitsClassifyId,
        })(
          <Select style={{width: '100%'}} placeholder="请选择类别" >
            {measurementUnitClassificationList.rows.map(item => (
              <Select.Option key={item.unitsClassifyId} value={item.unitsClassifyId}>{item.name}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="默认">
        {form.getFieldDecorator('isDefault', {
          rules: [{ required: true, message: '请选择默认字段！'}],
          initialValue: record.isDefault,
        })(
          <Select style={{width: '100%'}} placeholder="请选择默认字段" >
            <Select.Option key={true} value={true}>是</Select.Option>
            <Select.Option key={false} value={false}>不是</Select.Option>
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ measurementUnit, loading }) => ({
  measurementUnit,
  loading: loading.models.measurementUnit,
}))
@Form.create()
class MeasurementUnit extends PureComponent {
  rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];

  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedKeys: [],
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'code',
      width: '10%',
    },
    {
      title: '名称',
      dataIndex: 'name',
      width: '10%',
    },
    {
      title: '类别',
      dataIndex: 'unitsClassify',
      width: '10%',
    },
    {
      title: '默认',
      dataIndex: 'isDefault',
      width: '10%',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '状态',
      width: '10%',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '导入编号',
      dataIndex: 'unitsId',
      width: '10%',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: '20%',
    },
    {
      title: '操作',
      width: '20%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.unitsId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
          {/*>*/}
            {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  changeStatus = record => {
    const { dispatch } = this.props;
    const { selectedKeys } = this.state;
    dispatch({
      type: 'measurementUnit/changeMeasurementUnitStatus',
      payload: { unitsId: record.unitsId, isEnable: !record.isEnable },
      selectedKey: selectedKeys.length>0 ? selectedKeys[0] : '',
    });
  };

  recordRemove = unitsIds => {
    console.log('unitsIds: ', unitsIds);
    const { dispatch } = this.props;
    const { selectedKeys } = this.state;
    dispatch({
      type: 'measurementUnit/removeMeasurementUnit',
      payload: { unitsIds },
      selectedKey: selectedKeys.length>0 ? selectedKeys[0] : '',
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    //获取计量单位分类列表
    dispatch({
      type: 'measurementUnit/fetchMeasurementUnitClassificationList',
      // callback: (data) => {
      //   if (data.rows.length > 0) {
      //     const unitsClassifyId = data.rows[0].unitsClassifyId;
      //     this.setState({ selectedKeys: [unitsClassifyId+'']})
      //
      //     //获取计量单位列表
      //     dispatch({
      //       type: 'measurementUnit/fetchMeasurementUnitList',
      //       payload: {
      //         unitsClassifyId
      //       }
      //     });
      //   }
      // }
    });

    //获取计量单位列表
    dispatch({
      type: 'measurementUnit/fetchMeasurementUnitList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'measurementUnit/fetch',
      payload: params,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'measurementUnit/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { selectedKeys } = this.state;
    dispatch({
      type: 'measurementUnit/addMeasurementUnit',
      payload: fields,
      selectedKey: selectedKeys.length>0 ? selectedKeys[0] : '',
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedKeys } = this.state;
    dispatch({
      type: 'measurementUnit/updateMeasurementUnit',
      payload: {
        ...fields,
        unitsId: record.unitsId
      },
      selectedKey: selectedKeys.length>0 ? selectedKeys[0] : '',
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch } = this.props;

    //获取计量单位列表
    dispatch({
      type: 'measurementUnit/fetchMeasurementUnitList',
      payload: {
        unitsClassifyId: key === '-1' ? '' : key,
      }
    });
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.unitsId === record.unitsId) {
          newSelectedRows = newSelectedRows.filter(item => item.unitsId !== record.unitsId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  render() {
    const {
      measurementUnit: { measurementUnitClassificationList, measurementUnitList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, stepFormValues, selectedKeys, record } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Row gutter={16}>
                <Col span={10} push={4}>
                  <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                    新增
                  </Button>
                  {/*{selectedRows.length > 0 && (*/}
                    {/*<span>*/}
                      {/*<Popconfirm*/}
                        {/*title="确认批量删除？"*/}
                        {/*onConfirm={() =>*/}
                          {/*this.recordRemove(selectedRows.map(item => item.unitsId).join(','))*/}
                        {/*}*/}
                        {/*okText="确认"*/}
                        {/*cancelText="取消"*/}
                      {/*>*/}
                        {/*<Button>批量删除</Button>*/}
                      {/*</Popconfirm>*/}
                    {/*</span>*/}
                  {/*)}*/}
                </Col>
              </Row>
            </div>
            <Row gutter={16}>
              <Col span={4}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  // selectedKeys={selectedKeys}
                  onSelect={({ item, key, selectedKeys }) => {
                    console.log('selectedKeys: ', selectedKeys);
                    this.setState({ selectedKeys });
                  }}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                  {measurementUnitClassificationList.rows.map(item => (
                    <Menu.Item key={item.unitsClassifyId}>{item.name}</Menu.Item>
                  ))}
                </Menu>
              </Col>
              <Col span={20}>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={measurementUnitList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='unitsId'
                  onRow={(record) => {
                    return {
                      onClick: () => {// 点击行
                        this.onRowClick(record);
                      },
                    };
                  }}
                />
              </Col>
            </Row>
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
          measurementUnitClassificationList={measurementUnitClassificationList}
          selectedKeys={selectedKeys}
        />
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          values={stepFormValues}
          record={record}
          measurementUnitClassificationList={measurementUnitClassificationList}
        />
      </div>
    );
  }
}

export default MeasurementUnit;
