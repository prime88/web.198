import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Divider,
  Popconfirm,
  TreeSelect,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isWarehouseNameRepeated, isWarehouseCodeRepeated } from '@/services/warehouse';
import styles from './WarehouseManagement.less';
import { imgDomainName } from "../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const TreeNode = TreeSelect.TreeNode;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, warehouseClassificationList: { rows }, branchList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isWarehouseNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const loadData = (dataTree) => {
    if (!dataTree) return [];
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.branchId} value={item.branchId}>
            {item.childList ? loadData(item.childList) : null}
          </TreeNode>
        )
      })
    )
  }

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isWarehouseCodeRepeated({ code: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的仓库编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增仓库"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入仓库编号！' }, { validator: checkCode }],
        })(<Input placeholder="请输入仓库编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！'}, { validator: checkName }],
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="位置">
        {form.getFieldDecorator('position', {
          rules: [{ required: true, message: '请输入位置！'}],
        })(<Input placeholder="请输入位置" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="仓库类别">
        {form.getFieldDecorator('warehouseClassifyCode', {
          rules: [{ required: true, message: '请选择仓库类别！'}],
        })(
          <Select placeholder="请选择仓库类别" style={{width: '100%'}}>
            {rows.map(item => <Option key={item.dictDataCode}>{item.dictDataName}</Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem
        labelCol={{ span: 5 }}
        wrapperCol={{ span: 15 }}
        label="所属部门"
      >
        <div
          id="area"
          style={{
            // height: 500,
            // background: '#eee',
            position: 'relative'
          }}
        >
          {form.getFieldDecorator('branchId', {
            rules: [{ required: true, message: '请选择部门！' }],
          })(
              <TreeSelect
                style={{ width: '100%' }}
                // dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                placeholder="请选择部门"
                allowClear
                // treeDefaultExpandAll
                getPopupContainer={() => document.getElementById('area')}
              >
                {/*{loadData(branchList.length > 0 ? branchList[0].childList : [])}*/}
                {branchList.length > 0 ? loadData(branchList[0].childList) : []}
              </TreeSelect>
          )}
        </div>
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record, warehouseClassificationList: { rows }, branchList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isWarehouseNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const loadData = (dataTree) => {
    if (!dataTree) return [];

    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.branchId} value={item.branchId}>
            {item.childList ? loadData(item.childList) : null}
          </TreeNode>
        )
      })
    )
  }

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value === record.code) {
      callback();
      return;
    }

    const response = isWarehouseCodeRepeated({ code: value, warehouseId: record.warehouseId });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的仓库编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改仓库"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入仓库编号！' }, { validator: checkCode }],
          initialValue: record.code
        })(<Input placeholder="请输入仓库编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！'}, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="位置">
        {form.getFieldDecorator('position', {
          rules: [{ required: true, message: '请输入位置！'}],
          initialValue: record.position,
        })(<Input placeholder="请输入位置" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="仓库类别">
        {form.getFieldDecorator('warehouseClassifyCode', {
          rules: [{ required: true, message: '请选择仓库类别！'}],
          initialValue: record.warehouseClassifyCode,
        })(
          <Select placeholder="请选择仓库类别" style={{width: '100%'}}>
            {rows.map(item => <Option key={item.dictDataCode}>{item.dictDataName}</Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="所属部门">
        <div
          id="area"
          style={{
            // height: 500,
            // background: '#eee',
            position: 'relative'
          }}
        >
          {form.getFieldDecorator('branchId', {
            rules: [{ required: true, message: '请选择部门！' }],
            initialValue: record.branchId
          })(
            <TreeSelect
              style={{ width: '100%' }}
              // dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
              placeholder="请选择部门"
              allowClear
              getPopupContainer={() => document.getElementById('area')}
            >
              {branchList.length > 0 ? loadData(branchList[0].childList) : []}
            </TreeSelect>
          )}
        </div>
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ warehouse, loading }) => ({
  warehouse,
  loading: loading.models.warehouse,
}))
@Form.create()
class WarehouseManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '导入编号',
      dataIndex: 'warehouseId',
    },
    {
      title: '仓库编号',
      dataIndex: 'code',
    },
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '位置',
      dataIndex: 'position',
    },
    {
      title: '仓库类别',
      dataIndex: 'warehouseClassify',
    },
    {
      title: '所属部门',
      dataIndex: 'branch',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 150,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.warehouseId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
          {/*>*/}
            {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'warehouse/changeWarehouseStatus',
      payload: { warehouseId: record.warehouseId, isEnable: !record.isEnable },
    });
  };

  recordRemove = warehouseIds => {
    console.log('warehouseIds: ', warehouseIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'warehouse/removeWarehouse',
      payload: { warehouseIds },
      callback: () => {
      this.setState({
        selectedRows: [],
      });
    },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //获取仓库列表
    dispatch({
      type: 'warehouse/fetchWarehouseList',
    });

    //获取仓库类别列表
    dispatch({
      type: 'warehouse/fetchWarehouseClassificationList',
      payload: {
        dictCode: 'warehouseClassify'
      }
    });

    //部门列表
    dispatch({
      type: 'warehouse/fetchBranchList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'warehouse/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'warehouse/addWarehouse',
      payload: fields,
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'warehouse/updateWarehouse',
      payload: {
        ...fields,
        warehouseId: record.warehouseId
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.warehouseId === record.warehouseId) {
          newSelectedRows = newSelectedRows.filter(item => item.warehouseId !== record.warehouseId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  render() {
    const {
      warehouse: { warehouseList, warehouseClassificationList, branchList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {/*{selectedRows.length > 0 && (*/}
                {/*<span>*/}
                  {/*<Popconfirm*/}
                    {/*title="确认批量删除？"*/}
                    {/*onConfirm={() =>*/}
                      {/*this.recordRemove(selectedRows.map(item => item.warehouseId).join(','))*/}
                    {/*}*/}
                    {/*okText="确认"*/}
                    {/*cancelText="取消"*/}
                  {/*>*/}
                    {/*<Button>批量删除</Button>*/}
                  {/*</Popconfirm>*/}
                {/*</span>*/}
              {/*)}*/}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={warehouseList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey='warehouseId'
              onRow={(record) => {
                return {
                  onClick: () => {// 点击行
                    this.onRowClick(record);
                  },
                };
              }}
              scroll={{x: 1200}}
            />
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
          warehouseClassificationList={warehouseClassificationList}
          branchList={branchList}
        />
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          warehouseClassificationList={warehouseClassificationList}
          record={record}
          branchList={branchList}
        />
      </div>
    );
  }
}

export default WarehouseManagement;
