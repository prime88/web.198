import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isBrandNameRepeated, isBrandCodeRepeated } from '@/services/brand';
import { imgDomainName } from "../../constants";
import styles from './InventoryClassification.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, inventoryClassificationList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isBrandNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isBrandCodeRepeated({ code: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的品牌编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增品牌"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入品牌编号！' }, { validator: checkCode }],
        })(<Input placeholder="请输入品牌编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [
            { required: true, message: '请输入名称！' },
            { validator: checkName }
          ],
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="分类">
        {form.getFieldDecorator('stockClassifyIds', {
          rules: [
            { required: true, message: '请选择分类！' },
          ],
        })(
          <Select mode="multiple" placeholder="请选择分类" style={{width: '100%'}}>
            {inventoryClassificationList.rows.map(item => <Select.Option key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Select.Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record, inventoryClassificationList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isBrandNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value === record.code) {
      callback();
      return;
    }

    const response = isBrandCodeRepeated({ code: value, brandId: record.brandId });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的品牌编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改品牌"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入品牌编号！' }, { validator: checkCode }],
          initialValue: record.code
        })(<Input placeholder="请输入品牌编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="分类">
        {form.getFieldDecorator('stockClassifyIds', {
          rules: [
            { required: true, message: '请选择分类！' },
          ],
          initialValue: record.stockClassifyIds ? record.stockClassifyIds.split(',') : ''
        })(
          <Select mode="multiple" placeholder="请选择分类" style={{width: '100%'}}>
            {inventoryClassificationList.rows.map(item => <Select.Option key={item.stockClassifyId}>{item.name}</Select.Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(
          <Input.TextArea rows={3} placeholder="请输入备注" />
        )}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ brand, warehouse, loading }) => ({
  brand,
  warehouse,
  loading: loading.models.brand,
}))
@Form.create()
class BrandManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '品牌编号',
      dataIndex: 'code',
      width: '10%',
    },
    {
      title: '名称',
      dataIndex: 'name',
      width: '15%',
    },
    {
      title: '分类',
      dataIndex: 'classifyList',
      width: '15%',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      width: '10%',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '导入编号',
      dataIndex: 'brandId',
      width: '10%',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: '25%',
    },
    {
      title: '操作',
      width: '15%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.brandId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
          {/*>*/}
            {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  recordRemove = brandIds => {
    console.log('brandIds: ', brandIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'brand/removeBrand',
      payload: { brandIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'brand/changeBrandStatus',
      payload: { brandId: record.brandId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //获取品牌列表
    dispatch({
      type: 'brand/fetchBrandList',
    });

    //获取存货分类列表
    dispatch({
      type: 'warehouse/fetchInventoryClassificationList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'brand/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    fields.stockClassifyIds = fields.stockClassifyIds.join(',');
    dispatch({
      type: 'brand/addBrand',
      payload: fields,
      // callback: () => this.handleModalVisible(true)
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    fields.stockClassifyIds = fields.stockClassifyIds.join(',');
    dispatch({
      type: 'brand/updateBrand',
      payload: {
        ...fields,
        brandId: record.brandId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.brandId === record.brandId) {
          newSelectedRows = newSelectedRows.filter(item => item.brandId !== record.brandId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'brand/fetchBrandList',
        payload: {
          ...fieldsValue,
        },
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'brand/fetchBrandList',
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      brand: { brandList },
      warehouse: { inventoryClassificationList },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={7} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入品牌名称" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="编号">
              {getFieldDecorator('code')(<Input placeholder="请输入品牌编号" />)}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="分类">
              {getFieldDecorator('stockClassifyId')(
                <Select
                  placeholder="请选择品牌类别"
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  allowClear
                >
                  {inventoryClassificationList.rows.map(item => (
                    <Select.Option value={item.stockClassifyId}>{item.name}</Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={4} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      brand: { brandList },
      warehouse: { inventoryClassificationList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {/*{selectedRows.length > 0 && (*/}
                {/*<span>*/}
                  {/*<Popconfirm*/}
                    {/*title="确认批量删除？"*/}
                    {/*onConfirm={() =>*/}
                      {/*this.recordRemove(selectedRows.map(item => item.brandId).join(','))*/}
                    {/*}*/}
                    {/*okText="确认"*/}
                    {/*cancelText="取消"*/}
                  {/*>*/}
                    {/*<Button>批量删除</Button>*/}
                  {/*</Popconfirm>*/}
                {/*</span>*/}
              {/*)}*/}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={brandList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="brandId"
              onRow={(record) => {
                return {
                  onClick: () => {// 点击行
                    this.onRowClick(record);
                  },
                };
              }}
            />
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          inventoryClassificationList={inventoryClassificationList}
          modalVisible={modalVisible}
        />
        <UpdateForm
          {...updateMethods}
          inventoryClassificationList={inventoryClassificationList}
          updateModalVisible={updateModalVisible}
          record={record}
        />
      </div>
    );
  }
}

export default BrandManagement;
