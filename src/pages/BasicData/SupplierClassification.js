import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isSupplierClassificationNameRepeated, isSupplierClassificationCodeRepeated } from '@/services/supplier';
import styles from './SupplierClassification.less';
import { imgDomainName } from "../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isSupplierClassificationNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isSupplierClassificationCodeRepeated({ code: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的供应商分类编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增供应商分类"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入供应商分类编号！' }, { validator: checkCode }],
        })(<Input placeholder="请输入供应商分类编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {})(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isSupplierClassificationNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value === record.code) {
      callback();
      return;
    }

    const response = isSupplierClassificationCodeRepeated({ code: value, supplierClassifyId: record.supplierClassifyId });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的供应商分类编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改供应商分类"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入供应商分类编号！' }, { validator: checkCode }],
          initialValue: record.code
        })(<Input placeholder="请输入供应商分类编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(
          <Input.TextArea rows={3} placeholder="请输入备注" />
        )}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ supplier, loading }) => ({
  supplier,
  loading: loading.models.supplier,
}))
@Form.create()
class SupplierClassification extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '导入编号',
      dataIndex: 'supplierClassifyId',
      width: '10%',
    },
    {
      title: '分类编号',
      dataIndex: 'code',
      width: '10%',
    },
    {
      title: '名称',
      dataIndex: 'name',
      width: '20%',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      width: '10%',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: '35%',
    },
    {
      title: '操作',
      width: '15%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.supplierClassifyId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
          {/*>*/}
            {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/changeSupplierClassificationStatus',
      payload: { supplierClassifyId: record.supplierClassifyId, isEnable: !record.isEnable },
    });
  };

  recordRemove = supplierClassifyIds => {
    console.log('supplierClassifyIds: ', supplierClassifyIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/removeSupplierClassification',
      payload: { supplierClassifyIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/fetchSupplierClassificationList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'supplier/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/addSupplierClassification',
      payload: fields,
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'supplier/updateSupplierClassification',
      payload: {
        ...fields,
        supplierClassifyId: record.supplierClassifyId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.supplierClassifyId === record.supplierClassifyId) {
          newSelectedRows = newSelectedRows.filter(item => item.supplierClassifyId !== record.supplierClassifyId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }


  render() {
    const {
      supplier: { supplierClassificationList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {/*{selectedRows.length > 0 && (*/}
                {/*<span>*/}
                  {/*<Popconfirm*/}
                    {/*title="确认批量删除？"*/}
                    {/*onConfirm={() =>*/}
                      {/*this.recordRemove(selectedRows.map(item => item.supplierClassifyId).join(','))*/}
                    {/*}*/}
                    {/*okText="确认"*/}
                    {/*cancelText="取消"*/}
                  {/*>*/}
                    {/*<Button>批量删除</Button>*/}
                  {/*</Popconfirm>*/}
                {/*</span>*/}
              {/*)}*/}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={supplierClassificationList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="supplierClassifyId"
              onRow={(record) => {
                return {
                  onClick: () => {// 点击行
                    this.onRowClick(record);
                  },
                };
              }}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />
      </div>
    );
  }
}

export default SupplierClassification;
