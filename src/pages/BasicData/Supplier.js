import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Button, Modal, Divider, Select, Popconfirm, Icon, Row, Col } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isSupplierNameRepeated, isSupplierCodeRepeated } from '@/services/supplier';
import styles from './Supplier.less';
import { imgDomainName } from "../../constants";

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const {
    modalVisible,
    form,
    handleAdd,
    handleModalVisible,
    supplierClassificationList: { rows },
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isSupplierNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isSupplierCodeRepeated({ code: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的供应商编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增供应商"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入供应商编号！' }, { validator: checkCode }],
        })(<Input placeholder="请输入供应商编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
        {form.getFieldDecorator('supplierClassifyIds', {
          rules: [{ required: true, message: '请选择类别！' }],
        })(
          <Select mode="multiple" placeholder="请选择类别" style={{ width: '100%' }}>
            {rows.map(item => (
              <Select.Option key={item.supplierClassifyId}>{item.name}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="地址">
        {form.getFieldDecorator('address', {
          rules: [{ required: true, message: '请输入地址！' }],
        })(<Input placeholder="请输入地址" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开户行">
        {form.getFieldDecorator('bank', {
          rules: [{ required: true, message: '请输入开户行！' }],
        })(<Input placeholder="请输入开户行" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="银行账号">
        {form.getFieldDecorator('account', {
          rules: [{ required: true, message: '请输入银行账号！' }],
        })(<Input placeholder="请输入银行账号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="税号">
        {form.getFieldDecorator('taxpayerNum', {
          rules: [{ required: true, message: '请输入税号！' }],
        })(<Input placeholder="请输入税号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="联系人">
        {form.getFieldDecorator('contact', {
          rules: [{ required: true, message: '请输入联系人！' }],
        })(<Input placeholder="请输入联系人" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="联系电话">
        {form.getFieldDecorator('phone', {
          rules: [{ required: true, message: '请输入联系电话！' }],
        })(<Input placeholder="请输入联系电话" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const {
    updateModalVisible,
    form,
    handleUpdate,
    handleUpdateModalVisible,
    record,
    supplierClassificationList: { rows },
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isSupplierNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const checkCode = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value === record.code) {
      callback();
      return;
    }

    const response = isSupplierCodeRepeated({ code: value, supplierId: record.supplierId });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的供应商编号!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改供应商"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="编号">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请输入供应商编号！' }, { validator: checkCode }],
          initialValue: record.code
        })(<Input placeholder="请输入供应商编号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
        {form.getFieldDecorator('supplierClassifyIds', {
          rules: [{ required: true, message: '请选择类别！' }],
          initialValue: record.supplierClassifyIds && record.supplierClassifyIds.split(','),
        })(
          <Select
            mode="multiple"
            placeholder="请选择类别"
            style={{ width: '100%' }}
          >
            {rows.map(item => (
              <Select.Option key={item.supplierClassifyId}>
                {item.name}
              </Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="地址">
        {form.getFieldDecorator('address', {
          rules: [{ required: true, message: '请输入地址！' }],
          initialValue: record.address,
        })(<Input placeholder="请输入地址" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开户行">
        {form.getFieldDecorator('bank', {
          rules: [{ required: true, message: '请输入开户行！' }],
          initialValue: record.bank,
        })(<Input placeholder="请输入开户行" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="银行账号">
        {form.getFieldDecorator('account', {
          rules: [{ required: true, message: '请输入银行账号！' }],
          initialValue: record.account,
        })(<Input placeholder="请输入银行账号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="税号">
        {form.getFieldDecorator('taxpayerNum', {
          rules: [{ required: true, message: '请输入税号！' }],
          initialValue: record.taxpayerNum,
        })(<Input placeholder="请输入税号" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="联系人">
        {form.getFieldDecorator('contact', {
          rules: [{ required: true, message: '请输入联系人！' }],
          initialValue: record.contact,
        })(<Input placeholder="请输入联系人" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="联系电话">
        {form.getFieldDecorator('phone', {
          rules: [{ required: true, message: '请输入联系电话！' }],
          initialValue: record.phone,
        })(<Input placeholder="请输入联系电话" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(
          <Input.TextArea rows={3} placeholder="请输入备注" />
        )}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ supplier, loading }) => ({
  supplier,
  loading: loading.models.supplier,
}))
@Form.create()
class Supplier extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    // {
    //   title: '编号',
    //   dataIndex: 'supplierId',
    //   width: 70,
    //   fixed: 'left',
    // },
    {
      title: '供应商编号',
      dataIndex: 'code',
      width: 150,
      fixed: 'left',
    },
    {
      title: '导入编号',
      dataIndex: 'supplierId',
      width: 100,
    },
    {
      title: '供应商名称',
      dataIndex: 'name',
      // width: 150,
      // fixed: 'left',
    },
    {
      title: '供应商类别',
      dataIndex: 'supplierClassify',
    },
    {
      title: '地址',
      dataIndex: 'address',
    },
    {
      title: '开户行',
      dataIndex: 'bank',
    },
    {
      title: '银行账号',
      dataIndex: 'account',
    },
    {
      title: '税号',
      dataIndex: 'taxpayerNum',
    },
    {
      title: '联系人',
      dataIndex: 'contact',
    },
    {
      title: '联系电话',
      dataIndex: 'phone',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      fixed: 'right',
      width: 150,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.supplierId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
          {/*>*/}
            {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/changeSupplierStatus',
      payload: { supplierId: record.supplierId, isEnable: !record.isEnable },
    });
  };

  recordRemove = supplierIds => {
    console.log('supplierIds: ', supplierIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/removeSupplier',
      payload: { supplierIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    // 获取供应商分类列表
    dispatch({
      type: 'supplier/fetchSupplierClassificationList',
    });

    // 获取供应商列表
    this.fetchSupplierList();
  }

  fetchSupplierList = (payload = {}) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/fetchSupplierList',
      payload
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'supplier/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'supplier/addSupplier',
      payload: { ...fields, supplierClassifyIds: fields.supplierClassifyIds.join(',') },
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'supplier/updateSupplier',
      payload: {
        ...fields,
        supplierId: record.supplierId,
        supplierClassifyIds:fields.supplierClassifyIds.join(',')
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.supplierId === record.supplierId) {
          newSelectedRows = newSelectedRows.filter(item => item.supplierId !== record.supplierId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.fetchSupplierList();
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);
      this.fetchSupplierList(fieldsValue)
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      supplier: { supplierList, supplierClassificationList },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="供应商名称">
              {getFieldDecorator('name')(
                <Select
                  showSearch
                  allowClear
                  placeholder="请选择供应商名称"
                  style={{ width: '100%' }}
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {supplierList.rows.map(item => (
                    <Select.Option key={item.name}>{item.name}</Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="供应商类别">
              {getFieldDecorator('supplierClassifyId')(
                <Select allowClear placeholder="请选择供应商类别" style={{ width: '100%' }}>
                  {supplierClassificationList.rows.map(item => (
                    <Select.Option key={item.supplierClassifyId}>{item.name}</Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      supplier: { supplierList, supplierClassificationList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {/*{selectedRows.length > 0 && (*/}
                {/*<span>*/}
                  {/*<Popconfirm*/}
                    {/*title="确认批量删除？"*/}
                    {/*onConfirm={() =>*/}
                      {/*this.recordRemove(selectedRows.map(item => item.supplierId).join(','))*/}
                    {/*}*/}
                    {/*okText="确认"*/}
                    {/*cancelText="取消"*/}
                  {/*>*/}
                    {/*<Button>批量删除</Button>*/}
                  {/*</Popconfirm>*/}
                {/*</span>*/}
              {/*)}*/}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={supplierList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="supplierId"
              scroll={{ x: 2500 }}
              onRow={(record) => {
                return {
                  onClick: () => {// 点击行
                    this.onRowClick(record);
                  },
                };
              }}
            />
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
          supplierClassificationList={supplierClassificationList}
        />
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          record={record}
          supplierClassificationList={supplierClassificationList}
        />
      </div>
    );
  }
}

export default Supplier;
