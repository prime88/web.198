import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, InputNumber, Upload, Row, Col, message } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isVipLevelNameRepeated } from '@/services/vip';
import styles from './VipLevelSettings.less';
import {apiDomainName, imgDomainName} from "../../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    lightLoading: false,
    lightImageUrl: '',
    closingLoading: false,
    closingImageUrl: '',
  }

  okHandle = () => {
    const { form, handleAdd } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.icon = fieldsValue.icon.length >0 ? fieldsValue.icon[0].response.msg : '';
      fieldsValue.lightIcon = fieldsValue.lightIcon.length >0 ? fieldsValue.lightIcon[0].response.msg : '';
      fieldsValue.closeIcon = fieldsValue.closeIcon.length >0 ? fieldsValue.closeIcon[0].response.msg : '';
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ imageUrl: '', lightImageUrl: '', closingImageUrl: '' });
      handleAdd(fieldsValue);
    });
  };

  checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    const response = isVipLevelNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleImageChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        // console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  handleLightImageChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ lightLoading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, lightImageUrl => {
        // console.log('lightImageUrl: ', lightImageUrl);
        this.setState({
          lightImageUrl,
          lightLoading: false,
        });
      });
    }
  };

  handleCloseImageChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ closingLoading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, closingImageUrl => {
        // console.log('closingImageUrl: ', closingImageUrl);
        this.setState({
          closingImageUrl,
          closingLoading: false,
        });
      });
    }
  };

  render() {
    const { modalVisible, form, handleModalVisible, vipLevelList } = this.props;
    const { imageUrl, lightImageUrl, loading, lightLoading, closingImageUrl, closingLoading } = this.state;

    const uploadButton = (
      <div style={{ width: 40, height: 40, paddingTop: loading ? 0 : 10 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text" style={{ display: loading ? 'inline-block' : 'none', fontSize: 8, paddingTop: 10 }}>上传中</div>
      </div>
    );

    const ligntImgUploadButton = (
      <div style={{ width: 40, height: 40, paddingTop: lightLoading ? 0 : 10 }}>
        <Icon type="plus" style={{ display: lightLoading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text" style={{ display: lightLoading ? 'inline-block' : 'none', fontSize: 8, paddingTop: 10 }}>上传中</div>
      </div>
    );

    const closingImgUploadButton = (
      <div style={{ width: 40, height: 40, paddingTop: closingLoading ? 0 : 10 }}>
        <Icon type="plus" style={{ display: closingLoading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text" style={{ display: closingLoading ? 'inline-block' : 'none', fontSize: 8, paddingTop: 10 }}>上传中</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        // title="新增会员等级"
        title={(
          <div>
            <span>新增会员等级</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ imageUrl: '', lightImageUrl: '', closingImageUrl: '' })
        }}
        className={styles.customForm}
      >
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="名称">
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: '请输入名称！' }, { validator: this.checkName }],
          })(<Input placeholder="请输入名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="消费金额">
          {form.getFieldDecorator('amount', {
            rules: [{ required: true, message: '请输入消费金额！' }],
          })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入消费金额" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="折扣">
          {form.getFieldDecorator('discount', {
            rules: [{ required: true, message: '请输入折扣！' }],
          })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入折扣" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="连续消费月数">
          {form.getFieldDecorator('months', {
            rules: [{ required: true, message: '请输入连续消费月数！' }],
          })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入连续消费月数" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="图标">
          {form.getFieldDecorator('icon', {
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请选择图标！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleImageChange}
            >
              {imageUrl ? (
                <img style={{ width: 40, height: 40 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <Row>
          <Col span={12}>
            <FormItem labelCol={{ span: 12 }} wrapperCol={{ span: 12 }} label="点亮图标">
              {form.getFieldDecorator('lightIcon', {
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{ required: true, message: '请选择点亮图标！' }],
              })(
                <Upload
                  name="image"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  action={`${apiDomainName}/image/upload`}
                  // beforeUpload={this.beforeUpload}
                  onChange={this.handleLightImageChange}
                  style={{width: 40, height: 40}}
                >
                  {lightImageUrl ? (
                    <img style={{ width: 40, height: 40 }} src={lightImageUrl} alt="avatar" />
                  ) : (
                    ligntImgUploadButton
                  )}
                </Upload>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem labelCol={{ span: 12 }} wrapperCol={{ span: 12 }} label="关闭图标">
              {form.getFieldDecorator('closeIcon', {
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{ required: true, message: '请选择关闭图标！' }],
              })(
                <Upload
                  name="image"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  action={`${apiDomainName}/image/upload`}
                  // beforeUpload={this.beforeUpload}
                  onChange={this.handleCloseImageChange}
                >
                  {closingImageUrl ? (
                    <img style={{ width: 40, height: 40 }} src={closingImageUrl} alt="avatar" />
                  ) : (
                    closingImgUploadButton
                  )}
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="优先级">
          {form.getFieldDecorator('level', {
            rules: [{ required: true, message: '请输入等级！' }],
          })(
              <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入等级" min={0} max={vipLevelList.rows.length+1}/>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    lightLoading: false,
    lightImageUrl: '',
    closingLoading: false,
    closingImageUrl: '',
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record, updateModalVisible }  = nextProps;
    const { imageUrl, lightImageUrl, closingImageUrl } = prevState;
    let newState = null;
    if (record.icon && !imageUrl) {
      newState = {
        ...newState,
        imageUrl: `${imgDomainName}${record.icon}`,
      };
    }
    if (record.lightIcon && !lightImageUrl) {
      newState = {
        ...newState,
        lightImageUrl: `${imgDomainName}${record.lightIcon}`,
      };
    }
    if (record.closeIcon && !closingImageUrl) {
      newState = {
        ...newState,
        closingImageUrl: `${imgDomainName}${record.closeIcon}`,
      };
    }
    return newState
  }

  okHandle = () => {
    const { form, handleUpdate } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.icon = fieldsValue.icon.length >1 ? fieldsValue.icon[1].response.msg : '';
      fieldsValue.lightIcon = fieldsValue.lightIcon.length >1 ? fieldsValue.lightIcon[1].response.msg : '';
      fieldsValue.closeIcon = fieldsValue.closeIcon.length >1 ? fieldsValue.closeIcon[1].response.msg : '';
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
      this.setState({ imageUrl: '', lightImageUrl: '', closingImageUrl: '' });
    });
  };

  checkName = (rule, value, callback) => {
    const { record } = this.props;
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }
    const response = isVipLevelNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleImageChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        // console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  handleLightImageChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ lightLoading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, lightImageUrl => {
        // console.log('lightImageUrl: ', lightImageUrl);
        this.setState({
          lightImageUrl,
          lightLoading: false,
        });
      });
    }
  };

  handleCloseImageChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ closingLoading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, closingImageUrl => {
        // console.log('closingImageUrl: ', closingImageUrl);
        this.setState({
          closingImageUrl,
          closingLoading: false,
        });
      });
    }
  };

  render() {
    const { updateModalVisible, form, handleUpdateModalVisible, record, vipLevelList } = this.props;
    const { imageUrl, lightImageUrl, loading, lightLoading, closingImageUrl, closingLoading } = this.state;

    const uploadButton = (
      <div style={{ width: 40, height: 40, paddingTop: loading ? 0 : 10 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text" style={{ display: loading ? 'inline-block' : 'none', fontSize: 8, paddingTop: 10 }}>上传中</div>
      </div>
    );

    const ligntImgUploadButton = (
      <div style={{ width: 40, height: 40, paddingTop: lightLoading ? 0 : 10 }}>
        <Icon type="plus" style={{ display: lightLoading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text" style={{ display: lightLoading ? 'inline-block' : 'none', fontSize: 8, paddingTop: 10 }}>上传中</div>
      </div>
    );

    const closingImgUploadButton = (
      <div style={{ width: 40, height: 40, paddingTop: closingLoading ? 0 : 10 }}>
        <Icon type="plus" style={{ display: closingLoading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text" style={{ display: closingLoading ? 'inline-block' : 'none', fontSize: 8, paddingTop: 10 }}>上传中</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        // title="修改会员等级"
        title={(
          <div>
            <span>修改会员等级</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ imageUrl: '', lightImageUrl: '', closingImageUrl: '' });
        }}
        className={styles.customForm}
      >
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="名称">
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: '请输入名称！' }, { validator: this.checkName }],
            initialValue: record.name,
          })(<Input placeholder="请输入名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="消费金额">
          {form.getFieldDecorator('amount', {
            rules: [{ required: true, message: '请输入消费金额！' }],
            initialValue: record.amount,
          })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入消费金额" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="折扣">
          {form.getFieldDecorator('discount', {
            rules: [{ required: true, message: '请输入折扣！' }],
            initialValue: record.discount,
          })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入折扣" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="连续消费月数">
          {form.getFieldDecorator('months', {
            rules: [{ required: true, message: '请输入连续消费月数！' }],
            initialValue: record.months,
          })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入连续消费月数" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="图标">
          {form.getFieldDecorator('icon', {
            initialValue: record.icon ? [{ response: { msg: record.icon } }] : '',
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请选择图标！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleImageChange}
            >
              {imageUrl ? (
                <img style={{ width: 40, height: 40 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <Row>
          <Col span={12}>
            <FormItem labelCol={{ span: 12 }} wrapperCol={{ span: 12 }} label="点亮图标">
              {form.getFieldDecorator('lightIcon', {
                initialValue: record.lightIcon ? [{ response: { msg: record.lightIcon } }] : '',
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{ required: true, message: '请选择点亮图标！' }],
              })(
                <Upload
                  name="image"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  action={`${apiDomainName}/image/upload`}
                  // beforeUpload={this.beforeUpload}
                  onChange={this.handleLightImageChange}
                  style={{width: 40, height: 40}}
                >
                  {lightImageUrl ? (
                    <img style={{ width: 40, height: 40 }} src={lightImageUrl} alt="avatar" />
                  ) : (
                    ligntImgUploadButton
                  )}
                </Upload>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem labelCol={{ span: 12 }} wrapperCol={{ span: 12 }} label="关闭图标">
              {form.getFieldDecorator('closeIcon', {
                initialValue: record.closeIcon ? [{ response: { msg: record.closeIcon } }] : '',
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{ required: true, message: '请选择关闭图标！' }],
              })(
                <Upload
                  name="image"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  action={`${apiDomainName}/image/upload`}
                  // beforeUpload={this.beforeUpload}
                  onChange={this.handleCloseImageChange}
                >
                  {closingImageUrl ? (
                    <img style={{ width: 40, height: 40 }} src={closingImageUrl} alt="avatar" />
                  ) : (
                    closingImgUploadButton
                  )}
                </Upload>
              )}
            </FormItem>
          </Col>
        </Row>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="优先级">
          {form.getFieldDecorator('level', {
            rules: [{ required: true, message: '请输入等级！' }],
            initialValue: record.level,
          })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入等级" min={0} max={vipLevelList.rows.length} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark', {
            initialValue: record.remark,
          })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ vip, loading }) => ({
  vip,
  loading: loading.models.vip,
}))
@Form.create()
class VipLevelSettings extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'memberLevelId',
      width: '7%',
    },
    {
      title: '等级',
      dataIndex: 'name',
      width: '7%',
    },
    {
      title: '消费金额 (￥)',
      dataIndex: 'amount',
      width: '12%',
    },
    {
      title: '折扣',
      dataIndex: 'discount',
      width: '7%',
    },
    {
      title: '连续消费月份',
      dataIndex: 'months',
      width: '12%',
    },
    {
      title: '图标',
      dataIndex: 'icon',
      width: '7%',
      render: (text) => (text ? <img style={{width: 40, height: 40}} alt='' src={`${imgDomainName}${text}`} /> : ''),
    },
    {
      title: '点亮图标',
      width: '8%',
      dataIndex: 'lightIcon',
      render: (text) => (text ? <img style={{width: 40, height: 40}} alt='' src={`${imgDomainName}${text}`} /> : ''),
    },
    {
      title: '关闭图标',
      width: '8%',
      dataIndex: 'closeIcon',
      render: (text) => (text ? <img style={{width: 40, height: 40}} alt='' src={`${imgDomainName}${text}`} /> : ''),
    },
    {
      title: '优先级',
      dataIndex: 'level',
      width: '7%',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: '15%',
    },
    {
      title: '操作',
      width: '10%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.memberLevelId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = memberLevelIds => {
    console.log('memberLevelIds: ', memberLevelIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'vip/removeVipLevel',
      payload: { memberLevelIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'vip/fetchVipLevelList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'vip/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'vip/addVipLevel',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'vip/updateVipLevel',
      payload: {
        ...fields,
        memberLevelId: record.memberLevelId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  render() {
    const {
      vip: { vipLevelList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.memberLevelId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={vipLevelList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="memberLevelId"
            />
            <div className={styles.desc}>
              <h3>说明</h3>
              {vipLevelList.rows.filter(item => item.amount!==0).map(item => <h4>{`连续${item.months}个月消费超过${item.amount}元的升级为${item.name}会员`}</h4>)}
              {/*<h4>默认为铜标会员</h4>*/}
            </div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} vipLevelList={vipLevelList} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} vipLevelList={vipLevelList} />
      </div>
    );
  }
}

export default VipLevelSettings;
