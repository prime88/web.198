import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Button,
  Row,
  Col,
  DatePicker,
  Icon,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/vip';
import moment from 'moment';
import styles from './EleWallRechargeRecord.less';
import {apiDomainName} from "../../../constants";
import { stringify } from 'qs';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ vip, loading }) => ({
  vip,
  loading: loading.models.vip,
}))
@Form.create()
class EleWallRechargeRecord extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    expandedRowTableData: [],
    time: '',
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'rechargeId',
      width: '7%',
    },
    {
      title: '注册手机号',
      dataIndex: 'phone',
      width: '13%',
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
      width: '10%',
    },
    {
      title: '充值前余额 (￥)',
      dataIndex: 'beforeRecharge',
      width: '13%',
    },
    {
      title: <div>面额 (￥)</div>,
      dataIndex: 'rechargeAmount',
      width: '10%',
    },
    {
      title: <div>售价 (￥)</div>,
      dataIndex: 'price',
      width: '10%',
    },
    {
      title: '充值后余额 (￥)',
      dataIndex: 'afterRecharge',
      width: '13%',
    },
    {
      title: '支付方式',
      dataIndex: 'paymentMethod',
      width: '10%',
    },
    {
      title: '充值时间',
      dataIndex: 'createTime',
      width: '14%',
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;

    // 电子钱包充值记录列表
    dispatch({
      type: 'vip/fetchEleWallRechargeRecordList',
    });

    // 支付方式录列表
    dispatch({
      type: 'vip/fetchPaymentMethodList',
      payload: {
        dictCode: 'paymentMethod',
      },
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'vip/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const purchaseTime = fieldsValue.purchaseTime;
      if (purchaseTime.length === 2) {
        fieldsValue.startTime = purchaseTime[0].format('YYYY-MM-DD');
        fieldsValue.endTime = moment(purchaseTime[1].format('YYYY-MM-DD'))
          .add(1, 'days')
          .format('YYYY-MM-DD');
      }
      const params = {
        ...fieldsValue,
      };
      delete params.purchaseTime;
      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'vip/fetchEleWallRechargeRecordList',
        payload: params,
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'vip/fetchEleWallRechargeRecordList',
    });
  };

  renderSimpleForm() {
    const {
      vip: { paymentMethodList },
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="注册手机号">
              {getFieldDecorator('phone', {
              })(<Input placeholder="请输入注册手机号" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="支付方式">
              {getFieldDecorator('paymentMethodCode')(
                <Select allowClear style={{ width: '100%' }} placeholder="请选择支付方式">
                  {paymentMethodList.rows.map(item => (
                    <Select.Option key={item.dictDataCode}>{item.dictDataName}</Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="购买时间">
              {getFieldDecorator('purchaseTime', {
                initialValue: [],
              })(<DatePicker.RangePicker />)}
            </FormItem>
          </Col>
          <Col md={5} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleBtnSearch = time => {
    const { dispatch, form } = this.props;
    const fieldsValue = form.getFieldsValue();

    const purchaseTime = fieldsValue.purchaseTime;
    if (purchaseTime.length === 2) {
      fieldsValue.startTime = purchaseTime[0].format('YYYY-MM-DD');
      fieldsValue.endTime = moment(purchaseTime[1].format('YYYY-MM-DD'))
        .add(1, 'days')
        .format('YYYY-MM-DD');
    }
    delete fieldsValue.purchaseTime;

    const params = {
      time,
      ...fieldsValue
    };
    console.log('params: ', params);
    dispatch({
      type: 'vip/fetchEleWallRechargeRecordList',
      payload: params
    });

    //导出用
    this.setState({ time })
  };

  export = () => {
    const { form } = this.props;
    const { paymentMethodCode, phone, purchaseTime } = form.getFieldsValue();
    const params = {
      paymentMethodCode,
      phone,
      startTime: purchaseTime[0] ? purchaseTime[0].format('YYYY-MM-DD') : undefined,
      endTime: purchaseTime[1] ? purchaseTime[1].add(1, 'days').format('YYYY-MM-DD') : undefined,
      time: this.state.time
    };
    console.log('export-params: ', params);
    window.location.href = `${apiDomainName}/server/eWallet/export?${stringify(params)}`;
  }

  render() {
    const {
      vip: { eleWallRechargeRecordList },
      loading,
    } = this.props;
    const { selectedRows } = this.state;

    let total = 0;
    if (eleWallRechargeRecordList.rows.length > 0){
      total = eleWallRechargeRecordList.rows.map(item => item.price).reduce((prev, curr) => prev + curr);
    }

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button onClick={() => this.handleBtnSearch('thisWeek')}>
                本周充值记录
              </Button>
              <Button onClick={() => this.handleBtnSearch('thisMonth')}>
                本月充值记录
              </Button>
              <Button onClick={() => this.handleBtnSearch('')}>
                全部充值记录
              </Button>
              <Button type="primary" onClick={this.export}>导出</Button>
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={eleWallRechargeRecordList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="rechargeId"//price
              footer={() => <div>总计：{eleWallRechargeRecordList.rows.length}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                总金额：{total}</div>
              }
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default EleWallRechargeRecord;
