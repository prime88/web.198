// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchVipLevelList,
  fetchElectronicWalletList,
  fetchEleWallRechargeRecordList,
  fetchBuyingGivingList,
  fetchBuyingGivingGoodsList,
  fetchBuyingGivingRechargeRecordList,
  addVipLevel,
  addElectronicWallet,
  addBuyingGiving,
  addBuyingGivingGoods,
  updateVipLevel,
  updateElectronicWallet,
  updateBuyingGiving,
  updateBuyingGivingGoods,
  removeVipLevel,
  removeElectronicWallet,
  removeBuyingGiving,
  removeBuyingGivingGoods,
  changeElectronicWalletStatus,
  changeBuyingGivingStatus,
  changeBuyingGivingGoodsStatus,
  queryInventoryClassificationList,
} from '@/services/vip';
import {
  fetchGoodsList,
} from '@/services/goods';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'vip',

  state: {
    //会员等级分类列表
    vipLevelList: {
      rows: [],
      pagination: {},
    },
    //电子钱包列表
    electronicWalletList: {
      rows: [],
      pagination: {},
    },
    //电子钱包充值记录列表
    eleWallRechargeRecordList: {
      rows: [],
      pagination: {},
    },
    //支付方式列表
    paymentMethodList: {
      rows: [],
      pagination: {},
    },
    //买赠设置红包列表
    buyingGivingList: {
      rows: [],
      pagination: {},
    },
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //买赠商品列表
    buyingGivingGoodsList: {
      rows: [],
      pagination: {},
    },
    //商品管理的商品列表
    goodsList: {
      rows: [],
      pagination: {},
    },
    //买赠充值记录列表
    buyingGivingRechargeRecordList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchVipLevelList({ payload, callback }, { call, put }) {
      const response = yield call(fetchVipLevelList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveVipLevelList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchElectronicWalletList({ payload, callback }, { call, put }) {
      const response = yield call(fetchElectronicWalletList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveElectronicWalletList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchEleWallRechargeRecordList({ payload, callback }, { call, put }) {
      const response = yield call(fetchEleWallRechargeRecordList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveEleWallRechargeRecordList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchPaymentMethodList({ payload }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'savePaymentMethodList',
        payload: response.data,
      });
    },
    *fetchBuyingGivingList({ payload }, { call, put }) {
      const response = yield call(fetchBuyingGivingList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBuyingGivingList',
        payload: response.data,
      });
    },
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBuyingGivingGoodsList({ payload, callback }, { call, put }) {
      const response = yield call(fetchBuyingGivingGoodsList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBuyingGivingGoodsList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchGoodsList({ payload, callback }, { call, put }) {
      const response = yield call(fetchGoodsList, { ...payload, isEnable: true });
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveGoodsList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBuyingGivingRechargeRecordList({ payload, callback }, { call, put }) {
      const response = yield call(fetchBuyingGivingRechargeRecordList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBuyingGivingRechargeRecordList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addVipLevel({ payload, callback }, { call, put }) {
      const response = yield call(addVipLevel, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchVipLevelList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addElectronicWallet({ payload, callback }, { call, put }) {
      const response = yield call(addElectronicWallet, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchElectronicWalletList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addBuyingGiving({ payload, callback }, { call, put }) {
      const response = yield call(addBuyingGiving, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBuyingGivingList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addBuyingGivingGoods({ payload, callback }, { call, put }) {
      const response = yield call(addBuyingGivingGoods, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBuyingGivingGoodsList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeVipLevel({ payload, callback }, { call, put }) {
      const response = yield call(removeVipLevel, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchVipLevelList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeElectronicWallet({ payload, callback }, { call, put }) {
      const response = yield call(removeElectronicWallet, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchElectronicWalletList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeBuyingGiving({ payload, callback }, { call, put }) {
      const response = yield call(removeBuyingGiving, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchBuyingGivingList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeBuyingGivingGoods({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(removeBuyingGivingGoods, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchBuyingGivingGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateVipLevel({ payload, callback }, { call, put }) {
      const response = yield call(updateVipLevel, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchVipLevelList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateElectronicWallet({ payload, callback }, { call, put }) {
      const response = yield call(updateElectronicWallet, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchElectronicWalletList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateBuyingGiving({ payload, callback }, { call, put }) {
      const response = yield call(updateBuyingGiving, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBuyingGivingList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateBuyingGivingGoods({ payload, callback }, { call, put }) {
      const response = yield call(updateBuyingGivingGoods, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBuyingGivingGoodsList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeElectronicWalletStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeElectronicWalletStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchElectronicWalletList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeBuyingGivingStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeBuyingGivingStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBuyingGivingList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeBuyingGivingGoodsStatus({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(changeBuyingGivingGoodsStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchBuyingGivingGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveVipLevelList(state, action) {
      return {
        ...state,
        vipLevelList: action.payload,
      };
    },
    saveElectronicWalletList(state, action) {
      return {
        ...state,
        electronicWalletList: action.payload,
      };
    },
    saveEleWallRechargeRecordList(state, action) {
      return {
        ...state,
        eleWallRechargeRecordList: action.payload,
      };
    },
    savePaymentMethodList(state, action) {
      return {
        ...state,
        paymentMethodList: action.payload,
      };
    },
    saveBuyingGivingList(state, action) {
      return {
        ...state,
        buyingGivingList: action.payload,
      };
    },
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveBuyingGivingGoodsList(state, action) {
      return {
        ...state,
        buyingGivingGoodsList: action.payload,
      };
    },
    saveGoodsList(state, action) {
      return {
        ...state,
        goodsList: action.payload,
      };
    },
    saveBuyingGivingRechargeRecordList(state, action) {
      return {
        ...state,
        buyingGivingRechargeRecordList: action.payload,
      };
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/app-management/vip-management/electronic-wallet') {
          dispatch({
            type: 'fetchElectronicWalletList',
          })
        }
        if (location.pathname === '/app-management/vip-management/ele-wall-recharge-record') {
          dispatch({
            type: 'fetchEleWallRechargeRecordList',
          })
        }
        if (location.pathname === '/app-management/vip-management/vip-level-settings') {
          dispatch({
            type: 'fetchVipLevelList',
          })
        }
      });
    },
  },
};
