import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, InputNumber, Row, Col, Tabs } from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './ElectronicWallet.less';
import BuyingGivingGoods from './BuyingGivingGoods'
import { imgDomainName } from "../../../constants";

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增买赠"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="面额">
        {form.getFieldDecorator('denomination', {
          rules: [{ required: true, message: '请输入面额！' }],
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入面额" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="赠送金额">
        {form.getFieldDecorator('give', {
          rules: [{ required: true, message: '请输入赠送金额！' }],
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入赠送金额" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改买赠"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="面额">
        {form.getFieldDecorator('denomination', {
          rules: [{ required: true, message: '请输入面额！' }],
          initialValue: record.denomination,
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入面额" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="赠送金额">
        {form.getFieldDecorator('give', {
          rules: [{ required: true, message: '请输入赠送金额！' }],
          initialValue: record.give,
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入赠送金额" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ vip, loading }) => ({
  vip,
  loading: loading.models.vip,
}))
@Form.create()
class ElectronicWallet extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    clickingId: ''
  };

  columns = [
    // {
    //   title: '编号',
    //   dataIndex: 'buyGiveId',
    // },
    {
      title: '面额 (￥)',
      dataIndex: 'denomination',
    },
    {
      title: '售价 (￥)',
      dataIndex: 'price',
    },
    {
      title: '赠送金额 (￥)',
      dataIndex: 'give',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: '15%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.buyGiveId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = buyGiveIds => {
    console.log('buyGiveIds: ', buyGiveIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'vip/removeBuyingGiving',
      payload: { buyGiveIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'vip/changeBuyingGivingStatus',
      payload: { buyGiveId: record.buyGiveId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'vip/fetchBuyingGivingList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'vip/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'vip/addBuyingGiving',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'vip/updateBuyingGiving',
      payload: {
        ...fields,
        buyGiveId: record.buyGiveId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  callback = (key) =>  {
    console.log('key: ', key);
  }

  render() {
    const {
      vip: { buyingGivingList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <Row >
            <Col span={24}>
              <Tabs defaultActiveKey="1" onChange={this.callback}>
                <TabPane tab="买赠设置" key="1">
                  <div className={styles.tableList}>
                    <div className={styles.tableListOperator}>
                      <Button style={{marginTop: 16}} icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                        新增
                      </Button>
                      {selectedRows.length > 0 && (
                        <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.buyGiveId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
                      )}
                    </div>
                    <StandardTable
                      size='small'
                      className={styles.customTable}
                      selectedRows={selectedRows}
                      loading={loading}
                      data={buyingGivingList}
                      columns={this.columns}
                      onSelectRow={this.handleSelectRows}
                      onChange={this.handleStandardTableChange}
                      rowKey="buyGiveId"
                    />
                  </div>
                </TabPane>
                <TabPane tab="商品明细" key="2" style={{paddingTop: 24}}>
                  <BuyingGivingGoods />
                </TabPane>
              </Tabs>
            </Col>
          </Row>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />
      </div>
    );
  }
}

export default ElectronicWallet;
