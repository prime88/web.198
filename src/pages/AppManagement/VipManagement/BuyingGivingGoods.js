import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Button,
  InputNumber,
  Modal,
  Divider,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import BuyingGivingGoodsAdd from './BuyingGivingGoodsAdd'
import styles from './BuyingGivingGoods.less';
import {apiDomainName, imgDomainName} from "../../../constants";
import ImagePreviewModal from '@/components/ImagePreviewModal';

const FormItem = Form.Item;
const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

class EditableCell extends React.Component {
  state = {
    editing: false,
  }

  componentDidMount() {
    if (this.props.editable) {
      document.addEventListener('click', this.handleClickOutside, true);
    }
  }

  componentWillUnmount() {
    if (this.props.editable) {
      document.removeEventListener('click', this.handleClickOutside, true);
    }
  }

  toggleEdit = () => {
    const editing = !this.state.editing;
    this.setState({ editing }, () => {
      if (editing) {
        this.input.focus();
      }
    });
  }

  handleClickOutside = (e) => {
    const { editing } = this.state;
    if (editing && this.cell !== e.target && !this.cell.contains(e.target)) {
      this.save();
    }
  }

  save = () => {
    const { record, handleSave } = this.props;
    this.form.validateFields((error, values) => {
      if (error) {
        return;
      }
      this.toggleEdit();
      handleSave({ ...record, ...values });
    });
  }

  render() {
    const { editing } = this.state;
    const {
      editable,
      dataIndex,
      title,
      record,
      index,
      handleSave,
      ...restProps
    } = this.props;
    return (
      <td ref={node => (this.cell = node)} {...restProps}>
        {editable ? (
          <EditableContext.Consumer>
            {(form) => {
              this.form = form;
              return (
                editing ? (
                  <FormItem style={{ margin: 0 }}>
                    {form.getFieldDecorator(dataIndex, {
                      rules: [{
                        required: true,
                        message: `请输入${title}`,
                      }],
                      initialValue: record[dataIndex],
                    })(
                      <InputNumber
                        precision={2}
                        style={{width: '100%'}}
                        min={0}
                        ref={node => (this.input = node)}
                        onPressEnter={this.save}
                      />
                    )}
                  </FormItem>
                ) : (
                  <div
                    className={styles["editable-cell-value-wrap"]}
                    style={{ paddingRight: 24 }}
                    onClick={this.toggleEdit}
                  >
                    {restProps.children}
                  </div>
                )
              );
            }}
          </EditableContext.Consumer>
        ) : restProps.children}
      </td>
    );
  }
}

@Form.create()
class CreateForm extends PureComponent {

  okHandle = () => {
    const {
      handleAdd,
    } = this.props;
    const { selectedRows } = this.buyingGivingGoodsAdd.state;
    console.log('selectedRows: ', selectedRows);
    const goodsIds = selectedRows.map(item => item.goodsId).join(',');
    handleAdd(goodsIds);
  };

  render() {
    const {
      modalVisible,
      handleModalVisible,
    } = this.props;

    const saveFormRef = (formRef) => {
      this.buyingGivingGoodsAdd = formRef;
    }

    return (
      <Modal
        width={'95%'}
        destroyOnClose
        title="选择商品"
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => handleModalVisible()}
      >
        <BuyingGivingGoodsAdd wrappedComponentRef={saveFormRef} />
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ vip, loading }) => ({
  vip,
  loading: loading.models.vip,
}))
@Form.create()
class BuyingGivingGoods extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    record: {},
    previewVisible: false,
    imageList: [],
  };

  onImageClick = (images) => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true })
  }

  columns = [
    // {
    //   title: '编号',
    //   dataIndex: 'goodsId',
    //   fixed: 'left',
    //   width: 70
    // },
    {
      title: '图片',
      dataIndex: 'imageUrl',
      // render: (text) => (text ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text}`} /> : ''),
      render: (text) => {
        const imageUrlArray = text ? text.split(',') : [];
        return imageUrlArray.length > 0 ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text.split(',')[0]}`} /> : '';
      },
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '存货简称',
      dataIndex: 'shorthand',
    },
    {
      title: '类目',
      dataIndex: 'stockClassify',
    },
    {
      title: '计量单位',
      dataIndex: 'units',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '活动价 (￥)',
      dataIndex: 'activityPrice',
      editable: true,
      width: 130
    },
    {
      title: '总库存',
      dataIndex: 'totalStock',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    // {
    //   title: '商品详情',
    //   dataIndex: 'details',
    // },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 100,
      fixed: 'right',
      render: (text, record) => {
        return (
          <Fragment>
            <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
            <Divider type="vertical" />
            {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
            {/*<Divider type="vertical" />*/}
            <Popconfirm
              title="确认删除？"
              onConfirm={() => this.recordRemove(record.goodsId)}
              okText="确认"
              cancelText="取消"
            >
              <a href="#">删除</a>
            </Popconfirm>
          </Fragment>
        )
      },
    },
  ];

  changeStatus = record => {
    const { dispatch, form } = this.props;
    dispatch({
      type: 'vip/changeBuyingGivingGoodsStatus',
      payload: { goodsId: record.goodsId, isEnable: !record.isEnable },
      fetchForm: {
        ...form.getFieldsValue(),
      }
    });
  };

  recordRemove = goodsIds => {
    console.log('goodsId: ', goodsIds);
    const { dispatch, form } = this.props;
    // const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'vip/removeBuyingGivingGoods',
      payload: { goodsIds },
      fetchForm: {
        ...form.getFieldsValue(),
      },
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    //存货分类列表
    dispatch({
      type: 'vip/fetchInventoryClassificationList',
    });

    //买赠商品列表
    dispatch({
      type: 'vip/fetchBuyingGivingGoodsList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'vip/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'vip/fetchBuyingGivingGoodsList',
      // payload: {
      //   stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      // },
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'vip/fetchBuyingGivingGoodsList',
        payload: {
          ...fieldsValue,
        },
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = goodsIds => {
    const { dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'vip/addBuyingGivingGoods',
      payload: {
        goodsIds
      },
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1]
    console.log('fields: ', fields);
    dispatch({
      type: 'vip/updateGoods',
      payload: {
        ...fields,
        vipId: record.vipId,
      },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间下限">
              {getFieldDecorator('lowerPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间下限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间上限">
              {getFieldDecorator('upperPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间上限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button type="primary" style={{ marginLeft: 8 }} onClick={() => this.handleModalVisible(true)}>
                新增商品
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  loadNewOptions = (newOptions) => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  }

  handleSave = (row) => {
    console.log('row: ', row);
    const {
      vip: {
        buyingGivingGoodsList,
      },
      dispatch
    } = this.props;

    //没有修改的话，不需要调接口更新数据
    const newData = [...buyingGivingGoodsList.rows];
    const index = newData.findIndex(item => row.goodsId === item.goodsId);
    const item = newData[index];
    if (item.activityPrice !== row.activityPrice){
      //修改买赠商品
      dispatch({
        type: 'vip/updateBuyingGivingGoods',
        payload: { goodsId: row.goodsId, activityPrice: row.activityPrice },
      });
    }
  }

  render() {
    const {
      vip: {
        buyingGivingGoodsList,
      },
      loading,
    } = this.props;
    const {
      modalVisible,
      previewVisible,
      imageList,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });

    return (
      <div>
        <div>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <Row gutter={16}>
              <Col span={24}>
                <StandardTable
                  // selectedRows={selectedRows}
                  loading={loading}
                  data={buyingGivingGoodsList}
                  // columns={this.columns}
                  columns={columns}
                  // onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='goodsId'
                  size='small'
                  scroll={{x: 1500}}
                  components={components}
                  rowClassName={() => styles.editableRow}
                />
              </Col>
            </Row>
          </div>
        </div>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
        />
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={120}
          imgHeight={120}
        />
      </div>
    );
  }
}

export default BuyingGivingGoods;
