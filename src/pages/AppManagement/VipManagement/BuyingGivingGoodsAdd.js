import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Cascader,
  Popconfirm,
  Upload,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { isInventoryNameRepeated } from '@/services/vip';
import styles from './BuyingGivingGoodsAdd.less';
import {apiDomainName, imgDomainName} from "../../../constants";
import ImagePreviewModal from '@/components/ImagePreviewModal';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ vip, loading }) => ({
  vip,
  loading: loading.models.vip,
}))
@Form.create()
class BuyingGivingGoodsAdd extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    record: {},
    previewVisible: false,
    imageList: [],
  };

  onImageClick = (images) => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true })
  }

  columns = [
    {
      title: '编号',
      dataIndex: 'goodsId',
      fixed: 'left',
      width: 70
    },
    {
      title: '图片',
      dataIndex: 'imageUrl',
      // render: (text) => (text ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text}`} /> : ''),
      render: (text) => {
        const imageUrlArray = text ? text.split(',') : [];
        return imageUrlArray.length > 0 ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text.split(',')[0]}`} /> : '';
      },
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '存货简称',
      dataIndex: 'shorthand',
    },
    {
      title: '类目',
      dataIndex: 'stockClassify',
    },
    {
      title: '计量单位',
      dataIndex: 'units',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '总库存',
      dataIndex: 'totalStock',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    // {
    //   title: '商品详情',
    //   dataIndex: 'details',
    // },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    //存货分类列表
    dispatch({
      type: 'vip/fetchInventoryClassificationList',
    });

    //商品列表
    dispatch({
      type: 'vip/fetchGoodsList',
    });

    //获取品牌列表
    dispatch({
      type: 'vip/fetchBrandList',
    });

    //获取计量单位列表
    dispatch({
      type: 'vip/fetchMeasurementUnitClassificationList',
      callback: (data) => {
        this.setState({ addModalMeasurementUnitOptions: data.rows.map(item => (
            {
              value: item.unitsClassifyId,
              label: item.name,
              isLeaf: false,
            }
          ))
        })
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'vip/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'vip/fetchGoodsList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'vip/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'vip/fetchGoodsList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        },
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间下限">
              {getFieldDecorator('lowerPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间下限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间上限">
              {getFieldDecorator('upperPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间上限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;

    //商品列表
    dispatch({
      type: 'vip/fetchGoodsList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        ...form.getFieldsValue()
      }
    });

    this.setState({ selectedMenuItemKey: key })
  }

  loadNewOptions = (newOptions) => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  }

  render() {
    const {
      vip: { inventoryClassificationList, BuyingGivingGoodsAdd, goodsList },
      loading,
    } = this.props;
    const {
      selectedRows,
      previewVisible,
      imageList,
    } = this.state;
    return (
      <div>
        <div>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <Row gutter={16}>
              <Col span={4}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                  { inventoryClassificationList.rows.map(item => (
                    <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Menu.Item>
                  )) }
                </Menu>
              </Col>
              <Col span={20}>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={goodsList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='goodsId'
                  size='small'
                  scroll={{x: 1500}}
                  onRow={(record) => {
                    return {
                      onClick: () => {// 点击行
                        console.log('record: ', record);
                        console.log('oldSelectedRows: ', this.state.selectedRows);
                        this.setState(prevState => {
                          let newSelectedRows = [...prevState.selectedRows];
                          let flag = true;
                          for (let item of prevState.selectedRows) {
                            if (item.goodsId === record.goodsId) {
                              newSelectedRows = newSelectedRows.filter(item => item.goodsId !== record.goodsId);
                              flag = false;
                              break;
                            }
                          }
                          if (flag){
                            newSelectedRows.push(record)
                          }
                          console.log('newSelectedRows: ', newSelectedRows);
                          return ({
                            selectedRows: newSelectedRows
                          })
                        })
                      },
                    };
                  }}
                />
              </Col>
            </Row>
          </div>
        </div>
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={120}
          imgHeight={120}
        />
      </div>
    );
  }
}

export default BuyingGivingGoodsAdd;
