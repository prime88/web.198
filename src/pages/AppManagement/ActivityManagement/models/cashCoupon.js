// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchCouponList,
  addCoupon,
  updateCoupon,
  removeCoupon,
  changeCouponStatus,
} from '@/services/cashCoupon';
import { queryStoreList } from '@/services/store';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'cashCoupon',

  state: {
    //优惠券列表
    couponList: {
      rows: [],
      pagination: {},
    },
    //优惠券类型列表
    couponTypeList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchCouponList({ payload, callback }, { call, put }) {
      const response = yield call(fetchCouponList, { ...payload, type: 1 });
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveCouponList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchCouponTypeList({ payload, callback }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveCouponTypeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addCoupon({ payload, callback }, { call, put }) {
      const response = yield call(addCoupon, { ...payload, type: 1 });
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchCouponList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeCoupon({ payload, callback }, { call, put }) {
      const response = yield call(removeCoupon, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchCouponList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateCoupon({ payload, callback }, { call, put }) {
      const response = yield call(updateCoupon, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchCouponList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeCouponStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeCouponStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchCouponList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveCouponList(state, action) {
      return {
        ...state,
        couponList: action.payload,
      };
    },
    saveCouponTypeList(state, action) {
      return {
        ...state,
        couponTypeList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //代金券
        if (location.pathname === '/app-management/activity-management/cash-coupon-management') {
          dispatch({
            type: 'fetchCouponList',
          })
        }
      });
    },
  },
};
