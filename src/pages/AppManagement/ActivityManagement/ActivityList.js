import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Cascader,
  Popconfirm,
  Alert,
  Table,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { isInventoryNameRepeated } from '@/services/activity';
import styles from './ActivityList.less';
import ActivityListAdd from './ActivityListAdd'
import {imgDomainName} from "../../../constants";
import ImagePreviewModal from '@/components/ImagePreviewModal';
import DescriptionList from '@/components/DescriptionList';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const AddActivityForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="新增活动"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="活动名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入活动名称！' }],
        })(<Input placeholder="请输入活动名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="优先级">
        {form.getFieldDecorator('sort', {
          rules: [{ required: true, message: '请输入优先级！' }],
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入优先级" min={0}/>)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateActivityForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="修改活动"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="活动名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入活动名称！' }],
          initialValue: record.name,
        })(<Input placeholder="请输入活动名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="优先级">
        {form.getFieldDecorator('sort', {
          rules: [{ required: true, message: '请输入优先级！' }],
          initialValue: record.sort,
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入优先级" min={0}/>)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateGoodsForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="修改活动价"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="活动价">
        {form.getFieldDecorator('activityPrice', {
          rules: [{ required: true, message: '请输入活动价！' }],
          initialValue: record.activityPrice,
        })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入活动价" min={0}/>)}
      </FormItem>
    </Modal>
  );
});

const AddGoodsForm = Form.create()(props => {
  let goodsListAddRef;

  const saveFormRef = (formRef) => {
    goodsListAddRef = formRef;
  }

  const {
    modalVisible,
    handleModalVisible,
  } = props;

  const okHandle = () => {
    const {
      handleAdd,
    } = props;
    const { selectedRows } = goodsListAddRef.state;

    if (selectedRows.length === 0) {
      message.warning('请选择商品!');
      return;
    }

    const goodsIds = selectedRows.map(item => item.goodsId).join(',');
    handleAdd(goodsIds);
  };

  return (
    <Modal
      width={'80%'}
      destroyOnClose
      // title="选择商品"
      title={(
        <div>
          <span>选择商品</span>
          <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{ top: 20 }}
    >
      <ActivityListAdd wrappedComponentRef={saveFormRef} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ activity, brand, measurementUnit, loading }) => ({
  activity,
  brand,
  measurementUnit,
  activityLoading: loading.effects['activity/fetchActivityList'],
  goodsLoading: loading.effects['activity/fetchPromotionGoodsList'],
}))
@Form.create()
class ActivityList extends PureComponent {
  state = {
    modalGoodsVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    selectedGoodsRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    record: {},
    previewVisible: false,
    modalActivityAddVisible: false,
    modalActivityUpdateVisible: false,
    modalGoodsUpdateVisible: false,
    imageList: [],
    batchDeletionPopconfirmVisible: false,
  };

  onImageClick = (images) => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true })
  }

  activityColumns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '活动名称',
      dataIndex: 'name',
    },
    {
      title: '排序',
      dataIndex: 'sort',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '操作',
      width: '13%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleModalActivityUpdateVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.promotionId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ]

  columns = [
    // {
    //   title: '编号',
    //   dataIndex: 'promotionGoodsId',
    //   width: 50,
    //   // fixed: 'left',
    //   align: 'center',
    // },
    {
      title: '图片',
      dataIndex: 'imageUrl',
      // fixed: 'left',
      render: (text) => {
        const imageUrlArray = text ? text.split(',') : [];
        return imageUrlArray.length > 0 ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text.split(',')[0]}`} /> : '';
      },
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '存货简称',
      dataIndex: 'shorthand',
    },
    {
      title: '类目',
      dataIndex: 'stockClassify',
    },
    // {
    //   title: '计量单位',
    //   dataIndex: 'units',
    // },
    // {
    //   title: '所属门店',
    //   dataIndex: 'store',
    // },
    // {
    //   title: '总库存',
    //   dataIndex: 'totalStock',
    // },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    // {
    //   title: '规格型号',
    //   dataIndex: 'specification',
    // },
    // {
    //   title: '状态',
    //   dataIndex: 'status',
    // },
    {
      title: '活动价(¥)',
      dataIndex: 'activityPrice',
    },
    {
      title: '总库存',
      dataIndex: 'totalStock'
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 100,
      // fixed: 'right',
      render: (text, record) => {
        return (
          <Fragment>
            <a onClick={() => this.handleModalGoodsUpdateVisible(true, record)}>修改</a>
            <Divider type="vertical" />
            <Popconfirm
              title="确认删除？"
              onConfirm={() => this.recordGoodsRemove(record.promotionGoodsId)}
              okText="确认"
              cancelText="取消"
            >
              <a href="#">删除</a>
            </Popconfirm>
          </Fragment>
        )
      },
    },
  ];

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'activity/changeActivityStatus',
      payload: { promotionId: record.promotionId, isEnable: !record.isEnable },
    });
  };

  recordRemove = promotionIds => {
    console.log('promotionIds: ', promotionIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'activity/removeActivity',
      payload: { promotionIds },
      // callback: () => {
      //   this.setState({
      //     selectedRows: [],
      //   });
      // },
    });
  };

  recordGoodsRemove = promotionGoodsIds => {
    console.log('promotionGoodsIds: ', promotionGoodsIds);
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey, selectedRows } = this.state;

    dispatch({
      type: 'activity/removeGoods',
      payload: {
        promotionGoodsIds,
      },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        promotionId: selectedRows[0].promotionId
      },
      callback: () => {
        this.setState({
          selectedGoodsRows: [],
        });
      },
    });
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    //存货分类列表
    dispatch({
      type: 'activity/fetchInventoryClassificationList',
    });

    //门店列表
    dispatch({
      type: 'activity/fetchStoreList',
    });


    //活动列表
    dispatch({
      type: 'activity/fetchActivityList',
    });

    //初始化活动商品列表
    dispatch({
      type: 'activity/savePromotionGoodsList',
      payload: {
        rows: [],
      },
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'activity/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey, selectedRows } = this.state;

    if (selectedRows.length === 0) {
      message.warning('请先选择活动!');
      return;
    }

    form.resetFields();
    dispatch({
      type: 'activity/fetchPromotionGoodsList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        promotionId: selectedRows[0].promotionId
      },
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'activity/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {

    this.setState({
      selectedRows: rows,
    });

    if (rows.length > 0) {
      this.updatePromotionGoodsList(rows);
    }
  };

  handleSelectGoodsRows = rows => {
    this.setState({
      selectedGoodsRows: rows,
    });
  };

  //更新活动商品列表
  updatePromotionGoodsList = (rows) => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    dispatch({
      type: 'activity/fetchPromotionGoodsList',
      payload: {
        promotionId: rows[0].promotionId,
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      }
    });
  }

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey, selectedRows } = this.state;

    if (selectedRows.length === 0) {
      message.warning('请先选择活动!');
      return;
    }

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'activity/fetchPromotionGoodsList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
          promotionId: selectedRows[0].promotionId
        },
      });
    });
  };

  handleModalGoodsVisible = flag => {
    this.setState({
      modalGoodsVisible: !!flag,
    });
  };

  handleModalActivityAddVisible = flag => {
    this.setState({
      modalActivityAddVisible: !!flag,
    });
  };

  handleModalActivityUpdateVisible = (flag, record) => {
    this.setState({
      modalActivityUpdateVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleModalGoodsUpdateVisible = (flag, record) => {
    this.setState({
      modalGoodsUpdateVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleGoodsAdd = ( goodsIds) => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey, selectedRows } = this.state;
    dispatch({
      type: 'activity/addActivityGoods',
      payload: {
        goodsIds,
        promotionId: selectedRows[0].promotionId
      },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        promotionId: selectedRows[0].promotionId
      }
    });

    // message.success('添加成功');
    this.handleModalGoodsVisible();
  };

  handleActivityAdd = (values) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'activity/addActivity',
      payload: values,
    });

    // message.success('添加成功');
    this.handleModalActivityAddVisible();
  };

  handleActivityUpdate = (values) => {
    const { dispatch } = this.props;
    const { record } = this.state;

    values.promotionId = record.promotionId;
    dispatch({
      type: 'activity/updateActivity',
      payload: values,
    });

    // message.success('添加成功');
    this.handleModalActivityUpdateVisible();
  };

  handleGoodsUpdate = (values) => {
    const { dispatch, form } = this.props;
    const { record, selectedMenuItemKey, selectedRows } = this.state;

    values.promotionGoodsId = record.promotionGoodsId;
    dispatch({
      type: 'activity/updateActivityGoodsPrice',
      payload: values,
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        promotionId: selectedRows[0].promotionId
      },
    });

    // message.success('添加成功');
    this.handleModalGoodsUpdateVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1];
    dispatch({
      type: 'activity/updateInventory',
      payload: {
        ...fields,
        stockId: record.stockId,
      },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      activity: { storeList },
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24} push={4}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24} push={4}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;
    const { selectedRows } = this.state;

    if (selectedRows.length === 0) {
      message.warning('请先选择活动!');
      return;
    }

    //获取存货分类列表
    dispatch({
      type: 'activity/fetchPromotionGoodsList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        ...form.getFieldsValue(),
        promotionId: selectedRows[0].promotionId
      }
    });

    this.setState({ selectedMenuItemKey: key })
  }

  loadNewOptions = (newOptions) => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  }

  cleanSelectedKeys = () => {
    this.setState({ selectedRows: [] })
  };

  onRowClick = (record) => {
    console.log('record: ', record);

    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedGoodsRows];
      let flag = true;
      for (let item of prevState.selectedGoodsRows) {
        if (item.promotionGoodsId === record.promotionGoodsId) {
          newSelectedRows = newSelectedRows.filter(item => item.promotionGoodsId !== record.promotionGoodsId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedGoodsRows: newSelectedRows
      })
    })
  }

  handleBatchDeletionPopconfirmVisibleChange = (visible) => {
    console.log('visible: ', visible);
    const { selectedGoodsRows } = this.state;
    if (selectedGoodsRows.length > 0) {
      this.setState({ batchDeletionPopconfirmVisible: visible });
    }
  }

  render() {
    const {
      activity: { inventoryClassificationList, promotionGoodsList, storeList, activityList },
      activityLoading,
      goodsLoading,
    } = this.props;
    const {
      selectedRows,
      selectedGoodsRows,
      modalGoodsVisible,
      selectedMenuItemKey,
      addModalMeasurementUnitOptions,
      previewVisible,
      imageList,
      modalActivityAddVisible,
      modalActivityUpdateVisible,
      modalGoodsUpdateVisible,
      record,
      batchDeletionPopconfirmVisible,
    } = this.state;

    const goodsMethods = {
      handleAdd: this.handleGoodsAdd,
      handleModalVisible: this.handleModalGoodsVisible,
    };

    const activityAddMethods = {
      handleAdd: this.handleActivityAdd,
      handleModalVisible: this.handleModalActivityAddVisible,
    };

    const activityUpdateMethods = {
      handleAdd: this.handleActivityUpdate,
      handleModalVisible: this.handleModalActivityUpdateVisible,
    };

    const goodsUpdateMethods = {
      handleAdd: this.handleGoodsUpdate,
      handleModalVisible: this.handleModalGoodsUpdateVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.titleTop}>活动列表</div>
            <div className={styles.tableListOperator}>
              <Row gutter={16}>
                <Col span={24}>
                  <Button icon="plus" type="primary" onClick={() => this.handleModalActivityAddVisible(true)}>
                    新增活动
                  </Button>
                  <Button onClick={() => {
                    const { selectedRows } = this.state;
                    if (selectedRows.length === 0) {
                      message.warning('请先选择活动!');
                      return;
                    }
                    this.handleModalGoodsVisible(true);
                  }}>
                    选择活动商品
                  </Button>
                </Col>
              </Row>
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={activityLoading}
              data={activityList}
              columns={this.activityColumns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey='promotionId'
              size='small'
              style={{marginTop: 16}}
              type='radio'
              onRow={(record) => {
                return {
                  onClick: () => {// 点击行
                    console.log('record: ', record);
                    this.setState({ selectedRows: [record] })

                    //更新活动商品列表
                    this.updatePromotionGoodsList([record]);
                  },
                };
              }}
            />
            <div className={styles.titleBottom}>活动商品</div>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <Row gutter={16}>
              <Col span={4}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                  { inventoryClassificationList.rows.map(item => (
                    <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Menu.Item>
                  )) }
                </Menu>
              </Col>
              <Col span={20}>
                <div className={styles.tableListOperator}>
                  <Popconfirm
                    title="确认删除？"
                    onConfirm={() => this.recordGoodsRemove( selectedGoodsRows.map(item => item.promotionGoodsId).join(','))}
                    okText="确认"
                    cancelText="取消"
                    visible={batchDeletionPopconfirmVisible}
                    onVisibleChange={this.handleBatchDeletionPopconfirmVisibleChange}
                  >
                    <Button disabled={selectedGoodsRows.length <= 0}>批量删除</Button>
                  </Popconfirm>
                </div>
                <StandardTable
                  selectedRows={selectedGoodsRows}
                  loading={goodsLoading}
                  data={promotionGoodsList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectGoodsRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='promotionGoodsId'
                  size='small'
                  scroll={{x: 1400}}
                  onRow={(record) => {
                    return {
                      onClick: () => {// 点击行
                        this.onRowClick(record);
                      },
                    };
                  }}
                />
              </Col>
            </Row>
          </div>
        </Card>
        <AddActivityForm
          {...activityAddMethods}
          modalVisible={modalActivityAddVisible}
        />
        <UpdateActivityForm
          {...activityUpdateMethods}
          modalVisible={modalActivityUpdateVisible}
          record={record}
        />
        <UpdateGoodsForm
          {...goodsUpdateMethods}
          modalVisible={modalGoodsUpdateVisible}
          record={record}
        />
        <AddGoodsForm
          {...goodsMethods}
          modalVisible={modalGoodsVisible}
          selectedMenuItemKey={selectedMenuItemKey}
          addModalMeasurementUnitOptions={addModalMeasurementUnitOptions}
          loadNewOptions={this.loadNewOptions}
        />
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={120}
          imgHeight={120}
        />
      </div>
    );
  }
}

export default ActivityList;
