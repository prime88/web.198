import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, InputNumber, DatePicker, Radio, Popover, Dropdown, Menu } from 'antd';
import StandardTable from '@/components/StandardTable';
import moment from 'moment';
import styles from './CouponManagement.less';
import { imgDomainName } from "../../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {
  state = {
    couponTypeCode: '0',
    grantCountInput: '',
  }

  okHandle = () => {
    const { form, handleAdd } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.storeIds){
        fieldsValue.storeIds = fieldsValue.storeIds.join(',');
      }
      fieldsValue.grantStartTime = fieldsValue.grantStartTime.format('YYYY-MM-DD HH:mm:ss');
      fieldsValue.grantEndTime = fieldsValue.grantEndTime.format('YYYY-MM-DD HH:mm:ss');
      fieldsValue.grantCount = fieldsValue.grantCount === 0 ? fieldsValue.grantCountInput : fieldsValue.grantCount;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ couponTypeCode: 0, grantCountInput: '' });
      handleAdd(fieldsValue);
    });
  };

  onCouponTypeChange = value => {
    console.log('value: ', value);
    this.setState({ couponTypeCode: value });
  }

  onGrantCountChange = (e) => {
    if (e.target.value === 0) {
      this.setState({ grantCountInput: 1 })
    }
    else{
      this.setState({ grantCountInput: '' })
    }
  }

  render() {
    const { modalVisible, form, handleModalVisible, couponTypeList, storeList } = this.props;
    const { couponTypeCode, grantCountInput } = this.state;

    const storeItemDisplay = couponTypeCode.toString() === '1' ? 'block' : 'none';

    return (
      <Modal
        destroyOnClose
        // title="新增优惠券"
        title={(
          <div>
            <span>新增优惠券</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible()
          this.setState({ couponTypeCode: 0, grantCountInput: '' });
        }}
      >
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="优惠券名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入优惠券名称！' },
            ],
          })(<Input placeholder="请输入优惠券名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="金额">
          {form.getFieldDecorator('couponAmount', {
            rules: [{ required: true, message: '请输入金额！' }],
          })(
            <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入金额" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="优惠券类型">
          {form.getFieldDecorator('couponTypeCode', {
            rules: [{ required: true, message: '请选择优惠券类型！' }],
          })(
            <Select
              onSelect={this.onCouponTypeChange}
              style={{width: '100%'}}
              placeholder='请选择优惠券类型'
            >
              {couponTypeList.rows.map(item => <Select.Option key={item.dictDataCode}>{item.dictDataName}</Select.Option>)}
            </Select>
          )}
        </FormItem>
        <FormItem style={{display: storeItemDisplay}} labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="门店">
          {form.getFieldDecorator('storeIds', {
            rules: [{ required: storeItemDisplay === 'block', message: '请选择门店！' }],
          })(
            <Select
              mode='multiple'
              style={{width: '100%'}}
              placeholder='请选择门店'
            >
              {storeList.rows.map(item => <Select.Option key={item.storeId}>{item.name}</Select.Option>)}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="发放开始时间">
          {form.getFieldDecorator('grantStartTime', {
            rules: [{ required: true, message: '请选择发放开始时间！' }],
          })(
            <DatePicker
              showTime
              format="YYYY-MM-DD HH:mm:ss"
              placeholder="请选择开始时间"
              style={{width: '100%'}}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="发放结束时间">
          {form.getFieldDecorator('grantEndTime', {
            rules: [{ required: true, message: '请选择发放结束时间！' }],
          })(
            <DatePicker
              showTime
              format="YYYY-MM-DD HH:mm:ss"
              placeholder="请选择结束时间"
              style={{width: '100%'}}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="满减限制">
          {form.getFieldDecorator('limitedAmount', {
            rules: [{ required: true, message: '请输入满减限制！' }],
          })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入满减限制" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="发放数量">
          {form.getFieldDecorator('grantCount', {
            rules: [{ required: true, message: '请输入发放数量！' }],
          })(
            <Radio.Group onChange={this.onGrantCountChange} className={styles.customRadioGroup}>
              <Radio value={-1}>无限量</Radio>
              <Radio value={0}>
                <div style={{ display: 'inline-block' }}>
                  <FormItem>
                    {form.getFieldDecorator('grantCountInput', {
                      initialValue: grantCountInput
                    })(<InputNumber min={1} style={{ width: 75 }} size="small" />)}
                  </FormItem>
                </div>
                &nbsp;&nbsp;张
              </Radio>
            </Radio.Group>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="有效时间(天)">
          {form.getFieldDecorator('effectiveTime', {
            rules: [{ required: true, message: '请输入有效时间！' }],
          })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入有效时间" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="说明">
          {form.getFieldDecorator('introduce')(<Input.TextArea rows={3} style={{width: '100%'}} placeholder="请输入优惠券说明" />)}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  state = {
    couponTypeCode: '0',
    grantCount: '',
    grantCountInput: '',
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record }  = nextProps;
    const { grantCount, couponTypeCode } = prevState;
    let newState = null;

    if (record.couponTypeCode !== undefined && record.couponTypeCode !== couponTypeCode && couponTypeCode.toString() === '0') {
      newState = {
        ...newState,
        couponTypeCode: record.couponTypeCode
      }
    }

    return newState;
  }

  okHandle = () => {
    const { handleUpdate, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.couponTypeCode === 1){//门店券
        if (fieldsValue.storeIds){
          fieldsValue.storeIds = fieldsValue.storeIds.join(',');
        }
      }
      else if (fieldsValue.couponTypeCode === 2) {//通用券
        fieldsValue.storeIds = '';
      }
      fieldsValue.grantStartTime = fieldsValue.grantStartTime.format('YYYY-MM-DD HH:mm:ss');
      fieldsValue.grantEndTime = fieldsValue.grantEndTime.format('YYYY-MM-DD HH:mm:ss');
      fieldsValue.grantCount = fieldsValue.grantCount === 0 ? fieldsValue.grantCountInput : fieldsValue.grantCount;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({
        couponTypeCode: 0,
        grantCount: '',
        grantCountInput: '',
      });
      handleUpdate(fieldsValue);
    });
  };

  onCouponTypeChange = value => {
    console.log('value: ', value);
    this.setState({ couponTypeCode: value });
  }

  render() {
    const { updateModalVisible, form, handleUpdateModalVisible, record, couponTypeList, storeList } = this.props;
    const { couponTypeCode } = this.state;

    const storeItemDisplay = couponTypeCode.toString() === '1' ? 'block' : 'none';

    return (
      <Modal
        destroyOnClose
        // title="修改优惠券"
        title={(
          <div>
            <span>修改优惠券</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible()
          this.setState({
            couponTypeCode: 0,
            grantCount: '',
            grantCountInput: '',
          });
        }}
      >
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="优惠券名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入优惠券名称！' },
            ],
            initialValue: record.name
          })(<Input placeholder="请输入优惠券名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="金额">
          {form.getFieldDecorator('couponAmount', {
            rules: [{ required: true, message: '请输入金额！' }],
            initialValue: record.couponAmount
          })(
            <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入金额" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="优惠券类型">
          {form.getFieldDecorator('couponTypeCode', {
            rules: [{ required: true, message: '请选择优惠券类型！' }],
            initialValue: record.couponTypeCode
          })(
            <Select
              onSelect={this.onCouponTypeChange}
              style={{width: '100%'}}
              placeholder='请选择优惠券类型'
            >
              {couponTypeList.rows.map(item => <Select.Option key={item.dictDataCode} value={item.dictDataCode}>{item.dictDataName}</Select.Option>)}
            </Select>
          )}
        </FormItem>
        <FormItem style={{display: storeItemDisplay}} labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="门店">
          {form.getFieldDecorator('storeIds', {
            rules: [{ required: storeItemDisplay === 'block', message: '请选择门店！' }],
            initialValue: record.storeIds ? record.storeIds.split(',') : []
          })(
            <Select
              mode='multiple'
              style={{width: '100%'}}
              placeholder='请选择门店'
            >
              {storeList.rows.map(item => <Select.Option key={item.storeId}>{item.name}</Select.Option>)}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="发放开始时间">
          {form.getFieldDecorator('grantStartTime', {
            rules: [{ required: true, message: '请选择发放开始时间！' }],
            initialValue: moment(record.grantStartTime)
          })(
            <DatePicker
              showTime
              format="YYYY-MM-DD HH:mm:ss"
              placeholder="请选择开始时间"
              style={{width: '100%'}}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="发放结束时间">
          {form.getFieldDecorator('grantEndTime', {
            rules: [{ required: true, message: '请选择发放结束时间！' }],
            initialValue: moment(record.grantEndTime)
          })(
            <DatePicker
              showTime
              format="YYYY-MM-DD HH:mm:ss"
              placeholder="请选择结束时间"
              style={{width: '100%'}}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="满减限制">
          {form.getFieldDecorator('limitedAmount', {
            rules: [{ required: true, message: '请输入满减限制！' }],
            initialValue: record.limitedAmount
          })(<InputNumber precision={2} style={{width: '100%'}} placeholder="请输入满减限制" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="有效时间(天)">
          {form.getFieldDecorator('effectiveTime', {
            rules: [{ required: true, message: '请输入有效时间！' }],
            initialValue: record.effectiveTime
          })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入有效时间" min={0} />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="说明">
          {form.getFieldDecorator('introduce',{
            initialValue: record.introduce
          })(<Input.TextArea rows={3} style={{width: '100%'}} placeholder="请输入优惠券说明" />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ coupon, store, loading }) => ({
  coupon,
  store,
  loading: loading.models.coupon,
}))
@Form.create()
class CouponManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    formValues: {},
    record: {},
  };

  getStoreList = (record) => {
    // const { store: { storeList } } = this.props;
    const { coupon: { storeList } } = this.props;
    const { storeIds } = record;
    let content = null;
    if (storeList){
      const filterStoreListRows = storeList.rows.filter(item => storeIds.split(',').includes(item.storeId.toString()));
      content = filterStoreListRows.map(item => <p>{item.name}</p>);
    }
    return <div>{content}</div>
  }

  columns = [
    {
      title: '编号',
      dataIndex: 'couponId',
      // width: '6%',
    },
    {
      title: '优惠券名称',
      dataIndex: 'name',
      // width: '16%',
    },
    {
      title: '金额 (￥)',
      dataIndex: 'couponAmount',
      // width: '6%',
    },
    {
      title: '优惠券类型',
      dataIndex: 'couponType',
      // width: '7%',
      render: (text, record) => (
        text === '门店券' ?
        <Popover content={this.getStoreList(record)} title="使用门店">
          <span>{text}</span>
        </Popover> : text
      )
    },
    {
      title: '发放开始时间',
      dataIndex: 'grantStartTime',
      // width: '12%',
    },
    {
      title: '发放结束时间',
      dataIndex: 'grantEndTime',
      // width: '12%',
    },
    {
      title: '满减限制 (￥)',
      dataIndex: 'limitedAmount',
      // width: '9%',
    },
    {
      title: '发放总量',
      dataIndex: 'grantCount',
      // width: '9%',
      render: text => (text === -1 ? `无限量` : text),
    },
    {
      title: '已发数量',
      dataIndex: 'number',
      // render: (text, record) => <div>{record.grantCount-record.allowance}</div>
    },
    {
      title: '剩余总量',
      dataIndex: 'allowance',
      render: text => <span>{parseInt(text) < 0 ? '无限量' : text}</span>
    },
    {
      title: '有效时间(天)',
      dataIndex: 'effectiveTime',
      // width: '9%',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      // width: '6%',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    // {
    //   title: '说明',
    //   dataIndex: 'introduce',
    //   width: '14%',
    // },
    {
      title: '操作',
      width: '8%',
      render: (text, record) => {
        const menu = (
          <Menu>
            <Menu.Item>
              <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
            </Menu.Item>
            {/*<Popconfirm*/}
              {/*title="确认删除？"*/}
              {/*onConfirm={() => this.recordRemove(record.couponId)}*/}
              {/*okText="确认"*/}
              {/*cancelText="取消"*/}
            {/*>*/}
              {/*<a href="#">删除</a>*/}
            {/*</Popconfirm>*/}
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement='bottomRight'>
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        )
      },
    },
  ];

  recordRemove = couponIds => {
    console.log('couponIds: ', couponIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'coupon/removeCoupon',
      payload: { couponIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'coupon/changeCouponStatus',
      payload: { couponId: record.couponId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //优惠券列表
    dispatch({
      type: 'coupon/fetchCouponList',
    });
    //优惠券类型列表
    dispatch({
      type: 'coupon/fetchCouponTypeList',
      payload: {
        dictCode: 'couponType'
      }
    });
    //门店列表
    dispatch({
      type: 'coupon/fetchStoreList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'coupon/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'coupon/addCoupon',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'coupon/updateCoupon',
      payload: {
        ...fields,
        couponId: record.couponId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.couponId === record.couponId) {
          newSelectedRows = newSelectedRows.filter(item => item.couponId !== record.couponId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  render() {
    const {
      coupon: { couponList, couponTypeList, storeList },
      // store: { storeList },
      loading,
    } = this.props;
    const { selectedRows, expandedRowKeys, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.couponId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              size='small'
              selectedRows={selectedRows}
              expandedRowKeys={expandedRowKeys}
              loading={loading}
              data={couponList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="couponId"
              expandedRowRender={record => <p style={{ margin: 0 }}>说明：{record.introduce}</p>}
              onExpand={() => {}}
              onExpandedRowsChange={expandedRows => this.setState({ expandedRowKeys: expandedRows }) }
              onRow={(record) => {
                return {
                  onClick: () => {// 点击行
                    this.onRowClick(record);
                  },
                };
              }}
            />
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
          couponTypeList={couponTypeList}
          storeList={storeList}
        />
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          record={record}
          couponTypeList={couponTypeList}
          storeList={storeList}
        />
      </div>
    );
  }
}

export default CouponManagement;
