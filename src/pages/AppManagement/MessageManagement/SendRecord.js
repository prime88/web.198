import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Button, Popconfirm } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/message';

import styles from './SendRecord.less';

const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ message, loading }) => ({
  message,
  loading: loading.models.message,
}))
@Form.create()
class SendRecord extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'messageLogId',
    },
    {
      title: '标题',
      dataIndex: 'title',
      render: text => <div>{text}</div>
    },
    {
      title: '内容',
      dataIndex: 'text',
    },
    {
      title: '发送时间',
      dataIndex: 'createTime',
    },
    {
      title: '接收人',
      dataIndex: 'recipient',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '1':
            result = '部分';
            break;
          case '2':
            result = '全员';
            break;
        }
        return result;
      }
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.messageLogId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = messageLogIds => {
    console.log('messageLogIds: ', messageLogIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'message/removeMessageRecord',
      payload: { messageLogIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'message/fetchMessageRecordList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'message/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  render() {
    const {
      message: { messageRecordList },
      loading,
    } = this.props;
    const { selectedRows } = this.state;

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.messageLogId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={messageRecordList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="messageLogId"
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default SendRecord;
