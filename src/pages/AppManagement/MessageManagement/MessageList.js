import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, InputNumber, Upload } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/message';
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
import { ContentUtils } from 'braft-utils'
import { upload } from '@/services/common-api';

import styles from './MessageList.less';
import {apiDomainName, imgDomainName} from "../../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');


const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      fieldsValue.content = fieldsValue.content.toHTML();
      console.log('fieldsValue: ', fieldsValue);
      handleAdd(fieldsValue);
    });
  };

  const uploadHandler = (param) => {

    if (!param.file) {
      return false
    }

    let formData = new FormData();
    // if (param.file.size / 1024 > 1024) {
    //   Modal.error({
    //     title: '图片大小不得大于1MB',
    //     content: '请重新选择图片',
    //     okText: '确认',
    //     cancelText: '取消',
    //   })
    // }

    formData.append('image', param.file);

    const response = upload(formData);
    response.then(result => {
      console.log('result: ', result);
      const real = result.msg;
      form.setFieldsValue({
        content: ContentUtils.insertMedias(form.getFieldsValue().content, [{
          type: 'IMAGE',
          // url: URL.createObjectURL(param.file)
          url: `${imgDomainName}${real}`,
          width: '100%'
        }])
      });
    });

  }

  const extendControls = [
    {
      key: 'antd-uploader',
      type: 'component',
      component: (
        <Upload
          accept="image/*"
          showUploadList={false}
          customRequest={uploadHandler}
        >
          {/* 这里的按钮最好加上type="button"，以避免在表单容器中触发表单提交，用Antd的Button组件则无需如此 */}
          <button type="button" className="control-item button upload-button" data-title="插入图片">
            <Icon type="picture" theme="filled" />
          </button>
        </Upload>
      )
    }
  ]

  return (
    <Modal
      destroyOnClose
      // title="新增消息"
      title={(
        <div>
          <span>新增消息</span>
          <Button type='primary' style={{float: 'right', marginRight: 16}} onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      width={1000}
    >
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 19 }} label="标题">
        {form.getFieldDecorator('title', {
          rules: [{ required: true, message: '请输入标题！' }],
        })(
          <Input style={{width: '100%'}} placeholder="请输入标题" />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 19 }} label="内容">
        {form.getFieldDecorator('text', {
          rules: [{ required: true, message: '请输入内容！' }],
        })(<Input.TextArea rows={3} placeholder="请输入内容" />)}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 19 }} label="富文本">
        {form.getFieldDecorator('content', {
          validateTrigger: 'onBlur',
          rules: [{
            required: true,
            validator: (_, value, callback) => {
              if (value.isEmpty()) {
                callback('请输入富文本内容')
              } else {
                callback()
              }
            }
          }],
        })(
          <BraftEditor
            className="my-editor"
            // controls={controls}
            placeholder="请输入富文本内容"
            style={{border: '1px solid #d9d9d9', borderRadius: 4}}
            extendControls={extendControls}
            excludeControls={['media', 'emoji']}
            stripPastedStyles
          />
        )}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      fieldsValue.content = fieldsValue.content.toHTML();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isInventoryClassificationNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  const uploadHandler = (param) => {

    if (!param.file) {
      return false
    }

    let formData = new FormData();
    // if (param.file.size / 1024 > 1024) {
    //   Modal.error({
    //     title: '图片大小不得大于1MB',
    //     content: '请重新选择图片',
    //     okText: '确认',
    //     cancelText: '取消',
    //   })
    // }

    formData.append('image', param.file);

    const response = upload(formData);
    response.then(result => {
      console.log('result: ', result);
      const real = result.msg;
      form.setFieldsValue({
        content: ContentUtils.insertMedias(form.getFieldsValue().content, [{
          type: 'IMAGE',
          // url: URL.createObjectURL(param.file)
          url: `${imgDomainName}${real}`,
          width: '100%'
        }])
      });
    });

  }

  const extendControls = [
    {
      key: 'antd-uploader',
      type: 'component',
      component: (
        <Upload
          accept="image/*"
          showUploadList={false}
          customRequest={uploadHandler}
        >
          {/* 这里的按钮最好加上type="button"，以避免在表单容器中触发表单提交，用Antd的Button组件则无需如此 */}
          <button type="button" className="control-item button upload-button" data-title="插入图片">
            <Icon type="picture" theme="filled" />
          </button>
        </Upload>
      )
    }
  ]

  return (
    <Modal
      destroyOnClose
      // title="修改消息"
      title={(
        <div>
          <span>修改消息</span>
          <Button type='primary' style={{float: 'right', marginRight: 16}} onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
      width={1000}
    >
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 19 }} label="标题">
        {form.getFieldDecorator('title', {
          rules: [{ required: true, message: '请输入标题！' }],
          initialValue: record.title,
        })(
          <Input style={{width: '100%'}} placeholder="请输入标题" />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 19 }} label="内容">
        {form.getFieldDecorator('text', {
          rules: [{ required: true, message: '请输入内容！' }],
          initialValue: record.text,
        })(<Input.TextArea rows={3} placeholder="请输入内容" />)}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 19 }} label="富文本">
        {form.getFieldDecorator('content', {
          validateTrigger: 'onBlur',
          rules: [{
            required: true,
            validator: (_, value, callback) => {
              if (value.isEmpty()) {
                callback('请输入富文本内容')
              } else {
                callback()
              }
            }
          }],
          initialValue: BraftEditor.createEditorState(record.content)
        })(
          <BraftEditor
            className="my-editor"
            // controls={controls}
            placeholder="请输入富文本内容"
            style={{border: '1px solid #d9d9d9', borderRadius: 4}}
            extendControls={extendControls}
            excludeControls={['media', 'emoji']}
            stripPastedStyles
          />
        )}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ message, loading }) => ({
  message,
  loading: loading.models.message,
}))
@Form.create()
class MessageList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'messageTemplateId',
      width: '8%',
    },
    {
      title: '标题',
      dataIndex: 'title',
      width: '19%',
      render: text => <div>{text}</div>
    },
    {
      title: '内容',
      dataIndex: 'text',
      width: '39%',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      width: '17%',
    },
    {
      title: '操作',
      width: '17%',
      render: (text, record) => (
        <Fragment>
          <Popconfirm
            title="确认全员发送？"
            onConfirm={() => this.onSendAllClick(record)}
            okText="确认"
            cancelText="取消"
          >
            <a href='#'>全员发送</a>
          </Popconfirm>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.messageTemplateId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  onSendAllClick = (record) => {
    console.log('record: ', record);
    const { dispatch } = this.props;
    dispatch({
      type: 'message/sendMessage',
      payload: {
        messageTemplateId: record.messageTemplateId
      },
    });
  }

  recordRemove = messageTemplateIds => {
    console.log('messageTemplateIds: ', messageTemplateIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'message/removeMessage',
      payload: { messageTemplateIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'message/fetchMessageList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'message/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);

    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'message/addMessage',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'message/updateMessage',
      payload: {
        ...fields,
        messageTemplateId: record.messageTemplateId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  render() {
    const {
      message: { messageList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.messageTemplateId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={messageList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="messageTemplateId"
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />
      </div>
    );
  }
}

export default MessageList;
