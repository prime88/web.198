// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchMessageList,
  fetchMessageRecordList,
  addMessage,
  sendMessage,
  updateMessage,
  removeMessage,
  removeMessageRecord,
} from '@/services/message';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'message',

  state: {
    //消息列表
    messageList: {
      rows: [],
      pagination: {},
    },
    //消息发送记录列表
    messageRecordList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchMessageList({ payload, callback }, { call, put }) {
      const response = yield call(fetchMessageList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveMessageList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchMessageRecordList({ payload, callback }, { call, put }) {
      const response = yield call(fetchMessageRecordList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveMessageRecordList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addMessage({ payload, callback }, { call, put }) {
      const response = yield call(addMessage, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMessageList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *sendMessage({ payload, callback }, { call, put }) {
      const response = yield call(sendMessage, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeMessage({ payload, callback }, { call, put }) {
      const response = yield call(removeMessage, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchMessageList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeMessageRecord({ payload, callback }, { call, put }) {
      const response = yield call(removeMessageRecord, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchMessageRecordList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateMessage({ payload, callback }, { call, put }) {
      const response = yield call(updateMessage, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchMessageList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveMessageList(state, action) {
      return {
        ...state,
        messageList: action.payload,
      };
    },
    saveMessageRecordList(state, action) {
      return {
        ...state,
        messageRecordList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       if (location.pathname === '/app-management/vip-management/electronic-wallet') {
  //         dispatch({
  //           type: 'fetchElectronicWalletList',
  //         })
  //       }
  //       if (location.pathname === '/app-management/vip-management/ele-wall-recharge-record') {
  //         dispatch({
  //           type: 'fetchEleWallRechargeRecordList',
  //         })
  //       }
  //       if (location.pathname === '/app-management/vip-management/vip-level-settings') {
  //         dispatch({
  //           type: 'fetchVipLevelList',
  //         })
  //       }
  //     });
  //   },
  // },
};
