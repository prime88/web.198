import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, InputNumber, Row, Col } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/comment';

import styles from './UserComment.less';
import {imgDomainName} from "../../../constants";

const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ comment, loading }) => ({
  comment,
  loading: loading.models.comment,
}))
@Form.create()
class UserComment extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    expandedRowKeys: [],
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    {
      title: '酒名称',
      dataIndex: 'goodsName',
    },
    // {
    //   title: '图片',
    //   dataIndex: 'price',
    // },
    {
      title: '所在门店',
      dataIndex: 'storeName',
    },
    {
      title: '用户昵称',
      dataIndex: 'nickName',
    },
    {
      title: '注册手机号',
      dataIndex: 'phone',
    },
    {
      title: '评分',
      dataIndex: 'score',
    },
    {
      title: '评论',
      dataIndex: 'content',
    },
    {
      title: '评论时间',
      dataIndex: 'createTime',
    },
    {
      title: '操作',
      width: 70,
      render: (text, record) => (
        <Fragment>
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.evaluateId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = evaluateIds => {
    console.log('evaluateIds: ', evaluateIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'comment/removeUserComment',
      payload: { evaluateIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'comment/fetchUserCommentList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'comment/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  onExpandedRowsChange = (expandedRows) => {
    console.log('expandedRows: ', expandedRows);
    this.setState({ expandedRowKeys: expandedRows })
  }

  render() {
    const {
      comment: { userCommentList },
      loading,
    } = this.props;
    const { selectedRows, expandedRowKeys } = this.state;

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.evaluateId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              // selectedRows={selectedRows}
              loading={loading}
              data={userCommentList}
              columns={this.columns}
              // onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="evaluateId"
              expandedRowRender={record => (
                record.imageUrlList ?
                <Row gutter={16}>
                  {record.imageUrlList.split(',').map(item => <Col span={4}><img style={{width: 175, height: 'auto'}} src={`${imgDomainName}${item}`} /></Col>)}
                </Row> : <span>此评论无图片</span>
              )}
              expandedRowKeys={expandedRowKeys}
              onExpandedRowsChange={this.onExpandedRowsChange}
              onExpand={() => {}}
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default UserComment;
