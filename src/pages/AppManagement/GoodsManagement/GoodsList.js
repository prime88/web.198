import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Cascader,
  Popconfirm,
  Upload,
  Tag,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import ImagePreviewModal from '@/components/ImagePreviewModal';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { isInventoryNameRepeated } from '@/services/goods';
import GoodsListAdd from './GoodsListAdd'
import styles from './GoodsList.less';
import {apiDomainName, imgDomainName} from "../../../constants";
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
import { ContentUtils } from 'braft-utils'
import { upload } from '@/services/common-api';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {

  okHandle = () => {
    const {
      handleAdd,
    } = this.props;
    const { selectedRows } = this.goodsListAddRef.state;
    console.log('selectedRows: ', selectedRows);
    const stockIds = selectedRows.map(item => item.stockId).join(',');
    handleAdd(stockIds);
  };

  render() {
    const {
      modalVisible,
      handleModalVisible,
    } = this.props;

    const saveFormRef = (formRef) => {
      this.goodsListAddRef = formRef;
    }

    return (
      <Modal
        width={'95%'}
        destroyOnClose
        // title="选择商品"
        title={(
          <div>
            <span>选择商品</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => handleModalVisible()}
      >
        <GoodsListAdd wrappedComponentRef={saveFormRef} />
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {

  state = {
    loading: false,
    imageUrl: '',
    fileList: [],
  };

  okHandle = () => {
    const {
      form,
      handleUpdate,
    } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);
      fieldsValue.imageUrl = fieldsValue.imageUrl.length >0 ? fieldsValue.imageUrl.map(item => item.response.msg).join(','): '';
      fieldsValue.details = fieldsValue.details.toHTML();
      if (fieldsValue.labelIds && fieldsValue.labelIds.length > 0) {
        fieldsValue.labelIds = fieldsValue.labelIds.join(',');
      }
      form.resetFields();
      this.setState({ imageUrl: '' });
      handleUpdate(fieldsValue);
    });
  };

  loadData = (selectedOptions) => {
    const {
      addModalMeasurementUnitOptions,
      loadNewOptions,
    } = this.props;
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: targetOption.value });
    response.then(result => {
      console.log('result: ', result);
      targetOption.loading = false;
      targetOption.children = result.data.rows.map(item => (
        {
          value: item.unitsId,
          label: item.name,
        }
      ));
      const newOptions = [...addModalMeasurementUnitOptions];
      loadNewOptions(newOptions);
    });
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList.filter((item, index) => index <= 5);
  };

  render() {
    const {
      updateModalVisible,
      form,
      handleUpdateModalVisible,
      inventoryClassificationList,
      addModalMeasurementUnitOptions,
      record,
      brandList,
      tagList,
    } = this.props;

    //由于是异步加载数据，所以initialValue后面的值是显示不出来的，在options中模拟出来才可显示
    if (addModalMeasurementUnitOptions.length > 0){
      const displayTarget = addModalMeasurementUnitOptions.find(item => item.value === record.unitsClassifyId);
      if (displayTarget) {
        displayTarget.children = [{ value: record.unitsId, label: record.units }];
      }
    }

    const initialImageUrlValue = record.imageUrl ? (
      record.imageUrl.split(',').map(item => ({ url: `${imgDomainName}${item}`, response: { msg: `${item}` }, uid: item }))
    ) : [];

    const uploadHandler = (param) => {

      if (!param.file) {
        return false
      }

      let formData = new FormData();
      // if (param.file.size / 1024 > 1024) {
      //   Modal.error({
      //     title: '图片大小不得大于1MB',
      //     content: '请重新选择图片',
      //     okText: '确认',
      //     cancelText: '取消',
      //   })
      // }

      formData.append('image', param.file);

      const response = upload(formData);
      response.then(result => {
        console.log('result: ', result);
        const real = result.msg;
        form.setFieldsValue({
          details: ContentUtils.insertMedias(form.getFieldsValue().details, [{
            type: 'IMAGE',
            // url: URL.createObjectURL(param.file)
            url: `${imgDomainName}${real}`,
            width: '100%'
          }])
        });
      });
    }

    const extendControls = [
      {
        key: 'antd-uploader',
        type: 'component',
        component: (
          <Upload
            accept="image/*"
            showUploadList={false}
            customRequest={uploadHandler}
          >
            {/* 这里的按钮最好加上type="button"，以避免在表单容器中触发表单提交，用Antd的Button组件则无需如此 */}
            <button type="button" className="control-item button upload-button" data-title="插入图片">
              <Icon type="picture" theme="filled" />
            </button>
          </Upload>
        )
      }
    ]

    return (
      <Modal
        destroyOnClose
        // title="修改商品"
        title={(
          <div>
            <span>修改商品</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ imageUrl: '' });
        }}
        width={1200}
      >
        <Row>
          <Col span={10}>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="图片">
              {form.getFieldDecorator('imageUrl', {
                initialValue: initialImageUrlValue,
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{ required: true, message: '请选择商品图片！' }],
              })(
                <Upload
                  name="image"
                  listType="picture"
                  action={`${apiDomainName}/image/uploadList`}
                  multiple
                  className={styles["upload-list-inline"]}
                >
                  {form.getFieldsValue().imageUrl.length > 5 ? null : (
                    <Button>
                      <Icon type="upload" /> 点击上传
                    </Button>
                  )}
                </Upload>
              )}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
              {form.getFieldDecorator('name', {
                rules: [
                  { required: true, message: '请输入名称！' },
                ],
                initialValue: record.name,
              })(<Input placeholder="请输入名称" />)}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="简称">
              {form.getFieldDecorator('shorthand', {
                rules: [
                  { required: true, message: '请输入简称！' },
                ],
                initialValue: record.shorthand,
              })(<Input placeholder="请输入简称" />)}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="计量单位">
              {form.getFieldDecorator('unitsId', {
                rules: [
                  { required: true, message: '请选择计量单位！' },
                  // { validator: checkUnitsId }
                ],
                initialValue: [record.unitsClassifyId, record.unitsId],
              })(
                <Cascader
                  options={addModalMeasurementUnitOptions}
                  loadData={this.loadData}
                  changeOnSelect
                  placeholder="请选择计量单位"
                  style={{width: '100%'}}
                />
              )}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="物品分类">
              {form.getFieldDecorator('stockClassifyId', {
                rules: [{ required: true, message: '请选择物品分类！' }],
                initialValue: record.stockClassifyId,
              })(
                <Select style={{width: '100%'}} placeholder="请选择物品分类" >
                  { inventoryClassificationList.rows.map(item => (
                    <Select.Option key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售参考价">
              {form.getFieldDecorator('referencePrice', {
                rules: [{ required: true, message: '请输入零售参考价！' }],
                initialValue: record.referencePrice
              })(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入零售参考价" min={0} />
              )}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="品牌">
              {form.getFieldDecorator('brandId', {
                rules: [{ required: true, message: '请选择品牌！' }],
                initialValue: record.brandId
              })(
                <Select style={{width: '100%'}} placeholder="请选择品牌" >
                  { brandList.rows.map(item => (
                    <Select.Option key={item.brandId} value={item.brandId}>{item.name}</Select.Option>
                  )) }
                </Select>
              )}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="规格型号">
              {form.getFieldDecorator('specification', {
                rules: [{ required: true, message: '请输入规格型号！' }],
                initialValue: record.specification
              })(<Input placeholder="请输入规格型号" />)}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签">
              {form.getFieldDecorator('labelIds', {
                // rules: [{ required: true, message: '请选择门店标签！' }],
                initialValue: record.labelIds ? record.labelIds.split(',') : [],
              })(
                <Select mode="multiple" style={{width: '100%'}} placeholder='请选择门店标签'>
                  {tagList.rows.map(item => (
                    <Select.Option key={item.storeLabelId}>
                      {item.name}
                      {/*<Tag color={item.color}>{item.name}</Tag>*/}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
              {form.getFieldDecorator('remark', {
                initialValue: record.remark
              })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col span={14}>
            <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 20 }} label="商品详情">
              {form.getFieldDecorator('details', {
                validateTrigger: 'onBlur',
                rules: [{
                  required: true,
                  validator: (_, value, callback) => {
                    if (value.isEmpty()) {
                      callback('请输入商品详情')
                    } else {
                      callback()
                    }
                  }
                }],
                initialValue: BraftEditor.createEditorState(record.details)
              })(
                <BraftEditor
                  className="my-editor"
                  // controls={controls}
                  placeholder="请输入商品详情"
                  style={{border: '1px solid #d9d9d9', borderRadius: 4, height: 651}}
                  extendControls={extendControls}
                  excludeControls={['media', 'emoji']}
                  stripPastedStyles
                />
              )}
            </FormItem>
          </Col>
        </Row>

      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ goods, loading }) => ({
  goods,
  loading: loading.models.goods,
}))
@Form.create()
class GoodsList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    record: {},
    previewVisible: false,
    imageList: [],
  };

  onImageClick = (images) => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true })
  }

  columns = [
    // {
    //   title: '编号',
    //   dataIndex: 'goodsId',
    //   fixed: 'left',
    //   width: 70
    // },
    {
      title: '图片',
      dataIndex: 'imageUrl',
      render: (text) => {
        const imageUrlArray = text ? text.split(',') : [];
        return imageUrlArray.length > 0 ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text.split(',')[0]}`} /> : '';
      },
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '存货简称',
      dataIndex: 'shorthand',
    },
    {
      title: '类目',
      dataIndex: 'stockClassify',
    },
    {
      title: '计量单位',
      dataIndex: 'units',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    // {
    //   title: '总库存',
    //   dataIndex: 'totalStock',
    // },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '标签',
      dataIndex: 'label',
      render: (text, record) => text && text.split(',').map((item, index) => <Tag color={record.colors && record.colors.split(',')[index]}>{item}</Tag>)
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 140,
      fixed: 'right',
      render: (text, record) => {
        return (
          <Fragment>
            <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
            <Divider type="vertical" />
            <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
            <Divider type="vertical" />
            <Popconfirm
              title="确认删除？"
              onConfirm={() => this.recordRemove(record.goodsId)}
              okText="确认"
              cancelText="取消"
            >
              <a href="#">删除</a>
            </Popconfirm>
          </Fragment>
        )
      },
    },
  ];

  changeStatus = record => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/changeGoodsStatus',
      payload: { goodsId: record.goodsId, isEnable: !record.isEnable },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      }
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });
  };

  batchChangeStatus = isEnable => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey, selectedRows } = this.state;
    dispatch({
      type: 'goods/changeGoodsStatus',
      payload: { goodsId: selectedRows.map(item => item.goodsId).join(','), isEnable },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      }
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });
  };

  recordRemove = goodsIds => {
    console.log('goodsIds: ', goodsIds);
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/removeGoods',
      payload: { goodsIds },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    //存货分类列表
    dispatch({
      type: 'goods/fetchInventoryClassificationList',
    });

    //商品列表
    dispatch({
      type: 'goods/fetchGoodsList',
    });

    //获取品牌列表
    dispatch({
      type: 'goods/fetchBrandList',
    });

    //获取计量单位列表
    dispatch({
      type: 'goods/fetchMeasurementUnitClassificationList',
      callback: (data) => {
        this.setState({ addModalMeasurementUnitOptions: data.rows.map(item => (
            {
              value: item.unitsClassifyId,
              label: item.name,
              isLeaf: false,
            }
          ))
        })
      }
    });

    //门店标签列表
    dispatch({
      type: 'goods/fetchTagList',
      payload: {
        typeCode: '2'
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'goods/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'goods/fetchGoodsList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'goods/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'goods/fetchGoodsList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        },
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = stockIds => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/addGoods',
      payload: {
        stockIds
      },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      }
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch, form } = this.props;
    const { record, selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1]
    console.log('fields: ', fields);
    dispatch({
      type: 'goods/updateGoods',
      payload: {
        ...fields,
        goodsId: record.goodsId,
      },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      }
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间下限">
              {getFieldDecorator('lowerPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间下限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间上限">
              {getFieldDecorator('upperPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间上限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;

    //商品列表
    dispatch({
      type: 'goods/fetchGoodsList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        ...form.getFieldsValue()
      }
    });

    this.setState({ selectedMenuItemKey: key })
  }

  loadNewOptions = (newOptions) => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  }

  expandedRowRender = record => {
    const imageUrlArray = record.imageUrl ? record.imageUrl.split(',') : [];
    let result = null;
    if (imageUrlArray.length > 0) {
      console.log('imageUrlArray: ', imageUrlArray);
      result = imageUrlArray.map(item => (
        <Col span={4}>
          <img style={{width: '100%'}} alt='' src={`${imgDomainName}${item}`} />
        </Col>
      ))
    }
    return (
      <Row gutter={16} >
        {result}
      </Row>
    )
  }

  onSelectAll = (selected, selectedRows, changeRows, dataSource) => {
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    console.log('changeRows: ', changeRows);
    console.log('dataSource: ', dataSource);
    console.log('dataSource: ', dataSource);
    let newSelectedRows = [];
    if (selected) {
      newSelectedRows = dataSource;
    }
    this.setState({ selectedRows: newSelectedRows })
  }

  render() {
    const {
      goods: { inventoryClassificationList, goodsList, brandList, tagList },
      loading,
    } = this.props;
    const {
      selectedRows,
      modalVisible,
      updateModalVisible,
      addModalMeasurementUnitOptions,
      record,
      previewVisible,
      imageList,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <div className={styles.tableListOperator}>
              <Row gutter={16}>
                <Col span={10} push={4}>
                  <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                    上架到APP
                  </Button>
                  {selectedRows.length > 0 && (
                    <span>
                      <Button onClick={() => this.batchChangeStatus('true')}>批量开启</Button>
                      <Button onClick={() => this.batchChangeStatus('false')}>批量关闭</Button>
                      <Popconfirm
                        title="确认批量删除？"
                        onConfirm={() =>
                          this.recordRemove(selectedRows.map(item => item.goodsId).join(','))
                        }
                        okText="确认"
                        cancelText="取消"
                      >
                        <Button>批量删除</Button>
                      </Popconfirm>
                    </span>
                  )}
                </Col>
              </Row>
            </div>
            <Row gutter={16}>
              <Col span={4}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                  { inventoryClassificationList.rows.map(item => (
                    <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Menu.Item>
                  )) }
                </Menu>
              </Col>
              <Col span={20}>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={goodsList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='goodsId'
                  size='small'
                  scroll={{x: 1800}}
                  // expandedRowRender={this.expandedRowRender}
                  // onExpand={() => {}}
                  onSelectAll={this.onSelectAll}
                  footer={() => `总计：${goodsList.rows.length}`}
                />
              </Col>
            </Row>
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
        />
        <UpdateForm
          {...updateMethods}
          inventoryClassificationList={inventoryClassificationList}
          brandList={brandList}
          updateModalVisible={updateModalVisible}
          addModalMeasurementUnitOptions={addModalMeasurementUnitOptions}
          // values={stepFormValues}
          record={record}
          loadNewOptions={this.loadNewOptions}
          tagList={tagList}
        />
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={120}
          imgHeight={120}
        />
      </div>
    );
  }
}

export default GoodsList;
