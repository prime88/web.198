import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Cascader,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { isInventoryNameRepeated } from '@/services/goods';
import { imgDomainName } from "../../../constants";
import styles from './GoodsList.less';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const {
    modalVisible,
    form,
    handleAdd,
    handleModalVisible,
    inventoryClassificationList,
    addModalMeasurementUnitOptions,
    selectedMenuItemKey,
    loadNewOptions,
    brandList,
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  const loadData = (selectedOptions) => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: targetOption.value });
    response.then(result => {
      console.log('result: ', result);
      targetOption.loading = false;
      targetOption.children = result.data.rows.map(item => (
        {
          value: item.unitsId,
          label: item.name,
        }
      ));
      const newOptions = [...addModalMeasurementUnitOptions];
      loadNewOptions(newOptions);
    });
  }

  const checkUnitsId = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value.length === 2) {
      callback();
      return;
    }
    callback('请选择完整的计量单位!');
  }

  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isInventoryNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="选择商品"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >

    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const {
    updateModalVisible,
    form,
    handleUpdate,
    handleUpdateModalVisible,
    inventoryClassificationList,
    addModalMeasurementUnitOptions,
    loadNewOptions,
    record,
    brandList,
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  const loadData = (selectedOptions) => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: targetOption.value });
    response.then(result => {
      console.log('result: ', result);
      targetOption.loading = false;
      targetOption.children = result.data.rows.map(item => (
        {
          value: item.unitsId,
          label: item.name,
        }
      ));
      const newOptions = [...addModalMeasurementUnitOptions];
      loadNewOptions(newOptions);
    });
  }

  //由于是异步加载数据，所以initialValue后面的值是显示不出来的，在options中模拟出来才可显示
  if (addModalMeasurementUnitOptions.length > 0){
    const displayTarget = addModalMeasurementUnitOptions.find(item => item.value === record.unitsClassifyId);
    if (displayTarget) {
      displayTarget.children = [{ value: record.unitsId, label: record.units }];
    }
  }

  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isInventoryNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改存货"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="计量单位">
        {form.getFieldDecorator('unitsId', {
          rules: [
            { required: true, message: '请选择计量单位！' },
          ],
          initialValue: [record.unitsClassifyId, record.unitsId],
        })(
          <Cascader
            options={addModalMeasurementUnitOptions}
            loadData={loadData}
            placeholder="请选择计量单位"
            style={{width: '100%'}}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="物品分类">
        {form.getFieldDecorator('stockClassifyId', {
          rules: [{ required: true, message: '请选择物品分类！' }],
          initialValue: record.stockClassifyId,
        })(
          <Select style={{width: '100%'}} placeholder="请选择物品分类" >
            { inventoryClassificationList.rows.map(item => (
              <Select.Option key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Select.Option>
            )) }
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="参考成本">
        {form.getFieldDecorator('referenceCost', {
          rules: [{ required: true, message: '请输入参考成本！' }],
          initialValue: record.referenceCost,
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入参考成本" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售参考价">
        {form.getFieldDecorator('referencePrice', {
          rules: [{ required: true, message: '请输入零售参考价！' }],
          initialValue: record.referencePrice,
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入零售参考价" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="品牌">
        {form.getFieldDecorator('brandId', {
          rules: [{ required: true, message: '请选择品牌！' }],
          initialValue: record.brandId
        })(
          <Select style={{width: '100%'}} placeholder="请选择品牌" >
            { brandList.rows.map(item => (
              <Select.Option key={item.brandId} value={item.brandId}>{item.name}</Select.Option>
            )) }
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ goods, brand, measurementUnit, loading }) => ({
  goods,
  brand,
  measurementUnit,
  loading: loading.models.goods,
}))
@Form.create()
class GoodsList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    record: {},
  };

  columns = [
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 150,
      fixed: 'left',
    },
    {
      title: '名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '物品分类',
      dataIndex: 'stockClassify',
      width: 100,
      fixed: 'left',
    },
    {
      title: '计量单位',
      dataIndex: 'units',
    },
    {
      title: '最后一次采购价 (￥)',
      dataIndex: 'referenceCost',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  changeStatus = record => {
    const { dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/changeInventory',
      payload: { stockId: record.stockId, isEnable: !record.isEnable },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });
  };

  recordRemove = stockIds => {
    console.log('stockIds: ', stockIds);
    const { dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/removeInventory',
      payload: { stockIds },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'goods/fetchInventoryClassificationList',
    });

    //获取存货列表
    dispatch({
      type: 'goods/fetchInventoryList',
    });

    //获取品牌列表
    dispatch({
      type: 'brand/fetchBrandList',
    });

    //获取计量单位列表
    dispatch({
      type: 'measurementUnit/fetchMeasurementUnitClassificationList',
      callback: (data) => {
        this.setState({ addModalMeasurementUnitOptions: data.rows.map(item => (
            {
              value: item.unitsClassifyId,
              label: item.name,
              isLeaf: false,
            }
          ))
        })
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'goods/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'goods/fetchInventoryList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'goods/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'goods/fetchInventoryList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        },
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1];
    dispatch({
      type: 'goods/addInventory',
      payload: fields,
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1]
    dispatch({
      type: 'goods/updateInventory',
      payload: {
        ...fields,
        stockId: record.stockId,
      },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间下限">
              {getFieldDecorator('lowerPriceLimit')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间上限">
              {getFieldDecorator('upperPriceLimit')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'goods/fetchInventoryList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        ...form.getFieldsValue()
      }
    });

    this.setState({ selectedMenuItemKey: key })
  }

  loadNewOptions = (newOptions) => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.stockId === record.stockId) {
          newSelectedRows = newSelectedRows.filter(item => item.stockId !== record.stockId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  onSelectAll = (selected, selectedRows, changeRows, dataSource) => {
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    console.log('changeRows: ', changeRows);
    console.log('dataSource: ', dataSource);
    // const warehouseStockChangeIds = dataSource.filter(item => item.number !== 0).map(item => item.warehouseStockChangeId)
    //去掉库存为0的库存
    // const newSelectedRows = dataSource.filter(item => item.number !== 0)
    console.log('dataSource: ', dataSource);
    let newSelectedRows = [];
    if (selected) {
      newSelectedRows = dataSource;
    }
    this.setState({ selectedRows: newSelectedRows })
  }

  render() {
    const {
      goods: { inventoryClassificationList, inventoryList },
      // brand: { brandList },
      // measurementUnit: { measurementUnitClassificationList },
      loading,
    } = this.props;
    const {
      selectedRows,
      modalVisible,
      updateModalVisible,
      stepFormValues,
      selectedMenuItemKey,
      addModalMeasurementUnitOptions,
      record,
    } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <div>
        <div>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <Row gutter={16}>
              <Col span={3}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                  { inventoryClassificationList.rows.map(item => (
                    <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Menu.Item>
                  )) }
                </Menu>
              </Col>
              <Col span={21}>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={inventoryList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='stockId'
                  size='small'
                  scroll={{x: 1500}}
                  onRow={record => {
                    return {
                      onClick: () => this.onRowClick(record)
                    }
                  }}
                  onSelectAll={this.onSelectAll}
                />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

export default GoodsList;
