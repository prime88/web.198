import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Cascader,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { isInventoryNameRepeated } from '@/services/warehouse';

import styles from './InventoryQuery.less';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const {
    modalVisible,
    form,
    handleAdd,
    handleModalVisible,
    inventoryClassificationList,
    addModalMeasurementUnitOptions,
    selectedMenuItemKey,
    loadNewOptions,
    brandList,
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  const loadData = (selectedOptions) => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: targetOption.value });
    response.then(result => {
      console.log('result: ', result);
      targetOption.loading = false;
      targetOption.children = result.data.rows.map(item => (
        {
          value: item.unitsId,
          label: item.name,
        }
      ));
      const newOptions = [...addModalMeasurementUnitOptions];
      loadNewOptions(newOptions);
    });
  }

  const checkUnitsId = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value.length === 2) {
      callback();
      return;
    }
    callback('请选择完整的计量单位!');
  }

  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isInventoryNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增存货"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="计量单位">
        {form.getFieldDecorator('unitsId', {
          rules: [
            { required: true, message: '请选择计量单位！' },
            { validator: checkUnitsId }
          ],
        })(
          <Cascader
            options={addModalMeasurementUnitOptions}
            loadData={loadData}
            changeOnSelect
            placeholder="请选择计量单位"
            style={{width: '100%'}}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="物品分类">
        {form.getFieldDecorator('stockClassifyId', {
          rules: [{ required: true, message: '请选择物品分类！' }],
          initialValue: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
        })(
          <Select style={{width: '100%'}} placeholder="请选择物品分类" >
            { inventoryClassificationList.rows.map(item => (
              <Select.Option key={item.stockClassifyId}>{item.name}</Select.Option>
            )) }
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="参考成本">
        {form.getFieldDecorator('referenceCost', {
          rules: [{ required: true, message: '请输入参考成本！' }],
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入参考成本" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售参考价">
        {form.getFieldDecorator('referencePrice', {
          rules: [{ required: true, message: '请输入零售参考价！' }],
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入零售参考价" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="品牌">
        {form.getFieldDecorator('brandId', {
          rules: [{ required: true, message: '请选择品牌！' }],
        })(
          <Select style={{width: '100%'}} placeholder="请选择品牌" >
            { brandList.rows.map(item => (
              <Select.Option key={item.brandId}>{item.name}</Select.Option>
            )) }
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const {
    updateModalVisible,
    form,
    handleUpdate,
    handleUpdateModalVisible,
    inventoryClassificationList,
    addModalMeasurementUnitOptions,
    loadNewOptions,
    record,
    brandList,
  } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  const loadData = (selectedOptions) => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: targetOption.value });
    response.then(result => {
      console.log('result: ', result);
      targetOption.loading = false;
      targetOption.children = result.data.rows.map(item => (
        {
          value: item.unitsId,
          label: item.name,
        }
      ));
      const newOptions = [...addModalMeasurementUnitOptions];
      loadNewOptions(newOptions);
    });
  }

  //由于是异步加载数据，所以initialValue后面的值是显示不出来的，在options中模拟出来才可显示
  if (addModalMeasurementUnitOptions.length > 0){
    const displayTarget = addModalMeasurementUnitOptions.find(item => item.value === record.unitsClassifyId);
    if (displayTarget) {
      displayTarget.children = [{ value: record.unitsId, label: record.units }];
    }
  }

  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isInventoryNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改存货"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="计量单位">
        {form.getFieldDecorator('unitsId', {
          rules: [
            { required: true, message: '请选择计量单位！' },
          ],
          initialValue: [record.unitsClassifyId, record.unitsId],
        })(
          <Cascader
            options={addModalMeasurementUnitOptions}
            loadData={loadData}
            placeholder="请选择计量单位"
            style={{width: '100%'}}
          />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="物品分类">
        {form.getFieldDecorator('stockClassifyId', {
          rules: [{ required: true, message: '请选择物品分类！' }],
          initialValue: record.stockClassifyId,
        })(
          <Select style={{width: '100%'}} placeholder="请选择物品分类" >
            { inventoryClassificationList.rows.map(item => (
              <Select.Option key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Select.Option>
            )) }
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="参考成本">
        {form.getFieldDecorator('referenceCost', {
          rules: [{ required: true, message: '请输入参考成本！' }],
          initialValue: record.referenceCost,
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入参考成本" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="零售参考价">
        {form.getFieldDecorator('referencePrice', {
          rules: [{ required: true, message: '请输入零售参考价！' }],
          initialValue: record.referencePrice,
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入零售参考价" min={0} />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="品牌">
        {form.getFieldDecorator('brandId', {
          rules: [{ required: true, message: '请选择品牌！' }],
          initialValue: record.brandId
        })(
          <Select style={{width: '100%'}} placeholder="请选择品牌" >
            { brandList.rows.map(item => (
              <Select.Option key={item.brandId} value={item.brandId}>{item.name}</Select.Option>
            )) }
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ warehouse, brand, measurementUnit, loading }) => ({
  warehouse,
  brand,
  measurementUnit,
  loading: loading.models.warehouse,
}))
@Form.create()
class InventoryQuery extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'stockId',
    },
    {
      title: '存货编号',
      dataIndex: 'name',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '存货简称',
      dataIndex: 'name',
    },
    {
      title: '类目',
      dataIndex: 'units',
    },
    {
      title: '品牌',
      dataIndex: 'stockClassify',
    },
    {
      title: '门店',
      dataIndex: 'referenceCost',
    },
    {
      title: '库存',
      dataIndex: 'referencePrice',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'brand',
    },
  ];

  changeStatus = record => {
    const { dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'warehouse/changeInventory',
      payload: { stockId: record.stockId, isEnable: !record.isEnable },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });
  };

  recordRemove = stockIds => {
    console.log('stockIds: ', stockIds);
    const { dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'warehouse/removeInventory',
      payload: { stockIds },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'warehouse/fetchInventoryClassificationList',
    });

    //获取品牌列表
    dispatch({
      type: 'brand/fetchBrandList',
    });

    //获取存货列表
    dispatch({
      type: 'warehouse/fetchInventoryList',
    });

    //获取计量单位列表
    dispatch({
      type: 'measurementUnit/fetchMeasurementUnitClassificationList',
      callback: (data) => {
        this.setState({ addModalMeasurementUnitOptions: data.rows.map(item => (
            {
              value: item.unitsClassifyId,
              label: item.name,
              isLeaf: false,
            }
          ))
        })
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'warehouse/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'warehouse/fetchInventoryList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'warehouse/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'warehouse/fetchInventoryList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        },
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1];
    dispatch({
      type: 'warehouse/addInventory',
      payload: fields,
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1]
    dispatch({
      type: 'warehouse/updateInventory',
      payload: {
        ...fields,
        stockId: record.stockId,
      },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间下限">
              {getFieldDecorator('name1')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间上限">
              {getFieldDecorator('name2')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="门店">
              {getFieldDecorator('name3')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={24} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'warehouse/fetchInventoryList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key
      }
    });

    this.setState({ selectedMenuItemKey: key })
  }

  loadNewOptions = (newOptions) => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  }

  render() {
    const {
      // warehouse: { inventoryClassificationList, inventoryList },
      // brand: { brandList },
      // measurementUnit: { measurementUnitClassificationList },
      loading,
    } = this.props;
    const {
      selectedRows,
      modalVisible,
      updateModalVisible,
      stepFormValues,
      selectedMenuItemKey,
      addModalMeasurementUnitOptions,
      record,
    } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <Row gutter={16}>
              <Col span={3}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                  {/*{ inventoryClassificationList.rows.map(item => (*/}
                    {/*<Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Menu.Item>*/}
                  {/*)) }*/}
                </Menu>
              </Col>
              <Col span={21}>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={[]}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='stockId'
                />
              </Col>
            </Row>
          </div>
        </Card>
        {/*<CreateForm*/}
          {/*{...parentMethods}*/}
          {/*modalVisible={modalVisible}*/}
          {/*inventoryClassificationList={inventoryClassificationList}*/}
          {/*brandList={brandList}*/}
          {/*selectedMenuItemKey={selectedMenuItemKey}*/}
          {/*addModalMeasurementUnitOptions={addModalMeasurementUnitOptions}*/}
          {/*loadNewOptions={this.loadNewOptions}*/}
        {/*/>*/}
        {/*<UpdateForm*/}
          {/*{...updateMethods}*/}
          {/*inventoryClassificationList={inventoryClassificationList}*/}
          {/*brandList={brandList}*/}
          {/*updateModalVisible={updateModalVisible}*/}
          {/*addModalMeasurementUnitOptions={addModalMeasurementUnitOptions}*/}
          {/*values={stepFormValues}*/}
          {/*record={record}*/}
          {/*loadNewOptions={this.loadNewOptions}*/}
        {/*/>*/}
      </div>
    );
  }
}

export default InventoryQuery;
