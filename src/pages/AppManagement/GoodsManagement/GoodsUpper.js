import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Cascader,
  Popconfirm,
  Alert,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { isInventoryNameRepeated } from '@/services/goods';
import styles from './GoodsUpper.less';
import GoodsUpperAdd from './GoodsUpperAdd'
import {imgDomainName} from "../../../constants";
import ImagePreviewModal from '@/components/ImagePreviewModal';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  let goodsListAddRef;

  const saveFormRef = (formRef) => {
    goodsListAddRef = formRef;
  }

  const {
    modalVisible,
    form,
    handleAdd,
    handleModalVisible,
    addModalMeasurementUnitOptions,
    loadNewOptions,
  } = props;

  const okHandle = () => {
    const {
      handleAdd,
    } = props;
    const { selectedRows, selectedStoreRows } = goodsListAddRef.state;

    if (selectedStoreRows.length === 0) {
      message.warning('请选择门店!');
      return;
    }
    else if (selectedRows.length === 0) {
      message.warning('请选择商品!');
      return;
    }

    const storeIds = selectedStoreRows.map(item => item.storeId).join(',');
    const goodsIds = selectedRows.map(item => item.goodsId).join(',');
    handleAdd(storeIds, goodsIds);
  };

  const loadData = (selectedOptions) => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    targetOption.loading = true;

    // load options lazily
    const response = queryMeasurementUnitList({ unitsClassifyId: targetOption.value });
    response.then(result => {
      console.log('result: ', result);
      targetOption.loading = false;
      targetOption.children = result.data.rows.map(item => (
        {
          value: item.unitsId,
          label: item.name,
        }
      ));
      const newOptions = [...addModalMeasurementUnitOptions];
      loadNewOptions(newOptions);
    });
  }

  const checkUnitsId = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }

    if (value.length === 2) {
      callback();
      return;
    }
    callback('请选择完整的计量单位!');
  }

  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isInventoryNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      width={'80%'}
      destroyOnClose
      // title="选择商品"
      title={(
        <div>
          <span>选择商品</span>
          <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{ top: 20 }}
    >
      <GoodsUpperAdd wrappedComponentRef={saveFormRef} />
    </Modal>
  );
});

@connect(({ goods, loading }) => ({
  goods,
  loading: loading.models.goods,
}))
@Form.create()
class UpperForm extends PureComponent {

  state = {
    selectedStoreRows: [],
  }

  storeColumns = [
    {
      title: '编号',
      dataIndex: 'storeId',
    },
    {
      title: '门店名称',
      dataIndex: 'name',
    },
    {
      title: '门店地址',
      dataIndex: 'address',
    },
    {
      title: '门店电话',
      dataIndex: 'phone',
    },
    {
      title: '门店介绍',
      dataIndex: 'introduce',
    },
  ];

  handleSelectStoreRows = rows => {
    this.setState({
      selectedStoreRows: rows,
    });
  };

  okHandle = () => {
    const { handleAdd } = this.props;
    const { selectedStoreRows } = this.state;

    const storeIds = selectedStoreRows.map(item => item.storeId).join(',');
    handleAdd(storeIds);
  };

  render() {
    const { modalVisible, handleModalVisible, loading, goods: { storeList } } = this.props;
    const { selectedStoreRows } = this.state;

    return (
      <Modal
        width={'80%'}
        destroyOnClose
        title="按门店上架"
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => handleModalVisible()}
      >
        <StandardTable
          selectedRows={selectedStoreRows}
          loading={loading}
          data={storeList}
          columns={this.storeColumns}
          onSelectRow={this.handleSelectStoreRows}
          // onChange={this.handleStandardTableChange}
          rowKey="storeId"
          size='small'
          // paginationOfTable={{defaultPageSize: 5, size: 'small'}}
        />
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ goods, brand, measurementUnit, user, loading }) => ({
  goods,
  brand,
  measurementUnit,
  user,
  loading: loading.models.goods,
}))
@Form.create()
class GoodsUpper extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    upperModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    record: {},
    previewVisible: false,
    imageList: [],
    dataPermission: false,
  };

  onImageClick = (images) => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true })
  }

  columns = [
    {
      title: '图片',
      dataIndex: 'imageUrl',
      fixed: 'left',
      width: 100,
      render: (text) => {
        const imageUrlArray = text ? text.split(',') : [];
        return imageUrlArray.length > 0 ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text.split(',')[0]}`} /> : '';
      },
    },
    // {
    //   title: '编号',
    //   dataIndex: 'storeGoodsId',
    //   // fixed: 'left',
    //   // width: 70
    // },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '存货简称',
      dataIndex: 'shorthand',
    },
    {
      title: '类目',
      dataIndex: 'stockClassify',
    },
    {
      title: '计量单位',
      dataIndex: 'units',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '所属门店',
      dataIndex: 'store',
    },
    // {
    //   title: '总库存',
    //   dataIndex: 'totalStock',
    // },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '状态',
      dataIndex: 'status',
    },
    // {
    //   title: '商品详情',
    //   dataIndex: 'details',
    // },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 100,
      fixed: 'right',
      render: (text, record) => {
        let statusBtn = '';
        const statusCode = record.statusCode.toString()
        switch (statusCode) {
          case '1':
            statusBtn = '上架';
            break;
          case '2':
            statusBtn = '下架';
            break;
          case '3':
            statusBtn = '上架';
            break;
        }
        return (
          <Fragment>
            <a onClick={() => this.changeStatus(record.statusCode.toString(), record.storeGoodsId)}>{statusBtn}</a>
            <Divider type="vertical" />
            {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
            {/*<Divider type="vertical" />*/}
            <Popconfirm
              title="确认删除？"
              onConfirm={() => this.recordRemove(record.storeGoodsId)}
              okText="确认"
              cancelText="取消"
            >
              <a href="#">删除</a>
            </Popconfirm>
          </Fragment>
        )
      },
    },
  ];

  changeStatus = (statusCode, storeGoodsIds) => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    let newStatusCode = '';
    // const statusCode = record.statusCode.toString();
    if (statusCode === '1' || statusCode === '3') {
      newStatusCode = '2';
    }
    else{
      newStatusCode = '3';
    }

    dispatch({
      type: 'goods/changeUpperGoodsStatus',
      payload: { storeGoodsIds, statusCode: newStatusCode },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      }
    });
  };

  recordRemove = (storeGoodsIds) => {
    console.log('storeGoodsIds: ', storeGoodsIds);
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/removeUpperGoods',
      payload: { storeGoodsIds },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //存货分类列表
    dispatch({
      type: 'goods/fetchInventoryClassificationList',
    });

    //门店列表
    dispatch({
      type: 'goods/fetchStoreList',
      callback: (data) => {
        //上架商品列表
        dispatch({
          type: 'goods/fetchUpperGoodsList',
          payload: {
            storeId: data.rows[0].storeId
          }
        });
      }
    });

    //数据权限:判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });

    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }

  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'goods/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'goods/fetchUpperGoodsList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      },
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'goods/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'goods/fetchUpperGoodsList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        },
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpperModalVisible = flag => {
    this.setState({
      upperModalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = (storeIds, goodsIds) => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/addUpperGoods',
      payload: {
        storeIds,
        goodsIds
      },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      }
    });

    // message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpper = storeIds => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'goods/batchChangeUpperGoodsStatus',
      payload: { storeIds, statusCode: '2' },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
      }
    });

    // message.success('添加成功');
    this.handleUpperModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedMenuItemKey } = this.state;
    fields.unitsId = fields.unitsId[fields.unitsId.length-1];
    dispatch({
      type: 'goods/updateInventory',
      payload: {
        ...fields,
        stockId: record.stockId,
      },
      selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      goods: { storeList },
      form: { getFieldDecorator },
    } = this.props;
    const { dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="存货编号">
              {getFieldDecorator('stockId')(<Input placeholder="请输入编号" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="门店">
              {getFieldDecorator('storeId', {
                initialValue: storeList.rows.length > 0 ? storeList.rows[0].storeId : ''
              })(
                <Select
                  allowClear
                  style={{width: '100%'}}
                  placeholder='请选择门店'
                  disabled={!dataPermission}
                >
                  { storeList.rows.map(item => <Select.Option key={item.storeId} value={item.storeId}>{item.name}</Select.Option>) }
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'goods/fetchUpperGoodsList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        ...form.getFieldsValue()
      }
    });

    this.setState({ selectedMenuItemKey: key })
  }

  loadNewOptions = (newOptions) => {
    this.setState({ addModalMeasurementUnitOptions: newOptions });
  }

  cleanSelectedKeys = () => {
    this.setState({ selectedRows: [] })
  };

  onSelectAll = (selected, selectedRows, changeRows, dataSource) => {
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    console.log('changeRows: ', changeRows);
    console.log('dataSource: ', dataSource);
    console.log('dataSource: ', dataSource);
    let newSelectedRows = [];
    if (selected) {
      newSelectedRows = dataSource;
    }
    this.setState({ selectedRows: newSelectedRows })
  }

  render() {
    const {
      goods: { inventoryClassificationList, upperGoodsList, storeList },
      loading,
    } = this.props;
    const {
      selectedRows,
      modalVisible,
      upperModalVisible,
      selectedMenuItemKey,
      addModalMeasurementUnitOptions,
      previewVisible,
      imageList,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const upperMethods = {
      handleAdd: this.handleUpper,
      handleModalVisible: this.handleUpperModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>
              {this.renderSimpleForm()}
            </div>
            <div className={styles.tableListOperator}>
              <Row gutter={16}>
                <Col span={24}>
                  <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                    新增
                  </Button>
                  {/*<Button>*/}
                    {/*批量上架*/}
                  {/*</Button>*/}
                  {/*<Button onClick={() => this.handleUpperModalVisible(true)}>*/}
                    {/*按门店上架*/}
                  {/*</Button>*/}
                  {selectedRows.length > 0 && (
                    <span>
                      <Button onClick={() => this.changeStatus('3', selectedRows.map(item => item.storeGoodsId).join(','))} >
                      批量上架
                      </Button>
                      <Button onClick={() => this.changeStatus('2', selectedRows.map(item => item.storeGoodsId).join(','))} >
                      批量下架
                      </Button>
                      <Popconfirm
                        title="确认批量删除？"
                        onConfirm={() =>
                          this.recordRemove(selectedRows.map(item => item.storeGoodsId).join(','))
                        }
                        okText="确认"
                        cancelText="取消"
                      >
                        <Button>批量删除</Button>
                      </Popconfirm>
                      <Alert
                        message={
                          <Fragment>
                            已选择 <a>{selectedRows.length}</a> 项
                            <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>
                              清空
                            </a>
                          </Fragment>
                        }
                        type="info"
                        showIcon
                        style={{display: 'inline-block', paddingTop: 4, paddingBottom: 5}}
                        className={styles.customAlert}
                      />
                    </span>
                  )}
                </Col>
              </Row>
            </div>
            <Row gutter={16}>
              <Col span={4}>
                <Menu
                  mode="inline"
                  openKeys={this.state.openKeys}
                  onOpenChange={this.onOpenChange}
                  defaultSelectedKeys={['-1']}
                  onClick={this.onMenuItemClick}
                >
                  <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                  { inventoryClassificationList.rows.map(item => (
                    <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Menu.Item>
                  )) }
                </Menu>
              </Col>
              <Col span={20}>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={upperGoodsList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey='storeGoodsId'
                  size='small'
                  scroll={{x: 2000}}
                  onSelectAll={this.onSelectAll}
                  footer={() => `总计：${upperGoodsList.rows.length}`}
                />
              </Col>
            </Row>
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
          // brandList={brandList}
          selectedMenuItemKey={selectedMenuItemKey}
          addModalMeasurementUnitOptions={addModalMeasurementUnitOptions}
          loadNewOptions={this.loadNewOptions}
        />
        <UpperForm
          {...upperMethods}
          modalVisible={upperModalVisible}
          storeList={storeList}
        />
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={120}
          imgHeight={120}
        />
      </div>
    );
  }
}

export default GoodsUpper;
