// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchUserCommentList,
  removeUserComment,
} from '@/services/comment';
import { message } from 'antd';

export default {
  namespace: 'comment',

  state: {
    //用户评论列表
    userCommentList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchUserCommentList({ payload, callback }, { call, put }) {
      const response = yield call(fetchUserCommentList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveUserCommentList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *removeUserComment({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(removeUserComment, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchUserCommentList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
  },

  reducers: {
    saveUserCommentList(state, action) {
      return {
        ...state,
        userCommentList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
