// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  fetchUpperGoodsList,
  fetchGoodsList,
  addInventoryClassification,
  addGoods,
  addUpperGoods,
  updateGoods,
  removeGoods,
  removeUpperGoods,
  changeGoodsStatus,
  changeUpperGoodsStatus,
  batchChangeUpperGoodsStatus,
} from '@/services/goods';
import { queryInventoryList } from '@/services/warehouse';
import { queryBrandList } from '@/services/brand';
import {
  queryMeasurementUnitClassificationList,
} from '@/services/measurementUnit';
import { querySupplierList } from '@/services/supplier';
import { queryStoreList } from '@/services/store';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';
import { fetchTagList } from '@/services/tags';

export default {
  namespace: 'goods',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //商品列表
    goodsList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //品牌列表
    brandList: {
      rows: [],
      pagination: {},
    },
    //计量单位分类列表
    measurementUnitClassificationList: {
      rows: [],
      pagination: {},
    },
    //供应商列表
    supplierList: {
      rows: [],
      pagination: {},
    },
    //上架商品列表
    upperGoodsList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //标签列表
    tagList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchGoodsList({ payload, callback }, { call, put }) {
      const response = yield call(fetchGoodsList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveGoodsList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, { ...payload, isEnable: true });
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchBrandList({ payload, callback }, { call, put }) {
      const response = yield call(queryBrandList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBrandList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchMeasurementUnitClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryMeasurementUnitClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveMeasurementUnitClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchSupplierList({ payload }, { call, put }) {
      const response = yield call(querySupplierList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSupplierList',
        payload: response.data,
      });
    },
    *fetchUpperGoodsList({ payload }, { call, put }) {
      const response = yield call(fetchUpperGoodsList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveUpperGoodsList',
        payload: response.data,
      });
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data)
    },
    *fetchTagList({ payload, callback }, { call, put }) {
      const response = yield call(fetchTagList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveTagList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addGoods({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(addGoods, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addUpperGoods({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(addUpperGoods, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchUpperGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeGoods({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(removeGoods, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeUpperGoods({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(removeUpperGoods, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchUpperGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateGoods({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(updateGoods, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeGoodsStatus({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(changeGoodsStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeUpperGoodsStatus({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(changeUpperGoodsStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchUpperGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *batchChangeUpperGoodsStatus({ payload, fetchForm, callback }, { call, put }) {
      const response = yield call(batchChangeUpperGoodsStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchUpperGoodsList',
          payload: {
            ...fetchForm,
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveGoodsList(state, action) {
      return {
        ...state,
        goodsList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    saveBrandList(state, action) {
      return {
        ...state,
        brandList: action.payload,
      };
    },
    saveMeasurementUnitClassificationList(state, action) {
      return {
        ...state,
        measurementUnitClassificationList: action.payload,
      };
    },
    saveSupplierList(state, action) {
      return {
        ...state,
        supplierList: action.payload,
      };
    },
    saveUpperGoodsList(state, action) {
      return {
        ...state,
        upperGoodsList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveTagList(state, action) {
      return {
        ...state,
        tagList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
