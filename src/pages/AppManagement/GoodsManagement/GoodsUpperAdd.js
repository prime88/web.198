import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, InputNumber, Row, Col, Menu } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/goods';
import ImagePreviewModal from '@/components/ImagePreviewModal';
import styles from './GoodsUpperAdd.less';
import {imgDomainName} from "../../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ goods, warehouse, loading }) => ({
  goods,
  warehouse,
  loading: loading.models.goods,
}))
@Form.create()
class GoodsUpperAdd extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    selectedStoreRows: [],
    formValues: {},
    record: {},
    imageList: [],
    previewVisible: false,
  };

  storeColumns = [
    {
      title: '编号',
      dataIndex: 'storeId',
    },
    {
      title: '门店名称',
      dataIndex: 'name',
    },
    {
      title: '门店地址',
      dataIndex: 'address',
    },
    {
      title: '门店电话',
      dataIndex: 'phone',
    },
    {
      title: '门店介绍',
      dataIndex: 'introduce',
    },
  ];

  columns = [
    {
      title: '图片',
      dataIndex: 'imageUrl',
      fixed: 'left',
      width: 100,
      render: (text) => {
        const imageUrlArray = text ? text.split(',') : [];
        return imageUrlArray.length > 0 ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text.split(',')[0]}`} /> : '';
      },
    },
    {
      title: '编号',
      dataIndex: 'goodsId',
      // fixed: 'left',
      // width: 70
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '存货简称',
      dataIndex: 'shorthand',
    },
    {
      title: '类目',
      dataIndex: 'stockClassify',
    },
    {
      title: '计量单位',
      dataIndex: 'units',
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '总库存',
      dataIndex: 'totalStock',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    // {
    //   title: '商品详情',
    //   dataIndex: 'details',
    // },
  ];

  onImageClick = (images) => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true })
  }

  componentDidMount() {
    const { dispatch } = this.props;

    //门店列表
    dispatch({
      type: 'goods/fetchStoreList',
    });

    //存货分类列表
    dispatch({
      type: 'goods/fetchInventoryClassificationList',
    });

    //商品列表
    dispatch({
      type: 'goods/fetchGoodsList',
      payload: {
        isEnable: true
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'goods/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSelectStoreRows = rows => {
    this.setState({
      selectedStoreRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'goods/fetchGoodsList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
          isEnable: true,
        },
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'goods/fetchGoodsList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        isEnable: true,
      },
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间下限">
              {getFieldDecorator('lowerPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间下限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="价格区间上限">
              {getFieldDecorator('upperPriceLimit')(
                <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入区间上限" min={0} />
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;

    //商品列表
    dispatch({
      type: 'goods/fetchGoodsList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        isEnable: true,
        ...form.getFieldsValue()
      }
    });

    this.setState({ selectedMenuItemKey: key })
  }

  onSelectAll = (selected, selectedRows, changeRows, dataSource) => {
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    console.log('changeRows: ', changeRows);
    console.log('dataSource: ', dataSource);
    console.log('dataSource: ', dataSource);
    let newSelectedRows = [];
    if (selected) {
      newSelectedRows = dataSource;
    }
    this.setState({ selectedRows: newSelectedRows })
  }

  onSelectStoreAll = (selected, selectedRows, changeRows, dataSource) => {
    console.log('selected: ', selected);
    console.log('selectedRows: ', selectedRows);
    console.log('changeRows: ', changeRows);
    console.log('dataSource: ', dataSource);
    console.log('dataSource: ', dataSource);
    let newSelectedRows = [];
    if (selected) {
      newSelectedRows = dataSource;
    }
    this.setState({ selectedStoreRows: newSelectedRows })
  }

  render() {
    const {
      goods: { storeList, inventoryClassificationList, goodsList },
      loading,
    } = this.props;
    const { selectedRows, selectedStoreRows, previewVisible, imageList } = this.state;

    return (
      <div>
        <Row >
          <Col span={24}>
            <div className={styles.tableList}>
              <StandardTable
                selectedRows={selectedStoreRows}
                loading={loading}
                data={storeList}
                columns={this.storeColumns}
                onSelectRow={this.handleSelectStoreRows}
                onChange={this.handleStandardTableChange}
                rowKey="storeId"
                size='small'
                paginationOfTable={{defaultPageSize: 5, size: 'small'}}
                onSelectAll={this.onSelectStoreAll}
              />
            </div>
          </Col>
        </Row>
        <Row style={{marginTop: 24}}>
          <Col span={24}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm}>
                {this.renderSimpleForm()}
              </div>
              <Row gutter={16}>
                <Col span={4}>
                  <Menu
                    mode="inline"
                    openKeys={this.state.openKeys}
                    onOpenChange={this.onOpenChange}
                    defaultSelectedKeys={['-1']}
                    onClick={this.onMenuItemClick}
                  >
                    <Menu.Item key={-1} value={-1}>全部分类</Menu.Item>
                    { inventoryClassificationList.rows.map(item => (
                      <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>{item.name}</Menu.Item>
                    )) }
                  </Menu>
                </Col>
                <Col span={20}>
                  <StandardTable
                    selectedRows={selectedRows}
                    loading={loading}
                    data={goodsList}
                    columns={this.columns}
                    onSelectRow={this.handleSelectRows}
                    onChange={this.handleStandardTableChange}
                    rowKey='goodsId'
                    size='small'
                    onSelectAll={this.onSelectAll}
                    scroll={{x: 1500}}
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={120}
          imgHeight={120}
        />
      </div>
    );
  }
}

export default GoodsUpperAdd;
