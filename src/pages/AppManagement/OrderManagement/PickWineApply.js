import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Col, Row, DatePicker, message, Table, Tabs } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isStoreGainNameRepeated } from '@/services/storeGain';
import DescriptionList from '@/components/DescriptionList';
import ReactToPrint from "react-to-print";
import styles from './PickWineApply.less';
import PickWineApplyDetail from './PickWineApplyDetail'
import router from 'umi/router';

const { Description } = DescriptionList;
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isStoreGainNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改存货分类"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(
          <Input.TextArea rows={3} placeholder="请输入备注" />
        )}
      </FormItem>
    </Modal>
  );
});

const OrderDetail = props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      width={500}
      visible={detailModalVisible}
      // onOk={this.handleOk}
      destroyOnClose
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{marginLeft: 15}}>
          <Row>
            <Col span={20}>
              <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>提酒申请</span>
            </Col>
            <Col span={4}>
              <span
                style={{float: 'right'}}
              >
                <ReactToPrint
                  trigger={() => <a href="#"><Icon type="printer" /></a>}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef
                  }}
                />
              </span>
            </Col>
          </Row>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <PickWineApplyDetail record={record}/>
    </Modal>
  );
}

@Form.create()
class DeliveryForm extends PureComponent {
  state = {
    activeKey: '1'
    // activeKey: this.props.record.deliveryTime === '快递配送' ? '2' : '1'
  }

  onChange = (activeKey) => {
    const { form } = this.props;
    console.log('activeKey: ', activeKey);
    this.setState({ activeKey })
    form.resetFields();
  }

  static getDerivedStateFromProps(props, state) {
    const { record } = props;
    const activeKey = record.deliveryTime === '快递配送' ? '2' : '1'
    if (activeKey !== state.activeKey) {
      return { activeKey };
    }
    return null;
  }

  render() {
    const { modalVisible, form, handleDelivery, handleDeliveryModalVisible, staffList, record } = this.props;
    const { activeKey } = this.state;

    const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleDelivery(fieldsValue);
      });
    };
    return (
      <Modal
        destroyOnClose
        title="配送"
        visible={modalVisible}
        onOk={okHandle}
        onCancel={() => handleDeliveryModalVisible()}
      >
        <Tabs activeKey={activeKey} onChange={this.onChange} style={{textAlign: 'center'}}>
          <TabPane tab={<span>门店配送</span>} key="1">
            <div style={{display: record.type === 0 && record.deliveryTime !== '快递配送' ? 'block' : 'none'}}>
              <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="防伪码">
                {form.getFieldDecorator('securityCode', {
                  rules: [{ required: activeKey === '1', message: '请输入防伪码！' }],
                })(<Input placeholder="请输入防伪码" />)}
              </FormItem>
              <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="配送员">
                {form.getFieldDecorator('distributionId', {
                  rules: [{ required: activeKey === '1', message: '请选择配送员！' }],
                })(
                  <Select placeholder="请选择配送员" style={{ width: '100%' }}>
                    {staffList.rows.map(item => <Option value={item.personnelId}>{item.name}</Option>)}
                  </Select>
                )}
              </FormItem>
            </div>
          </TabPane>
          <TabPane tab={<span>快递配送</span>} key="2">
            <div style={{display: record.type === 0 && record.deliveryTime === '快递配送' ? 'block' : 'none'}}>
              <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="快递编号">
                {form.getFieldDecorator('courierNumber', {
                  rules: [{ required: activeKey === '2', message: '请输入快递编号！' }],
                })(<Input placeholder="请输入快递编号" />)}
              </FormItem>
              <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="快递名称">
                {form.getFieldDecorator('courierName', {
                  rules: [{ required: activeKey === '2', message: '请输入快递名称！' }],
                })(<Input placeholder="请输入快递名称" />)}
              </FormItem>
            </div>
          </TabPane>
        </Tabs>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ wineCellar, user, loading }) => ({
  wineCellar,
  user,
  loading: loading.models.wineCellar,
}))
@Form.create()
class PickWineApply extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    detailModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    deliveryModalVisible: false,
    dataPermission: false,
  };

  columns = [
    {
      title: '申请时间',
      dataIndex: 'createTime',
      width: 150,
      fixed: 'left',
    },
    {
      title: '取酒单号',
      dataIndex: 'orderNumber',
      width: 150,
      fixed: 'left',
      render: (text, record) => <a onClick={() => this.handleDetailModalVisible(true, record)}>{text}</a>
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '待确认';
            break;
          case '1':
            result = '待出库';
            break;
          case '2':
            result = '待收货';
            break;
          case '3':
            result = '已完成';
            break;
          case '4':
            result = '已关闭';
            break;
          case '5':
            result = '待配送';
            break;
        }
        return result;
      }
    },
    {
      title: '自取时间/送达时间',
      dataIndex: 'deliveryTime',
      render: (text, record) => ({ 1: `${text} 自取`, 0: text === '快递配送' ? '快递配送': `${text} 送达` }[record.type])
    },
    {
      title: '留言',
      dataIndex: 'remark',
    },
    {
      title: '配送方式',
      dataIndex: 'type',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '在线配送';
            break;
          case '1':
            result = '到店自取';
            break;
        }
        return result;
      }
    },
    {
      title: '申请人',
      dataIndex: 'nickName',
    },
    {
      title: '申请人电话',
      dataIndex: 'phone',
    },
    {
      title: '收货地址',
      dataIndex: 'detailedAddress',
      render: (text, record) => {
        if (!record.placeName && !text ) {
          return null;
        }
        else{
          return <span>{`${record.placeName} ${text}`}</span>;
        }
      }
    },
    {
      title: '配送门店',
      dataIndex: 'store',
    },
    {
      title: '配送距离(米)',
      dataIndex: 'distance'
    },
    // {
    //   title: '操作',
    //   width: 60,
    //   fixed: 'right',
    //   align: 'center',
    //   render: (text, record) => (
    //     <Fragment>
    //       <a>打印</a>
    //     </Fragment>
    //   ),
    // },
  ];

  recordRemove = stockClassifyIds => {
    console.log('stockClassifyIds: ', stockClassifyIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'wineCellar/removeStoreGain',
      payload: { stockClassifyIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'wineCellar/changeStoreGainStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  handleDelivery = fields => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    dispatch({
      type: 'wineCellar/deliveryOrder',
      payload: {
        ...fields,
        wineCellarOrderId: selectedRows[0].wineCellarOrderId,
      },
      callback: this.refreshSelectedRows,
      formValues: this.getFormValues()
    });

    // message.success('添加成功');
    this.handleDeliveryModalVisible();
  };

  refreshSelectedRows = (orderList) => {
    const { selectedRows } = this.state;
    let selectedNewRows = [];
    if (selectedRows.length > 0) {
      const target = orderList.rows.find(item => item.wineCellarOrderId === selectedRows[0].wineCellarOrderId);
      selectedNewRows = target ? [target] : [];
      // selectedNewRows = [orderList.rows.find(item => item.wineCellarOrderId === selectedRows[0].wineCellarOrderId)]
    }
    this.setState({ selectedRows:selectedNewRows })
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //获取门店列表
    dispatch({
      type: 'wineCellar/fetchStoreList',
    });

    //获取当前部门员工列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'wineCellar/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    this.fetchOrderList();
  }

  fetchOrderList = (payload = {}) => {
    const { dispatch, user: { currentUser } } = this.props;

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      dispatch({
        type: 'wineCellar/fetchPickWineApplyList',
        payload
      });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const storeId =  currentUser.personnel.storeId;
      //2、判断是否是门店员工
      if (storeId || storeId === 1) {
        console.log('是门店员工');
        dispatch({
          type: 'wineCellar/fetchPickWineApplyList',
          payload: {
            ...payload,
            storeId
          }
        });
      }
      else{
        console.log('不是门店员工')
      }
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'wineCellar/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetailModalVisible = (flag, record) => {
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'wineCellar/addStoreGain',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'wineCellar/updateStoreGain',
      payload: {
        ...fields,
        stockClassifyId: record.stockClassifyId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.fetchOrderList({ ...form.getFieldsValue() });

    //解决bug：后台选择订单，执行"确认"、"出库"操作，app端"确认收货",此时选择某行再进行"查询"、"重置"操作后，行选择未清除
    this.setState({ selectedRows: [] });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const startTime = fieldsValue.startTime;
      const endTime = fieldsValue.endTime;
      if (startTime) {
        fieldsValue.startTime = startTime.format('YYYY-MM-DD HH:mm');
      }
      if (endTime) {
        fieldsValue.endTime = endTime.format('YYYY-MM-DD HH:mm');
      }
      this.fetchOrderList({ ...fieldsValue });

      //解决bug：后台选择订单，执行"确认"、"出库"操作，app端"确认收货",此时选择某行再进行"查询"、"重置"操作后，行选择未清除
      this.setState({ selectedRows: [] });
    });
  };

  renderSimpleForm() {
    const {
      wineCellar: { storeList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;
    const { dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label='取酒单号'>
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="订单状态">
              {getFieldDecorator('status')(
                <Select allowClear placeholder="请选择订单状态" style={{ width: '100%' }}>
                  <Option value={0}>待确认</Option>
                  <Option value={1}>待出库</Option>
                  <Option value={2}>待收货</Option>
                  <Option value={3}>已完成</Option>
                  <Option value={4}>已关闭</Option>
                  <Option value={5}>待配送</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="配送门店">
              {getFieldDecorator('storeId', {
                initialValue: dataPermission ? '' : currentUser.personnel.storeId
              })(
                <Select
                  allowClear
                  disabled={!dataPermission}
                  placeholder="请选择门店"
                  style={{ width: '100%' }}
                >
                  {storeList.rows.map(item => <Select.Option key={item.storeId} value={item.storeId}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="开始时间">
              {getFieldDecorator('startTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择开始时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择结束时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (['3', '4'].includes(record.status.toString())) return;

    // this.setState(prevState => {
    //   let newSelectedRows = [...prevState.selectedRows];
    //   let flag = true;
    //   for (let item of prevState.selectedRows) {
    //     if (item.orderId === record.orderId) {
    //       newSelectedRows = newSelectedRows.filter(item => item.orderId !== record.orderId);
    //       flag = false;
    //       break;
    //     }
    //   }
    //   if (flag){
    //     newSelectedRows.push(record)
    //   }
    //   return ({
    //     selectedRows: newSelectedRows
    //   })
    // })

    this.setState({ selectedRows: [record] })
  }

  collectGoods = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    dispatch({
      type: 'wineCellar/confirmReceipt',
      payload: {
        wineCellarOrderId: selectedRows[0].wineCellarOrderId,
      },
      callback: () => {
        this.setState({ selectedRows: [] })
      },
      formValues: this.getFormValues()
    });
  }

  confirmOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    console.log('selectedRows: ', selectedRows);

    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    dispatch({
      type: 'wineCellar/confirmApplyOrder',
      payload: {
        wineCellarOrderId: selectedRows[0].wineCellarOrderId,
      },
      callback: this.refreshSelectedRows,
      formValues: this.getFormValues()
    });
  }

  getFormValues = () => {
    const { form } = this.props;
    const formData = form.getFieldsValue();
    const startTime = formData.startTime;
    const endTime = formData.endTime;
    if (startTime) {
      formData.startTime = startTime.format('YYYY-MM-DD HH:mm');
    }
    if (endTime) {
      formData.endTime = endTime.format('YYYY-MM-DD HH:mm');
    }
    return { ...formData }
  }

  outOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    //测试出库代码，待销售出库提交接口完整后，删掉此块代码
    // dispatch({
    //   type: 'wineCellar/outOrder',
    //   payload: {
    //     wineCellarOrderId: selectedRows[0].wineCellarOrderId,
    //   }
    // });
    //------------------------------------------------

    router.push({
      pathname: '/app-management/order-management/sale-out-order',
      query: {
        // wineCellarOrderId: selectedRows[0].wineCellarOrderId,
        orderId: selectedRows[0].wineCellarOrderId,
        type: 1
      }
    });

  }

  cancelOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    dispatch({
      type: 'wineCellar/cancelOrder',
      payload: {
        wineCellarOrderId: selectedRows[0].wineCellarOrderId,
      },
      callback: () => {
        this.setState({ selectedRows: [] })
      },
      formValues: this.getFormValues()
    });
  }

  handleDeliveryModalVisible = flag => {
    this.setState({
      deliveryModalVisible: !!flag,
    });
  };

  render() {
    const {
      wineCellar: { pickWineApplyList, staffList },
      loading,
    } = this.props;
    const { selectedRows, updateModalVisible, record, detailModalVisible, deliveryModalVisible } = this.state;

    const deliveryMethods = {
      handleDelivery: this.handleDelivery,
      handleDeliveryModalVisible: this.handleDeliveryModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    const selectedRowStatus = selectedRows.length > 0 ? selectedRows[0].status : '';
    const selectedRowType = selectedRows.length > 0 ? selectedRows[0].type : '';
    console.log('selectedRows: ', selectedRows);

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button type='primary' onClick={this.confirmOrder} disabled={selectedRowStatus.toString() !== '0'}>确认申请</Button>
              <Button type='primary' onClick={this.outOrder} disabled={selectedRowStatus.toString() !== '1'}>出库</Button>
              {/*<Button type='primary' onClick={() => this.handleDeliveryModalVisible(true)} disabled={selectedRowType.toString() !== '0' && selectedRowStatus.toString() !== '5'}>配送</Button>*/}
              <Button type='primary' onClick={() => this.handleDeliveryModalVisible(true)} disabled={!(selectedRowType.toString() === '0' && selectedRowStatus.toString() === '5')}>配送</Button>
              <Button type='primary' onClick={this.collectGoods} disabled={selectedRowStatus.toString() !== '2'}>确认收货</Button>
              <Popconfirm
                title="确认取消申请？"
                onConfirm={this.cancelOrder}
                okText="确认"
                cancelText="取消"
              >
                <Button
                  type='primary'
                  // onClick={this.cancelOrder}
                  disabled={!['0', '1', '5'].includes(selectedRowStatus.toString())}
                >取消申请</Button>
              </Popconfirm>
              {/*<Button type='primary' >打印订单</Button>*/}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={pickWineApplyList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="wineCellarOrderId"
              scroll={{x: 1500}}
              size='small'
              checkboxProps={record => ({
                disabled: ['3', '4'].includes(record.status.toString()),
              })}
              onRow={(record) => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
              type='radio'
            />
          </div>
        </Card>
        <DeliveryForm {...deliveryMethods} modalVisible={deliveryModalVisible} record={selectedRows.length > 0 ? selectedRows[0] : {}} staffList={staffList} />
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
        {/*<UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />*/}
      </div>
    );
  }
}

export default PickWineApply;
