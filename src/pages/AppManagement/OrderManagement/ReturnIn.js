import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Divider,
  Table,
  Popconfirm,
} from 'antd';
import ReturnInAdd from './ReturnInAdd'
import styles from './ReturnIn.less';
import moment from 'moment'
import { fetchStaffList } from '@/services/branch';
import EditableCell from '@/customComponents/EditableCell';

//单元格编辑--------------------------------------------
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
//-----------------------------------------------------

const FormItem = Form.Item;
const { Option } = Select;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, supplierId } = props;
  let ReturnInModalAdd;
  const okHandle = () => {
    const { selectedRows } = ReturnInModalAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = (ref) => {
    ReturnInModalAdd = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      title="入库明细"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{top: 20}}
    >
      <ReturnInAdd wrappedComponentRef={addModalRef} supplierId={supplierId} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改入库明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="入库数量">
        {form.getFieldDecorator('number', {
          rules: [{ required: true, message: '请输入入库数量！' }],
          initialValue: record.number,
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入入库数量" min={1} />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ returnIn, user, loading }) => ({
  returnIn,
  user,
  loading: loading.models.returnIn,
}))
@Form.create()
class ReturnIn extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    referenceModalVisible: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    supplierId: '',
    orderId: '',
    returnGoodsId: '',
    warehouseId: '',
    dataPermission: false,
  };

  columns = [
    // {
    //   title: '序号',
    //   dataIndex: '',
    //   width: 60,
    //   fixed: 'left',
    //   render: (text, record, index) => index+1
    // },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '单位',
      dataIndex: 'units',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    // {
    //   title: '仓库',
    //   dataIndex: 'warehouse',
    // },
    {
      title: '入库数量',
      dataIndex: 'number',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 0,
      inputNumberMin: 1,
      width: 120,
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
      width: 200
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.stockId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = (stockId) => {
    console.log('stockId: ', stockId);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.stockId !== stockId) })
    message.success('删除成功');
  }

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser } ,
      location: { query: { returnGoodsId, orderId } },
    } = this.props;

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'returnIn/fetchOrderNumber',
    });

    // 获取供应商列表
    dispatch({
      type: 'returnIn/fetchSupplierList',
    });

    const personnel = currentUser.personnel;
    if (personnel) {
      //获取当前部门员工列表
      dispatch({
        type: 'returnIn/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //从其他页面跳转过来出库
    if (returnGoodsId && orderId) {
      this.setState({
        returnGoodsId,
        orderId,
      })

      //获取入库详情
      dispatch({
        type: 'returnIn/fetchOrderDetailList',
        payload: {
          orderId
        },
        callback: (data) => {
          this.setState({ dataSource: data.goodsList })
        }
      })
    }

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      dispatch({
        type: 'returnIn/fetchWarehouseList',
        callback: (data) => {
          const warehouse = data.rows.find(item => item.branchId === currentUser.personnel.branchId);
          if (warehouse) {
            this.setState({ warehouseId: warehouse.warehouseId })
          }
        }
      });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId =  currentUser.personnel.branchId;
      //2、判断是否是门店员工
      if (branchId || branchId === 1) {
        console.log('是门店员工')
        //获取仓库列表
        dispatch({
          type: 'returnIn/fetchWarehouseList',
          payload: {
            branchId
          },
          callback: (data) => {
            const warehouse = data.rows.find(item => item.branchId === currentUser.personnel.branchId);
            if (warehouse) {
              this.setState({ warehouseId: warehouse.warehouseId })
            }
          }
        });
      }
      else{
        console.log('不是门店员工')
      }
    }
  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ dataSource: [], supplierId: '' })
  };

  submit = (isSubmit) => {
    const { dispatch, form } = this.props;
    const { dataSource, returnGoodsId, warehouseId } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增入库明细');
        return;
      }

      fieldsValue.putInTime = fieldsValue.putInTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      fieldsValue.warehouseId = warehouseId;

      //从退货管理跳转过来的数据
      if (returnGoodsId) {
        fieldsValue.returnGoodsId = returnGoodsId;
      }

      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'returnIn/addReturnInOrder',//addPurchaseingOrder
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [] })

          //更新订单编号
          dispatch({
            type: 'returnIn/fetchOrderNumber',
          });
        }
      });
    });
  };

  handleSearch = e => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0){
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            if (newItem.stockId.toString() === oldItem.stockId.toString()) {
              oldItem.number++;
              flag = false;
              break;
            }
          }
          if (flag) {
            oldDataSource.push({ ...newItem, number: 1 })
          }
        }
      }
      else{
        oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1 }))];
      }

      //计算无税单价、含税合计、无税合计
      oldDataSource = this.getNewDataSource(oldDataSource);

      return {
        dataSource: oldDataSource
      }
    });

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.stockId === record.stockId);
    if (index > -1){
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });

      //计算无税单价、含税合计、无税合计
      const dataSource = this.getNewDataSource(newData);

      this.setState({ dataSource });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    }
    else{
      message.error('修改失败');
    }
  };

  onWarehouseChange = (warehouse, e) => {
    const { returnIn: { warehouseList }, user: { currentUser }, dispatch, form } = this.props;
    this.setState({ warehouseId: e.props.warehouseId })

    //更新出库人员下拉列表
    //1、根据warehouse找branchId
    const branchId = warehouseList.rows.find(item => item.name === warehouse).branchId;

    //2、获取当前部门员工列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'returnIn/fetchStaffList',
        payload: {
          branchId,
        },
      });
    }
    //3、清除出库人员
    form.setFieldsValue({ putInBy: '' });
  }

  renderTopSimpleForm() {
    const {
      returnIn: { orderNumber, supplierList, warehouseList, staffList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;
    const { dataPermission } = this.state;

    const warehouse = warehouseList.rows.find(item => item.branchId === currentUser.personnel.branchId);

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;订单编号</span>}>
              {orderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>入库仓库</span>}>
              {getFieldDecorator('warehouse', {
                rules: [{ required: true, message: '请选择入库仓库' }],
                initialValue: warehouse ? warehouse.name : '',
              })(
                <Select
                  // disabled={!dataPermission}
                  placeholder="请选择入库仓库"
                  style={{ width: '100%' }}
                  onChange={this.onWarehouseChange}
                >
                  {warehouseList.rows.map(item => <Select.Option warehouseId={item.warehouseId} key={item.name}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库人员">
              {getFieldDecorator('putInBy', {
                rules: [{ required: true, message: '请输入入库人员' }],
                initialValue: currentUser.personnel.name
              })(
                <Select placeholder="请输入入库人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="入库时间">
              {getFieldDecorator('putInTime', {
                rules: [{ required: true, message: '请选择入库时间' }],
                initialValue: moment(new Date(), 'YYYY-MM-DD HH:mm')
              })(
                <DatePicker
                  showTime={{ format: 'HH:mm' }}
                  style={{width: '100%'}}
                  placeholder="请选择入库时间"
                  format="YYYY-MM-DD HH:mm"
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="入库批次">
              {getFieldDecorator('batchNumber', {
                rules: [{ required: true, message: '请输入批次' }],
                initialValue: '9999999999999999'
              })(
                <Input
                  style={{width: '100%'}}
                  placeholder="请输入批次"
                  disabled
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
      loading
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark')(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">
              {currentUser.personnel.name}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
            </span>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons} style={{float: 'right'}}>
              {/*<Button style={{ marginRight: 8 }} onClick={this.handleFormReset}>*/}
                {/*重置*/}
              {/*</Button>*/}
              <Button loading={loading} disabled={loading} style={{ marginRight: 8 }} onClick={() => this.submit(false)}>
                保存
              </Button>
              <Button loading={loading} disabled={loading} type="primary" htmlType="submit">
                提交
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onAddClick = () => {
    this.handleModalVisible(true);
  }

  getNewDataSource = (oldDataSource) => {
    return oldDataSource.map(item => {
      let taxFreeUnitPrice = item.referenceCost/(1+item.taxRate/100);
      taxFreeUnitPrice = parseInt(taxFreeUnitPrice*10000)/10000;

      let taxTotal = item.referenceCost * item.number;
      taxTotal = parseInt(taxTotal*10000)/10000;

      let taxFreeTotal = taxFreeUnitPrice * item.number;
      taxFreeTotal = parseInt(taxFreeTotal*10000)/10000;

      return {
        ...item,
        taxFreeUnitPrice,//无税单价
        taxTotal, //含税合计
        taxFreeTotal, //无税合计
      }
    });
  }

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.orderGoodsId === item.orderGoodsId);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  }

  render() {
    const { modalVisible, dataSource, updateModalVisible, record, supplierId } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          isInputNumber: col.isInputNumber,
          inputNumberPrecision: col.inputNumberPrecision,
          inputNumberMin: col.inputNumberMin,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });


    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              入库明细
              {/*<Button*/}
                {/*type="primary"*/}
                {/*style={{ marginLeft: 16 }}*/}
                {/*// icon="plus"*/}
                {/*onClick={this.onAddClick}*/}
              {/*>*/}
                {/*新增*/}
              {/*</Button>*/}
            </h2>
            <Divider />
            <Table
              rowKey='orderGoodsId'
              components={components}
              columns={columns}
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              scroll={{x: 1800}}
              footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{paddingTop: 24}} className={styles.tableListForm}>{this.renderBottomSimpleForm()}</div>
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} supplierId={supplierId} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
      </div>
    );
  }
}

export default ReturnIn;
