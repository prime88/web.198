import React, { Component } from 'react';
import { connect } from 'dva';
import { Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import styles from './SaleOrderListDetail.less';

const { Description } = DescriptionList;

@connect(({ saleOrder, loading }) => ({
  saleOrder,
  // loading: loading.effects['profile/fetchBasic'],
}))
class SaleOrderListDetail extends Component {
  state = {
    dataSource: []
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const salesPutOutId = record.salesPutOutId;
    if (salesPutOutId) {
      dispatch({
        type: 'saleOrder/fetchSaleOrderInventoryList',
        payload: {
          salesPutOutIds: salesPutOutId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }
  }

  columns = [
    // {
    //   title: '序号',
    //   dataIndex: '',
    //   render: (text, record, index) => index+1
    // },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '单位',
      dataIndex: 'units',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '数量',
      dataIndex: 'number',
    },
    // {
    //   title: '含税单价',
    //   // dataIndex: 'referenceCost',
    //   dataIndex: 'warehousePrice',
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="出库时间">{record.putOutTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="出库人员">{record.putOutBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="出库仓库">{record.warehouse}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>订单明细</div>
          <Table
            style={{ marginBottom: 24 }}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="dpoStockId"
            pagination={{defaultPageSize: 9}}
            size='small'
          />
        </div>
      </div>
    );
  }
}

export default SaleOrderListDetail;
