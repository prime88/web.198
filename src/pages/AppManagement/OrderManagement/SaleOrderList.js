import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  DatePicker,
  Modal,
  message,
  Table,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import router from 'umi/router';
import styles from './SaleOrderList.less';
import { fetchStaffList } from '@/services/branch';
import SaleOrderListEdit from './SaleOrderListEdit'
import SaleOrderListDetail from './SaleOrderListDetail'
import ReactToPrint from "react-to-print";
import {apiDomainName} from "../../../constants";
import { stringify } from 'qs';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const DetailForm = Form.create()(props => {
  const { detailModalVisible, handleDetailModalVisible, record } = props;
  let componentRef;
  return (
    <Modal
      width={'80%'}
      destroyOnClose
      visible={detailModalVisible}
      // onOk={okHandle}
      onCancel={() => handleDetailModalVisible()}
      style={{top: 10}}
      title={
        <div>
          销售单详情
          <span
            style={{float: 'right'}}
          >
            <ReactToPrint
              trigger={() => <a href="#"><Icon type="printer" /></a>}
              content={() => {
                console.log('componentRef: ', componentRef);
                return componentRef
              }}
            />
          </span>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
    >
      <SaleOrderListDetail record={record} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  let saleOrderEdit;
  const okHandle = (isSubmit) => {
    const { form } = saleOrderEdit.props;
    const { dataSource, warehouseId } = saleOrderEdit.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (dataSource.length === 0) {
        message.warning('请新增销售单明细');
        return;
      }

      fieldsValue.stockJson = JSON.stringify([...dataSource]);
      // fieldsValue.estimateTime = fieldsValue.estimateTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.putOutTime = fieldsValue.putOutTime.format('YYYY-MM-DD HH:mm');
      fieldsValue.warehouseId = warehouseId;
      if (isSubmit) {
        fieldsValue.submit = isSubmit;
      }
      console.log('fieldsValue: ', fieldsValue);
      handleUpdate(fieldsValue);
    });
  };

  const editModalRef = (ref) => {
    saleOrderEdit = ref;
  }

  return (
    <Modal
      width={'95%'}
      destroyOnClose
      // title="修改销售订单"
      title={(
        <div>
          <span>修改销售订单</span>
          <Button style={{float: 'right', marginRight: 50}} type='primary' onClick={() => okHandle(true)}>提交</Button>
          <Button style={{float: 'right', marginRight: 16}} onClick={() => okHandle(false)}>保存</Button>
        </div>
      )}
      visible={updateModalVisible}
      // onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
      style={{top: 10}}
      footer={[
        <Button onClick={() => handleUpdateModalVisible()} >取消</Button>,
        <Button onClick={() => okHandle(false)}>保存</Button>,
        <Button type='primary' onClick={() => okHandle(true)}>提交</Button>
      ]}
    >
      <SaleOrderListEdit row={record} wrappedComponentRef={editModalRef} />
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ saleOrder, user, loading }) => ({
  saleOrder,
  user,
  loading: loading.models.saleOrder,
}))
@Form.create()
class SaleOrderList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
    dataPermission: false,
  };

  columns = [
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
    },
    {
      title: '出库仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '出库时间',
      dataIndex: 'putOutTime',
      // render: text => text.split(' ').length > 0 ? text.split(' ')[0] : text
    },
    {
      title: '制单时间',
      dataIndex: 'createTime',
    },
    {
      title: '制单人',
      dataIndex: 'createBy',
    },
    {
      title: '是否提交',
      dataIndex: 'isSubmit',
      render: text => {
        let result = '';
        switch (text) {
          case true:
            result = '已提交';
            break;
          case false:
            result = '未提交';
            break;
        }
        return result;
      }
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: text => ({ 0: '正常销售', 1: '订单取消，已重新入库' }[text])
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      render: (text, record) => {
        const menu = (
          <Menu onClick={(e) => e.domEvent.stopPropagation()}>
            <Menu.Item disabled={!!record.isSubmit} >
              { !!record.isSubmit ? '修改' : <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a> }
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.handleDetailModalVisible(true, record, this)}>查看</a>
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement='bottomRight'>
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        )
      },
    },
  ];

  recordSubmit = (salesPutOutIds) => {
    console.log('salesPutOutIds: ', salesPutOutIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'saleOrder/submitSaleOrders',
      payload: { salesPutOutIds },
      callback: () => {
        //1、刷新列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、清空selectedRows
        this.setState({ selectedRows: [] })
      },
    });
  }

  recordRemove = salesPutOutId => {
    console.log('salesPutOutId: ', salesPutOutId);
    const { dispatch } = this.props;
    dispatch({
      type: 'saleOrder/removeSaleOrder',
      payload: { salesPutOutId },
      callback: () => {
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        this.setState({ selectedRows: [] })
      },
    });
  };

  expandedRowRender = record => {
    const {
      saleOrder: { saleOrderInventoryList },
    } = this.props;

    const columns = [
      // {
      //   title: '序号',
      //   dataIndex: '',
      //   width: 60,
      //   fixed: 'left',
      //   render: (text, record, index) => index+1
      // },
      {
        title: '存货编号',
        dataIndex: 'stockNum',
        width: 100,
        fixed: 'left',
      },
      {
        title: '存货名称',
        dataIndex: 'name',
        width: 200,
        fixed: 'left',
      },
      {
        title: '规格型号',
        dataIndex: 'specification',
      },
      {
        title: '单位',
        dataIndex: 'units',
      },
      {
        title: '分类',
        dataIndex: 'stockClassify',
      },
      {
        title: '品牌',
        dataIndex: 'brand',
      },
      // {
      //   title: '仓库',
      //   dataIndex: 'warehouse',
      // },
      {
        title: '数量',
        dataIndex: 'number',
      },
      // {
      //   title: '含税单价',
      //   // dataIndex: 'referenceCost',
      //   dataIndex: 'warehousePrice',
      // },
      {
        title: '供应商',
        dataIndex: 'supplier',
      },
      {
        title: '批次',
        dataIndex: 'batchNumber',
      },
      {
        title: '备注',
        dataIndex: 'remark',
      },
    ];

    return <Table
      size='small'
      columns={columns}
      dataSource={saleOrderInventoryList}
      footer={() => `合计: ${saleOrderInventoryList.length}`}
      rowKey='dpoStockId'
    />
  };

  componentDidMount() {
    const { dispatch, user: { currentUser }, saleOrder: { warehouseList } } = this.props;

    //门店列表
    dispatch({
      type: 'saleOrder/fetchStoreList',
    });

    //获取当前部门请购人员列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'saleOrder/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //销售订单列表
    this.fetchOrderList();
  }

  fetchOrderList = (payload = {}) => {
    const { dispatch, user: { currentUser } } = this.props;
    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      dispatch({
        type: 'saleOrder/fetchSaleOrderList',
        payload,
      });

      dispatch({
        type: 'saleOrder/fetchWarehouseList',
        payload: {
          ...payload
        }
      })
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId =  currentUser.personnel.branchId;
      //2、判断是否是门店员工
      if (branchId || branchId === 1) {
        console.log('是门店员工');
        dispatch({
          type: 'saleOrder/fetchSaleOrderList',
          payload: {
            ...payload,
            branchId: currentUser.personnel.branchId
          },
        });

        dispatch({
          type: 'saleOrder/fetchWarehouseList',
          payload: {
            ...payload,
            branchId:  currentUser.personnel.branchId,
            // storeId,
          },
          // callback: result => {
          //   dispatch({
          //     type: 'saleOrder/fetchSaleOrderList',
          //     payload: {
          //       ...payload,
          //       warehouse: result.rows[0] ? result.rows[0].name : ''
          //     },
          //   });
          // },
        });
      }
      else{
        console.log('不是门店员工')
      }
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'saleOrder/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({ purchasePersonnel: [] })
    // dispatch({
    //   type: 'saleOrder/fetchSaleOrderList',
    //   payload: {
    //     ...form.getFieldsValue()
    //   }
    // });
    this.fetchOrderList({ ...form.getFieldsValue() });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.executeSearch(fieldsValue)
    });
  };

  executeSearch = (fieldsValue) => {
    const { dispatch, user: { currentUser } } = this.props;
    const { dataPermission } = this.state;
    const personnel = currentUser.personnel;
    const putOutTime = fieldsValue.putOutTime;
    if (putOutTime) {
      fieldsValue.putOutTime = putOutTime.format('YYYY-MM-DD')
    }
    if (!dataPermission) {
      fieldsValue.branchId = personnel.branchId;
    }

    this.fetchOrderList({ ...fieldsValue });
    // dispatch({
    //   type: 'saleOrder/fetchSaleOrderList',
    //   payload: { ...fieldsValue },
    // });
  }

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    })
  }

  handleDetailModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetail = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'saleOrder/updatePurchase',
      payload: { ...fields, purchaseRequisitionId: record.purchaseRequisitionId },
    });

    this.handleUpdateModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, selectedRows, expandedRowKeys } = this.state;
    dispatch({
      type: 'saleOrder/updateSaleOrder',
      payload: { ...fields, salesPutOutId: record.salesPutOutId },
      callback: () => {
        //1、刷新申请单列表
        const fieldsValue = this.props.form.getFieldsValue();
        this.executeSearch(fieldsValue);

        //2、刷新申请单明细列表
        if (expandedRowKeys.length > 0){
          this.onExpand(true, record);
        }
      }
    });

    //若该项被选择了，则需要取消对该项的选择
    if (fields.submit) {
      this.setState({ selectedRows: selectedRows.filter(item => item.salesPutOutId !== record.salesPutOutId) })
    }

    this.handleUpdateModalVisible();
  };

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const putOutTime = fieldsValue.putOutTime;
      if (putOutTime) {
        fieldsValue.putOutTime = putOutTime.format('YYYY-MM-DD')
      }

      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/salesPutOut/exportList?${stringify(params)}`;
    });
  }

  renderSimpleForm() {
    const {
      saleOrder: { storeList, staffList, warehouseList },
      form: { getFieldDecorator },
    } = this.props;
    const { purchasePersonnel, dataPermission } = this.state;

    const warehouse = dataPermission ? '' : (warehouseList.rows[0] ? warehouseList.rows[0].name : '');

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="订单编号">
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单编号" />)}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="出库仓库">
              {getFieldDecorator('warehouse', {
                // initialValue: warehouse
              })(
                <Select allowClear placeholder="请选择出库仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="出库日期">
              {getFieldDecorator('putOutTime')(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择出库日期"
                />
              )}
            </FormItem>
          </Col>
          <Col md={5} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      const salesPutOutId = record.salesPutOutId;
      dispatch({
        type: 'saleOrder/fetchSaleOrderInventoryList',
        payload: {
          salesPutOutIds: salesPutOutId
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (record.isSubmit) return;

    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.purchaseRequisitionId === record.purchaseRequisitionId) {
          newSelectedRows = newSelectedRows.filter(item => item.purchaseRequisitionId !== record.purchaseRequisitionId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return ({
        selectedRows: newSelectedRows
      })
    })
  }

  render() {
    const {
      saleOrder: { saleOrderList },
      loading,
    } = this.props;
    const { selectedRows, updateModalVisible, expandedRowKeys, record, detailModalVisible } = this.state;

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
      handleDetail: this.handleDetail,
    };
    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              {/*<Button icon="plus" type="primary" onClick={() => router.push('/app-management/order-management/sale-out-order')}>*/}
                {/*新增*/}
              {/*</Button>*/}
              {/*<Button >审核</Button>*/}
              {selectedRows.length > 0 && (
                <span>
                  <Button
                    onClick={() => this.recordSubmit( selectedRows.map(item => item.salesPutOutId).join(','))}
                  >批量提交</Button>
                  <Popconfirm
                    title="确认删除？"
                    onConfirm={() => this.recordRemove( selectedRows.map(item => item.salesPutOutId).join(','))}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={saleOrderList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              rowKey='salesPutOutId'
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
              onRow={(record) => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
              checkboxProps={record => ({
                disabled: record.isSubmit,
              })}
            />
          </div>
        </Card>
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
        <DetailForm {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
      </div>
    );
  }
}

export default SaleOrderList;
