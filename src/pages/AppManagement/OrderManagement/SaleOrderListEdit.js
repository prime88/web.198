import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Divider,
  Table,
  Popconfirm,
  TreeSelect,
} from 'antd';
import SaleOrderAdd from './SaleOrderAdd'
import styles from './SaleOrderListEdit.less';
import moment from 'moment'
import { fetchStaffList } from '@/services/branch';
import EditableCell from '@/customComponents/EditableCell';
import { fetchWarehouseStockBatchList } from '@/services/common-api';

//单元格编辑--------------------------------------------
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
//-----------------------------------------------------

const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item;
const { Option } = Select;

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, warehouseId } = props;
  let saleOrderAdd;
  const okHandle = () => {
    const { selectedRows } = saleOrderAdd.state;
    console.log('selectedRows: ', selectedRows);
    handleAdd(selectedRows);
  };

  const addModalRef = (ref) => {
    saleOrderAdd = ref;
  }

  return (
    <Modal
      width={'90%'}
      destroyOnClose
      title="新增销售明细"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      style={{top: 50}}
    >
      <SaleOrderAdd wrappedComponentRef={addModalRef} warehouseId={warehouseId} />
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);

      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改销售明细"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="数量">
        {form.getFieldDecorator('number', {
          rules: [{ required: true, message: '请输入数量！' }],
          initialValue: record.number,
        })(<InputNumber precision={0} style={{width: '100%'}} placeholder="请输入数量" min={1} />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ saleOrder, user, loading }) => ({
  saleOrder,
  user,
  loading: loading.models.saleOrder,
}))
@Form.create()
class SaleOrderListEdit extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    referenceModalVisible: false,
    selectedRows: [],
    dataSource: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    warehouseId: '',
    // lastOperationStatus: '',
    type: '',//0：出库订单；1：取酒订单；2、从库存出库(新增),
    orderId: '',
    dataPermission: false,
    warehouseStockBatchList: [],
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      width: 60,
      fixed: 'left',
      render: (text, record, index) => index+1
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 100,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '单位',
      dataIndex: 'units',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '数量',
      dataIndex: 'number',
      editable: true,
      isInputNumber: true,
      inputNumberPrecision: 0,
      inputNumberMin: 1,
      width: 120,
    },
    // {
    //   title: '含税单价',
    //   // dataIndex: 'referenceCost',
    //   dataIndex: 'warehousePrice',
    // },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '批次',
      dataIndex: 'batchNumber',
      isSelect: true,
      editable: true,
      width: 220,
    },
    {
      title: '备注',
      dataIndex: 'remark',
      editable: true,
      width: 200
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          {/*<a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>*/}
          {/*<Divider type="vertical" />*/}
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.ID)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = (ID) => {
    console.log('ID: ', ID);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.ID !== ID).map((item, index) => ({ ...item, ID: index })) })
    message.success('删除成功');
  }

  handleSelectChange = (record, key, value, stockNumber) => {
    console.log('row: ', record);
    console.log('key: ', key);
    console.log('value: ', value);
    const newData = [...this.state.dataSource];
    const row = { ...record };
    if (key === 'batchNumber') {
      row.batchNumber = value;
    }

    // 自定义业务需求
    // 当选择的批次在库存中的数量小于要出库的商品数量时：a、提示用户“该批次最多只能出库n件”；
    // record.number ? stockNumber
    if (stockNumber < record.number) {//库存数 < 要出库的数
      message.info(`该批次最多只能出库${stockNumber}件`);
      //row.number = stockNumber;--------------不能改，关联太强、成本太大
    }

    const index = newData.findIndex(item => row.ID === item.ID);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData.map((item, index) => ({ ...item, ID: index })) });
  }

  setSelectOptions = (row, callback) => {
    const stockNum = row.stockNum;
    console.log('stockNum: ', stockNum);
    const { warehouseId } = this.state;

    //获取仓库批次列表
    const response = fetchWarehouseStockBatchList({ warehouseId, stockNum });
    response.then(result => {
      console.log('result: ', result);
      if (callback) callback(result.data.map(item => ({ number: item.number, value: item.batchNumber, label: item.batchNumber })))
      // this.setState(prevState => ({
      //   // warehouseStockBatchList: result.data.map(item => item.batchNumber),
      //   // dataSource: prevState.dataSource.map(item => ({ ...item, batchNumber: '' }))
      // }))
    });
  }

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
      // location: { query: { orderId, type } },
      row,
    } = this.props;

    //获取订单编号: 假的，只用于展示
    dispatch({
      type: 'saleOrder/fetchOrderNumber',
    });

    //部门列表
    dispatch({
      type: 'saleOrder/fetchBranchList',
      payload: {
        isWarehouse: true
      }
    });

    //订单明细列表
    const salesPutOutId = row.salesPutOutId;
    if (salesPutOutId) {
      dispatch({
        type: 'saleOrder/fetchSaleOrderInventoryList',
        payload: {
          salesPutOutIds: salesPutOutId
        },
        callback: (data) => {
          this.setState({
            dataSource: data.map((item, index) => ({ ...item, warehousePrice: item.referenceCost, ID: index })),
          })
        }
      })
    }

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });

      //仓库列表
      dispatch({
        type: 'saleOrder/fetchWarehouseList',
        callback: (warehouseList) => {
          //初始化warehouseId
          const target = warehouseList.rows.find(item => item.name === row.warehouse)
          this.setState({ warehouseId: target.warehouseId });

          //获取当前部门员工列表
          const warehouse = row.warehouse;
          if (warehouse) {
            const branchId = warehouseList.rows.find(item => item.name === warehouse).branchId;
            dispatch({
              type: 'saleOrder/fetchStaffList',
              payload: {
                branchId,
              },
            });
          }

          // //获取仓库批次列表
          // const response = fetchWarehouseStockBatchList({ warehouseId: target.warehouseId });
          // response.then(result => {
          //   console.log('result: ', result);
          //   this.setState(prevState => ({
          //     warehouseStockBatchList: result.data,
          //     dataSource: prevState.dataSource
          //   }))
          // });
        }
      });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const branchId =  currentUser.personnel.branchId;
      //2、判断是否是门店员工
      if (branchId || branchId === 1) {
        console.log('是门店员工')

        //仓库列表
        dispatch({
          type: 'saleOrder/fetchWarehouseList',
          payload: {
            branchId
          },
          callback: (warehouseList) => {
            //初始化warehouseId
            const target = warehouseList.rows.find(item => item.name === row.warehouse)
            this.setState({ warehouseId: target.warehouseId });

            //获取当前部门员工列表
            const warehouse = row.warehouse;
            if (warehouse) {
              const branchId = warehouseList.rows.find(item => item.name === warehouse).branchId;
              dispatch({
                type: 'saleOrder/fetchStaffList',
                payload: {
                  branchId,
                },
              });
            }

            // //获取仓库批次列表
            // const response = fetchWarehouseStockBatchList({ warehouseId: target.warehouseId });
            // response.then(result => {
            //   console.log('result: ', result);
            //   this.setState(prevState => ({
            //     warehouseStockBatchList: result.data,
            //     dataSource: prevState.dataSource
            //   }))
            // });
          }
        });
      }
      else{
        console.log('不是门店员工')
      }
    }

  }

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ dataSource: [], warehouseId: '' })
  };

  submit = (isSubmit) => {
    const { dispatch, form, user: { currentUser } } = this.props;
    const { dataSource, lastOperationStatus, type, orderId } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      if (dataSource.length === 0) {
        message.warning('请新增销售单明细');
        return;
      }

      //含税单价：warehousePrice -> referenceCost
      const newDataSource = dataSource.map(item => ({ ...item, referenceCost: item.warehousePrice }))
      fieldsValue.stockJson = JSON.stringify(newDataSource);
      fieldsValue.putOutTime = fieldsValue.putOutTime.format('YYYY-MM-DD HH:mm');
      //------------未彻底完成------------
      fieldsValue.type = type;
      if (type === 0) {
        fieldsValue.orderId = orderId;
      }
      //-----------------------------


      console.log('fieldsValue: ', fieldsValue);
      dispatch({
        type: 'saleOrder/addSaleOrder',
        payload: { ...fieldsValue, isSubmit },
        callback: () => {
          form.resetFields();
          this.setState({ dataSource: [], warehouseId: '' });

          //更新订单编号
          dispatch({
            type: 'saleOrder/fetchOrderNumber',
          });
        }
      });
    });
  }

  handleSearch = (e) => {
    e.preventDefault();
    this.submit(true);
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = selectedRows => {
    // if (lastOperationStatus === 'add' || !lastOperationStatus) {
    selectedRows = selectedRows.map(item => ({ ...item, referenceCost: item.warehousePrice }))
    console.log('dataSource: ', this.state.dataSource);
    this.setState(prevState => {
      let oldDataSource = [...prevState.dataSource];
      //新存货跟就存货对比: 1.新增相同存货，数量+1；2.新增不同存货，明细+1
      if (oldDataSource.length > 0){
        for (let newItem of selectedRows) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            if (newItem.stockNum.toString() === oldItem.stockNum.toString() && newItem.warehousePrice.toString() === oldItem.warehousePrice.toString()) {
              oldItem.number++;
              flag = false;
              break;
            }
          }
          if (flag) {
            oldDataSource.push({ ...newItem, number: 1 })
          }
        }
      }
      else{
        oldDataSource = [...selectedRows.map(item => ({ ...item, number: 1 }))];
      }
      return {
        // dataSource: [...selectedRows.map(item => ({ ...item, number: 1 }))]
        dataSource: oldDataSource.map((item, index) => ({ ...item, ID: index })),
        // lastOperationStatus: 'add',
      }
    });
    // }
    // if (lastOperationStatus && lastOperationStatus === 'reference') {
    //   this.setState({ dataSource: selectedRows, lastOperationStatus: 'add', })
    // }

    message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dataSource, record } = this.state;
    const newData = [...dataSource];
    const index = newData.findIndex(item => item.ID === record.ID);
    if (index > -1){
      const item = newData[index];
      newData.splice(index, 1, {
        ...item,
        ...fields,
      });
      this.setState({ dataSource: newData.map((item, index) => ({ ...item, ID: index })) });
      message.success('修改成功');
      this.handleUpdateModalVisible();
    }
    else{
      message.error('修改失败');
    }
  };

  loadData = (dataTree) => {
    return (
      dataTree.map(item =>{
        return (
          <TreeNode title={item.name} key={item.name} value={item.name}>
            {item.childList ? this.loadData(item.childList) : null}
          </TreeNode>
        )
      })
    )
  }

  onWarehouseChange = (warehouse) => {
    const { saleOrder: { warehouseList }, dispatch, user: { currentUser }, form} = this.props;
    const warehouseId = warehouseList.rows.find(item => item.name === warehouse).warehouseId;
    if (warehouseId) {
      this.setState(prevState => ({
        // dataSource: type === 2 ? [] : prevState.dataSource,
        // dataSource: [],
        warehouseId,
      }));

      //更新出库人员下拉列表
      //1、根据warehouse找branchId
      const branchId = warehouseList.rows.find(item => item.name === warehouse).branchId;

      //2、获取当前部门员工列表
      const personnel = currentUser.personnel;
      if (personnel) {
        dispatch({
          type: 'saleOrder/fetchStaffList',
          payload: {
            branchId,
          },
        });
      }
      //3、清除出库人员
      form.setFieldsValue({ putOutBy: '' });

      //4、获取仓库批次列表
      // const response = fetchWarehouseStockBatchList({ warehouseId });
      // response.then(result => {
      //   console.log('result: ', result);
      //   this.setState(prevState => ({
      //     warehouseStockBatchList: result.data,
      //     dataSource: prevState.dataSource.map(item => ({ ...item, batchNumber: '' }))
      //   }))
      // });
    }
  }

  renderTopSimpleForm() {
    const {
      saleOrder: { orderNumber, staffList, branchList, warehouseList },
      form: { getFieldDecorator },
      user: { currentUser },
      row,
    } = this.props;
    const { dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={<span>&nbsp;&nbsp;&nbsp;订单编号</span>}>
              {row.orderNumber}
              {/*{getFieldDecorator('a')(<Input placeholder="请输入订单编号" />)}*/}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="出库仓库">
              {getFieldDecorator('warehouse', {
                rules: [{ required: true, message: '请选择出库仓库' }],
                initialValue: row.warehouse
              })(
                <Select onChange={this.onWarehouseChange} placeholder="请选择出库仓库" style={{ width: '100%' }}>
                  {warehouseList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库人员">
              {getFieldDecorator('putOutBy', {
                rules: [{ required: true, message: '请选择出库人员' }],
                initialValue: row.putOutBy
              })(
                <Select placeholder="请选择出库人员" style={{ width: '100%' }}>
                  {staffList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="出库时间">
              {getFieldDecorator('putOutTime', {
                rules: [{ required: true, message: '请选择出库时间' }],
                initialValue: moment(row.putOutTime, 'YYYY-MM-DD HH:mm')
              })(
                <DatePicker
                  style={{width: '100%'}}
                  placeholder="请选择出库时间"
                  format="YYYY-MM-DD HH:mm"
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  renderBottomSimpleForm() {
    const {
      form: { getFieldDecorator },
      user: { currentUser },
      row,
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark', {
                initialValue: row.remark
              })(<Input.TextArea rows={2} placeholder="请输入备注" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="制单人员">
              {row.createBy}
            </FormItem>
          </Col>
          {/*<Col md={8} sm={24}>*/}
            {/*<span className={styles.submitButtons} style={{float: 'right'}}>*/}
              {/*<Button style={{ marginRight: 8 }} onClick={this.handleFormReset}>*/}
                {/*重置*/}
              {/*</Button>*/}
              {/*<Button style={{ marginRight: 8 }} onClick={() => this.submit(false)}>*/}
                {/*保存*/}
              {/*</Button>*/}
              {/*<Button type="primary" htmlType='submit' >*/}
                {/*提交*/}
              {/*</Button>*/}
            {/*</span>*/}
          {/*</Col>*/}
        </Row>
      </Form>
    );
  }

  handleReferenceModalVisible = flag => {
    this.setState({
      referenceModalVisible: !!flag,
    });
  };

  handleReference = (dataSource) => {
    console.log('dataSource: ', dataSource);
    const { lastOperationStatus } = this.state;
    if (lastOperationStatus === 'reference' || !lastOperationStatus) {
      //一、newDataSource中：referenceCost、warehousePrice必须都存在且一样
      dataSource = dataSource.map(item => {
        let result = '';
        if (item.referenceCost) {
          result = { ...item, warehousePrice: item.referenceCost }
        } else if (item.warehousePrice) {
          result = { ...item, referenceCost: item.warehousePrice }
        }
        return result;
      } )
      //dataSource去重
      this.setState(prevState => {
        //二、oldDataSource中：referenceCost、warehousePrice必须都存在且一样
        let oldDataSource = [...prevState.dataSource.map(item => {
          let result = '';
          if (item.referenceCost) {
            result = { ...item, warehousePrice: item.referenceCost }
          } else if (item.warehousePrice) {
            result = { ...item, referenceCost: item.warehousePrice }
          }
          return result;
        })];
        //新存货跟旧存货对比: 1.相同存货，数量+；2.新增不同存货，push
        // if (oldDataSource.length > 0){
        for (let newItem of dataSource) {
          let flag = true;
          for (let oldItem of oldDataSource) {
            if (newItem.stockNum.toString() === oldItem.stockNum.toString() && newItem.warehousePrice.toString() === oldItem.warehousePrice.toString()) {
              // oldItem.number = oldItem.number + newItem.number;
              flag = false;
              break;
            }
          }
          if (flag) {
            oldDataSource.push(newItem)
          }
        }

        return {
          dataSource: oldDataSource.map((item, index) => ({ ...item, ID: index })),
          lastOperationStatus: 'reference',
        }
      });
    }

    if (lastOperationStatus && lastOperationStatus === 'add') {
      this.setState({ dataSource, lastOperationStatus: 'reference', })
    }

    this.handleReferenceModalVisible();
  }

  onAddClick = () => {
    const { warehouseId } = this.state;
    if (!warehouseId) {
      message.warning('请先选择出库仓库!');
      return;
    }
    this.handleModalVisible(true);
  }

  onReferenceClick = () => {
    // const { warehouseId } = this.state;
    // if (!warehouseId) {
    //   message.warning('请先选择出库仓库!');
    //   return;
    // }
    this.handleReferenceModalVisible(true)
  }

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.ID === item.ID);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData.map((item, index) => ({ ...item, ID: index }))  });
  }

  handleInputNumberChange = (record, key, value) => {
    const { form } = this.props;
    console.log('row: ', record);
    console.log('key: ', key);
    console.log('value: ', value);
    const newData = [...this.state.dataSource];
    const row = { ...record };
    //改row
    // if (isNaN(value)) {
    //   row.number = '';
    //   row.number2 = '';
    //   return;
    // }
    // else if (key === 'number') {
    //   row.number2 = parseInt((value/row.unitRatio)*100)/100;
    // }
    // else if (key === 'number2') {
    //   //保留两位小数
    //   value = parseInt(value*100)/100;
    //   row.number = parseInt(value*row.unitRatio);
    // }

    const index = newData.findIndex(item => row.ID === item.ID);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData.map((item, index) => ({ ...item, ID: index })) });
  }

  render() {
    const { modalVisible, dataSource, updateModalVisible, record, referenceModalVisible, warehouseId, warehouseStockBatchList } = this.state;


    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const referenceMethods = {
      handleReference: this.handleReference,
      handleReferenceModalVisible: this.handleReferenceModalVisible,
    };

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          isInputNumber: col.isInputNumber,
          inputNumberPrecision: col.inputNumberPrecision,
          inputNumberMin: col.inputNumberMin,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
          isSelect: col.isSelect,
          selectOptions: warehouseStockBatchList,
          inputNumberChange: (value, row) => this.handleInputNumberChange(row, col.dataIndex, value),
          // selectChange: (value, row) => this.handleSelectChange(row, col.dataIndex, value),
          selectFocus: (row, callback) => this.setSelectOptions(row, callback),
          selectChange: (value, stockNumber, row) => this.handleSelectChange(row, col.dataIndex, value, stockNumber),
        }),
      };
    });

    return (
      <div>
        <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderTopSimpleForm()}</div>
            <h2>
              销售明细
              {/*<Button*/}
                {/*type="primary"*/}
                {/*style={{ marginLeft: 16 }}*/}
                {/*// icon="plus"*/}
                {/*// onClick={() => this.handleModalVisible(true)}*/}
                {/*onClick={this.onAddClick}*/}
              {/*>*/}
                {/*新增*/}
              {/*</Button>*/}
              {/*<Button*/}
              {/*style={{ marginLeft: 16 }}*/}
              {/*// onClick={() => this.handleReferenceModalVisible(true)}*/}
              {/*onClick={this.onReferenceClick}*/}
              {/*>*/}
              {/*参照*/}
              {/*</Button>*/}
            </h2>
            <Divider />
            <Table
              // loading={loading}
              // rowKey='warehouseStockId'
              components={components}
              columns={columns}
              dataSource={dataSource}
              rowClassName={() => styles.editableRow}
              scroll={{x: 1800}}
              footer={() => `合计: ${dataSource.length > 0 ? dataSource.map(item => item.number || 0).reduce((a, b) => parseInt(a)+parseInt(b)) : 0}`}
            />
            <div style={{paddingTop: 24}} className={styles.tableListForm}>{this.renderBottomSimpleForm()}</div>
          </div>
        <CreateForm {...parentMethods} modalVisible={modalVisible} warehouseId={warehouseId} />
        {/*<ReferenceForm {...referenceMethods} referenceModalVisible={referenceModalVisible} />*/}
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record}/>
      </div>
    );
  }
}

export default SaleOrderListEdit;
