// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchOrderList,
  fetchOrderDetailList,
  // addInventoryClassification,
  // updateInventoryClassification,
  // removeInventoryClassification,
  confirmReceipt,
  confirmOrder,
  outOrder,
  cancelOrder,
  refund,
} from '@/services/returnGood';
import { queryStoreList } from '@/services/store';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'returnGood',

  state: {
    //订单列表
    orderList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //订单详情列表
    orderDetailList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchOrderList({ payload, callback }, { call, put, select }) {

      //数据权限:判断是否有数据权限
      const user = yield select(state => state.user);
      const permissionList = user.currentUser.permissionList;
      const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
      if (!target) {
        console.log('没有数据权限');
        payload = { ...payload, storeId: user.currentUser.personnel.storeId }
      }

      const response = yield call(fetchOrderList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveOrderList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *refreshOrderList({ payload, callback }, { call, put, select }) {
      const user = yield select(state => state.user);
      //数据权限
      //1、判断是否有数据权限
      const permissionList = user.currentUser.permissionList;
      console.log('permissionList: ', permissionList);
      const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
      if (target) {
        console.log('有数据权限');
        yield put({
          type: 'fetchOrderList',
        });
      }
      else{
        console.log('没有数据权限');
        yield put({
          type: 'fetchOrderList',
          payload: {
            storeId: user.currentUser.personnel.storeId
          }
        });
      }
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveStoreList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }
      else {
        message.error(response.msg);
      }
    },
    *fetchOrderDetailList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderDetailList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
      // yield put({
      //   type: 'saveOrderDetailList',
      //   payload: response.data,
      // });
    },
    // *addInventoryClassification({ payload, callback }, { call, put }) {
    //   const response = yield call(addInventoryClassification, payload);
    //   if (response.code === '0') {
    //     message.success(response.msg);
    //     yield put({
    //       type: 'fetchInventoryClassificationList',
    //     });
    //     if (callback) callback();
    //   } else {
    //     message.error(response.msg);
    //   }
    // },
    // *removeInventoryClassification({ payload, callback }, { call, put }) {
    //   const response = yield call(removeInventoryClassification, payload);
    //   const jsonResponse = JSON.parse(response);
    //   if (jsonResponse.code === '0') {
    //     message.success(jsonResponse.msg);
    //     yield put({
    //       type: 'fetchInventoryClassificationList',
    //     });
    //     if (callback) callback();
    //   } else {
    //     message.error(jsonResponse.msg);
    //   }
    // },
    // *updateInventoryClassification({ payload, callback }, { call, put }) {
    //   const response = yield call(updateInventoryClassification, payload);
    //   if (response.code === '0') {
    //     message.success(response.msg);
    //     yield put({
    //       type: 'fetchInventoryClassificationList',
    //     });
    //     if (callback) callback();
    //   } else {
    //     message.error(response.msg);
    //   }
    // },
    *confirmOrder({ payload, callback, formValues }, { call, put }) {
      const response = yield call(confirmOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchOrderList',
          payload: {
            ...formValues
          },
          callback: callback
        });
        // if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *refund({ payload, callback, formValues }, { call, put }) {
      const response = yield call(refund, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchOrderList',
          payload: {
            ...formValues,
          },
          callback: callback
        });
        // if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *outOrder({ payload, callback }, { call, put }) {
      const response = yield call(outOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchOrderList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *confirmReceipt({ payload, callback, formValues }, { call, put }) {
      const response = yield call(confirmReceipt, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchOrderList',
          payload: {
            ...formValues
          },
          callback: callback
        });
        // if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *cancelOrder({ payload, callback }, { call, put }) {
      const response = yield call(cancelOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchOrderList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveOrderList(state, action) {
      return {
        ...state,
        orderList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveOrderDetailList(state, action) {
      return {
        ...state,
        orderDetailList: action.payload,
      };
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //停留在本页面时的订单通知的数据刷新
        if (location.pathname === '/app-management/order-management/return-management' && location.query.type === '同页面跳转') {
          console.log('同页面跳转');
          dispatch({
            type: 'refreshOrderList',
          });
        }
        // //存货分类
        // if (location.pathname === '/basic-data/inventory-classification') {
        //   dispatch({
        //     type: 'fetchInventoryClassificationList',
        //   })
        // }
        // //存货管理
        // if (location.pathname === '/basic-data/inventory-management') {
        //   dispatch({
        //     type: 'fetchInventoryList',
        //   })
        // }
        // //仓库管理
        // if (location.pathname === '/basic-data/Warehouse-management') {
        //   dispatch({
        //     type: 'fetchWarehouseList',
        //   })
        // }
      });
    },
  },
};
