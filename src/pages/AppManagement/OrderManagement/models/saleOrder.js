// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
  fetchOrderNumber,
  fetchSaleOrderInventoryList,
  fetchDeliveryApplyOrderList,//参照用，不能删除
  fetchSaleOrderList,
  addSaleOrder,
  removeSaleOrder,
  updateSaleOrder,
  submitSaleOrders,
  fetchOrderDetailList,
} from '@/services/saleOrder';
import { fetchStockList } from '@/services/checkOrder'
import { fetchChildBranchList, fetchStaffList } from '@/services/branch';
import { queryStoreList } from '@/services/store';
import { queryWarehouseList } from '@/services/warehouse';
import { message } from 'antd';
import {fetchDeliveryOrderList } from '@/services/deliveryOrder';

export default {
  namespace: 'saleOrder',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //订单编号
    orderNumber: '',
    //销售出库单存货列表
    saleOrderInventoryList: [],
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //部门列表
    branchList: {
      rows: [],
      pagination: {},
    },
    //调货申请单列表-参照用
    deliveryApplyOrderList: {
      rows: [],
      pagination: {},
    },
    //销售出库单列表
    saleOrderList: {
      rows: [],
      pagination: {},
    },
    //仓库列表
    warehouseList: {
      rows: [],
      pagination: {},
    },
    //库存列表
    stockList: {
      rows: [],
      pagination: {},
    },
    //调货单列表
    deliveryOrderList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
        return;
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOrderNumber({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderNumber, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOrderNumber',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchSaleOrderInventoryList({ payload, callback }, { call, put }) {
      const response = yield call(fetchSaleOrderInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSaleOrderInventoryList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      // debugger
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchChildBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryApplyOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryApplyOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryApplyOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchSaleOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchSaleOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSaleOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload, callback }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        yield put({
          type: 'saveWarehouseList',
          payload: response.data,
        });
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *fetchStockList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStockList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStockList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchDeliveryOrderList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryOrderList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryOrderList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchOrderDetailList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderDetailList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
    *addSaleOrder({ payload, callback }, { call, put }) {
      const response = yield call(addSaleOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        // yield put({
        //   type: 'fetchInventoryClassificationList',
        // });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeSaleOrder({ payload, callback }, { call, put }) {
      const response = yield call(removeSaleOrder, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateSaleOrder({ payload, callback }, { call, put }) {
      const response = yield call(updateSaleOrder, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *submitSaleOrders({ payload, callback }, { call, put }) {
      const response = yield call(submitSaleOrders, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveOrderNumber(state, action) {
      return {
        ...state,
        orderNumber: action.payload,
      };
    },
    saveSaleOrderInventoryList(state, action) {
      return {
        ...state,
        saleOrderInventoryList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveDeliveryApplyOrderList(state, action) {
      return {
        ...state,
        deliveryApplyOrderList: action.payload,
      };
    },
    saveSaleOrderList(state, action) {
      return {
        ...state,
        saleOrderList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
    saveStockList(state, action) {
      return {
        ...state,
        stockList: action.payload,
      };
    },
    saveDeliveryOrderList(state, action) {
      return {
        ...state,
        deliveryOrderList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //存货分类
  //       if (location.pathname === '/basic-data/inventory-classification') {
  //         dispatch({
  //           type: 'fetchInventoryClassificationList',
  //         })
  //       }
  //       //存货管理
  //       if (location.pathname === '/basic-data/inventory-management') {
  //         dispatch({
  //           type: 'fetchInventoryList',
  //         })
  //       }
  //       //仓库管理
  //       if (location.pathname === '/basic-data/Warehouse-management') {
  //         dispatch({
  //           type: 'fetchWarehouseList',
  //         })
  //       }
  //     });
  //   },
  // },
};
