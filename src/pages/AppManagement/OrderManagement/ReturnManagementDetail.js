import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider, Row, Col } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './ReturnManagementDetail.less';
import NP from 'number-precision';

const { Description } = DescriptionList;

const renderContent = (value, row, index) => {
  const obj = {
    children: value,
    props: {},
  };
  // if (index === 4) {
  obj.props.colSpan = 0;
  // }
  return obj;
};

@connect(({ returnGood, loading }) => ({
  returnGood,
  // loading: loading.effects['profile/fetchBasic'],
}))
class ReturnManagementDetail extends Component {
  state = {
    dataSource: [],
    record: {}
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const orderId = record.orderId;
    if (orderId) {
      dispatch({
        type: 'returnGood/fetchOrderDetailList',
        payload: {
          orderId
        },
        callback: (data) => {
          this.setState({ dataSource: data.goodsList, record: data })
        }
      })
    }
  }

  columns = [
    {
      title: '商品名称',
      dataIndex: 'name',
      render: (text, record) => {
        return {
          children: (
            <div>
              <Row>
                <Col span={24}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{record.name}</span></Col>
              </Row>
              <Row>
                <Col span={5} offset={4} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{record.stockNum}</span></Col>
                <Col span={5} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{record.referencePrice}</span></Col>
                <Col span={4} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{record.number}</span></Col>
                {/*<Col span={4} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{((record.referencePrice*100) * parseInt(record.number))/100}</span></Col>*/}
                <Col span={4} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{NP.times(record.number, record.referencePrice)}</span></Col>
              </Row>
            </div>
          ),
          props: {
            colSpan: 5,
          },
        }
      }
    },
    {
      title: '商品编码',
      dataIndex: 'stockNum',
      render: renderContent,
    },
    {
      title: '单价(¥)',
      // dataIndex: 'price'
      dataIndex: 'referencePrice',
      render: renderContent,
    },
    {
      title: '数量',
      dataIndex: 'number',
      render: renderContent,
    },
    {
      title: '小计(¥)',
      dataIndex: '',
      render: renderContent,
      // render: (text, record) => record.number*record.referencePrice
    },
  ]

  getStatus = (status) => {
    let result = '';

    if (!status && status !== 0) return result;

    switch (status.toString()) {
      case '0':
        result = '待付款';
        break;
      case '1':
        result = '待收货/待自取';
        break;
      case '2':
        result = '待评价';
        break;
      case '3':
        result = '已完成';
        break;
      case '4':
        result = '已关闭';
        break;
      case '5':
        result = '待确认';
        break;
      case '6':
        result = '待出库';
        break;
      case '7':
        result = '待配送';
        break;
    }
    return result;
  };

  getPaymentMethod = (paymentMethod) => {
    let result = '';

    if (!paymentMethod) return result;

    switch (paymentMethod.toString()) {
      case '0':
        result = '微信支付';
        break;
      case '1':
        result = '支付宝支付';
        break;
      case '2':
        result = '会员支付';
        break;
      case '3':
        result = '现金支付';
        break;
      case '4':
        result = '买赠支付';
        break;
    }
    return result;
  }

  render() {
    // const { record } = this.props;
    const { dataSource, record } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="small" className={styles.font} title={<span className={styles.font} style={{marginLeft: 5}}>基本信息</span>} style={{ marginBottom: 16, marginLeft: 15 }} col={1}>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="订单编号"><span className={styles.font}>{record.orderNumber}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="下单时间"><span className={styles.font}>{record.createTime}</span></Description>
            {/*<Description style={{ display: 'block', boxSizing: 'border-box' }} term="订单状态"><span className={styles.font}>{this.getStatus(record.status)}</span></Description>*/}
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="原总额"><span className={styles.font}>¥ {record.orderPrice}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="付款金额"><span className={styles.font} style={{color: 'red'}}>¥ {record.actualPrice}</span></Description>
            {/*<Description style={{ display: 'block', boxSizing: 'border-box' }} term="支付方式"><span className={styles.font}>{this.getPaymentMethod(record.paymentMethod)}</span></Description>*/}
            {/*<Description style={{ display: 'block', boxSizing: 'border-box' }} term="运费"><span className={styles.font}>¥ {record.distributionCost}</span></Description>*/}
            {/*<Description style={{ display: 'block', boxSizing: 'border-box' }} term={<span style={{fontWeight: 'bolder'}}>优惠</span>}><span style={{fontWeight: 'bolder'}}><span className={styles.font}>¥ {record.couponAmount}</span></span></Description>*/}
            {/*<Description style={{ display: 'block', boxSizing: 'border-box' }} term="取货时间"><span className={styles.font}>{record.deliveryTime}</span></Description>*/}
            {/*<Description style={{ display: 'block', boxSizing: 'border-box' }} term="留言"><span className={styles.font}>{record.remark}</span></Description>*/}
          </DescriptionList>
          {/*<Divider style={{ marginBottom: 32 }} />*/}
          <DescriptionList size="small" className={styles.font} title={<span className={styles.font} style={{marginLeft: 5}}>发票信息</span>} style={{ marginBottom: 16, marginLeft: 15 }} col={1}>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="抬头"><span className={styles.font}>{record.rise}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="税号"><span className={styles.font}>{record.taxpayerNum}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="邮箱"><span className={styles.font}>{record.mailbox}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="注册电话"><span className={styles.font}>{record.registerPhone}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="注册地址"><span className={styles.font}>{record.registerAddress}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="开户银行"><span className={styles.font}>{record.bank}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="开户账号"><span className={styles.font}>{record.account}</span></Description>
          </DescriptionList>
          {/*<Divider style={{ marginBottom: 32 }} />*/}
          {/*<div className={styles.title}>商品明细</div>*/}
          <Table
            // style={{ marginBottom: 24 }}
            // loading={loading}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="id"
            pagination={false}
            size='small'
          />
        </div>
      </div>
    );
  }
}

export default ReturnManagementDetail;
