import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './ReturnInListDetail.less';

const { Description } = DescriptionList;

@connect(({ returnIn, loading }) => ({
  returnIn,
  // loading: loading.effects['profile/fetchBasic'],
}))
class ReturnInListDetail extends Component {
  state = {
    dataSource: []
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const returnGoodsPutInId = record.returnGoodsPutInId;
    if (returnGoodsPutInId) {
      dispatch({
        type: 'returnIn/fetchReturnInInventoryList',
        payload: {
          returnGoodsPutInIds: returnGoodsPutInId
        },
        callback: (data) => {
          this.setState({ dataSource: data })
        }
      })
    }
  }

  columns = [
    // {
    //   title: '序号',
    //   dataIndex: '',
    //   // width: 60,
    //   // fixed: 'left',
    //   render: (text, record, index) => index+1
    // },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      // width: 100,
      // fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      // width: 200,
      // fixed: 'left',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '单位',
      dataIndex: 'units',
    },
    // {
    //   title: '分类',
    //   dataIndex: 'stockClassify',
    // },
    // {
    //   title: '品牌',
    //   dataIndex: 'brand',
    // },
    // {
    //   title: '仓库',
    //   dataIndex: 'warehouse',
    // },
    {
      title: '入库数量',
      dataIndex: 'number',
    },
    // {
    //   title: '供应商',
    //   dataIndex: 'supplier',
    // },
    // {
    //   title: '备注',
    //   dataIndex: 'remark',
    // },
  ];

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="large" title="基本信息" style={{ marginBottom: 32 }}>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="订单编号">{record.orderNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库时间">{record.putInTime}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库人员">{record.putInBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库仓库">{record.warehouse}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="入库批次">{record.batchNumber}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="制单人员">{record.createBy}</Description>
            <Description style={{ display: 'block', boxSizing: 'border-box', width: '33.33333333%' }} term="备注">{record.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>入库明细</div>
          <Table
            style={{ marginBottom: 24 }}
            pagination={{defaultPageSize: 9}}
            size='small'
            // loading={loading}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="rgpiStockId"
            // scroll={{x: 1500}}
          />
        </div>
      </div>
    );
  }
}

export default ReturnInListDetail;
