import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Radio, Button, Modal, Divider, Popconfirm, Col, Row, DatePicker, message, Table, InputNumber } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isReturnManagementNameRepeated } from '@/services/returnGood';
import DescriptionList from '@/components/DescriptionList';
import ReactToPrint from "react-to-print";
import styles from './ReturnManagement.less';
import ReturnManagementDetail from './ReturnManagementDetail'
import router from 'umi/router';
import {apiDomainName, imgDomainName} from "../../../constants";
import ImagePreviewModal from '@/components/ImagePreviewModal';
import { stringify } from 'qs';

const { Description } = DescriptionList;
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CheckForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="审核"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="审核">
        {form.getFieldDecorator('code', {
          rules: [{ required: true, message: '请选择是否同意退货申请！' }],
          initialValue: 1
        })(
          <RadioGroup>
            <Radio value={1}>同意</Radio>
            <Radio value={0}>不同意</Radio>
          </RadioGroup>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="理由">
        {form.getFieldDecorator('reason')(<Input.TextArea rows={3} placeholder="请输入理由" />)}
      </FormItem>
    </Modal>
  );
});

const RefundForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="退款"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="支付金额">
        ¥ {record.actualPrice}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="退款金额">
        {form.getFieldDecorator('refundAmount', {
          initialValue: record.actualPrice
        })(
          <InputNumber precision={2} style={{width: '100%'}} placeholder="请输入退款金额" min={0} />
        )}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isReturnManagementNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改存货分类"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(
          <Input.TextArea rows={3} placeholder="请输入备注" />
        )}
      </FormItem>
    </Modal>
  );
});

const OrderDetail = props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      width={500}
      visible={detailModalVisible}
      // onOk={this.handleOk}
      destroyOnClose
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{marginLeft: 15}}>
          退货详情
          <span
            style={{float: 'right'}}
          >
            <ReactToPrint
              trigger={() => <a href="#"><Icon type="printer" /></a>}
              content={() => {
                console.log('componentRef: ', componentRef);
                return componentRef
              }}
            />
          </span>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <ReturnManagementDetail record={record}/>
    </Modal>
  );
}

/* eslint react/no-multi-comp:0 */
@connect(({ returnGood, user, loading }) => ({
  returnGood,
  user,
  loading: loading.models.returnGood,
}))
@Form.create()
class ReturnManagement extends PureComponent {
  state = {
    checkModalVisible: false,
    updateModalVisible: false,
    detailModalVisible: false,
    refundModalVisible: false,
    previewVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: { },
    dataPermission: false,
  };

  columns = [
    {
      title: '申请时间',
      dataIndex: 'createTime',
      width: 150,
      fixed: 'left',
    },
    {
      title: '订单编号',
      dataIndex: 'normalOrderNumber',
      width: 150,
      fixed: 'left',
      render: (text, record) => <a onClick={() => this.handleDetailModalVisible(true, record)}>{text}</a>
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '待审核';
            break;
          case '1':
            result = '待签收';
            break;
          case '2':
            result = '待入库';
            break;
          case '3':
            result = '已关闭';
            break;
          case '4':
            result = '待退款';
            break;
          case '5':
            result = '已退款';
            break;
        }
        return result;
      }
    },
    {
      title: '留言',
      dataIndex: 'explain',
    },
    {
      title: '退货方式',
      dataIndex: 'refundMethod',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '物流配送';
            break;
          case '1':
            result = '自送门店';
            break;
        }
        return result;
      }
    },
    {
      title: '物流单号',
      dataIndex: 'logisticsNumber',
    },
    {
      title: '申请人',
      dataIndex: 'nickName',
    },
    {
      title: '申请人电话',
      dataIndex: 'applyPhone',
    },
    {
      title: '门店',
      dataIndex: 'store',
    },
    {
      title: '操作',
      width: 70,
      fixed: 'right',
      align: 'center',
      render: (text, record) => (
        <Fragment>
          {/*<a>打印</a>*/}
          <div style={{display: record.imageUrls ? 'inline-block' : 'none'}}>
            {/*<Divider type="vertical" />*/}
            <a onClick={() => this.setState({ previewVisible: true, record })} >查看</a>
          </div>
        </Fragment>
      ),
    },
  ];

  recordRemove = stockClassifyIds => {
    console.log('stockClassifyIds: ', stockClassifyIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'returnGood/removeReturnManagement',
      payload: { stockClassifyIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'returnGood/changeReturnManagementStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //获取门店列表
    dispatch({
      type: 'returnGood/fetchStoreList',
    });

    /*
    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '数据权限') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      this.fetchOrderList({});
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const storeId =  currentUser.personnel.storeId;
      //2、判断是否是门店员工
      if (storeId || storeId === 1) {
        console.log('是门店员工')
        this.fetchOrderList({
          storeId
        });
      }
      else{
        console.log('不是门店员工')
      }
    }
    */
    this.fetchOrderList();
  }

  fetchOrderList = (payload = {}) => {
    const { dispatch, user: { currentUser } } = this.props;

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      dispatch({
        type: 'returnGood/fetchOrderList',
        payload
      });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const storeId =  currentUser.personnel.storeId;
      //2、判断是否是门店员工
      if (storeId || storeId === 1) {
        console.log('是门店员工')
        dispatch({
          type: 'returnGood/fetchOrderList',
          payload: {
            ...payload,
            storeId
          }
        });
      }
      else{
        console.log('不是门店员工')
      }
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'returnGood/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleCheckModalVisible = flag => {
    this.setState({
      checkModalVisible: !!flag,
    });
  };

  handleRefundModalVisible = flag => {
    this.setState({
      refundModalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetailModalVisible = (flag, record) => {
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleCheck = fields => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    dispatch({
      type: 'returnGood/confirmOrder',
      payload: {
        ...fields,
        returnGoodsId: selectedRows[0].returnGoodsId,
      },
      callback: this.refreshSelectedRows,
      formValues: this.getFormValues()
    });

    // message.success('新增成功');
    this.handleCheckModalVisible();
  };

  getFormValues = () => {
    const { form } = this.props;
    const formData = form.getFieldsValue();
    const startTime = formData.startTime;
    const endTime = formData.endTime;
    if (startTime) {
      formData.startTime = startTime.format('YYYY-MM-DD HH:mm');
    }
    if (endTime) {
      formData.endTime = endTime.format('YYYY-MM-DD HH:mm');
    }
    return { ...formData }
  }

  handleRefund = fields => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    dispatch({
      type: 'returnGood/refund',
      payload: {
        ...fields,
        returnGoodsId: selectedRows[0].returnGoodsId,
      },
      callback: this.refreshSelectedRows,
      formValues: this.getFormValues()
      // callback: () => {
      //   this.setState({ selectedRows: [] })
      // }
    });

    // message.success('新增成功');
    this.handleRefundModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'returnGood/updateReturnManagement',
      payload: {
        ...fields,
        stockClassifyId: record.stockClassifyId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.fetchOrderList({
      ...form.getFieldsValue()
    })
    // dispatch({
    //   type: 'returnGood/fetchOrderList',
    //   payload: {
    //     ...form.getFieldsValue()
    //   }
    // });

    //解决bug(防止类似门店自取的bug产生)：后台选择订单，执行"确认"、"出库"操作，app端"确认收货",此时选择某行再进行"查询"、"重置"操作后，行选择未清除
    this.setState({ selectedRows: [] });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const startTime = fieldsValue.startTime;
      const endTime = fieldsValue.endTime;
      if (startTime) {
        fieldsValue.startTime = startTime.format('YYYY-MM-DD HH:mm');
      }
      if (endTime) {
        fieldsValue.endTime = endTime.format('YYYY-MM-DD HH:mm');
      }

      // dispatch({
      //   type: 'returnGood/fetchOrderList',
      //   payload: {
      //     ...fieldsValue,
      //   }
      // });

      this.fetchOrderList({
        ...fieldsValue,
      });

      //解决bug(防止类似门店自取的bug产生)：后台选择订单，执行"确认"、"出库"操作，app端"确认收货",此时选择某行再进行"查询"、"重置"操作后，行选择未清除
      this.setState({ selectedRows: [] });
    });
  };

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const startTime = fieldsValue.startTime;
      const endTime = fieldsValue.endTime;
      if (startTime) {
        fieldsValue.startTime = startTime.format('YYYY-MM-DD HH:mm');
      }
      if (endTime) {
        fieldsValue.endTime = endTime.format('YYYY-MM-DD HH:mm');
      }
      const params = {
        ...fieldsValue,
        personnelId: currentUser.personnelId
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/returnGoods/exportList?${stringify(params)}`;
    });
  }

  renderSimpleForm() {
    const {
      returnGood: { storeList },
      form: { getFieldDecorator },
      user: { currentUser },
    } = this.props;
    const { dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label='订单编号'>
              {getFieldDecorator('normalOrderNumber')(<Input placeholder="请输入订单号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="订单状态">
              {getFieldDecorator('status')(
                <Select allowClear placeholder="请选择订单状态" style={{ width: '100%' }}>
                  <Option value={0}>待审核</Option>
                  {/*<Option value={1}>待收货/待自取</Option>*/}
                  <Option value={1}>待签收</Option>
                  <Option value={2}>待入库</Option>
                  <Option value={3}>已关闭</Option>
                  <Option value={4}>待退款</Option>
                  <Option value={5}>已退款</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="门店">
              {getFieldDecorator('storeId', {
                initialValue: dataPermission ? '' : currentUser.personnel.storeId
              })(
                <Select allowClear disabled={!dataPermission} placeholder="请选择门店" style={{ width: '100%' }}>
                  {storeList.rows.map(item => <Select.Option key={item.storeId} value={item.storeId}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="开始时间">
              {getFieldDecorator('startTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择开始时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择结束时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    // if (['0', '2', '3', '4'].includes(record.status.toString())) return;

    /*
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.returnGoodsId === record.returnGoodsId) {
          newSelectedRows = newSelectedRows.filter(item => item.returnGoodsId !== record.returnGoodsId);
          flag = false;
          break;
        }
      }
      if (flag){
        newSelectedRows.push(record)
      }
      return ({
        selectedRows: newSelectedRows
      })
    })
    */
    this.setState({ selectedRows: [record] })
  }

  collectGoods = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    dispatch({
      type: 'returnGood/confirmReceipt',
      payload: {
        returnGoodsId: selectedRows[0].returnGoodsId,
      },
      callback: this.refreshSelectedRows,
      formValues: this.getFormValues()
      // callback: () => {
      //   this.setState({ selectedRows: [] })
      // }
    });
  }

  cancelOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    dispatch({
      type: 'returnGood/cancelOrder',
      payload: {
        returnGoodsId: selectedRows[0].returnGoodsId,
      },
      callback: () => {
        this.setState({ selectedRows: [] })
      }
    });
  }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   const { returnGood: { orderList } }  = nextProps;
  //   const { selectedRows } = prevState;
  //   let selectedNewRows = [];
  //   if (selectedRows.length > 0) {
  //     selectedNewRows = orderList.rows.find(item => item.returnGoodsId === selectedRows[0].returnGoodsId)
  //   }
  //   return {
  //     selectedRows:selectedNewRows
  //   }
  // }

  confirmOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    console.log('selectedRows: ', selectedRows);

    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    this.handleCheckModalVisible(true);
  }

  refreshSelectedRows = (orderList) => {
    const { selectedRows } = this.state;
    let selectedNewRows = [];
    if (selectedRows.length > 0) {
      const target = orderList.rows.find(item => item.returnGoodsId === selectedRows[0].returnGoodsId);
      selectedNewRows = target ? [target] : [];
      // selectedNewRows = [orderList.rows.find(item => item.returnGoodsId === selectedRows[0].returnGoodsId)]
    }
    this.setState({ selectedRows:selectedNewRows })
  }

  inOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    //测试出库代码，待销售出库提交接口完整后，删掉此块代码
    // dispatch({
    //   type: 'returnGood/outOrder',
    //   payload: {
    //     returnGoodsId: selectedRows[0].returnGoodsId,
    //   }
    // });
    //------------------------------------------------

    router.push({
      pathname: '/app-management/order-management/return-in',
      query: {
        returnGoodsId: selectedRows[0].returnGoodsId,
        orderId: selectedRows[0].orderId
      }
    });
  }

  refund = () => {
    this.handleRefundModalVisible(true);

    const { selectedRows } = this.state;
    if (selectedRows.length > 0) {
      this.setState({ record: selectedRows[0] })
    }
  }

  render() {
    const {
      returnGood: { orderList },
      loading,
    } = this.props;
    const { selectedRows, updateModalVisible, checkModalVisible, record, detailModalVisible, previewVisible, refundModalVisible } = this.state;

    const parentMethods = {
      handleAdd: this.handleCheck,
      handleModalVisible: this.handleCheckModalVisible,
    };

    const refundMethods = {
      handleAdd: this.handleRefund,
      handleModalVisible: this.handleRefundModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    const selectedRowStatus = selectedRows.length > 0 ? selectedRows[0].status : '';

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button type='primary' onClick={this.confirmOrder} disabled={selectedRowStatus.toString() !== '0'}>审核</Button>
              <Button type='primary' onClick={this.collectGoods} disabled={selectedRowStatus.toString() !== '1'}>签收</Button>
              <Button type='primary' onClick={this.inOrder} disabled={selectedRowStatus.toString() !== '2'}>入库</Button>
              <Button type='primary' onClick={this.refund} disabled={selectedRowStatus.toString() !== '4'}>退款</Button>
              {/*<Button type='primary' onClick={this.confirmOrder} disabled={selectedRowStatus.toString() !== '5'}>确认订单</Button>*/}
              {/*<Button type='primary' onClick={this.outOrder} disabled={selectedRowStatus.toString() !== '6'}>出库</Button>*/}
              {/*<Button type='primary' onClick={this.collectGoods} disabled={selectedRowStatus.toString() !== '1'}>确认收货</Button>*/}
              {/*<Button type='primary' onClick={this.cancelOrder} disabled={!['0', '1', '5', '6'].includes(selectedRowStatus.toString())}>取消订单</Button>*/}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={orderList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="returnGoodsId"
              scroll={{x: 1500}}
              size='small'
              // checkboxProps={record => ({
              //   disabled: ['0', '2', '3', '4'].includes(record.status.toString()),
              // })}
              onRow={(record) => {
                return {
                  onClick: () => this.onRowClick(record),
                };
              }}
              type='radio'
            />
          </div>
        </Card>
        <CheckForm {...parentMethods} modalVisible={checkModalVisible} />
        <RefundForm {...refundMethods} modalVisible={refundModalVisible} record={record}/>
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
        {/*<UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />*/}
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={record.imageUrls ? record.imageUrls.split(',') : []}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={152}
          imgHeight={'auto'}
          colSpan={4}
          width={1040}
          title='退货图片'
        />
      </div>
    );
  }
}

export default ReturnManagement;
