import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Col, Row, DatePicker, message, Table } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isStoreGainNameRepeated } from '@/services/wineCellar';
import DescriptionList from '@/components/DescriptionList';
import styles from './KeepWineManagement.less';
import KeepWineDetail from './KeepWineDetail'
import router from 'umi/router';
import ReactToPrint from "react-to-print";
import {apiDomainName} from "../../../constants";
import { stringify } from 'qs';
import NP from 'number-precision';

const { Description } = DescriptionList;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };
  const checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isStoreGainNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改存货分类"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入名称！' }, { validator: checkName }],
          initialValue: record.name,
        })(<Input placeholder="请输入名称" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', { initialValue: record.remark })(
          <Input.TextArea rows={3} placeholder="请输入备注" />
        )}
      </FormItem>
    </Modal>
  );
});

const OrderDetail = props => {
  const { record, detailModalVisible, handleDetailModalVisible } = props;
  let componentRef;
  return (
    <Modal
      width={500}
      visible={detailModalVisible}
      // onOk={this.handleOk}
      destroyOnClose
      onCancel={() => handleDetailModalVisible()}
      title={
        <div style={{marginLeft: 15}}>
          <Row>
            <Col span={20}>
              <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{record.store}</span>
            </Col>
            <Col span={4}>
              <span
                style={{float: 'right'}}
              >
                <ReactToPrint
                  trigger={() => <a href="#"><Icon type="printer" /></a>}
                  content={() => {
                    console.log('componentRef: ', componentRef);
                    return componentRef
                  }}
                />
              </span>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <span style={{color: 'rgba(0, 0, 0, 0.85)', fontWeight: 900}}>{`地址: ${record.storeAddress}`}</span>
            </Col>
          </Row>
        </div>
      }
      closable={false}
      footer={[]}
      ref={el => (componentRef = el)}
      mask={false}
      className={styles.customDetailModal}
    >
      <KeepWineDetail record={record}/>
    </Modal>
  );
}

/* eslint react/no-multi-comp:0 */
@connect(({ wineCellar, user, loading }) => ({
  wineCellar,
  user,
  loading: loading.models.wineCellar,
}))
@Form.create()
class KeepWineManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    detailModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    expandedRowKeys: [],
    dataSource: [],
  };

  columns = [
    {
      title: '下单时间',
      dataIndex: 'createTime',
      // width: 150,
      // fixed: 'left',
    },
    {
      title: '订单编号',
      dataIndex: 'orderNumber',
      // width: 150,
      // fixed: 'left',
      render: (text, record) => <a onClick={() => this.handleDetailModalVisible(true, record)}>{text}</a>
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '待付款';
            break;
          case '1':
            // result = '待收货/待自取';
            result = '待自取';
            break;
          case '2':
            result = '待评价';
            break;
          case '3':
            result = '已完成';
            break;
          case '4':
            result = '已关闭';
            break;
          case '5':
            result = '待确认';
            break;
          case '6':
            result = '待出库';
            break;
        }
        return result;
      }
    },
    {
      title: '原总额(¥)',
      dataIndex: 'orderPrice',
    },
    {
      title: '付款金额(¥)',
      dataIndex: 'actualPrice',
    },
    {
      title: '支付方式',
      dataIndex: 'paymentMethod',
      render: text => {
        let result = '';
        switch (text.toString()) {
          case '0':
            result = '微信支付';
            break;
          case '1':
            result = '支付宝支付';
            break;
          case '2':
            result = '会员支付';
            break;
          case '3':
            result = '现金支付';
            break;
          case '4':
            result = '买赠支付';
            break;
        }
        return result;
      }
    },
    // {
    //   title: '取货时间',
    //   dataIndex: 'deliveryTime',
    // },
    {
      title: '留言',
      dataIndex: 'remark',
    },
    // {
    //   title: '发票',
    //   dataIndex: 'rise',
    // },
    {
      title: '下单门店',
      dataIndex: 'store',
    },
    // {
    //   title: '操作',
    //   // width: 60,
    //   // fixed: 'right',
    //   align: 'center',
    //   render: (text, record) => (
    //     <Fragment>
    //       <a>打印</a>
    //     </Fragment>
    //   ),
    // },
  ];

  recordRemove = stockClassifyIds => {
    console.log('stockClassifyIds: ', stockClassifyIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'wineCellar/removeStoreGain',
      payload: { stockClassifyIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'wineCellar/changeStoreGainStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;

    //获取门店列表
    dispatch({
      type: 'wineCellar/fetchStoreList',
    });

    this.fetchOrderList();
  }

  fetchOrderList = (payload = {}) => {
    const { dispatch, user: { currentUser } } = this.props;

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    //是否有数据权限
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
      dispatch({
        type: 'wineCellar/fetchOrderList',
        payload: {
          ...payload,
          type: 2,
        }
      });
    }
    else{
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
      const storeId =  currentUser.personnel.storeId;
      //2、判断是否是门店员工
      if (storeId || storeId === 1) {
        console.log('是门店员工')
        dispatch({
          type: 'wineCellar/fetchOrderList',
          payload: {
            ...payload,
            type: 2,
            storeId,
          }
        });
      }
      else{
        console.log('不是门店员工')
      }
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'wineCellar/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleDetailModalVisible = (flag, record) => {
    this.setState({
      detailModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'wineCellar/addStoreGain',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'wineCellar/updateStoreGain',
      payload: {
        ...fields,
        stockClassifyId: record.stockClassifyId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.fetchOrderList({
      ...form.getFieldsValue(),
      type: 2
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const startTime = fieldsValue.startTime;
      const endTime = fieldsValue.endTime;
      if (startTime) {
        fieldsValue.startTime = startTime.format('YYYY-MM-DD HH:mm');
      }
      if (endTime) {
        fieldsValue.endTime = endTime.format('YYYY-MM-DD HH:mm');
      }

      this.fetchOrderList({
        ...fieldsValue,
        type: 2
      });
    });
  };

  export = () => {
    const {
      form,
      user: { currentUser },
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const startTime = fieldsValue.startTime;
      const endTime = fieldsValue.endTime;
      if (startTime) {
        fieldsValue.startTime = startTime.format('YYYY-MM-DD HH:mm');
      }
      if (endTime) {
        fieldsValue.endTime = endTime.format('YYYY-MM-DD HH:mm');
      }

      const params = {
        ...fieldsValue,
        type: 2,
        personnelId: currentUser.personnelId
      };
      console.log('fieldsValue: ', fieldsValue);
      window.location.href = `${apiDomainName}/server/order/exportList?${stringify(params)}`;
    });
  }

  renderSimpleForm() {
    const {
      wineCellar: { storeList },
      form: { getFieldDecorator },
      user: { currentUser }
    } = this.props;
    const { dataPermission } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label='订单编号'>
              {getFieldDecorator('orderNumber')(<Input placeholder="请输入订单号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="订单状态">
              {getFieldDecorator('status')(
                <Select allowClear placeholder="请选择订单状态" style={{ width: '100%' }}>
                  <Option value={0}>待付款</Option>
                  {/*<Option value={1}>待收货/待自取</Option>*/}
                  <Option value={1}>待自取</Option>
                  <Option value={2}>待评价</Option>
                  <Option value={3}>已完成</Option>
                  <Option value={4}>已关闭</Option>
                  <Option value={5}>待确认</Option>
                  <Option value={6}>待出库</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="门店">
              {getFieldDecorator('storeId', {
                initialValue: dataPermission ? '' : currentUser.personnel.storeId
              })(
                <Select
                  allowClear
                  disabled={!dataPermission}
                  placeholder="请选择门店"
                  style={{ width: '100%' }}
                >
                  {storeList.rows.map(item => <Select.Option key={item.storeId} value={item.storeId}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="开始时间">
              {getFieldDecorator('startTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择开始时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime')(
                <DatePicker
                  style={{width: '100%'}}
                  showTime={{ format: 'HH:mm' }}
                  format="YYYY-MM-DD HH:mm"
                  placeholder="请选择结束时间"
                />
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.export}>
                导出
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  onRowClick = (record) => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);

    if (['0', '2', '3', '4'].includes(record.status.toString())) return;

    // this.setState(prevState => {
    //   let newSelectedRows = [...prevState.selectedRows];
    //   let flag = true;
    //   for (let item of prevState.selectedRows) {
    //     if (item.orderId === record.orderId) {
    //       newSelectedRows = newSelectedRows.filter(item => item.orderId !== record.orderId);
    //       flag = false;
    //       break;
    //     }
    //   }
    //   if (flag){
    //     newSelectedRows.push(record)
    //   }
    //   return ({
    //     selectedRows: newSelectedRows
    //   })
    // })
    this.setState({ selectedRows: [record] })
  }

  collectGoods = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    dispatch({
      type: 'wineCellar/changeDistributionStatus',
      payload: {
        orderId: selectedRows[0].orderId,
        distributionStatus: 2
      }
    });
  }

  confirmOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    console.log('selectedRows: ', selectedRows);

    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    dispatch({
      type: 'wineCellar/confirmOrder',
      payload: {
        orderId: selectedRows[0].orderId,
      },
      callback: this.setState({ selectedRows: [] })
    });
  }


  // refreshSelectedRows = (orderList) => {
  //   const { selectedRows } = this.state;
  //   let selectedNewRows = [];
  //   if (selectedRows.length > 0) {
  //     selectedNewRows = [orderList.rows.find(item => item.orderId === selectedRows[0].orderId)]
  //   }
  //   this.setState({ selectedRows:selectedNewRows })
  // }

  outOrder = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (!selectedRows.length > 0) {
      message.warning('请先选择订单!');
      return;
    }

    //测试出库代码，待销售出库提交接口完整后，删掉此块代码
    // dispatch({
    //   type: 'wineCellar/outOrder',
    //   payload: {
    //     orderId: selectedRows[0].orderId,
    //   }
    // });
    //------------------------------------------------

    router.push({
      pathname: '/app-management/order-management/sale-out-order',
      query: {
        orderId: selectedRows[0].orderId,
        type: 0
      }
    });
  }

  expandedRowRender = record => {
    // const {
    //   saleOrder: { saleOrderInventoryList },
    // } = this.props;
    const { dataSource } = this.state;

    const columns = [
      {
        title: '商品名称',
        dataIndex: 'name',
      },
      {
        title: '商品编号',
        dataIndex: 'stockNum',
      },
      {
        title: '单价(¥)',
        dataIndex: 'referencePrice',
      },
      {
        title: '数量',
        dataIndex: 'number',
      },
      {
        title: '小计(¥)',
        dataIndex: '',
        render: (text, record) => NP.times(record.number, record.referencePrice)
      },
    ];

    return (
      <Row>
        <Col span={12}>
          <Table
            size='small'
            columns={columns}
            dataSource={dataSource}
            // footer={() => `合计: ${dataSource.length}`}
            rowKey='id'
          />
        </Col>
        <Col span={12}>
          取酒信息区域
        </Col>
      </Row>
    )
  };

  onExpand = (expanded, record) => {
    console.log('expanded: ', expanded);
    console.log('record: ', record);
    const { dispatch } = this.props;
    if (expanded) {
      dispatch({
        type: 'wineCellar/fetchOrderDetailList',
        payload: {
          orderId: record.orderId
        },
        callback: (data) => {
          this.setState({ dataSource: data.goodsList })
        }
      })
    }
  }

  onExpandedRowsChange = (expandedRows) => {
    this.setState({ expandedRowKeys: expandedRows.length > 0 ? [expandedRows[expandedRows.length-1]] : [] })
  }

  render() {
    const {
      wineCellar: { orderList },
      loading,
    } = this.props;
    const { selectedRows, expandedRowKeys, updateModalVisible, record, detailModalVisible } = this.state;

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const detailMethods = {
      handleDetailModalVisible: this.handleDetailModalVisible,
    };

    const selectedRowStatus = selectedRows.length > 0 ? selectedRows[0].status : '';

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              {/*<Button type='primary' onClick={this.confirmOrder} disabled={selectedRowStatus.toString() !== '5'}>确认订单</Button>*/}
              {/*<Button type='primary' onClick={this.outOrder}>出库</Button>*/}
              {/*<Button type='primary' onClick={this.collectGoods}>确认收货</Button>*/}
              {/*<Button type='primary' >打印订单</Button>*/}
              {/*<Button type='primary' >取消订单</Button>*/}
            </div>
            <StandardTable
              // selectedRows={selectedRows}
              loading={loading}
              data={orderList}
              columns={this.columns}
              // onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="orderId"
              // scroll={{x: 1000}}
              size='small'
              // checkboxProps={record => ({
              //   disabled: ['0', '2', '3', '4'].includes(record.status.toString()),
              // })}
              // onRow={(record) => {
              //   return {
              //     onClick: () => this.onRowClick(record),
              //   };
              // }}
              type='radio'
              expandedRowKeys={expandedRowKeys}
              expandedRowRender={this.expandedRowRender}
              onExpand={this.onExpand}
              onExpandedRowsChange={this.onExpandedRowsChange}
            />
          </div>
        </Card>
        <OrderDetail {...detailMethods} detailModalVisible={detailModalVisible} record={record}/>
        {/*<UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />*/}
      </div>
    );
  }
}

export default KeepWineManagement;
