import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider, Row, Col } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './PickWineApplyDetail.less';

const { Description } = DescriptionList;

const renderContent = (value, row, index) => {
  const obj = {
    children: value,
    props: {},
  };
  // if (index === 4) {
  obj.props.colSpan = 0;
  // }
  return obj;
};

@connect(({ wineCellar, loading }) => ({
  wineCellar,
  // loading: loading.effects['profile/fetchBasic'],
}))
class PickWineApplyDetail extends Component {
  state = {
    dataSource: []
  }

  componentDidMount() {
    const { dispatch, record } = this.props;
    const wineCellarOrderId = record.wineCellarOrderId;
    if (wineCellarOrderId) {
      dispatch({
        type: 'wineCellar/fetchWineApplyOrderDetailList',
        payload: {
          wineCellarOrderId
        },
        callback: (data) => {
          this.setState({ dataSource: data.goodsList })
        }
      })
    }
  }

  columns = [
    {
      title: '商品名称',
      dataIndex: 'name',
      render: (text, record) => {
        return {
          children: (
            <div>
              <Row>
                <Col span={24}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{record.name}</span></Col>
              </Row>
              <Row>
                <Col span={6} offset={8} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{record.stockNum}</span></Col>
                <Col span={10} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{record.number}</span></Col>
              </Row>
            </div>
          ),
          props: {
            colSpan: 5,
          },
        }
      }
    },
    {
      title: '商品编码',
      dataIndex: 'stockNum',
      render: renderContent,
    },
    // {
    //   title: '单价',
    //   // dataIndex: 'price'
    //   dataIndex: 'referencePrice'
    // },
    {
      title: '数量',
      dataIndex: 'number',
      render: renderContent,
    },
    // {
    //   title: '小计',
    //   dataIndex: '',
    //   render: (text, record) => record.number*record.referencePrice
    // },
  ]

  getStatus = (status) => {
    let result = '';

    if (!status && status !== 0) return result;

    switch (status.toString()) {
      case '0':
        result = '待付款';
        break;
      case '1':
        // result = '待收货/待自取';
        result = '待自取';
        break;
      case '2':
        result = '待评价';
        break;
      case '3':
        result = '已完成';
        break;
      case '4':
        result = '已关闭';
        break;
    }
    return result;
  };

  getPaymentMethod = (paymentMethod) => {
    let result = '';

    if (!paymentMethod) return result;

    switch (paymentMethod.toString()) {
      case '0':
        result = '微信支付';
        break;
      case '1':
        result = '支付宝支付';
        break;
      case '2':
        result = '会员支付';
        break;
      case '3':
        result = '现金支付';
        break;
      case '4':
        result = '买赠支付';
        break;
    }
    return result;
  }

  render() {
    const { record } = this.props;
    const { dataSource } = this.state;

    return (
      <div>
        <div>
          <DescriptionList size="small" className={styles.font} style={{ marginBottom: 16, marginLeft: 15 }} col={1}>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="取酒单号"><span className={styles.font}>{record.orderNumber}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term="申请时间"><span className={styles.font}>{record.createTime}</span></Description>
            <Description style={{ display: 'block', boxSizing: 'border-box' }} term={{ 1: `自取时间`, 0: record.deliveryTime === '快递配送' ? '配送方式': `送达时间` }[record.type]}><span className={styles.font}>{record.deliveryTime}</span></Description>
            <Description style={{ display: (record.type === 1 || record.deliveryTime === '快递配送') ? 'none' : 'block', boxSizing: 'border-box' }} term="防伪码"><span className={styles.font}>{record.securityCode}</span></Description>
            <Description style={{ display: (record.type === 1 || record.deliveryTime === '快递配送') ? 'none' : 'block', boxSizing: 'border-box' }} term="配送员"><span className={styles.font}>{record.distributionBy}</span></Description>
            <Description style={{ display: (record.type === 0 && record.deliveryTime === '快递配送') ? 'block' : 'none', boxSizing: 'border-box' }} term="快递编号"><span className={styles.font}>{record.courierNumber}</span></Description>
            <Description style={{ display: (record.type === 0 && record.deliveryTime === '快递配送') ? 'block' : 'none', boxSizing: 'border-box' }} term="快递名称"><span className={styles.font}>{record.courierName}</span></Description>
          </DescriptionList>
          {/*<Divider style={{ marginBottom: 32 }} />*/}
          {/*<div className={styles.title}>商品明细</div>*/}
          <Table
            // style={{ marginBottom: 24 }}
            // loading={loading}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="id"
            pagination={false}
            size='small'
          />
        </div>
      </div>
    );
  }
}

export default PickWineApplyDetail;
