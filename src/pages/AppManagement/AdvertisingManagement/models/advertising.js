// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchSoftTextList,
  fetchAdvertisingList,
  addSoftText,
  addAdvertising,
  updateSoftText,
  updateAdvertising,
  removeSoftText,
  removeAdvertising,
  changeAdvertisingStatus,
} from '@/services/advertising';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'advertising',

  state: {
    //软文列表
    softTextList: {
      rows: [],
      pagination: {},
    },
    //广告列表
    advertisingList: {
      rows: [],
      pagination: {},
    },
    //广告状态列表
    advertisingStatusList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchSoftTextList({ payload, callback }, { call, put }) {
      const response = yield call(fetchSoftTextList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveSoftTextList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchAdvertisingList({ payload, callback }, { call, put }) {
      const response = yield call(fetchAdvertisingList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveAdvertisingList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchAdvertisingStatusList({ payload }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveAdvertisingStatusList',
        payload: response.data,
      });
    },
    *addSoftText({ payload, callback }, { call, put }) {
      const response = yield call(addSoftText, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSoftTextList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addAdvertising({ payload, callback }, { call, put }) {
      const response = yield call(addAdvertising, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchAdvertisingList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeSoftText({ payload, callback }, { call, put }) {
      const response = yield call(removeSoftText, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchSoftTextList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeAdvertising({ payload, callback }, { call, put }) {
      const response = yield call(removeAdvertising, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchAdvertisingList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateSoftText({ payload, callback }, { call, put }) {
      const response = yield call(updateSoftText, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchSoftTextList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateAdvertising({ payload, callback }, { call, put }) {
      const response = yield call(updateAdvertising, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchAdvertisingList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeAdvertisingStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeAdvertisingStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchAdvertisingList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveSoftTextList(state, action) {
      return {
        ...state,
        softTextList: action.payload,
      };
    },
    saveAdvertisingList(state, action) {
      return {
        ...state,
        advertisingList: action.payload,
      };
    },
    saveAdvertisingStatusList(state, action) {
      return {
        ...state,
        advertisingStatusList: action.payload,
      };
    },
  },
  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       if (location.pathname === '/app-management/vip-management/electronic-wallet') {
  //         dispatch({
  //           type: 'fetchElectronicWalletList',
  //         })
  //       }
  //     });
  //   },
  // },
};
