import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Button, Modal, Divider, Popconfirm, Upload, Icon } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/advertising';
import styles from './SoftTextManagement.less';
import {imgDomainName} from "../../../constants";
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
import { ContentUtils } from 'braft-utils'
import { upload } from '@/services/common-api';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      fieldsValue.content = fieldsValue.content.toHTML();
      handleAdd(fieldsValue);
    });
  };

  const uploadHandler = (param) => {

    if (!param.file) {
      return false
    }

    let formData = new FormData();
    // if (param.file.size / 1024 > 1024) {
    //   Modal.error({
    //     title: '图片大小不得大于1MB',
    //     content: '请重新选择图片',
    //     okText: '确认',
    //     cancelText: '取消',
    //   })
    //   return;
    // }

    formData.append('image', param.file);

    const response = upload(formData);
    response.then(result => {
      console.log('result: ', result);
      const real = result.msg;
      form.setFieldsValue({
        content: ContentUtils.insertMedias(form.getFieldsValue().content, [{
          type: 'IMAGE',
          // url: URL.createObjectURL(param.file)
          url: `${imgDomainName}${real}`,
          width: '100%'
        }])
      });
    });
  }

  const extendControls = [
    {
      key: 'antd-uploader',
      type: 'component',
      component: (
        <Upload
          accept="image/*"
          showUploadList={false}
          customRequest={uploadHandler}
        >
          {/* 这里的按钮最好加上type="button"，以避免在表单容器中触发表单提交，用Antd的Button组件则无需如此 */}
          <button type="button" className="control-item button upload-button" data-title="插入图片">
            <Icon type="picture" theme="filled" />
          </button>
        </Upload>
      )
    }
  ]

  return (
    <Modal
      destroyOnClose
      // title="新增软文"
      title={(
        <div>
          <span>新增软文</span>
          <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
      width={1020}
    >
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 20 }} label="软文名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入软文名称！' }],
        })(
          <Input placeholder="请输入软文名称" />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 20 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 20 }} label="软文内容">
        {form.getFieldDecorator('content', {
          validateTrigger: 'onBlur',
          rules: [{
            required: true,
            validator: (_, value, callback) => {
              if (value.isEmpty()) {
                callback('请输入软文内容')
              } else {
                callback()
              }
            }
          }],
        })(
          <BraftEditor
            className="my-editor"
            // controls={controls}
            placeholder="请输入软文内容"
            style={{border: '1px solid #d9d9d9', borderRadius: 4}}
            extendControls={extendControls}
            excludeControls={['media', 'emoji']}
            stripPastedStyles
          />
        )}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      fieldsValue.content = fieldsValue.content.toHTML();
      handleUpdate(fieldsValue);
    });
  };

  const uploadHandler = (param) => {

    if (!param.file) {
      return false
    }

    let formData = new FormData();
    // if (param.file.size / 1024 > 1024) {
    //   Modal.error({
    //     title: '图片大小不得大于1MB',
    //     content: '请重新选择图片',
    //     okText: '确认',
    //     cancelText: '取消',
    //   })
    //   return;
    // }

    formData.append('image', param.file);

    const response = upload(formData);
    response.then(result => {
      console.log('result: ', result);
      const real = result.msg;
      form.setFieldsValue({
        content: ContentUtils.insertMedias(form.getFieldsValue().content, [{
          type: 'IMAGE',
          // url: URL.createObjectURL(param.file)
          url: `${imgDomainName}${real}`,
          width: '100%'
        }])
      });
    });

  }

  const extendControls = [
    {
      key: 'antd-uploader',
      type: 'component',
      component: (
        <Upload
          accept="image/*"
          showUploadList={false}
          customRequest={uploadHandler}
        >
          {/* 这里的按钮最好加上type="button"，以避免在表单容器中触发表单提交，用Antd的Button组件则无需如此 */}
          <button type="button" className="control-item button upload-button" data-title="插入图片">
            <Icon type="picture" theme="filled" />
          </button>
        </Upload>
      )
    }
  ]

  return (
    <Modal
      destroyOnClose
      // title="修改软文"
      title={(
        <div>
          <span>修改软文</span>
          <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={okHandle}>确定</Button>
        </div>
      )}
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
      width={1020}
    >
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 20 }} label="软文名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入软文名称！' }],
          initialValue: record.name,
        })(
          <Input placeholder="请输入软文名称" />
        )}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 20 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
      <FormItem labelCol={{ span: 3 }} wrapperCol={{ span: 20 }} label="软文内容">
        {form.getFieldDecorator('content', {
          validateTrigger: 'onBlur',
          rules: [{
            required: true,
            validator: (_, value, callback) => {
              if (value.isEmpty()) {
                callback('请输入软文内容')
              } else {
                callback()
              }
            }
          }],
          initialValue: BraftEditor.createEditorState(record.content)
        })(
          <BraftEditor
            className="my-editor"
            // controls={controls}
            placeholder="请输入软文内容"
            style={{border: '1px solid #d9d9d9', borderRadius: 4}}
            extendControls={extendControls}
            excludeControls={['media', 'emoji']}
            stripPastedStyles
          />
        )}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ advertising, loading }) => ({
  advertising,
  loading: loading.models.advertising,
}))
@Form.create()
class SoftTextManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'softTextId',
    },
    {
      title: '软文名称',
      dataIndex: 'name',
    },
    // {
    //   title: '软文内容',
    //   dataIndex: 'content',
    // },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
    },
    {
      title: '操作',
      width: '15%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.softTextId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = softTextIds => {
    console.log('softTextIds: ', softTextIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'advertising/removeSoftText',
      payload: { softTextIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'advertising/changeSoftTextManagementStatus',
      payload: { eWalletId: record.eWalletId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'advertising/fetchSoftTextList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'advertising/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'advertising/addSoftText',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'advertising/updateSoftText',
      payload: {
        ...fields,
        softTextId: record.softTextId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  render() {
    const {
      advertising: { softTextList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.softTextId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={softTextList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="softTextId"
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />
      </div>
    );
  }
}

export default SoftTextManagement;
