import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Popconfirm,
  Upload,
  message,
  Row,
  Col,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isStoreNameRepeated } from '@/services/advertising';
import { imgDomainName } from '../../../constants/index';
import { queryGadMapInputtipsList } from '@/services/common-api';
import moment from 'moment'
import styles from './AdvertisingList.less';
import {apiDomainName} from "../../../constants";
import ImagePreviewModal from '@/components/ImagePreviewModal';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    tips: [],
    center: [0, 0],
  };

  okHandle = () => {
    const { form, handleAdd } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.imageUrl = fieldsValue.imageUrl.length >0 ? fieldsValue.imageUrl[0].response.msg : '';
      fieldsValue.startTime = fieldsValue.startTime.format('YYYY-MM-DD HH:mm:ss');
      fieldsValue.endTime = fieldsValue.endTime.format('YYYY-MM-DD HH:mm:ss');
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ imageUrl: '' })
      handleAdd(fieldsValue);
    });
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        // console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  render() {
    const { modalVisible, form, handleModalVisible, softTextList } = this.props;
    const { loading, imageUrl } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        // title="新增广告"
        title={(
          <div>
            <span>新增广告</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ imageUrl: '' })
        }}
        style={{top: 20}}
      >
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入广告名称！' },
            ],
          })(<Input placeholder="请输入广告名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告图片">
          {form.getFieldDecorator('imageUrl', {
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请上传广告图片！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告排序">
          {form.getFieldDecorator('sort', {
            rules: [{ required: true, message: '请输入广告排序！' }],
          })(
            <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入广告排序" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="软文名称">
          {form.getFieldDecorator('softTextId', {
            rules: [{ required: true, message: '请选择软文名称！' }],
          })(
            <Select placeholder="请选择软文名称" style={{width: '100%'}} >
              {softTextList.rows.map(item =>
                <Select.Option key={item.softTextId}>{item.name}</Select.Option>
              )}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告开始时间">
          <div
            id="area"
            style={{
              // height: 500,
              // background: '#eee',
              position: 'relative'
            }}
          >
            {form.getFieldDecorator('startTime', {
              rules: [{ required: true, message: '请选择广告开始时间！' }],
            })(
              <DatePicker
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="请选择广告开始时间"
                style={{width: '100%'}}
                getCalendarContainer={() => document.getElementById('area')}
              />
            )}
          </div>
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告结束时间">
          <div
            id="area"
            style={{
              // height: 500,
              // background: '#eee',
              position: 'relative'
            }}
          >
            {form.getFieldDecorator('endTime', {
              rules: [{ required: true, message: '请选择广告结束时间！' }],
            })(
              <DatePicker
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="请选择广告结束时间"
                style={{width: '100%'}}
                getCalendarContainer={() => document.getElementById('area')}
              />
            )}
          </div>
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    tips: [],
    center: [0, 0],
    record: {}
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record }  = nextProps;//imageUrl
    const { center, imageUrl } = prevState;
    let newState = null;
    if (record.imageUrl && !imageUrl) {
      newState = {
        ...newState,
        imageUrl: `${imgDomainName}${record.imageUrl}`,
      };
    }
    return newState
  }

  okHandle = () => {
    const { form, handleUpdate } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.imageUrl = fieldsValue.imageUrl.length >0 ? fieldsValue.imageUrl[fieldsValue.imageUrl.length-1].response.msg : '';
      fieldsValue.startTime = fieldsValue.startTime.format('YYYY-MM-DD HH:mm:ss');
      fieldsValue.endTime = fieldsValue.endTime.format('YYYY-MM-DD HH:mm:ss');
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    console.log('info: ', info);
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  render() {
    const { updateModalVisible, form, handleUpdateModalVisible, record, softTextList } = this.props;
    const { loading, imageUrl } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        // title="修改广告"
        title={(
          <div>
            <span>修改广告</span>
            <Button type='primary' style={{float: 'right', marginRight: 32}} onClick={this.okHandle}>确定</Button>
          </div>
        )}
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ imageUrl: '' })
        }}
        width={600}
        style={{top: 20}}
      >
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入广告名称！' },
            ],
            initialValue: record.name,
          })(<Input placeholder="请输入广告名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告图片">
          {form.getFieldDecorator('imageUrl', {
            initialValue: record.imageUrl ? [{ response: { msg: record.imageUrl } }] : '',
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请上传广告图片！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告排序">
          {form.getFieldDecorator('sort', {
            rules: [{ required: true, message: '请输入广告排序！' }],
            initialValue: record.sort,
          })(
            <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入广告排序" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="软文名称">
          {form.getFieldDecorator('softTextId', {
            rules: [{ required: true, message: '请选择软文名称！' }],
            initialValue: record.softTextId,
          })(
            <Select placeholder="请选择软文名称" style={{width: '100%'}} >
              {softTextList.rows.map(item =>
                <Select.Option key={item.softTextId} value={item.softTextId}>{item.name}</Select.Option>
              )}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告开始时间">
          <div
            id="area"
            style={{
              // height: 500,
              // background: '#eee',
              position: 'relative'
            }}
          >
            {form.getFieldDecorator('startTime', {
              rules: [{ required: true, message: '请选择广告开始时间！' }],
              initialValue: moment(record.startTime),
            })(
              <DatePicker
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="请选择广告开始时间"
                style={{width: '100%'}}
                getCalendarContainer={() => document.getElementById('area')}
              />
            )}
          </div>
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="广告结束时间">
          <div
            id="area"
            style={{
              // height: 500,
              // background: '#eee',
              position: 'relative'
            }}
          >
            {form.getFieldDecorator('endTime', {
              rules: [{ required: true, message: '请选择广告结束时间！' }],
              initialValue: moment(record.endTime),
            })(
              <DatePicker
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="请选择广告结束时间"
                style={{width: '100%'}}
                getCalendarContainer={() => document.getElementById('area')}
              />
            )}
          </div>
        </FormItem>
        <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark',{
            initialValue: record.remark,
          })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ advertising, loading }) => ({
  advertising,
  loading: loading.models.advertising,
}))
@Form.create()
class AdvertisingList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    previewVisible: false,
    imageList: [],
  };

  onImageClick = (images) => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true })
  }

  columns = [
    {
      title: '编号',
      dataIndex: 'bannerId',
      width: 70,
      fixed: 'left',
    },
    {
      title: '广告名称',
      dataIndex: 'name',
      width: 150,
      fixed: 'left',
    },
    {
      title: '广告图片',
      dataIndex: 'imageUrl',
      render: (text) => (text ? <img onClick={() => this.onImageClick(text)} style={{width: 60, height: 60}} alt='' src={`${imgDomainName}${text}`} /> : ''),
    },
    {
      title: '广告排序',
      dataIndex: 'sort',
    },
    {
      title: '软文名称',
      dataIndex: 'softText',
    },
    {
      title: '广告开始时间',
      dataIndex: 'startTime',
    },
    {
      title: '广告结束时间',
      dataIndex: 'endTime',
    },
    {
      title: '广告状态',
      dataIndex: 'bannerStatus',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
    },
    {
      title: '是否启用',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 80,
      fixed: 'right',
      render: (text, record) => {
        const menu = (
          <Menu>
            <Menu.Item>
              <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
            </Menu.Item>
            {/*<Popconfirm*/}
            {/*title="确认删除？"*/}
            {/*onConfirm={() => this.recordRemove(record.advertisingId)}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
            {/*>*/}
            {/*<a href="#">删除</a>*/}
            {/*</Popconfirm>*/}
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement='bottomRight'>
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        )
      },
    },
  ];

  recordRemove = advertisingIds => {
    console.log('advertisingIds: ', advertisingIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'advertising/removeAdvertising',
      payload: { advertisingIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'advertising/changeAdvertisingStatus',
      payload: { bannerId: record.bannerId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //广告列表
    dispatch({
      type: 'advertising/fetchAdvertisingList',
    });
    //软文列表
    dispatch({
      type: 'advertising/fetchSoftTextList',
    });
    //广告状态列表
    dispatch({
      type: 'advertising/fetchAdvertisingStatusList',
      payload: {
        dictCode: 'bannerStatus'
      }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'advertising/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'advertising/addAdvertising',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'advertising/updateAdvertising',
      payload: {
        ...fields,
        bannerId: record.bannerId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'advertising/fetchAdvertisingList',
        payload: fieldsValue
      });
    });
  };

  renderSimpleForm() {
    const {
      advertising: { advertisingStatusList },
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="广告状态">
              {getFieldDecorator('bannerStatusCode')(
                <Select style={{width: '100%'}} placeholder="请选择广告状态" >
                  {advertisingStatusList.rows.map(item => <Select.Option key={item.dictDataCode}>{item.dictDataName}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'advertising/fetchAdvertisingList',
    });
  };

  render() {
    const {
      advertising: { advertisingList, softTextList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record, previewVisible, imageList } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.bannerId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={advertisingList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="bannerId"
              scroll={{x: 1500}}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} softTextList={softTextList} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} softTextList={softTextList} />
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={250}
          imgHeight={250}
          colSpan={24}
        />
      </div>
    );
  }
}

export default AdvertisingList;
