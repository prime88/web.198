import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col } from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './ReceivingAddress.less';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ receivingAddress, loading }) => ({
  receivingAddress,
  loading: loading.models.receivingAddress,
}))
@Form.create()
class ReceivingAddress extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'addressId',
      width: '6%'
    },
    {
      title: '注册手机号',
      dataIndex: 'registerPhone',
      width: '13%'
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
      width: '10%'
    },
    {
      title: '收货人手机号',
      dataIndex: 'phone',
      width: '13%'
    },
    {
      title: '收货人',
      dataIndex: 'consignee',
      width: '9%'
    },
    {
      title: '收货地址',
      dataIndex: 'a',
      width: '38%',
      render: (text, record) => `${record.placeName} ${record.detailedAddress}`
    },
    {
      title: '添加时间',
      dataIndex: 'updateTime',
      width: '11%'
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'receivingAddress/fetchReceivingAddressList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'receivingAddress/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'receivingAddress/fetchReceivingAddressList',
        payload: fieldsValue
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="收货人手机号">
              {getFieldDecorator('phone')(<Input placeholder="请输入手机号" />)}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="收货地址">
              {getFieldDecorator('address')(<Input placeholder="请输入收货地址" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="收货人">
              {getFieldDecorator('consignee')(<Input placeholder="请输入收货人" />)}
            </FormItem>
          </Col>
          <Col md={5} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'receivingAddress/fetchReceivingAddressList',
    });
  };

  render() {
    const {
      receivingAddress: { receivingAddressList },
      loading,
    } = this.props;
    const { selectedRows } = this.state;

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={receivingAddressList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="addressId"
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default ReceivingAddress;
