// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchUserList,
  fetchVipLevelList,
  addInventoryClassification,
  updateInventoryClassification,
  removeInventoryClassification,
  changeInventoryClassificationStatus,
  sendMessage,
} from '@/services/users';
import { fetchMessageList } from '@/services/message';
import { message } from 'antd';

export default {
  namespace: 'users',

  state: {
    //用户列表
    userList: {
      rows: [],
      pagination: {},
    },
    //会员等级分类列表
    vipLevelList: {
      rows: [],
      pagination: {},
    },
    //消息列表
    messageList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchUserList({ payload, callback }, { call, put }) {
      const response = yield call(fetchUserList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveUserList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchVipLevelList({ payload, callback }, { call, put }) {
      const response = yield call(fetchVipLevelList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveVipLevelList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchMessageList({ payload, callback }, { call, put }) {
      const response = yield call(fetchMessageList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveMessageList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addInventoryClassification({ payload, callback }, { call, put }) {
      const response = yield call(addInventoryClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *sendMessage({ payload, callback }, { call, put }) {
      const response = yield call(sendMessage, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeInventoryClassification({ payload, callback }, { call, put }) {
      const response = yield call(removeInventoryClassification, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateInventoryClassification({ payload, callback }, { call, put }) {
      const response = yield call(updateInventoryClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeInventoryClassificationStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeInventoryClassificationStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchInventoryClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveUserList(state, action) {
      return {
        ...state,
        userList: action.payload,
      };
    },
    saveVipLevelList(state, action) {
      return {
        ...state,
        vipLevelList: action.payload,
      };
    },
    saveMessageList(state, action) {
      return {
        ...state,
        messageList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //代金券
        if (location.pathname === '/app-management/other-management/user-management') {
          dispatch({
            type: 'fetchUserList',
          })
        }
      });
    },
  },
};
