// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchReceivingAddressList,
} from '@/services/receivingAddress';
import { message } from 'antd';

export default {
  namespace: 'receivingAddress',

  state: {
    //收货地址列表
    receivingAddressList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchReceivingAddressList({ payload, callback }, { call, put }) {
      const response = yield call(fetchReceivingAddressList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveReceivingAddressList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
  },

  reducers: {
    saveReceivingAddressList(state, action) {
      return {
        ...state,
        receivingAddressList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        // 意见反馈
        if (location.pathname === '/app-management/other-management/receiving-address') {
          dispatch({
            type: 'fetchReceivingAddressList',
          })
        }
      });
    },
  },
};
