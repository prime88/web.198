// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchInvoiceList,
} from '@/services/invoice';
import { message } from 'antd';

export default {
  namespace: 'invoice',

  state: {
    //发票列表
    invoiceList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInvoiceList({ payload, callback }, { call, put }) {
      const response = yield call(fetchInvoiceList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveInvoiceList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
  },

  reducers: {
    saveInvoiceList(state, action) {
      return {
        ...state,
        invoiceList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        // 发票列表
        if (location.pathname === '/app-management/other-management/invoice-management') {
          dispatch({
            type: 'fetchInvoiceList',
          })
        }
      });
    },
  },
};
