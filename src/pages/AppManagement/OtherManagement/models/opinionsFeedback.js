import {
  fetchOpinionsFeedbackList,
  removeOpinionsFeedback,
  changeReadStatus,
  reply,
} from '@/services/opinionsFeedback';
import { message } from 'antd';

export default {
  namespace: 'opinionsFeedback',

  state: {
    //意见反馈列表
    opinionsFeedbackList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchOpinionsFeedbackList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOpinionsFeedbackList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveOpinionsFeedbackList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *removeOpinionsFeedback({ payload, callback }, { call, put }) {
      const response = yield call(removeOpinionsFeedback, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchOpinionsFeedbackList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *changeReadStatus({ payload, tableReadStatus, callback }, { call, put }) {
      const response = yield call(changeReadStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchOpinionsFeedbackList',
          payload: {
            isRead: tableReadStatus
          }
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *reply({ payload, tableReadStatus, callback }, { call, put }) {
      const response = yield call(reply, payload);
      if (response.code === '0') {
        message.success(response.msg);
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveOpinionsFeedbackList(state, action) {
      return {
        ...state,
        opinionsFeedbackList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        // 意见反馈
        if (location.pathname === '/app-management/other-management/opinions-feedback') {
          dispatch({
            type: 'fetchOpinionsFeedbackList',
          })
        }
      });
    },
  },
};
