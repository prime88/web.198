// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchDeliveryTimeList,
  fetchBaseSetting,
  addDeliveryTime,
  updateDeliveryTime,
  updateBaseSetting,
  changeDeliveryTimeStatus,
  removeDeliveryTime,
} from '@/services/settings';
import { message } from 'antd';

export default {
  namespace: 'settings',

  state: {
    //配送时间列表
    deliveryTimeList: {
      rows: [],
      pagination: {},
    },
    baseSetting: {},
  },

  effects: {
    *fetchDeliveryTimeList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDeliveryTimeList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDeliveryTimeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBaseSetting({ payload, callback }, { call, put }) {
      const response = yield call(fetchBaseSetting, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBaseSetting',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addDeliveryTime({ payload, callback }, { call, put }) {
      const response = yield call(addDeliveryTime, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchDeliveryTimeList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeDeliveryTime({ payload, callback }, { call, put }) {
      const response = yield call(removeDeliveryTime, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchDeliveryTimeList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateDeliveryTime({ payload, callback }, { call, put }) {
      const response = yield call(updateDeliveryTime, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchDeliveryTimeList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateBaseSetting({ payload, callback }, { call, put }) {
      const response = yield call(updateBaseSetting, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchDeliveryTimeList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeDeliveryTimeStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeDeliveryTimeStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchDeliveryTimeList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveDeliveryTimeList(state, action) {
      return {
        ...state,
        deliveryTimeList: action.payload,
      };
    },
    saveBaseSetting(state, action) {
      return {
        ...state,
        baseSetting: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       // 调货服务
  //       if (location.pathname === '/app-management/other-management/distribution-service') {
  //         dispatch({
  //           type: 'fetchDistributionList',
  //         })
  //       }
  //     });
  //   },
  // },
};
