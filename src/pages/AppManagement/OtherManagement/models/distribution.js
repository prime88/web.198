// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchDistributionList,
  fetchProblemList,
  addDistributionService,
  addProblem,
  updateDistributionService,
  updateProblem,
  removeDistributionService,
  removeProblem,
  changeDistributionServiceStatus,
  changeProblemStatus,
} from '@/services/distribution';
import { message } from 'antd';

export default {
  namespace: 'distribution',

  state: {
    //调货服务列表
    distributionList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchDistributionList({ payload, callback }, { call, put }) {
      const response = yield call(fetchDistributionList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveDistributionList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchProblemList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProblemList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProblemList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addDistributionService({ payload, callback }, { call, put }) {
      const response = yield call(addDistributionService, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchDistributionList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addProblem({ payload, callback }, { call, put }) {
      const response = yield call(addProblem, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeDistributionService({ payload, callback }, { call, put }) {
      const response = yield call(removeDistributionService, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchDistributionList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeProblem({ payload, callback }, { call, put }) {
      const response = yield call(removeProblem, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateDistributionService({ payload, callback }, { call, put }) {
      const response = yield call(updateDistributionService, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchDistributionList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateProblem({ payload, callback }, { call, put }) {
      const response = yield call(updateProblem, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeDistributionServiceStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeDistributionServiceStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchDistributionList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeProblemStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeProblemStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveDistributionList(state, action) {
      return {
        ...state,
        distributionList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        // 调货服务
        if (location.pathname === '/app-management/other-management/distribution-service') {
          dispatch({
            type: 'fetchDistributionList',
          })
        }
      });
    },
  },
};
