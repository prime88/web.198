// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchTagList,
  addTag,
  updateTag,
  removeTag,
  changeTagStatus,
} from '@/services/tags';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';

export default {
  namespace: 'tags',

  state: {
    //标签列表
    tagList: {
      rows: [],
      pagination: {},
    },
    //标签类型列表
    tagTypeList: {
      rows: [],
      pagination: {},
    },
    //标签颜色列表
    tagColorList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchTagList({ payload, callback }, { call, put }) {
      const response = yield call(fetchTagList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveTagList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchTagTypeList({ payload, callback }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveTagTypeList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchTagColorList({ payload, callback }, { call, put }) {
      const response = yield call(queryDictionary, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveTagColorList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addTag({ payload, callback }, { call, put }) {
      const response = yield call(addTag, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchTagList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeTag({ payload, callback }, { call, put }) {
      const response = yield call(removeTag, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchTagList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateTag({ payload, callback }, { call, put }) {
      const response = yield call(updateTag, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchTagList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeTagStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeTagStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchTagList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveTagList(state, action) {
      return {
        ...state,
        tagList: action.payload,
      };
    },
    saveTagTypeList(state, action) {
      return {
        ...state,
        tagTypeList: action.payload,
      };
    },
    saveTagColorList(state, action) {
      return {
        ...state,
        tagColorList: action.payload,
      };
    },
  },

  // subscriptions: {
  //   setup({ dispatch, history }) {
  //     history.listen(location => {
  //       //代金券
  //       if (location.pathname === '/app-management/store-management/store-list') {
  //         dispatch({
  //           type: 'fetchStoreList',
  //         })
  //       }
  //     });
  //   },
  // },
};
