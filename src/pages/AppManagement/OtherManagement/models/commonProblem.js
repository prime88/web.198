// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchProblemClassificationList,
  fetchProblemList,
  addProblemClassification,
  addProblem,
  updateProblemClassification,
  updateProblem,
  removeProblemClassification,
  removeProblem,
  changeProblemClassificationStatus,
  changeProblemStatus,
} from '@/services/commonProblem';
import { message } from 'antd';

export default {
  namespace: 'commonProblem',

  state: {
    //问题分类列表
    problemClassificationList: {
      rows: [],
      pagination: {},
    },
    //问题列表
    problemList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchProblemClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProblemClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProblemClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchProblemList({ payload, callback }, { call, put }) {
      const response = yield call(fetchProblemList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveProblemList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addProblemClassification({ payload, callback }, { call, put }) {
      const response = yield call(addProblemClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *addProblem({ payload, callback }, { call, put }) {
      const response = yield call(addProblem, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeProblemClassification({ payload, callback }, { call, put }) {
      const response = yield call(removeProblemClassification, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchProblemClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *removeProblem({ payload, callback }, { call, put }) {
      const response = yield call(removeProblem, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateProblemClassification({ payload, callback }, { call, put }) {
      const response = yield call(updateProblemClassification, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *updateProblem({ payload, callback }, { call, put }) {
      const response = yield call(updateProblem, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeProblemClassificationStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeProblemClassificationStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemClassificationList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeProblemStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeProblemStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchProblemList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveProblemClassificationList(state, action) {
      return {
        ...state,
        problemClassificationList: action.payload,
      };
    },
    saveProblemList(state, action) {
      return {
        ...state,
        problemList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        // 常见问题分类
        if (location.pathname === '/app-management/other-management/common-problem-classification') {
          dispatch({
            type: 'fetchProblemClassificationList',
          })
        }
        // 常见问题
        if (location.pathname === '/app-management/other-management/common-problem') {
          dispatch({
            type: 'fetchProblemList',
          })
        }
      });
    },
  },
};
