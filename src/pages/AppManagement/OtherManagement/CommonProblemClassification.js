import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Upload, message, InputNumber } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isProblemClassificationNameRepeated } from '@/services/commonProblem';
import styles from './CommonProblemClassification.less';
import {imgDomainName} from "../../../constants";
import { apiDomainName } from '../../../constants/index';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {

  state = {
    loading: false,
    imageUrl: '',
  }

  checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isProblemClassificationNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  okHandle = () => {
    const { form, handleAdd } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.icon = fieldsValue.icon.length >0 ? fieldsValue.icon[0].response.msg : '';
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ imageUrl: ''})
      handleAdd(fieldsValue);
    });
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        // console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  render() {
    const { modalVisible, form, handleModalVisible } = this.props;
    const { imageUrl, loading } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        title="新增问题分类"
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ imageUrl: ''})
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="图标">
          {form.getFieldDecorator('icon', {
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请选择图标！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入类别！' },
              { validator: this.checkName }
            ],
          })(<Input placeholder="请输入类别" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="优先级">
          {form.getFieldDecorator('sort', {
            rules: [
              { required: true, message: '请输入优先级！' },
            ],
          })(
            <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入优先级" min={0}/>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {

  state = {
    loading: false,
    imageUrl: '',
    record: {}
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record }  = nextProps;
    const { imageUrl } = prevState;
    let newState = null;
    if (record.icon && !imageUrl) {
      newState = {
        ...newState,
        imageUrl: `${imgDomainName}${record.icon}`,
      };
    }
    return newState
  }

  checkName = (rule, value, callback) => {
    const { record } = this.props;
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isProblemClassificationNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  okHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.icon = fieldsValue.icon.length >1 ? fieldsValue.icon[1].response.msg : '';
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        // console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  render() {
    const { updateModalVisible, form, handleUpdateModalVisible, record } = this.props;
    const { imageUrl, loading } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        title="修改问题分类"
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ imageUrl: ''})
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="图标">
          {form.getFieldDecorator('icon', {
            initialValue: record.icon ? [{ response: { msg: record.icon } }] : '',
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请选择图标！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入类别！' },
              { validator: this.checkName }
            ],
            initialValue: record.name
          })(<Input placeholder="请输入类别" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="优先级">
          {form.getFieldDecorator('sort', {
            rules: [
              { required: true, message: '请输入优先级！' },
            ],
            initialValue: record.sort
          })(
            <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入优先级" min={0}/>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark', {
            initialValue: record.remark
          })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ commonProblem, loading }) => ({
  commonProblem,
  loading: loading.models.commonProblem,
}))
@Form.create()
class CommonProblemClassification extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'problemClassifyId',
      width: '10%',
    },
    {
      title: '图标',
      dataIndex: 'icon',
      width: '10%',
      render: (text) => (text ? <img style={{width: 50, height: 50}} alt='' src={`${imgDomainName}${text}`} /> : ''),
    },
    {
      title: '类别',
      width: '10%',
      dataIndex: 'name',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      width: '10%',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '优先级',
      width: '10%',
      dataIndex: 'sort',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: '35%',
    },
    {
      title: '操作',
      width: '15%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.problemClassifyId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = problemClassifyIds => {
    console.log('problemClassifyIds: ', problemClassifyIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'commonProblem/removeProblemClassification',
      payload: { problemClassifyIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'commonProblem/changeProblemClassificationStatus',
      payload: { problemClassifyId: record.problemClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'commonProblem/fetchProblemClassificationList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'commonProblem/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'commonProblem/addProblemClassification',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'commonProblem/updateProblemClassification',
      payload: {
        ...fields,
        problemClassifyId: record.problemClassifyId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  render() {
    const {
      commonProblem: { problemClassificationList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.problemClassifyId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={problemClassificationList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="problemClassifyId"
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} />
      </div>
    );
  }
}

export default CommonProblemClassification;
