import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm } from 'antd';
import StandardTable from '@/components/StandardTable';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import styles from './OpinionsFeedback.less';

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ opinionsFeedback, loading }) => ({
  opinionsFeedback,
  loading: loading.models.opinionsFeedback,
}))
@Form.create()
class OpinionsFeedback extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    tableReadStatus: false,
    replyModalVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'feedbackId',
      width: '6%',
    },
    {
      title: '反馈手机号',
      dataIndex: 'phone',
      width: '14%',
    },
    {
      title: '反馈内容',
      dataIndex: 'content',
      width: '44%',
      render: text => <Ellipsis tooltip length={100}>{text}</Ellipsis>
    },
    {
      title: '反馈时间',
      dataIndex: 'createTime',
      width: '16%',
    },
    {
      title: '状态',
      dataIndex: 'isRead',
      render: text => text ? '已读':'未读',
      width: '8%',
    },
    {
      title: '操作',
      width: '12%',
      render: (text, record) => (
        <Fragment>
          {/*<a onClick={() => this.reply(record)}>回复</a>*/}
          <a onClick={() => this.setState({ replyModalVisible: true, record })}>回复</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.feedbackId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = feedbackIds => {
    console.log('feedbackIds: ', feedbackIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'opinionsFeedback/removeOpinionsFeedback',
      payload: { feedbackIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch({
      type: 'opinionsFeedback/fetchOpinionsFeedbackList',
      // payload: { isRead: false }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'opinionsFeedback/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  changeReadStatus = () => {
    const { dispatch } = this.props;
    const { selectedRows, tableReadStatus } = this.state;

    const feedbackIds = selectedRows.filter(item => item.isRead === false).map(item => item.feedbackId).join(',');
    console.log('feedbackIds: ', feedbackIds);
    dispatch({
      type: 'opinionsFeedback/changeReadStatus',
      payload: {
        feedbackIds
      },
      tableReadStatus,
    });
  }

  handleSearch = (isRead) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'opinionsFeedback/fetchOpinionsFeedbackList',
      payload: { isRead }
    });

    //标记列表的状态，用于search完后的重现
    this.setState({
      tableReadStatus: isRead,
      selectedRows: []
    })
  }

  onReplyModalFormOk = () => {
    const { form, dispatch } = this.props;
    const { record } = this.state;
    const feedbackId = record.feedbackId;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'opinionsFeedback/reply',
        payload: {
          ...fieldsValue,
          feedbackId,
        },
        callback: () => {
          this.setState({ replyModalVisible: false });
          form.resetFields();
        }
      });
    });
  }

  render() {
    const {
      opinionsFeedback: { opinionsFeedbackList },
      loading,
      form,
    } = this.props;
    const { selectedRows, replyModalVisible } = this.state;

    const readBtnDisabled = selectedRows.filter(item => item.isRead === false).length <= 0;

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button disabled={readBtnDisabled} type="primary" onClick={this.changeReadStatus}>
                标记为已读
              </Button>
              <Button onClick={() => this.handleSearch(false)}>查看未读</Button>
              <Button onClick={() => this.handleSearch(true)}>查看已读</Button>
              <Button onClick={() => this.handleSearch('')}>查看全部</Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.feedbackId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={opinionsFeedbackList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey='feedbackId'
            />
          </div>
        </Card>

        <Modal
          destroyOnClose
          title="回复"
          visible={replyModalVisible}
          onCancel={() => this.setState({ replyModalVisible: false })}
          onOk={this.onReplyModalFormOk}
        >
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="内容">
            {form.getFieldDecorator('reply', {
              rules: [{ required: true, message: '请输入回复内容！' }],
            })(
              <TextArea rows={4} placeholder="请输入回复内容" />
            )}
          </FormItem>
        </Modal>
      </div>
    );
  }
}

export default OpinionsFeedback;
