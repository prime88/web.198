import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, InputNumber } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/commonProblem';
import Ellipsis from 'ant-design-pro/lib/Ellipsis';
import { imgDomainName } from "../../../constants";
import styles from './CommonProblem.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, problemClassificationList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增问题"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
        {form.getFieldDecorator('problemClassifyId', {
          rules: [{ required: true, message: '请选择类别！' }],
        })(
          <Select style={{width: '100%'}} placeholder='请选择类别' >
            {problemClassificationList.rows.map(item => <Select.Option key={item.problemClassifyId}>{item.name}</Select.Option>)}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="问题简介">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入问题简介！' }],
        })(<Input placeholder="请输入问题简介" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="问题描述">
        {form.getFieldDecorator('describe', {
          rules: [{ required: true, message: '请输入问题描述！' }],
        })(<Input.TextArea rows={3} placeholder="请输入问题描述" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="回答">
        {form.getFieldDecorator('answer', {
          rules: [{ required: true, message: '请输入回答！' }],
        })(<Input.TextArea rows={3} placeholder="请输入回答" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="优先级">
        {form.getFieldDecorator('sort', {
          rules: [
            { required: true, message: '请输入优先级！' },
          ],
        })(
          <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入优先级" min={0}/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record, problemClassificationList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改问题"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类别">
        {form.getFieldDecorator('problemClassifyId', {//problemClassificationList
          rules: [{ required: true, message: '请选择类别！' }],
          initialValue: record.problemClassifyId
        })(
          <Select style={{width: '100%'}} placeholder='请选择类别' >
            {problemClassificationList.rows.map(item => (
              <Select.Option key={item.problemClassifyId} value={item.problemClassifyId}>{item.name}</Select.Option>
            ))}
          </Select>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="问题简介">
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: '请输入问题简介！' }],
          initialValue: record.name
        })(<Input placeholder="请输入问题简介" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="问题描述">
        {form.getFieldDecorator('describe', {
          rules: [{ required: true, message: '请输入问题描述！' }],
          initialValue: record.describe
        })(<Input.TextArea rows={3} placeholder="请输入问题描述" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="回答">
        {form.getFieldDecorator('answer', {
          rules: [{ required: true, message: '请输入回答！' }],
          initialValue: record.answer
        })(<Input.TextArea rows={3} placeholder="请输入回答" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="优先级">
        {form.getFieldDecorator('sort', {
          rules: [
            { required: true, message: '请输入优先级！' },
          ],
          initialValue: record.sort
        })(
          <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入优先级" min={0}/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ commonProblem, loading }) => ({
  commonProblem,
  loading: loading.models.commonProblem,
}))
@Form.create()
class CommonProblem extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'problemId',
      width: '6%',
    },
    {
      title: '类别',
      dataIndex: 'problemClassify',
      width: '10%',
    },
    {
      title: '问题简介',
      dataIndex: 'name',
      width: '10%',
    },
    {
      title: '问题描述',
      dataIndex: 'describe',
      width: '11%',
    },
    {
      title: '回答',
      dataIndex: 'answer',
      width: '25%',
      render: text => <Ellipsis tooltip length={60}>{text}</Ellipsis>
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
      width: '6%',
    },
    {
      title: '优先级',
      dataIndex: 'sort',
      width: '7%',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: '10%',
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.problemId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
      width: '15%',
    },
  ];

  recordRemove = problemIds => {
    console.log('stockClassifyIds: ', problemIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'commonProblem/removeProblem',
      payload: { problemIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'commonProblem/changeProblemStatus',
      payload: { problemId: record.problemId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //问题列表
    dispatch({
      type: 'commonProblem/fetchProblemList',
    });

    //问题分类列表
    dispatch({
      type: 'commonProblem/fetchProblemClassificationList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'commonProblem/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'commonProblem/addProblem',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'commonProblem/updateProblem',
      payload: {
        ...fields,
        problemId: record.problemId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'commonProblem/fetchProblemList',
        payload: fieldsValue
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'commonProblem/fetchProblemList',
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      commonProblem: { problemClassificationList },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="类别">
              {getFieldDecorator('problemClassifyId')(
                <Select allowClear style={{width: '100%'}} placeholder="请选择类别">
                  {problemClassificationList.rows.map(item => <Select.Option key={item.problemClassifyId}>{item.name}</Select.Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="问题简介">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      commonProblem: { problemList, problemClassificationList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.problemId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={problemList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="problemId"
            />
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
          problemClassificationList={problemClassificationList}
        />
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          record={record}
          problemClassificationList={problemClassificationList}
        />
      </div>
    );
  }
}

export default CommonProblem;
