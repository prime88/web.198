import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col } from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './InvoiceManagement.less';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ invoice, loading }) => ({
  invoice,
  loading: loading.models.invoice,
}))
@Form.create()
class InvoiceManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'invoiceId',
      fixed: 'left',
      width: 80
    },
    {
      title: '注册手机号',
      dataIndex: 'phone',
      fixed: 'left',
      width: 140
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
      fixed: 'left',
      width: 100,
    },
    {
      title: '抬头类型',
      dataIndex: 'riseType',
      width: 90,
    },
    {
      title: '发票抬头',
      dataIndex: 'rise',
    },
    {
      title: ' 纳税人识别号',
      dataIndex: 'taxpayerNum',
    },
    {
      title: '收票人邮箱',
      dataIndex: 'mailbox',
    },
    {
      title: '添加时间',
      dataIndex: 'createTime',
    },
    {
      title: '注册电话',
      dataIndex: 'registerPhone',
    },
    {
      title: '注册地址',
      dataIndex: 'registerAddress',
    },
    {
      title: '开户银行',
      dataIndex: 'bank',
    },
    {
      title: '开户账号',
      dataIndex: 'account',
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'invoice/fetchInvoiceList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'invoice/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'invoice/fetchInvoiceList',
        payload: fieldsValue
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="发票抬头">
              {getFieldDecorator('rise')(<Input placeholder="请输入发票抬头" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="纳税人识别号">
              {getFieldDecorator('taxpayerNum')(<Input placeholder="请输入纳税人识别号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'invoice/fetchInvoiceList',
    });
  };

  render() {
    const {
      invoice: { invoiceList },
      loading,
    } = this.props;
    const { selectedRows } = this.state;

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={invoiceList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="invoiceId"
              scroll={{ x: 1900 }}
            />
          </div>
        </Card>
      </div>
    );
  }
}

export default InvoiceManagement;
