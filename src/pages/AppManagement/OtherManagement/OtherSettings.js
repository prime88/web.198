import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Row, Col, InputNumber, Tabs } from 'antd';
import StandardTable from '@/components/StandardTable';
import styles from './OtherSettings.less';
import { imgDomainName } from "../../../constants";

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.lowerLimit = parseInt(fieldsValue.lowerLimit)*1000;
      fieldsValue.upperLimit = parseInt(fieldsValue.upperLimit)*1000;

      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="新增配送时间"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="配送时间(分钟)">
        {form.getFieldDecorator('deliveryTime', {
          rules: [{ required: true, message: '请输入配送时间！' }],
        })(<Input placeholder="请输入配送时间" />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="距离下限(公里)">
        {form.getFieldDecorator('lowerLimit', {
          rules: [{ required: true, message: '请输入距离下限！' }],
        })(
          <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入距离下限" min={0}/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="距离上限(公里)">
        {form.getFieldDecorator('upperLimit', {
          rules: [
            { required: true, message: '请输入距离上限！' },
          ],
        })(
          <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入距离上限" min={0}/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

const UpdateForm = Form.create()(props => {
  const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  return (
    <Modal
      destroyOnClose
      title="修改配送时间"
      visible={updateModalVisible}
      onOk={okHandle}
      onCancel={() => handleUpdateModalVisible()}
    >
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="配送时间(分钟)">
        {form.getFieldDecorator('deliveryTime', {
          rules: [{ required: true, message: '请输入配送时间！' }],
          initialValue: record.deliveryTime,
        })(<Input placeholder="请输入配送时间" />)}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="距离下限(公里)">
        {form.getFieldDecorator('lowerLimit', {
          rules: [{ required: true, message: '请输入距离下限！' }],
          initialValue: record.lowerLimit/1000,
        })(
          <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入距离下限" min={0}/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="距离上限(公里)">
        {form.getFieldDecorator('upperLimit', {
          rules: [
            { required: true, message: '请输入距离上限！' },
          ],
          initialValue: record.upperLimit/1000,
        })(
          <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入距离上限" min={0}/>
        )}
      </FormItem>
      <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label="备注">
        {form.getFieldDecorator('remark', {
          initialValue: record.remark,
        })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ settings, loading }) => ({
  settings,
  loading: loading.models.settings,
}))
@Form.create()
class OtherSettings extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '默认送达时长',
      dataIndex: 'deliveryTime',
    },
    {
      title: '公里数',
      dataIndex: '',
      render: (text, record) => <span>{`${record.lowerLimit/1000} ~ ${record.upperLimit/1000}`}</span>
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
        ),
    },
    {
      title: '操作',
      width: '14%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.deliveryTimeId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = deliveryTimeIds => {
    console.log('deliveryTimeIds: ', deliveryTimeIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'settings/removeDeliveryTime',
      payload: { deliveryTimeIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'settings/changeDeliveryTimeStatus',
      payload: { deliveryTimeId: record.deliveryTimeId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    // 调货服务列表
    dispatch({
      type: 'settings/fetchDeliveryTimeList',
    });

    //获取客服电话
    dispatch({
      type: 'settings/fetchBaseSetting',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'settings/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'settings/addDeliveryTime',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    fields.lowerLimit = fields.lowerLimit*1000;
    fields.upperLimit = fields.upperLimit*1000;
    dispatch({
      type: 'settings/updateDeliveryTime',
      payload: {
        ...fields,
        deliveryTimeId: record.deliveryTimeId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'settings/fetchDistributionList',
        payload: fieldsValue
      });
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'settings/fetchDistributionList',
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="标题">
              {getFieldDecorator('title')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  callback = (key) => {
    console.log('key: ', key);
  }

  handleGainSubmit = (e) => {
    e.preventDefault();
    const { dispatch } = this.props;

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        // const selfTakingDays = values.selfTakingDays;
        // console.log('selfTakingDays: ', selfTakingDays);
        dispatch({
          type: 'settings/updateBaseSetting',
          payload: values,
        });
      }
    });
  }

  handleOtherSubmit = (e) => {
    e.preventDefault();
    const { dispatch } = this.props;

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // const servicePhone = values.servicePhone;
        // console.log('servicePhone: ', servicePhone);
        console.log('Received values of form: ', values);
        dispatch({
          type: 'settings/updateBaseSetting',
          payload: values,
        });
      }
    });
  }

  render() {
    const {
      settings: { baseSetting, deliveryTimeList },
      loading,
      form,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const formItemGainLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 3 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
    };

    const tailFormItemGainLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 3,
        },
      },
    };

    const formItemOtherLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 2 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
    };

    const tailFormItemOtherLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 2,
        },
      },
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            {/*<div className={styles.tableListForm}>{this.renderSimpleForm()}</div>*/}
            <Tabs defaultActiveKey="1" onChange={this.callback}>
              <TabPane tab="配送设置" key="1" style={{paddingTop: 16}}>
                <div className={styles.tableListOperator}>
                  <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                    新增
                  </Button>
                  {selectedRows.length > 0 && (
                    <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.deliveryTimeId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
                  )}
                </div>
                <StandardTable
                  selectedRows={selectedRows}
                  loading={loading}
                  data={deliveryTimeList}
                  columns={this.columns}
                  onSelectRow={this.handleSelectRows}
                  onChange={this.handleStandardTableChange}
                  rowKey="deliveryTimeId"
                />
              </TabPane>
              <TabPane tab="自取设置" key="2" style={{paddingTop: 16}}>
                <Form onSubmit={this.handleGainSubmit}>
                  <FormItem
                    {...formItemGainLayout}
                    label="自取限制天数"
                  >
                    {form.getFieldDecorator('selfTakingDays', {
                      // rules: [
                      //   {
                      //     required: true, message: '请输入自取限制天数!',
                      //   }
                      // ],
                      initialValue: baseSetting.selfTakingDays
                    })(
                      <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入自取限制天数" min={1} />
                    )}
                  </FormItem>
                  <FormItem {...tailFormItemGainLayout}>
                    <Button type="primary" htmlType="submit">保存</Button>
                  </FormItem>
                </Form>
              </TabPane>
              <TabPane tab="其他设置" key="3" style={{paddingTop: 16}}>
                <Form onSubmit={this.handleOtherSubmit}>
                  <FormItem
                    {...formItemOtherLayout}
                    label="客服电话"
                  >
                    {form.getFieldDecorator('servicePhone', {
                      // rules: [
                      //   {
                      //     required: true, message: '请输入客服电话!',
                      //   }
                      // ],
                      initialValue: baseSetting.servicePhone
                    })(
                      <Input style={{width: '100%'}} placeholder="请输入客服电话" />
                    )}
                  </FormItem>
                  <FormItem {...tailFormItemOtherLayout}>
                    <Button type="primary" htmlType="submit">保存</Button>
                  </FormItem>
                </Form>
              </TabPane>
            </Tabs>
          </div>
        </Card>
        <CreateForm
          {...parentMethods}
          modalVisible={modalVisible}
        />
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          record={record}
        />
      </div>
    );
  }
}

export default OtherSettings;
