import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Avatar, Table, Row, Col, DatePicker, Dropdown, Menu } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isInventoryClassificationNameRepeated } from '@/services/users';
import { fetchInvoiceList } from '@/services/invoice';
import { fetchReceivingAddressList } from '@/services/receivingAddress';
import styles from './UserManagement.less';
import {imgDomainName} from "../../../constants";
import moment from 'moment';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ users, loading }) => ({
  users,
  loading: loading.models.users,
}))
@Form.create()
class UserManagement extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    invoiceList: [],
    receivingList: [],
    invoiceModalVisible: false,
    receivingModalVisible: false,
    messageModalVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'userId',
      width: '6%',
    },
    {
      title: '头像',
      dataIndex: 'avatarUrl',
      align: 'center',
      render: (text) => (text ? <Avatar icon="user" src={`${imgDomainName}${text}`} size={64} /> : ''),
    },
    {
      title: '昵称',
      dataIndex: 'nickName',
      width: '8%',
    },
    {
      title: '手机号',
      dataIndex: 'phone',
      width: '10%',
    },
    {
      title: '性别',
      dataIndex: 'gender',
      width: '6%',
      render: text => {
        let gender = '保密';
        switch (text.toString()) {
          case '1':
            gender = '男';
            break;
          case '2':
            gender = '女';
            break;
        }
        return gender
      }
    },
    {
      title: '生日',
      dataIndex: 'birthday',
      width: '11%',
    },
    {
      title: '注册时间',
      dataIndex: 'createTime',
      width: '11%',
    },
    {
      title: '会员等级',
      dataIndex: 'memberLevel',
      width: '6%',
    },
    {
      title: '钱包余额 (￥)',
      dataIndex: 'balance',
      width: '8%',
    },
    {
      title: '历史消费金额 (￥)',
      dataIndex: 'historicalAmount',
      width: '9%',
    },
    {
      title: '操作',
      render: (text, record) => {
        const menu = (
          <Menu>
            <Menu.Item>
              <a onClick={() => this.openInvoiceListModal(record)}>查看发票信息</a>
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => this.openReceivingListModal(record)}>查看收货地址</a>
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menu} placement='bottomRight'>
            <a className="ant-dropdown-link" href="#">
              操作 <Icon type="down" />
            </a>
          </Dropdown>
        )
      },
    },
  ];

  invoiceColumns = [
    {
      title: '编号',
      dataIndex: 'invoiceId',
      width: '6%',
    },
    {
      title: '注册手机号',
      dataIndex: 'phone',
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
    },
    {
      title: '抬头类型',
      dataIndex: 'riseType',
    },
    {
      title: '发票抬头',
      dataIndex: 'rise',
    },
    {
      title: ' 纳税人识别号',
      dataIndex: 'taxpayerNum',
    },
    {
      title: '收票人邮箱',
      dataIndex: 'mailbox',
    },
    {
      title: '添加时间',
      dataIndex: 'createTime',
    },
    {
      title: '注册电话',
      dataIndex: 'registerPhone',
    },
    {
      title: '注册地址',
      dataIndex: 'registerAddress',
    },
    {
      title: '开户银行',
      dataIndex: 'bank',
    },
    {
      title: '开户账号',
      dataIndex: 'account',
    },
  ];

  receivingcolumns = [
    {
      title: '编号',
      dataIndex: 'addressId',
    },
    {
      title: '注册手机号',
      dataIndex: 'registerPhone',
    },
    {
      title: '昵称',
      dataIndex: 'nickname',
    },
    {
      title: '收货人手机号',
      dataIndex: 'phone',
    },
    {
      title: '收货人',
      dataIndex: 'consignee',
    },
    {
      title: '收货地址',
      dataIndex: 'a',
      render: (text, record) => `${record.placeName} ${record.detailedAddress}`
    },
    {
      title: '添加时间',
      dataIndex: 'updateTime',
    },
  ];

  openInvoiceListModal = record => {
    console.log('record: ', record);
    //获取发票列表
    const response = fetchInvoiceList({ userId: record.userId });
    response.then(result => {
      this.setState({
        invoiceList: result.data,
        invoiceModalVisible: true,
      })
    });
  }

  openReceivingListModal = record => {
    console.log('record: ', record);
    //获取收货地址列表
    const response = fetchReceivingAddressList({ userId: record.userId });
    response.then(result => {
      this.setState({
        receivingList: result.data,
        receivingModalVisible: true,
      })
    });
  }

  recordRemove = stockClassifyIds => {
    console.log('stockClassifyIds: ', stockClassifyIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'users/removeInventoryClassification',
      payload: { stockClassifyIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'users/changeInventoryClassificationStatus',
      payload: { stockClassifyId: record.stockClassifyId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    //用户列表
    dispatch({
      type: 'users/fetchUserList',
    });

    //会员等级列表
    dispatch({
      type: 'users/fetchVipLevelList',
    });

    //消息列表
    dispatch({
      type: 'users/fetchMessageList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'users/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'users/addInventoryClassification',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'users/updateInventoryClassification',
      payload: {
        ...fields,
        stockClassifyId: record.stockClassifyId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const registerTime = fieldsValue.registerTime;
      if (registerTime.length === 2) {
        fieldsValue.startTime = registerTime[0].format('YYYY-MM-DD');
        fieldsValue.endTime = moment(registerTime[1].format('YYYY-MM-DD'))
          .add(1, 'days')
          .format('YYYY-MM-DD');
      }
      delete fieldsValue.registerTime;
      dispatch({
        type: 'users/fetchUserList',
        payload: fieldsValue,
      });
    });
  };

  onSendMessageClick = () => {
    this.setState({ messageModalVisible: true })
  }

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      users: { vipLevelList }
    } = this.props;
    const { selectedRows } = this.state;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={5} sm={24}>
            <FormItem label="手机号">
              {getFieldDecorator('phone')(<Input placeholder="请输入手机号" />)}
            </FormItem>
          </Col>
          <Col md={5} sm={24}>
            <FormItem label="会员等级">
              {getFieldDecorator('memberLevelId')(
                <Select allowClear placeholder="请选择等级" style={{ width: '100%' }}>
                  {vipLevelList.rows.map(item => <Option key={item.memberLevelId}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={7} sm={24}>
            <FormItem label="注册时间">
              {getFieldDecorator('registerTime', {
                initialValue: [],
              })(<DatePicker.RangePicker />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {selectedRows.length > 0 && (
                <Button onClick={this.onSendMessageClick} style={{ marginLeft: 8 }}>发送消息</Button>
              )}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'users/fetchUserList',
    });
  };

  onMessageTitleChange = (value) => {
    const { users: { messageList }, form } = this.props;
    const title = value.split(',')[0];
    const messageTemplateId = value.split(',')[1];
    const text = messageList.rows.find(item => item.messageTemplateId == messageTemplateId).text;
    form.setFieldsValue({ text });
  }

  onMessageModalFormOk = () => {
    const { form, dispatch } = this.props;
    const { selectedRows } = this.state;
    console.log('selectedRows: ', selectedRows);
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.messageTemplateId || fieldsValue.messageTemplateId === 1) {
        form.resetFields();
        dispatch({
          type: 'users/sendMessage',
          payload: {
            ...fieldsValue,
            cids: selectedRows.map(item => item.cid).filter(item => item !== '').join(','),
            // title: fieldsValue.title.split(',')[0],
            // text: fieldsValue.text
          },
          callback: () => {
            this.setState({ messageModalVisible: false })
          }
        });
      }
    });
  }

  render() {
    const {
      users: { userList, messageList },
      loading,
      form,
    } = this.props;

    const {
      selectedRows,
      invoiceModalVisible,
      invoiceList,
      receivingModalVisible,
      receivingList,
      messageModalVisible,
    } = this.state;

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={userList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="userId"
            />
          </div>
        </Card>
        <Modal
          title="发票信息"
          visible={invoiceModalVisible}
          onCancel={() => this.setState({ invoiceModalVisible: false })}
          width={'95%'}
          footer={[
            <Button onClick={() => this.setState({ invoiceModalVisible: false })} type="primary">
              关闭
            </Button>,
          ]}
        >
          <Table columns={this.invoiceColumns} dataSource={invoiceList.rows}/>
        </Modal>
        <Modal
          title="收货地址"
          visible={receivingModalVisible}
          onCancel={() => this.setState({ receivingModalVisible: false })}
          width={'95%'}
          footer={[
            <Button onClick={() => this.setState({ receivingModalVisible: false })} type="primary">
              关闭
            </Button>,
          ]}
        >
          <Table columns={this.receivingcolumns} dataSource={receivingList.rows}/>
        </Modal>
        <Modal
          title="消息发送"
          visible={messageModalVisible}
          onCancel={() => this.setState({ messageModalVisible: false })}
          onOk={this.onMessageModalFormOk}
        >
          <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="消息">
            {form.getFieldDecorator('messageTemplateId', {
              // rules: [{ required: true, message: '请选择消息！' }],
            })(
              <Select style={{width: '100%'}}>
                {messageList.rows.map(item => <Select.Option key={item.messageTemplateId} value={item.messageTemplateId}>{item.title}</Select.Option>)}
              </Select>
            )}
          </FormItem>
          {/*<FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标题">*/}
            {/*{form.getFieldDecorator('title', {*/}
              {/*rules: [{ required: true, message: '请输入标题！' }],*/}
            {/*})(*/}
              {/*<Select style={{width: '100%'}} onChange={this.onMessageTitleChange} >*/}
                {/*{messageList.rows.map(item => <Select.Option key={`${item.title},${item.messageTemplateId}`}>{item.title}</Select.Option>)}*/}
              {/*</Select>*/}
            {/*)}*/}
          {/*</FormItem>*/}
          {/*<FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="内容">*/}
            {/*{form.getFieldDecorator('text', {*/}
              {/*rules: [{ required: true, message: '请输入内容！' }],*/}
            {/*})(<Input.TextArea rows={3} placeholder="请输入内容" />)}*/}
          {/*</FormItem>*/}
        </Modal>
      </div>
    );
  }
}

export default UserManagement;
