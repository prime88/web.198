import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Form, Input, Select, Icon, Button, Modal, Divider, Popconfirm, Upload, message, TimePicker, Row, Col, Dropdown, Menu, Tag, InputNumber } from 'antd';
import StandardTable from '@/components/StandardTable';
import { isTagNameRepeated } from '@/services/tags';
import { imgDomainName } from '../../../constants/index';
import { queryGadMapInputtipsList } from '@/services/common-api';
import { Map, Marker } from 'react-amap';
import moment from 'moment'
import styles from './StoreTags.less';
import {apiDomainName} from "../../../constants";

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

@Form.create()
class CreateForm extends PureComponent {
  state = {
    loading: false,
  };

  handleSearch = value => {
    console.log('handleSearch-value: ', value);
    if (!value) return;
    // 获取位置信息列表
    const response = queryGadMapInputtipsList({ keywords: value.replace(/\s*/g, '') });
    console.log('response: ', response.then());
    response.then(result => {
      console.log('result: ', result);
      this.setState({ tips: result.tips });
    });
  };

  okHandle = () => {
    const { form, handleAdd } = this.props;
    const { center } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isTagNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  render() {
    const { modalVisible, form, handleAdd, handleModalVisible, tagTypeList, tagColorList } = this.props;
    const { loading } = this.state;

    return (
      <Modal
        destroyOnClose
        title="新增标签"
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ imageUrl: '', center: [0, 0], })
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入标签名称！' },
              // { validator: this.checkName }
            ],
          })(<Input placeholder="请输入标签名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签类型">
          {form.getFieldDecorator('typeCode', {
            rules: [
              { required: true, message: '请选择标签类型！' },
            ],
          })(
            <Select placeholder="请选择标签类型" style={{width: '100%'}} >
              {tagTypeList.rows.map(item => <Select.Option key={item.dictDataCode}>{item.dictDataName}</Select.Option>)}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签颜色">
          {form.getFieldDecorator('color', {
            rules: [
              { required: true, message: '请选择标签颜色！' },
            ],
          })(
            <Select placeholder="请选择标签颜色" style={{width: '100%'}} >
              {tagColorList.rows.map(item =>
                <Select.Option key={item.dictDataName}><Tag color={item.dictDataName}>{item.dictDataName}</Tag></Select.Option>
              )}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="排序">
          {form.getFieldDecorator('sort', {
            rules: [
              { required: true, message: '请输入排序数！' },
            ],
          })(
            <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入排序数" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark')(
            <Input.TextArea rows={3} placeholder="请输入排序数" />
          )}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    tips: [],
    center: [0, 0],
    record: {}
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record }  = nextProps;//imageUrl
    const { center, imageUrl } = prevState;
    let newState = null;
    // if (record.coordinate && record.coordinate !== center.join(',')) {
    if (record.coordinate && center.join(',') === '0,0') {
      newState = { center: record.coordinate.split(',') }
    }
    if (record.tagsUrl && !imageUrl) {
      newState = {
        ...newState,
        imageUrl: `${imgDomainName}${record.tagsUrl}`,
      };
    }
    return newState
  }

  handleSearch = value => {
    console.log('handleSearch-value: ', value);
    if (!value) return;
    // 获取位置信息列表
    const response = queryGadMapInputtipsList({ keywords: value.replace(/\s*/g, '') });
    console.log('response: ', response.then());
    response.then(result => {
      console.log('result: ', result);
      this.setState({ tips: result.tips });
    });
  };

  okHandle = () => {
    const { form, handleUpdate } = this.props;
    const { center } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  checkName = (rule, value, callback) => {
    const { record } = this.props;
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }
    const response = isTagNameRepeated({ name: value, typeCode: record.typeCode });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = info => {
    console.log('info: ', info);
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  onPositionChange = value => {
    console.log('onPositionChange-value: ', value);
    const { tips } = this.state;
    this.setState({ position: value });

    const target = tips[value.split(',')[0]];
    if (typeof target.location === 'string') {
      this.setState({ center: target.location.split(',') });
    } else {
      this.setState({ center: [0, 0] });
    }
  };

  render() {
    const { updateModalVisible, form, handleUpdate, handleUpdateModalVisible, record, tagTypeList, tagColorList } = this.props;
    const { loading, imageUrl, tips, center } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        title="修改门店"
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ imageUrl: '' })
        }}
        width={600}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入标签名称！' },
              { validator: this.checkName }
            ],
            initialValue: record.name,
          })(<Input placeholder="请输入标签名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签类型">
          {form.getFieldDecorator('typeCode', {
            rules: [
              { required: true, message: '请选择标签类型！' },
            ],
            initialValue: record.typeCode,
          })(
            <Select placeholder="请选择标签类型" style={{width: '100%'}} >
              {tagTypeList.rows.map(item => <Select.Option key={item.dictDataCode}>{item.dictDataName}</Select.Option>)}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签颜色">
          {form.getFieldDecorator('color', {
            rules: [
              { required: true, message: '请选择标签颜色！' },
            ],
            initialValue: record.color,
          })(
            <Select placeholder="请选择标签颜色" style={{width: '100%'}} >
              {tagColorList.rows.map(item =>
                <Select.Option key={item.dictDataName}><Tag color={item.dictDataName}>{item.dictDataName}</Tag></Select.Option>
              )}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="排序">
          {form.getFieldDecorator('sort', {
            rules: [
              { required: true, message: '请输入排序数！' },
            ],
            initialValue: record.sort,
          })(
            <InputNumber precision={0} style={{width: '100%'}} placeholder="请输入排序数" min={0} />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark', {
            initialValue: record.remark,
          })(
            <Input.TextArea rows={3} placeholder="请输入排序数" />
          )}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ tags, loading }) => ({
  tags,
  loading: loading.models.tags,
}))
@Form.create()
class StoreTags extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    previewVisible: false,
    imageList: [],
  };

  columns = [
    {
      title: '序号',
      dataIndex: '',
      render: (text, record, index) => index+1
    },
    {
      title: '标签名称',
      dataIndex: 'name',
    },
    {
      title: '类型',
      dataIndex: 'type',
    },
    {
      title: '默认',
      dataIndex: 'isDefault',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img style={{width: 14, height: 14}} src={`${imgDomainName}alcohol/1903131439581480.png`} />
      ),
    },
    {
      title: '颜色',
      dataIndex: 'color',
      render: text => <Tag color={text} style={{width: 50}}/>
    },
    {
      title: '排序',
      dataIndex: 'sort',
    },
    {
      title: '操作',
      width: '18%',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isDefault ? '取消' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <Popconfirm
            title="确认删除？"
            onConfirm={() => this.recordRemove(record.storeLabelId)}
            okText="确认"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  recordRemove = storeLabelIds => {
    console.log('storeLabelIds: ', storeLabelIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'tags/removeTag',
      payload: { storeLabelIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'tags/changeTagStatus',
      payload: { storeLabelId: record.storeLabelId, isDefault: !record.isDefault },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    //标签列表
    dispatch({
      type: 'tags/fetchTagList',
    });

    //标签类型列表
    dispatch({
      type: 'tags/fetchTagTypeList',
      payload: { dictCode: 'labelType' }
    });

    //标签颜色列表
    dispatch({
      type: 'tags/fetchTagColorList',
      payload: { dictCode: 'color' }
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'tags/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'tags/addTag',
      payload: { ...fields },
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'tags/updateTag',
      payload: {
        ...fields,
        storeLabelId: record.storeLabelId,
        typeCode: record.typeCode
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'tags/fetchStoreTags',
        payload: fieldsValue
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="门店名称">
              {getFieldDecorator('name')(<Input placeholder="请输入门店名称" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'tags/fetchStoreTags',
    });
  };

  render() {
    const {
      tags: { tagList, tagTypeList, tagColorList },
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            {/*<div className={styles.tableListForm}>{this.renderSimpleForm()}</div>*/}
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新增
              </Button>
              {/*<Button >*/}
                {/*开启默认*/}
              {/*</Button>*/}
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="确认批量删除？"
                    onConfirm={() =>
                      this.recordRemove(selectedRows.map(item => item.storeLabelId).join(','))
                    }
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button>批量删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              data={tagList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="storeLabelId"
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} tagTypeList={tagTypeList} tagColorList={tagColorList} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} record={record} tagTypeList={tagTypeList} tagColorList={tagColorList} />
      </div>
    );
  }
}

export default StoreTags;
