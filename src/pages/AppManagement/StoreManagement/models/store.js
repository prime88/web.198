// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryStoreList,
  addStore,
  updateStore,
  removeStore,
  changeStoreStatus,
} from '@/services/store';
import { fetchBranchList } from '@/services/branch';
import { queryDictionary } from '@/services/common-api';
import { message } from 'antd';
import { fetchTagList } from '@/services/tags';

export default {
  namespace: 'store',

  state: {
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //部门列表
    branchList: [],
    //标签列表
    tagList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchBranchList({ payload, callback }, { call, put }) {
      const response = yield call(fetchBranchList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveBranchList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchTagList({ payload, callback }, { call, put }) {
      const response = yield call(fetchTagList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
        return;
      }
      yield put({
        type: 'saveTagList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *addStore({ payload, callback }, { call, put }) {
      const response = yield call(addStore, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchStoreList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *removeStore({ payload, callback }, { call, put }) {
      const response = yield call(removeStore, payload);
      const jsonResponse = JSON.parse(response);
      if (jsonResponse.code === '0') {
        message.success(jsonResponse.msg);
        yield put({
          type: 'fetchStoreList',
        });
        if (callback) callback();
      } else {
        message.error(jsonResponse.msg);
      }
    },
    *updateStore({ payload, callback }, { call, put }) {
      const response = yield call(updateStore, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchStoreList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
    *changeStoreStatus({ payload, callback }, { call, put }) {
      const response = yield call(changeStoreStatus, payload);
      if (response.code === '0') {
        message.success(response.msg);
        yield put({
          type: 'fetchStoreList',
        });
        if (callback) callback();
      } else {
        message.error(response.msg);
      }
    },
  },

  reducers: {
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveBranchList(state, action) {
      return {
        ...state,
        branchList: action.payload,
      };
    },
    saveTagList(state, action) {
      return {
        ...state,
        tagList: action.payload,
      };
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //代金券
        if (location.pathname === '/app-management/store-management/store-list') {
          dispatch({
            type: 'fetchStoreList',
          })
        }
      });
    },
  },
};
