import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Modal,
  Divider,
  Popconfirm,
  Upload,
  message,
  TimePicker,
  Row,
  Col,
  Dropdown,
  Menu,
  TreeSelect,
  InputNumber,
  Tag,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import { isStoreNameRepeated } from '@/services/store';
import { imgDomainName } from '../../../constants/index';
import { queryGadMapInputtipsList } from '@/services/common-api';
import { Map, Marker } from 'react-amap';
import moment from 'moment';
import styles from './StoreList.less';
import { apiDomainName } from '../../../constants';
import ImagePreviewModal from '@/components/ImagePreviewModal';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const TreeNode = TreeSelect.TreeNode;

@Form.create()
class CreateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    tips: [],
    center: [0, 0],
  };

  handleSearch = value => {
    console.log('handleSearch-value: ', value);
    if (!value) return;
    // 获取位置信息列表
    const response = queryGadMapInputtipsList({ keywords: value.replace(/\s*/g, '') });
    console.log('response: ', response.then());
    response.then(result => {
      console.log('result: ', result);
      this.setState({ tips: result.tips });
    });
  };

  okHandle = () => {
    const { form, handleAdd } = this.props;
    const { center } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.storeUrl =
        fieldsValue.storeUrl.length > 0 ? fieldsValue.storeUrl[0].response.msg : '';
      fieldsValue.startBusinessHours = fieldsValue.startBusinessHours.format('HH:mm');
      fieldsValue.endBusinessHours = fieldsValue.endBusinessHours.format('HH:mm');
      fieldsValue.coordinate = center.join(',');
      const location = fieldsValue.location.split(',');
      fieldsValue.location = location.length > 1 ? location[1] : '';
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      this.setState({ imageUrl: '', center: [0, 0] });
      handleAdd(fieldsValue);
    });
  };

  checkName = (rule, value, callback) => {
    if (!value) {
      callback();
      return;
    }
    const response = isStoreNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        // console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  onPositionChange = value => {
    console.log('onPositionChange-value: ', value);
    const { tips } = this.state;
    this.setState({ position: value });

    const target = tips[value.split(',')[0]];
    if (typeof target.location === 'string') {
      this.setState({ center: target.location.split(',') });
    } else {
      this.setState({ center: [0, 0] });
    }
  };

  loadData = dataTree => {
    return dataTree.map(item => {
      return (
        <TreeNode title={item.name} key={item.branchId} value={item.branchId}>
          {item.childList ? this.loadData(item.childList) : null}
        </TreeNode>
      );
    });
  };

  render() {
    const { modalVisible, form, handleAdd, handleModalVisible, branchList } = this.props;
    const { loading, imageUrl, tips, center } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        title="新增门店"
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleModalVisible();
          this.setState({ imageUrl: '', center: [0, 0] });
        }}
        width={600}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店名称">
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: '请输入门店名称！' }, { validator: this.checkName }],
          })(<Input placeholder="请输入门店名称" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店图片">
          {form.getFieldDecorator('storeUrl', {
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请选择门店图片！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店地址">
          {form.getFieldDecorator('address', {
            rules: [{ required: true, message: '请输入门店地址！' }],
          })(<Input placeholder="请输入门店地址" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="定位地址">
          {form.getFieldDecorator('location', {
            rules: [{ required: true, message: '请选择定位地址！' }],
          })(
            <Select
              showSearch
              // value={position}
              placeholder="请输入定位地址"
              style={{ width: '100%' }}
              defaultActiveFirstOption={false}
              showArrow={false}
              filterOption={false}
              onSearch={this.handleSearch}
              onChange={this.onPositionChange}
              notFoundContent={null}
              allowClear
            >
              {tips.map((item, index) => (
                <Select.Option key={`${index},${item.name}`}>{item.name}</Select.Option>
              ))}
            </Select>
          )}
          <div style={{ width: '100%', height: 250 }}>
            <Map center={center} amapkey="质数-金汤面-管理端" zoom={16}>
              <Marker position={center} />
            </Map>
          </div>
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店电话">
          {form.getFieldDecorator('phone', {
            rules: [{ required: true, message: '请输入门店电话！' }],
          })(<Input placeholder="请输入门店电话" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开店时间">
          {form.getFieldDecorator('startBusinessHours', {
            rules: [{ required: true, message: '请选择开店时间！' }],
          })(<TimePicker format="HH:mm" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="关店时间">
          {form.getFieldDecorator('endBusinessHours', {
            rules: [{ required: true, message: '请选择关店时间！' }],
          })(<TimePicker format="HH:mm" />)}
        </FormItem>
        {/*<FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="所属部门">*/}
        {/*{form.getFieldDecorator('branchId', {*/}
        {/*rules: [{ required: true, message: '请选择部门！' }],*/}
        {/*})(*/}
        {/*<TreeSelect*/}
        {/*showSearch*/}
        {/*style={{ width: '100%' }}*/}
        {/*// dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}*/}
        {/*placeholder="请选择部门"*/}
        {/*allowClear*/}
        {/*treeDefaultExpandAll*/}
        {/*>*/}
        {/*{this.loadData(branchList.length > 0 ? branchList[0].childList : [])}*/}
        {/*</TreeSelect>*/}
        {/*)}*/}
        {/*</FormItem>*/}
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店介绍">
          {form.getFieldDecorator('introduce')(
            <Input.TextArea rows={3} placeholder="请输入门店介绍" />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark')(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  state = {
    loading: false,
    imageUrl: '',
    tips: [],
    center: [0, 0],
    record: {},
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { record } = nextProps; //imageUrl
    const { center, imageUrl } = prevState;
    let newState = null;
    // if (record.coordinate && record.coordinate !== center.join(',')) {
    if (record.coordinate && center.join(',') === '0,0') {
      newState = { center: record.coordinate.split(',') };
    }
    if (record.storeUrl && !imageUrl) {
      newState = {
        ...newState,
        imageUrl: `${imgDomainName}${record.storeUrl}`,
      };
    }
    return newState;
  }

  loadData = dataTree => {
    return dataTree.map(item => {
      return (
        <TreeNode title={item.name} key={item.branchId} value={item.branchId}>
          {item.childList ? this.loadData(item.childList) : null}
        </TreeNode>
      );
    });
  };

  handleSearch = value => {
    console.log('handleSearch-value: ', value);
    if (!value) return;
    // 获取位置信息列表
    const response = queryGadMapInputtipsList({ keywords: value.replace(/\s*/g, '') });
    console.log('response: ', response.then());
    response.then(result => {
      console.log('result: ', result);
      this.setState({ tips: result.tips });
    });
  };

  okHandle = () => {
    const { form, handleUpdate } = this.props;
    const { center } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      fieldsValue.storeUrl =
        fieldsValue.storeUrl.length > 1 ? fieldsValue.storeUrl[1].response.msg : '';
      fieldsValue.startBusinessHours = fieldsValue.startBusinessHours.format('HH:mm');
      fieldsValue.endBusinessHours = fieldsValue.endBusinessHours.format('HH:mm');
      fieldsValue.coordinate = center.join(',');
      const location = fieldsValue.location.split(',');
      fieldsValue.location = location.length > 1 ? location[1] : '';
      fieldsValue.labelIds = fieldsValue.labelIds.join(',');
      console.log('fieldsValue: ', fieldsValue);
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  checkName = (rule, value, callback) => {
    const { record } = this.props;
    if (!value) {
      callback();
      return;
    }
    if (value === record.name) {
      callback();
      return;
    }

    const response = isStoreNameRepeated({ name: value });
    response.then(result => {
      if (!result.data) {
        callback();
        return;
      }
      callback('存在相同的名称!');
    });
  };

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  handleChange = info => {
    console.log('info: ', info);
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // console.log('info.file: ', info.file);
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl => {
        console.log('imageUrl: ', imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  onPositionChange = value => {
    console.log('onPositionChange-value: ', value);
    const { tips } = this.state;
    this.setState({ position: value });

    const target = tips[value.split(',')[0]];
    if (typeof target.location === 'string') {
      this.setState({ center: target.location.split(',') });
    } else {
      this.setState({ center: [0, 0] });
    }
  };

  render() {
    const {
      updateModalVisible,
      form,
      handleUpdate,
      handleUpdateModalVisible,
      record,
      branchList,
      tagList,
    } = this.props;
    const { loading, imageUrl, tips, center } = this.state;

    const uploadButton = (
      <div style={{ width: 120, height: 120, paddingTop: loading ? 40 : 34 }}>
        <Icon type="plus" style={{ display: loading ? 'none' : 'inline-block' }} />
        <div className="ant-upload-text">{loading ? '上传中...' : '上传'}</div>
      </div>
    );

    return (
      <Modal
        destroyOnClose
        // title="修改门店"
        title={
          <div>
            <span>修改门店</span>
            <Button
              type="primary"
              style={{ float: 'right', marginRight: 16 }}
              onClick={this.okHandle}
            >
              确定
            </Button>
          </div>
        }
        visible={updateModalVisible}
        onOk={this.okHandle}
        onCancel={() => {
          handleUpdateModalVisible();
          this.setState({ imageUrl: '' });
        }}
        width={600}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店名称">
          {record.name}
          {/*{form.getFieldDecorator('name', {*/}
          {/*rules: [*/}
          {/*{ required: true, message: '请输入门店名称！' },*/}
          {/*{ validator: this.checkName }*/}
          {/*],*/}
          {/*initialValue: record.name,*/}
          {/*})(<Input placeholder="请输入门店名称" />)}*/}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店图片">
          {form.getFieldDecorator('storeUrl', {
            initialValue: record.storeUrl ? [{ response: { msg: record.storeUrl } }] : '',
            valuePropName: 'fileList',
            getValueFromEvent: this.normFile,
            rules: [{ required: true, message: '请选择门店图片！' }],
          })(
            <Upload
              name="image"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${apiDomainName}/image/upload`}
              // beforeUpload={this.beforeUpload}
              onChange={this.handleChange}
            >
              {/*<Button>*/}
              {/*<Icon type="upload" /> Click to upload*/}
              {/*</Button>*/}
              {imageUrl ? (
                <img style={{ width: 120, height: 120 }} src={imageUrl} alt="avatar" />
              ) : (
                uploadButton
              )}
            </Upload>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店地址">
          {form.getFieldDecorator('address', {
            rules: [{ required: true, message: '请输入门店地址！' }],
            initialValue: record.address,
          })(<Input placeholder="请输入门店地址" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="定位地址">
          {form.getFieldDecorator('location', {
            rules: [{ required: true, message: '请选择定位地址！' }],
            initialValue: record.location,
          })(
            <Select
              showSearch
              // value={position}
              placeholder="请输入定位地址"
              style={{ width: '100%' }}
              defaultActiveFirstOption={false}
              showArrow={false}
              filterOption={false}
              onSearch={this.handleSearch}
              onChange={this.onPositionChange}
              notFoundContent={null}
              allowClear
            >
              {tips.map((item, index) => (
                <Select.Option key={`${index},${item.name}`}>{item.name}</Select.Option>
              ))}
            </Select>
          )}
          <div style={{ width: '100%', height: 250 }}>
            <Map center={center} amapkey="质数-金汤面-管理端" zoom={16}>
              <Marker position={center} />
            </Map>
          </div>
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店电话">
          {form.getFieldDecorator('phone', {
            rules: [{ required: true, message: '请输入门店电话！' }],
            initialValue: record.phone,
          })(<Input placeholder="请输入门店电话" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="开店时间">
          {form.getFieldDecorator('startBusinessHours', {
            rules: [{ required: true, message: '请选择开店时间！' }],
            initialValue: record.startBusinessHours
              ? moment(record.startBusinessHours, 'HH:mm')
              : '',
          })(<TimePicker format="HH:mm" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="关店时间">
          {form.getFieldDecorator('endBusinessHours', {
            rules: [{ required: true, message: '请选择关店时间！' }],
            initialValue: record.endBusinessHours ? moment(record.endBusinessHours, 'HH:mm') : '',
          })(<TimePicker format="HH:mm" />)}
        </FormItem>
        {/*<FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="所属部门">*/}
        {/*{form.getFieldDecorator('branchId', {*/}
        {/*rules: [{ required: true, message: '请选择部门！' }],*/}
        {/*initialValue: record.branchId*/}
        {/*})(*/}
        {/*<TreeSelect*/}
        {/*showSearch*/}
        {/*style={{ width: '100%' }}*/}
        {/*// dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}*/}
        {/*placeholder="请选择部门"*/}
        {/*allowClear*/}
        {/*treeDefaultExpandAll*/}
        {/*>*/}
        {/*{this.loadData(branchList.length > 0 ? branchList[0].childList : [])}*/}
        {/*</TreeSelect>*/}
        {/*)}*/}
        {/*</FormItem>*/}
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="配送费用(¥)">
          {form.getFieldDecorator('distributionCost', {
            rules: [{ required: true, message: '请输入配送费用！' }],
            initialValue: record.distributionCost,
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入配送费用"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="起送价(¥)">
          {form.getFieldDecorator('distributionPriceLimit', {
            rules: [{ required: true, message: '请输入起送价！' }],
            initialValue: record.distributionPriceLimit,
          })(
            <InputNumber
              precision={2}
              style={{ width: '100%' }}
              placeholder="请输入起送价"
              min={0}
            />
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="自取限制天数">
          {form.getFieldDecorator('selfTakingDay', {
            rules: [{ required: true, message: '请输入自取限制天数！' }],
            initialValue: record.selfTakingDay,
          })(<InputNumber style={{ width: '100%' }} placeholder="请输入自取限制天数" min={1} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="收货人">
          {form.getFieldDecorator('consignee', {
            rules: [{ required: true, message: '请输入收货人！' }],
            initialValue: record.consignee,
          })(<Input placeholder="请输入收货人(退货专用)" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="门店介绍">
          {form.getFieldDecorator('introduce', {
            initialValue: record.introduce,
          })(<Input.TextArea rows={3} placeholder="请输入门店介绍" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标签">
          {form.getFieldDecorator('labelIds', {
            // rules: [{ required: true, message: '请选择门店标签！' }],
            initialValue: record.labelIds ? record.labelIds.split(',') : [],
          })(
            <Select mode="multiple" style={{ width: '100%' }} placeholder="请选择门店标签">
              {tagList.rows.map(item => (
                <Select.Option key={item.storeLabelId}>
                  {item.name}
                  {/*<Tag color={item.color}>{item.name}</Tag>*/}
                </Select.Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="备注">
          {form.getFieldDecorator('remark', {
            initialValue: record.remark,
          })(<Input.TextArea rows={3} placeholder="请输入备注" />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ store, loading }) => ({
  store,
  loading: loading.models.store,
}))
@Form.create()
class StoreList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    previewVisible: false,
    imageList: [],
  };

  onImageClick = images => {
    console.log('images: ', images);
    this.setState({ imageList: images.split(','), previewVisible: true });
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'storeId',
      width: 70,
      // fixed: 'left',
    },
    {
      title: '门店图片',
      dataIndex: 'storeUrl',
      // fixed: 'left',
      render: text =>
        text ? (
          <img
            onClick={() => this.onImageClick(text)}
            style={{ width: 60, height: 60 }}
            alt=""
            src={`${imgDomainName}${text}`}
          />
        ) : (
          ''
        ),
    },
    {
      title: '门店名称',
      dataIndex: 'name',
      // fixed: 'left',
      width: 180,
    },
    {
      title: '门店地址',
      dataIndex: 'address',
    },
    {
      title: '门店电话',
      dataIndex: 'phone',
    },
    {
      title: '配送费用(¥)',
      dataIndex: 'distributionCost',
    },
    {
      title: '起送价(¥)',
      dataIndex: 'distributionPriceLimit',
    },
    {
      title: '自取限制天数',
      dataIndex: 'selfTakingDay',
    },
    {
      title: '门店介绍',
      dataIndex: 'introduce',
    },
    {
      title: '开店时间',
      dataIndex: 'startBusinessHours',
    },
    {
      title: '关店时间',
      dataIndex: 'endBusinessHours',
    },
    // {
    //   title: '所属部门',
    //   dataIndex: 'branch',
    // },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img
            style={{ width: 14, height: 14 }}
            src={`${imgDomainName}alcohol/1903131439581480.png`}
          />
        ),
    },
    {
      title: '收货人',
      dataIndex: 'consignee',
    },
    {
      title: '标签',
      dataIndex: 'label',
      render: (text, record) =>
        text &&
        text
          .split(',')
          .map((item, index) => (
            <Tag color={record.colors && record.colors.split(',')[index]}>{item}</Tag>
          )),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      width: 120,
      fixed: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.changeStatus(record)}>{record.isEnable ? '关闭' : '开启'}</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          {/*<Divider type="vertical" />*/}
          {/*<Popconfirm*/}
          {/*title="确认删除？"*/}
          {/*onConfirm={() => this.recordRemove(record.storeId)}*/}
          {/*okText="确认"*/}
          {/*cancelText="取消"*/}
          {/*>*/}
          {/*<a href="#">删除</a>*/}
          {/*</Popconfirm>*/}
        </Fragment>
      ),
    },
  ];

  recordRemove = storeIds => {
    console.log('storeIds: ', storeIds);
    const { dispatch } = this.props;
    dispatch({
      type: 'store/removeStore',
      payload: { storeIds },
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  changeStatus = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'store/changeStoreStatus',
      payload: { storeId: record.storeId, isEnable: !record.isEnable },
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    //门店列表
    dispatch({
      type: 'store/fetchStoreList',
    });

    //部门列表
    dispatch({
      type: 'store/fetchBranchList',
    });

    //门店标签列表
    dispatch({
      type: 'store/fetchTagList',
      payload: {
        typeCode: '1',
      },
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'store/fetch',
      payload: params,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    console.log('record: ', record);
    this.setState({
      updateModalVisible: !!flag,
      record: flag ? record : {},
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'store/addStore',
      payload: fields,
    });

    // message.success('新增成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record } = this.state;
    dispatch({
      type: 'store/updateStore',
      payload: {
        ...fields,
        storeId: record.storeId,
      },
    });

    // message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'store/fetchStoreList',
        payload: fieldsValue,
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="门店名称">
              {getFieldDecorator('name')(<Input placeholder="请输入门店名称" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: 'store/fetchStoreList',
    });
  };

  render() {
    const {
      store: { storeList, branchList, tagList },
      loading,
    } = this.props;
    const {
      selectedRows,
      modalVisible,
      updateModalVisible,
      record,
      previewVisible,
      imageList,
    } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
            {/*<div className={styles.tableListOperator}>*/}
            {/*/!*<Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>*!/*/}
            {/*/!*新增*!/*/}
            {/*/!*</Button>*!/*/}
            {/*{selectedRows.length > 0 && (*/}
            {/*<span>*/}
            {/*<Popconfirm*/}
            {/*title="确认批量删除？"*/}
            {/*onConfirm={() =>*/}
            {/*this.recordRemove(selectedRows.map(item => item.storeId).join(','))*/}
            {/*}*/}
            {/*okText="确认"*/}
            {/*cancelText="取消"*/}
            {/*>*/}
            {/*<Button>批量删除</Button>*/}
            {/*</Popconfirm>*/}
            {/*</span>*/}
            {/*)}*/}
            {/*</div>*/}
            <StandardTable
              // selectedRows={selectedRows}
              loading={loading}
              data={storeList}
              columns={this.columns}
              // onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="storeId"
              // expandedRowRender={record => <p style={{ margin: 0 }}>备注: {record.remark}</p>}
              // onExpand={() => {}}
              scroll={{ x: 2000 }}
            />
          </div>
        </Card>
        {/*<CreateForm {...parentMethods} modalVisible={modalVisible} branchList={branchList} />*/}
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          record={record}
          branchList={branchList}
          tagList={tagList}
        />
        <ImagePreviewModal
          previewVisible={previewVisible}
          imageList={imageList}
          handleCancel={() => this.setState({ previewVisible: false })}
          imgWidth={250}
          imgHeight={250}
          colSpan={24}
        />
      </div>
    );
  }
}

export default StoreList;
