import React, { PureComponent, Fragment } from 'react';
import { Table, Input, Button, Popconfirm, Form, InputNumber, Select } from 'antd';
// import styles from "../../pages/Procurement/ProcurementPlan/ProcurementPlanOrder.less";
import styles from "./index.less";

const FormItem = Form.Item;
const Option = Select.Option;

const EditableContext = React.createContext();

// const EditableRow = ({ form, index, ...props }) => (
//   <EditableContext.Provider value={form}>
//     <tr {...props} />
//   </EditableContext.Provider>
// );
//
// const EditableFormRow = Form.create()(EditableRow);

@Form.create()
class EditableCell extends PureComponent {
  state = {
    editing: false,
    selectOptions: [],
  }

  componentDidMount() {
    // if (this.props.editable) {
    //   document.addEventListener('click', this.handleClickOutside, true);
    // }
    const {
      isSelect,
      editable,
    } = this.props;
    if (editable && !isSelect) {
      document.addEventListener('click', this.handleClickOutside, true);
    }
  }

  componentWillUnmount() {
    if (this.props.editable) {
      document.removeEventListener('click', this.handleClickOutside, true);
    }
  }

  toggleEdit = () => {
    const editing = !this.state.editing;
    this.setState({ editing }, () => {
      if (editing) {
        this.input.focus();

        //回调，用于获取像select下拉列表一样的数据
        const {
          isSelect,
          selectFocus,
          form,
          record,
        } = this.props;
        if (isSelect && selectFocus) {//获取select下拉列表
          selectFocus({ ...record, ...form.getFieldsValue() }, (selectOptions) => {
            console.log('selectOptions: ', selectOptions);
            this.setState({ selectOptions })
          });
        }
      }
    });
  }

  handleClickOutside = (e) => {
    const { editing } = this.state;
    if (editing && this.cell !== e.target && !this.cell.contains(e.target)) {
      this.save();
    }
  }

  save = () => {
    const { record, handleSave, form } = this.props;
    form.validateFields((error, values) => {
      if (error) {
        return;
      }
      this.toggleEdit();
      handleSave({ ...record, ...values });
    });
  }

  getInputNumberProps = () => {
    let result = {};
    const {
      inputNumberPrecision,
      inputNumberMin,
      inputNumberChange,
      record,
      form,
    } = this.props;
    if (!isNaN(inputNumberPrecision)) {
      result['precision'] = inputNumberPrecision;
    }
    if (!isNaN(inputNumberMin)) {
      result['min'] = inputNumberMin;
    }
    if (inputNumberChange) {
      result['onChange'] = (value) => {
        inputNumberChange(value, { ...record, ...form.getFieldsValue() })
      };
    }
    return result;
  }

  getSelectProps = () => {
    let result = {};
    const {
      selectChange,
      record,
      form,
    } = this.props;
    if (selectChange) {
      result['onChange'] = (value, e) => {
        console.log('e: ', e);
        const stockNumber = e.props.number;
        selectChange(value, stockNumber, { ...record, ...form.getFieldsValue() })
        const editing = !this.state.editing;
        this.setState({ editing })

        // 自定义业务需求
        // 当选择的批次在库存中的数量小于要出库的商品数量时：a、提示用户“该批次最多只能出库n件”；b、将用户手动输入的主数量限制最大是n
        // record.batchNumber ? e.props.number
        // if (e.props.number < record.batchNumber) {//库存数 < 要出库的数
        //
        // }
      };
    }
    return result;
  }

  onDropdownVisibleChange = (open) => {
    console.log('open: ', open);
    if (!open){
      this.toggleEdit();
    }
  }

  render() {
    const { editing, selectOptions } = this.state;
    const {
      editable,
      isInputNumber,
      isSelect,
      // selectOptions,
      dataIndex,
      record,
      form: { getFieldDecorator },
      ...restProps
    } = this.props;
    const inputNumberProps = this.getInputNumberProps();
    const selectProps = this.getSelectProps();
    return (
      <td ref={node => (this.cell = node)} {...restProps}>
        {editable ? (
          <EditableContext.Consumer>
            {(form) => {
              this.form = form;
              return (
                editing ? (
                  <FormItem style={{ margin: 0 }}>
                    {getFieldDecorator(dataIndex, {
                      rules: [{
                        // required: dataIndex === 'number',
                        required: isInputNumber,
                        // message: `${title} is required.`,
                        message: <span
                          style={{
                            fontSize: 12,
                            position: 'absolute',
                            marginTop: -4
                          }}
                        >
                          请输入
                        </span>,
                      }],
                      initialValue: record[dataIndex],
                    })(
                      isInputNumber ?
                        <InputNumber
                          ref={node => (this.input = node)}
                          onPressEnter={this.save}
                          { ...inputNumberProps }
                        />
                        :
                        (
                          isSelect ?
                            <Select
                              ref={node => (this.input = node)}
                              onPressEnter={this.save}
                              style={{width: '100%'}}
                              onDropdownVisibleChange={this.onDropdownVisibleChange}
                              { ...selectProps }
                            >
                              {selectOptions.map(item => <Option number={item.number} value={item.value}>{item.label}</Option>)}
                            </Select>
                            :
                          <Input
                            ref={node => (this.input = node)}
                            onPressEnter={this.save}
                          />
                        )
                    )}
                  </FormItem>
                ) : (
                  <div
                    // className="editable-cell-value-wrap"
                    className={styles["editable-cell-value-wrap"]}
                    style={{ paddingRight: 24 }}
                    onClick={this.toggleEdit}
                  >
                    {restProps.children}
                  </div>
                )
              );
            }}
          </EditableContext.Consumer>
        ) : restProps.children}
      </td>
    );
  }
}

export default EditableCell;
