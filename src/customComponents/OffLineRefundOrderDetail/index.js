import React, { Component } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider, Row, Col } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './index.less';
import NP from 'number-precision';

const { Description } = DescriptionList;

const renderContent = (value, row, index) => {
  const obj = {
    children: value,
    props: {},
  };
  // if (index === 4) {
  obj.props.colSpan = 0;
  // }
  return obj;
};

@connect(({ offLineSaleOrderDetail, loading }) => ({
  offLineSaleOrderDetail,
  // loading: loading.effects['profile/fetchBasic'],
}))
class OffLineRefundOrderDetail extends Component {
  state = {
    dataSource: [],
    payDataSource: [],
  };

  componentDidMount() {
    const { dispatch, record } = this.props;
    const salesReturnGoodsId = record.salesReturnGoodsId;
    if (salesReturnGoodsId) {
      dispatch({
        type: 'saleReturn/fetchsalesReturnGoodsDetails',
        payload: {
          salesReturnGoodsId,
        },
        callback: data => {
          console.log('data: ', data);
          this.setState({ dataSource: data });
        },
      });
      // dispatch({
      //   type: 'offLineSaleOrderDetail/fetchOrderDetailList',
      //   payload: {
      //     salesOrderId
      //   },
      //   callback: (data) => {
      //     //一、商品明细
      //     this.setState({ dataSource: data })
      //   }
      // })
    }

    //二、支付明细
    // const payDataSource = [];
    // if (record.cashPayment){
    //   payDataSource.push({ payMethod: '现金', payAmount: record.cashPayment})
    // }
    // if (record.alipayPayment){
    //   payDataSource.push({ payMethod: '支付宝', payAmount: record.alipayPayment})
    // }
    // if (record.weChatPayment){
    //   payDataSource.push({ payMethod: '微信', payAmount: record.weChatPayment})
    // }
    // if (record.bankCardPayment){
    //   payDataSource.push({ payMethod: '银行卡', payAmount: record.bankCardPayment})
    // }
    // this.setState({ payDataSource })
  }


  getReturnTotalPrice = (row) => {
    let returnTotalPrice = 0;
    const { dataSource } = this.state;
    const { record } = this.props;
    // 两种情况
    const a = dataSource.map(item => NP.times(item.number, item.referencePrice)).reduce((a, b) => NP.plus(a, b));
    if (record.refundAmount === a) { // 不是最后一次退货,按退货总额按售价*退后数计算
      // console.log('a1: ', a);
      // console.log('row: ', row);
      // console.log('dataSource: ', dataSource);
      returnTotalPrice = NP.times(row.referencePrice, parseInt(row.number))
    }
    else { // 最后一次退完时，按销售额-单价*(总数-退货数)
      // console.log('a2: ', a);
      // console.log('row: ', row);
      // console.log('dataSource: ', dataSource);
      returnTotalPrice = NP.minus(row.totalPrice, NP.times(row.referencePrice, (NP.minus(row.totalNumber, parseInt(row.number)))))
    }
    return returnTotalPrice;
  }

  // componentWillReceiveProps(nextProps) {
  //   debugger
  //   const { dispatch, record } = nextProps;
  //   const salesOrderId = record.salesOrderId;
  //   console.log('salesOrderId: ', salesOrderId);
  //   if (salesOrderId) {
  //     dispatch({
  //       type: 'offLineSaleOrderDetail/fetchOrderDetailList',
  //       payload: {
  //         salesOrderId
  //       },
  //       callback: (data) => {
  //         this.setState({ dataSource: data })
  //       }
  //     })
  //   }
  // }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   const { dispatch, record } = nextProps;
  //   const salesOrderId = record.salesOrderId;
  //   if (salesOrderId) {
  //     dispatch({
  //       type: 'offLineSaleOrderDetail/fetchOrderDetailList',
  //       payload: {
  //         salesOrderId
  //       },
  //       callback: (data) => {
  //         return {
  //           dataSource: data
  //         }
  //       }
  //     })
  //   }
  // }

  columns = [
    {
      title: <span className={styles.font}>商品名称</span>,
      dataIndex: 'name',
      className: styles.customColumn,
      render: (text, record) => {
        return {
          children: (
            <div>
              <Row>
                <Col span={24}>
                  <span style={{ color: 'rgba(0, 0, 0, 0.85)' }} className={styles.font}>
                    {record.name}
                  </span>
                </Col>
              </Row>
              <Row>
                <Col span={5} offset={4} style={{ textAlign: 'center' }}>
                  <span style={{ color: 'rgba(0, 0, 0, 0.85)' }} className={styles.font}>
                    {record.stockNum}
                  </span>
                </Col>
                <Col span={5} style={{ textAlign: 'center' }}>
                  <span style={{ color: 'rgba(0, 0, 0, 0.85)' }} className={styles.font}>
                    {record.referencePrice}
                  </span>
                </Col>
                <Col span={4} style={{ textAlign: 'center' }}>
                  <span style={{ color: 'rgba(0, 0, 0, 0.85)' }} className={styles.font}>
                    {record.number}
                  </span>
                </Col>
                {/*<Col span={4} style={{textAlign: 'center'}}><span style={{color: 'rgba(0, 0, 0, 0.85)'}} className={styles.font}>{((record.referencePrice*100) * parseInt(record.number))/100}</span></Col>*/}
                <Col span={4} style={{ textAlign: 'center' }}>
                  <span style={{ color: 'rgba(0, 0, 0, 0.85)' }} className={styles.font}>
                    { this.getReturnTotalPrice(record) }
                    {/*{ record.totalNumber === record.returnGoodsNumber ? record.totalPrice : NP.times(record.referencePrice, parseInt(record.number)) }*/}
                    {/*{NP.times(record.referencePrice, parseInt(record.number))}*/}
                  </span>
                </Col>
              </Row>
            </div>
          ),
          props: {
            colSpan: 5,
          },
        };
      },
    },
    {
      title: <span className={styles.font}>存货编号</span>,
      dataIndex: 'stockNum',
      render: renderContent,
    },
    {
      title: <span className={styles.font}>单价(¥)</span>,
      dataIndex: 'referencePrice',
      render: renderContent,
    },
    {
      title: <span className={styles.font}>退货数量</span>,
      dataIndex: 'number',
      render: renderContent,
    },
    {
      title: <span className={styles.font}>退货金额(¥)</span>,
      dataIndex: '',
      render: renderContent,
    },
  ];

  getStatus = status => {
    let result = '';

    if (!status && status !== 0) return result;

    switch (status.toString()) {
      case '0':
        result = '待付款';
        break;
      case '1':
        // result = '待收货/待自取';
        result = '待自取';
        break;
      case '2':
        result = '待评价';
        break;
      case '3':
        result = '已完成';
        break;
      case '4':
        result = '已关闭';
        break;
    }
    return result;
  };

  getPaymentMethod = paymentMethod => {
    let result = '';

    if (!paymentMethod) return result;

    switch (paymentMethod.toString()) {
      case '0':
        result = '微信支付';
        break;
      case '1':
        result = '支付宝支付';
        break;
      case '2':
        result = '会员支付';
        break;
      case '3':
        result = '现金支付';
        break;
      case '4':
        result = '买赠支付';
        break;
    }
    return result;
  };

  getRealRefundMethod = refundMethod => {
    let result;
    switch (refundMethod) {
      case 0:
        result = '现金';
        break;
      case 1:
        result = '支付宝';
        break;
      case 2:
        result = '微信';
        break;
      case 3:
        result = '银行卡';
        break;
    }

    return result;
  };

  render() {
    const { record, isDeliveryOrder } = this.props;
    const { dataSource, payDataSource } = this.state;
    return (
      <div>
        <div>
          {/*<DescriptionList size="large" title="基本信息" style={{ marginLeft: 40, float: 'right' }}>*/}
          <DescriptionList size="small" className={styles.font} style={{ marginLeft: 15 }} col={1}>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', float: 'right' }}
              term="退货单号"
            >
              <span className={styles.font}>{record.returnNumber}</span>
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', float: 'right' }}
              term="退货时间"
            >
              <span className={styles.font}>{record.createTime}</span>
            </Description>
            {/*<Description style={{ display: 'block', boxSizing: 'border-box', float: 'right'}} term="出库仓库"><span className={styles.font}>{record.warehouse}</span></Description>*/}
            <Description
              style={{ display: 'block', boxSizing: 'border-box', float: 'right' }}
              term="会员卡号"
            >
              <span style={{ color: 'rgba(0, 0, 0, 0.85)' }}>{record.memberPhone}</span>
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', float: 'right' }}
              term="退款金额"
            >
              <span style={{ color: 'rgba(0, 0, 0, 0.85)' }}>{record.refundAmount}</span>
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', float: 'right' }}
              term="退款方式"
            >
              <span style={{ color: 'rgba(0, 0, 0, 0.85)' }}>
                {this.getRealRefundMethod(record.refundMethod)}
              </span>
            </Description>
            <Description
              style={{ display: 'block', boxSizing: 'border-box', float: 'right' }}
              term="退货原因"
            >
              <span style={{ color: 'rgba(0, 0, 0, 0.85)' }}>{record.reason}</span>
            </Description>
            {/*<Description style={{ display: isDeliveryOrder ? 'block' : 'none', boxSizing: 'border-box', width: '33.33333333%' }} term="订单备注"><span style={{color: 'rgba(0, 0, 0, 0.85)'}}>{record.remark}</span></Description>*/}
          </DescriptionList>
          {/*<Divider />*/}
          <Table
            // loading={loading}
            // style={{marginLeft: 40, marginRight: 100}}
            style={{ marginLeft: 0 }}
            dataSource={dataSource}
            columns={this.columns}
            rowKey="salesOrderGoodsId"
            // pagination={{defaultPageSize: 9}}
            size="small"
            pagination={false}
            className={styles.customTable}
            width={250}
            // footer={() => <span className={styles.font}>合计: ¥ {dataSource.length > 0 ? dataSource.map(item => item.totalPrice).reduce((a, b) => NP.plus(a, b)) : 0}</span>}
            footer={() => (
              <span className={styles.font}>
                合计: ¥{' '}
                {dataSource.length > 0
                  ? dataSource
                      // .map(item => item.totalNumber === item.returnGoodsNumber ? item.totalPrice : NP.times(item.referencePrice, parseInt(item.number)))
                      .map(item => this.getReturnTotalPrice(item))
                      .reduce((a, b) => NP.plus(a, b))
                  : 0}
              </span>
            )}
          />
        </div>
      </div>
    );
  }
}

export default OffLineRefundOrderDetail;
