import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Row, Col, Form, Input, Icon, Button, Menu } from 'antd';
import StandardTable from '@/components/StandardTable';
import { queryMeasurementUnitList } from '@/services/measurementUnit';
import { querySupplierList } from '@/services/supplier';
import { isInventoryNameRepeated } from '@/services/warehouse';
import styles from './index.less';
import { imgDomainName } from '../../constants';
import { fetchStockList } from '@/services/checkOrder';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ inventory, brand, measurementUnit, supplier, user, loading }) => ({
  inventory,
  brand,
  measurementUnit,
  supplier,
  user,
  loading: loading.models.inventory,
}))
@Form.create()
class InventoryList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    openKeys: ['sub1'],
    selectedMenuItemKey: '',
    addModalMeasurementUnitOptions: [],
    addModalSupplierClassificationOptions: [],
    record: {},
    dataPermission: false,
    stockDisplay: 'none',
    stockTarget: {},
  };

  columns = [
    {
      title: '存货编号',
      dataIndex: 'stockNum',
      width: 150,
      fixed: 'left',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
      width: 200,
      fixed: 'left',
    },
    {
      title: '物品分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '主单位',
      dataIndex: 'units',
    },
    {
      title: '辅单位',
      dataIndex: 'units2',
    },
    {
      title: '单位转换率',
      dataIndex: 'unitRatio',
    },
    {
      title: '最后一次采购价 (￥)',
      dataIndex: 'referenceCost',
      render: text => (this.state.dataPermission ? <span>{text}</span> : ''),
    },
    {
      title: '零售参考价 (￥)',
      dataIndex: 'referencePrice',
    },
    {
      title: '零售最低价 (￥)',
      dataIndex: 'lowerPrice',
    },
    {
      title: '零售最高价 (￥)',
      dataIndex: 'upperPrice',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '税率(%)',
      dataIndex: 'taxRate',
    },
    {
      title: '状态',
      dataIndex: 'isEnable',
      render: text =>
        text ? (
          <Icon type="check-circle" theme="twoTone" />
        ) : (
          <img
            style={{ width: 14, height: 14 }}
            src={`${imgDomainName}alcohol/1903131439581480.png`}
          />
        ),
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  changeStatus = record => {
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'inventory/changeInventory',
      payload: { stockId: record.stockId, isEnable: !record.isEnable },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      },
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey
    });
  };

  recordRemove = stockIds => {
    console.log('stockIds: ', stockIds);
    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;
    dispatch({
      type: 'inventory/removeInventory',
      payload: { stockIds },
      fetchForm: {
        ...form.getFieldsValue(),
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      },
      // selectedMenuItemKey: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
      callback: () => {
        this.setState({
          selectedRows: [],
        });
      },
    });
  };

  onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };

  componentDidMount() {
    const {
      dispatch,
      user: { currentUser },
    } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'inventory/fetchInventoryClassificationList',
    });

    //获取存货列表
    dispatch({
      type: 'inventory/fetchInventoryList',
      payload: {
        isEnable: true,
      },
    });

    //数据权限
    //1、判断是否有数据权限
    const permissionList = currentUser.permissionList;
    console.log('permissionList: ', permissionList);
    const target = permissionList ? permissionList.find(item => item.name === '查看全部门店') : [];
    if (target) {
      console.log('有数据权限');
      this.setState({ dataPermission: true });
    } else {
      console.log('没有数据权限');
      this.setState({ dataPermission: false });
    }
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'inventory/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { selectedMenuItemKey } = this.state;
    form.resetFields();
    dispatch({
      type: 'inventory/fetchInventoryList',
      payload: {
        stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
        isEnable: true,
      },
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  onRowClick = record => {
    console.log('record: ', record);
    console.log('oldSelectedRows: ', this.state.selectedRows);
    this.setState(prevState => {
      let newSelectedRows = [...prevState.selectedRows];
      let flag = true;
      for (let item of prevState.selectedRows) {
        if (item.stockId === record.stockId) {
          newSelectedRows = newSelectedRows.filter(item => item.stockId !== record.stockId);
          flag = false;
          break;
        }
      }
      if (flag) {
        newSelectedRows.push(record);
      }
      console.log('newSelectedRows: ', newSelectedRows);
      return {
        selectedRows: newSelectedRows,
      };
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { selectedMenuItemKey } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: 'inventory/fetchInventoryList',
        payload: {
          ...fieldsValue,
          stockClassifyId: selectedMenuItemKey === '-1' ? '' : selectedMenuItemKey,
          isEnable: true,
        },
      });
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { stockDisplay, record, stockTarget } = this.state;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="编号">
              {getFieldDecorator('stockNum')(<Input placeholder="请输入编号" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="名称">
              {getFieldDecorator('name')(<Input placeholder="请输入名称" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
          <Col md={6} sm={24} style={{ display: stockDisplay }}>
            <FormItem label="库存">
              <span>
                主数量 <span style={{ fontWeight: 'bold' }}>{stockTarget.number || 0}</span>{' '}
                {record.units}
                ，辅数量 <span style={{ fontWeight: 'bold' }}>{stockTarget.number2 || 0}</span>{' '}
                {record.units2}
              </span>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }

  onMenuItemClick = ({ item, key, selectedKeys }) => {
    console.log('key: ', key);
    const { dispatch, form } = this.props;

    //获取存货分类列表
    dispatch({
      type: 'inventory/fetchInventoryList',
      payload: {
        stockClassifyId: key === '-1' ? '' : key,
        isEnable: true,
        ...form.getFieldsValue(),
      },
    });

    this.setState({ selectedMenuItemKey: key });
  };

  onRowMouseEnter = record => {
    console.log('onRowMouseEnter-record: ', record);
    const {
      user: { currentUser },
    } = this.props;
    //查询库存数量
    const response = fetchStockList({
      stockNum: record.stockNum,
      branchId: currentUser.personnel.branchId,
    });
    response.then(result => {
      console.log('result: ', result);
      if (result && result.data.rows.length > 0) {
        this.setState({ stockTarget: result.data.rows[0] });
      }
    });

    this.setState({ stockDisplay: 'block', record });
  };

  onRowMouseLeave = record => {
    console.log('onRowMouseLeave-record: ', record);
    this.setState({ stockDisplay: 'none', record: {}, stockTarget: {} });
  };

  render() {
    const {
      inventory: { inventoryClassificationList, inventoryList },
      loading,
    } = this.props;

    const { selectedRows } = this.state;

    return (
      <div>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          <Row gutter={16}>
            <Col span={4}>
              <Menu
                mode="inline"
                openKeys={this.state.openKeys}
                onOpenChange={this.onOpenChange}
                defaultSelectedKeys={['-1']}
                onClick={this.onMenuItemClick}
              >
                <Menu.Item key={-1} value={-1}>
                  全部分类
                </Menu.Item>
                {inventoryClassificationList.rows.map(item => (
                  <Menu.Item key={item.stockClassifyId} value={item.stockClassifyId}>
                    {item.name}
                  </Menu.Item>
                ))}
              </Menu>
            </Col>
            <Col span={20}>
              <StandardTable
                selectedRows={selectedRows}
                loading={loading}
                data={inventoryList}
                columns={this.columns}
                onSelectRow={this.handleSelectRows}
                onChange={this.handleStandardTableChange}
                rowKey="stockId"
                scroll={{ x: 2000 }}
                size="small"
                onRow={record => {
                  return {
                    onClick: () => this.onRowClick(record),
                    onMouseEnter: event => this.onRowMouseEnter(record), // 鼠标移入行
                    onMouseLeave: event => this.onRowMouseLeave(record), // 鼠标移出行
                  };
                }}
              />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default InventoryList;
