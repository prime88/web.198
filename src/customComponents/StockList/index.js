import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import router from 'umi/router';
import styles from './index.less';
import { fetchStaffList } from '@/services/branch';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ stock, user, loading }) => ({
  stock,
  user,
  loading: loading.models.stock,
}))
@Form.create()
class StockList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    expandedRowKeys: [],
    purchasePersonnel: [],
    formValues: {},
    stepFormValues: {},
    record: {},
    detailModalVisible: false,
  };

  columns = [
    {
      title: '编号',
      dataIndex: 'warehouseStockId',
    },
    {
      title: '存货编号',
      dataIndex: 'stockNum',
    },
    {
      title: '存货名称',
      dataIndex: 'name',
    },
    {
      title: '品牌',
      dataIndex: 'brand',
    },
    {
      title: '规格型号',
      dataIndex: 'specification',
    },
    {
      title: '分类',
      dataIndex: 'stockClassify',
    },
    {
      title: '单位',
      dataIndex: 'units',
    },
    {
      title: '数量',
      dataIndex: 'number',
    },
    {
      title: '供应商',
      dataIndex: 'supplier',
    },
    {
      title: '仓库',
      dataIndex: 'warehouse',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  fetchStockList = (params = {}) => {
    const { dispatch, warehouseId = '' }  = this.props;
    dispatch({
      type: 'stock/fetchStockList',
      payload: {
        warehouseId,
        ...params
      }
    });
  }

  componentDidMount() {
    const { dispatch, user: { currentUser } } = this.props;
    //库存列表
    this.fetchStockList();

    //门店列表
    dispatch({
      type: 'stock/fetchStoreList',
    });

    //获取当前部门请购人员列表
    const personnel = currentUser.personnel;
    if (personnel) {
      dispatch({
        type: 'stock/fetchStaffList',
        payload: {
          branchId: personnel.branchId,
        },
      });
    }

    //仓库列表
    dispatch({
      type: 'stock/fetchWarehouseList',
    })

    //存货分类列表
    dispatch({
      type: 'stock/fetchInventoryClassificationList',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'stock/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({ purchasePersonnel: [] })
    this.fetchStockList();
  };

  handleSearch = e => {
    e.preventDefault();

    const { form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.executeSearch(fieldsValue)
    });
  };

  executeSearch = (fieldsValue) => {
    this.fetchStockList(fieldsValue);
  }

  renderSimpleForm() {
    const {
      stock: { inventoryClassificationList },
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="存货编号">
              {getFieldDecorator('stockNum')(<Input placeholder="请输入存货编号" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="存货名称">
              {getFieldDecorator('name')(<Input placeholder="请输入存货名称" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="存货分类">
              {getFieldDecorator('stockClassify')(
                <Select placeholder="请选择存货分类" style={{ width: '100%' }}>
                  {/*<Option value={'白酒'}>白酒</Option>*/}
                  {inventoryClassificationList.rows.map(item => <Option value={item.name}>{item.name}</Option>)}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  render() {
    const {
      stock: { stockList },
      loading,
    } = this.props;
    const { selectedRows } = this.state;

    return (
      <div>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          <StandardTable
            selectedRows={selectedRows}
            onSelectRow={this.handleSelectRows}
            loading={loading}
            data={stockList}
            columns={this.columns}
            onChange={this.handleStandardTableChange}
            rowKey='warehouseStockId'
            footer={() => `合计: ${stockList.rows.length}`}
          />
        </div>
      </div>
    );
  }
}

export default StockList;
