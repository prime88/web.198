// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  fetchStockList,
} from '@/services/checkOrder';
import { fetchStaffList } from '@/services/branch';
import { queryStoreList } from '@/services/store';
import { queryWarehouseList } from '@/services/warehouse';

import { message } from 'antd';

export default {
  namespace: 'stock',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //门店列表
    storeList: {
      rows: [],
      pagination: {},
    },
    //部门员工列表
    staffList: {
      rows: [],
      pagination: {},
    },
    //盘点仓库的默认值：当中登录员工所在门店部门所关联的仓库之一
    warehouseList: {
      rows: [],
      pagination: {},
    },
    //库存列表
    stockList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStoreList({ payload, callback }, { call, put }) {
      const response = yield call(queryStoreList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
      }
      yield put({
        type: 'saveStoreList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStaffList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStaffList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
      }
      yield put({
        type: 'saveStaffList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchWarehouseList({ payload, callback }, { call, put }) {
      const response = yield call(queryWarehouseList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
      }
      yield put({
        type: 'saveWarehouseList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchStockList({ payload, callback }, { call, put }) {
      const response = yield call(fetchStockList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
      }
      yield put({
        type: 'saveStockList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveStoreList(state, action) {
      return {
        ...state,
        storeList: action.payload,
      };
    },
    saveStaffList(state, action) {
      return {
        ...state,
        staffList: action.payload,
      };
    },
    saveWarehouseList(state, action) {
      return {
        ...state,
        warehouseList: action.payload,
      };
    },
    saveStockList(state, action) {
      return {
        ...state,
        stockList: action.payload,
      };
    },
  },
};
