// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  queryInventoryClassificationList,
  queryInventoryList,
} from '@/services/warehouse';
import { message } from 'antd';

export default {
  namespace: 'inventory',

  state: {
    //存货分类列表
    inventoryClassificationList: {
      rows: [],
      pagination: {},
    },
    //存货列表
    inventoryList: {
      rows: [],
      pagination: {},
    },
  },

  effects: {
    *fetchInventoryClassificationList({ payload, callback }, { call, put }) {
      const response = yield call(queryInventoryClassificationList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
      }
      yield put({
        type: 'saveInventoryClassificationList',
        payload: response.data,
      });
      if (callback) callback(response.data);
    },
    *fetchInventoryList({ payload }, { call, put }) {
      const response = yield call(queryInventoryList, payload);
      console.log('response: ', response);
      if (response.code !== '0') {
        message.error(response.msg);
      }
      yield put({
        type: 'saveInventoryList',
        payload: response.data,
      });
    },
  },

  reducers: {
    saveInventoryClassificationList(state, action) {
      return {
        ...state,
        inventoryClassificationList: action.payload,
      };
    },
    saveInventoryList(state, action) {
      return {
        ...state,
        inventoryList: action.payload,
      };
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //存货分类
        if (location.pathname === '/basic-data/inventory-classification') {
          dispatch({
            type: 'fetchInventoryClassificationList',
          })
        }
        //存货管理
        if (location.pathname === '/basic-data/inventory-management') {
          dispatch({
            type: 'fetchInventoryList',
          })
        }
        //仓库管理
        if (location.pathname === '/basic-data/Warehouse-management') {
          dispatch({
            type: 'fetchWarehouseList',
          })
        }
      });
    },
  },
};
