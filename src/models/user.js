import { query as queryUsers, queryCurrent, changePassword } from '@/services/user';
// import message from "../pages/AppManagement/MessageManagement/models/message";
import { message } from 'antd';

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {
      personnel: {}
    },
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent({ authorityCallback, callback }, { call, put }) {
      const response = yield call(queryCurrent);
      // debugger
      if (response.code === '0') {
        //如果数据权限permissionList[0]存在，则将permissionList倒序,为什么要这么倒序，原因：198公司对象只能放在permissionList[0]的位置
        let permissionList = response.data.permissionList;
        if (permissionList && permissionList.length > 1 && permissionList[0].name === '查看全部门店') {
          permissionList.reverse();
        }
        console.log('realPermissionList: ', permissionList);

        yield put({
          type: 'saveCurrentUser',
          payload: response.data,
        });
        if (authorityCallback) authorityCallback(response.data);
        if (callback) callback();
      }
      else{
        if (callback) callback();
      }
    },
    *changePassword({ payload, callback }, { call, put }) {
      const response = yield call(changePassword, payload);
      if (response.code === '0') {
        if (callback) callback();
        message.success(response.msg);
      }
      else{
        message.error(response.msg);
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload,
        },
      };
    },
  },
};
