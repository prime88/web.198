// import { queryRule, removeRule, addRule, updateRule } from '@/services/api';
import {
  fetchOrderDetailList,
} from '@/services/offLineSaleOrderDetail';
import { message } from 'antd';

export default {
  namespace: 'offLineSaleOrderDetail',

  state: {

  },

  effects: {
    *fetchOrderDetailList({ payload, callback }, { call, put }) {
      const response = yield call(fetchOrderDetailList, payload);
      console.log('response: ', response);
      if (response.code === '0') {
        if (callback) callback(response.data);
      }
      else{
        message.error(response.msg);
      }
    },
  },

  reducers: {

  },
};
