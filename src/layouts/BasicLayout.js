import React from 'react';
import { Layout, Tabs, notification, Icon, Spin } from 'antd';
import DocumentTitle from 'react-document-title';
import isEqual from 'lodash/isEqual';
import memoizeOne from 'memoize-one';
import { connect } from 'dva';
import { ContainerQuery } from 'react-container-query';
import classNames from 'classnames';
import pathToRegexp from 'path-to-regexp';
import { enquireScreen, unenquireScreen } from 'enquire-js';
import { formatMessage } from 'umi/locale';
import SiderMenu from '@/components/SiderMenu';
import Authorized from '@/utils/Authorized';
import SettingDrawer from '@/components/SettingDrawer';
import logo from '../assets/logo.svg';
import Footer from './Footer';
import Header from './Header';
import Context from './MenuContext';
import Exception403 from '../pages/Exception/403';
import styles from './BasicLayout.less';
import Link from 'umi/link';
import router from 'umi/router';
import { ContextMenu, Item, Separator, ContextMenuProvider } from 'react-contexify';
import 'react-contexify/dist/ReactContexify.min.css';
import Websocket from 'react-websocket';
import { socketDomainName, apiDomainName } from '../constants/index';
import { stringify } from 'qs';
import { fetchOrderList } from '@/services/storeGain';

const { Content } = Layout;
// 自定义tab标签打开页
const TabPane = Tabs.TabPane;

// Conversion router to menu.
function formatter(data, parentPath = '', parentAuthority, parentName) {
  return data
    .map(item => {
      let locale = 'menu';
      if (parentName && item.name) {
        locale = `${parentName}.${item.name}`;
      } else if (item.name) {
        locale = `menu.${item.name}`;
      } else if (parentName) {
        locale = parentName;
      }
      if (item.path) {
        const result = {
          ...item,
          locale,
          authority: item.authority || parentAuthority,
        };
        if (item.routes) {
          const children = formatter(
            item.routes,
            `${parentPath}${item.path}/`,
            item.authority,
            locale
          );
          // Reduce memory usage
          result.children = children;
        }
        delete result.routes;
        return result;
      }

      return null;
    })
    .filter(item => item);
}

const memoizeOneFormatter = memoizeOne(formatter, isEqual);

const query = {
  'screen-xs': {
    maxWidth: 575,
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767,
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991,
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199,
  },
  'screen-xl': {
    minWidth: 1200,
    maxWidth: 1599,
  },
  'screen-xxl': {
    minWidth: 1600,
  },
};

// 自定义tab标签打开页
const panes = [{ title: '首页', key: '/homepage', closable: false }];

const onClick = ({ event, ref, data, dataFromProvider }) => console.log('Hello');

// 右键菜单
const MyAwesomeMenu = () => (
  <ContextMenu id="menu_id">
    <Item onClick={onClick}>关闭标签页</Item>
    <Separator />
    <Item onClick={onClick}>关闭其他标签页</Item>
  </ContextMenu>
);

class BasicLayout extends React.PureComponent {
  constructor(props) {
    super(props);
    this.getPageTitle = memoizeOne(this.getPageTitle);
    this.getBreadcrumbNameMap = memoizeOne(this.getBreadcrumbNameMap, isEqual);
    this.breadcrumbNameMap = this.getBreadcrumbNameMap();
    this.matchParamsPath = memoizeOne(this.matchParamsPath, isEqual);
  }

  state = {
    rendering: true,
    isMobile: false,
    menuData: this.getMenuData(),

    // 自定义tab标签打开页
    activeKey: panes[0].key,
    panes,
    // -------------------
  };

  componentWillMount() {
    const {
      dispatch,
      location: { pathname },
    } = this.props;
    const authority = localStorage.getItem('antd-pro-authority');
    console.log('authority: ', authority);
    if (!authority) {
      dispatch({
        type: 'login/logout',
      });
    }

    dispatch({
      type: 'user/fetchCurrent',
      authorityCallback: data => {
        console.log('data: ', data);
        console.log('判断该url是否有权限展示');
        const permissionList = data.permissionList;
        //去掉198公司的item
        if (permissionList && permissionList.length > 0) {
          //判断权限为空的情况
          if (permissionList[0] && permissionList[0].childList) {
            const realPermissionList = permissionList[0].childList;
            if (pathname !== '/') {
              const isExistInPermissionList = this.isExistInPermissionList(
                pathname,
                realPermissionList
              );
              // console.log('isExistInPermissionList: ', isExistInPermissionList);
              if (!isExistInPermissionList) {
                console.log('此页面没有权限展示');
                router.push('/exception/403');
              }
            }
          } else {
            console.log('此页面没有权限展示');
            router.push('/exception/403');
          }
        } else {
          console.log('此页面没有权限展示');
          router.push('/exception/403');
        }
      },
      callback: () => {
        // notification['warning']({
        //   message: '登录失败',
        //   description: '失败原因：1、登录失效；2、该账号在其他地方登录；',
        //   duration: 30,
        // });
        // dispatch({
        //   type: 'login/logout',
        // });

        //订单处理通知-定时器
        this.setInterval();
      },
    });

    // 拦截判断是否离开当前页面
    window.addEventListener('beforeunload', this.beforeunload);
  }

  //判断path在array中是否存在
  isExistInPermissionList = (path, array) => {
    let result = false;
    for (let item of array) {
      if (item.route && item.route === path) {
        result = true;
        break;
      } else {
        // console.log('result-dj: ', result);
        if (item.childList && item.childList.length > 0) {
          result = this.isExistInPermissionList(path, item.childList);
          if (result) break;
        }
      }
    }
    return result;
  };

  componentDidMount() {
    const {
      dispatch,
      location: { pathname },
      user: { currentUser },
    } = this.props;

    dispatch({
      type: 'setting/getSetting',
    });
    this.renderRef = requestAnimationFrame(() => {
      this.setState({
        rendering: false,
      });
    });
    this.enquireHandler = enquireScreen(mobile => {
      const { isMobile } = this.state;
      if (isMobile !== mobile) {
        this.setState({
          isMobile: mobile,
        });
      }
    });

    //初始化pane
    const currentPathname = this.props.location.pathname;
    if (currentPathname !== '/' && currentPathname !== '/homepage') {
      this.setState(prevState => ({
        panes: [
          ...prevState.panes,
          // { title: currentPathname, key: currentPathname }
          {
            title: formatMessage({ id: `menu${currentPathname.split('/').join('.')}` }),
            key: currentPathname,
          },
        ],
        activeKey: currentPathname,
      }));
      router.push(currentPathname);
    }

    if (apiDomainName === 'http://121.43.191.38:8025') {
      alert('测试环境')
    }
  }

  setInterval = () => {
    const {
      user: { currentUser },
    } = this.props;
    this.int = setInterval(() => {
      const storeId = currentUser.personnel.storeId;
      if (!storeId) return;
      const response = fetchOrderList({ statusList: '5,6,7', storeId });
      response.then(result => {
        console.log('未操作完成的订单列表：', result);
        const orderList = result.data ? result.data.rows : [];
        if (orderList.length > 0) {
          const order = orderList[0];
          let message = '';
          let description = '';
          if (order.type === 1) {
            //自取单
            if (order.status === 5) {
              //待确认
              console.log('?????-this: ', this);
              this.selfOrdering.play();
              message = 'APP下单通知';
              description = '您有一个新的自取订单待处理';
            } else if (order.status === 6) {
              //待出库
              if (this.selfGainToOut) this.selfGainToOut.play();
              message = 'APP订单处理通知';
              description = '您有自取订单待出库，请及时处理!';
            }
          } else if (order.type === 0) {
            //配送单
            if (order.status === 5) {
              //待确认
              if (this.deliveryOrdering) this.deliveryOrdering.play();
              message = 'APP下单通知';
              description = '您有一个新的配送订单待处理';
            } else if (order.status === 6) {
              //待出库
              if (this.deliveryToOut) this.deliveryToOut.play();
              message = 'APP订单处理通知';
              description = '您有配送订单待出库，请及时处理!';
            } else if (order.status === 7) {
              //待配送
              if (this.deliveryToDelivery) this.deliveryToDelivery.play();
              message = 'APP订单处理通知';
              description = '您有配送订单待配送，请及时处理!';
            }
          }

          notification.open({
            message: message,
            description: description,
            icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
            duration: 10,
            onClick: () => {
              let destination = '';
              if (order.type === 1) {
                destination = '/app-management/order-management/store-gain';
              } else if (order.type === 0) {
                destination = '/app-management/order-management/store-delivery';
              }

              // router.push(destination);
              const pathname = this.props.location.pathname;
              router.push({
                pathname: destination,
                query: {
                  type: pathname === destination ? '同页面跳转' : '不同页面跳转',
                },
              });
            },
          });
        }
      });
    }, 30000);
  };

  componentDidUpdate(preProps) {
    // After changing to phone mode,
    // if collapsed is true, you need to click twice to display
    this.breadcrumbNameMap = this.getBreadcrumbNameMap();
    const { isMobile } = this.state;
    const { collapsed } = this.props;
    if (isMobile && !preProps.isMobile && !collapsed) {
      this.handleMenuCollapse(false);
    }
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.renderRef);
    unenquireScreen(this.enquireHandler);
    if (this.int) clearInterval(this.int);

    // 销毁拦截判断是否离开当前页面
    window.removeEventListener('beforeunload', this.beforeunload);
  }

  getContext() {
    const { location } = this.props;
    return {
      location,
      breadcrumbNameMap: this.breadcrumbNameMap,
    };
  }

  getMenuData() {
    const {
      route: { routes },
    } = this.props;
    return memoizeOneFormatter(routes);
  }

  /**
   * 获取面包屑映射
   * @param {Object} menuData 菜单配置
   */
  getBreadcrumbNameMap() {
    const routerMap = {};
    const mergeMenuAndRouter = data => {
      data.forEach(menuItem => {
        if (menuItem.children) {
          mergeMenuAndRouter(menuItem.children);
        }
        // Reduce memory usage
        routerMap[menuItem.path] = menuItem;
      });
    };
    mergeMenuAndRouter(this.getMenuData());
    return routerMap;
  }

  matchParamsPath = pathname => {
    const pathKey = Object.keys(this.breadcrumbNameMap).find(key =>
      pathToRegexp(key).test(pathname)
    );
    return this.breadcrumbNameMap[pathKey];
  };

  getPageTitle = pathname => {
    const currRouterData = this.matchParamsPath(pathname);

    if (!currRouterData) {
      return '36519后台管理';
    }
    const message = formatMessage({
      id: currRouterData.locale || currRouterData.name,
      defaultMessage: currRouterData.name,
    });
    return `${message} - 36519后台管理`;
  };

  getLayoutStyle = () => {
    const { isMobile } = this.state;
    const { fixSiderbar, collapsed, layout } = this.props;
    if (fixSiderbar && layout !== 'topmenu' && !isMobile) {
      return {
        paddingLeft: collapsed ? '80px' : '256px',
      };
    }
    return null;
  };

  getContentStyle = () => {
    const { fixedHeader } = this.props;
    return {
      margin: 24,
      // margin: '0 1px 0', // 自定义tab标签打开页
      paddingTop: fixedHeader ? 64 : 0,
    };
  };

  handleMenuCollapse = collapsed => {
    const { dispatch } = this.props;
    dispatch({
      type: 'global/changeLayoutCollapsed',
      payload: collapsed,
    });
  };

  renderSettingDrawer() {
    // Do not render SettingDrawer in production
    // unless it is deployed in preview.pro.ant.design as demo
    const { rendering } = this.state;
    if ((rendering || process.env.NODE_ENV === 'production') && APP_TYPE !== 'site') {
      return null;
    }
    return <SettingDrawer />;
  }

  // 自定义tab标签打开页
  onChange = activeKey => {
    console.log('activeKey: ', activeKey);
    this.setState({ activeKey });
  };

  onTabClick = activeKey => {
    console.log('onTabClick-activeKey: ', activeKey);
    router.push(activeKey);
  };

  onEdit = (targetKey, action) => {
    this[action](targetKey);
  };

  remove = targetKey => {
    let activeKey = this.state.activeKey;
    let lastIndex;
    this.state.panes.forEach((pane, i) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1;
      }
    });
    const panes = this.state.panes.filter(pane => pane.key !== targetKey);
    if (lastIndex >= 0 && activeKey === targetKey) {
      activeKey = panes[lastIndex].key;
    }
    this.setState({ panes, activeKey });

    router.push(activeKey);
  };

  onMenuItemClick = e => {
    console.log('e: ', e);
    // 一、点击左边菜单栏，右边tabs出现对应的tab
    // 1.判断panes里是否已经存在该key
    const { panes } = this.state;
    const keyExistTarget = panes.find(item => item.key === e.key);
    if (!keyExistTarget) {
      //不存在：添加该pane并显示
      const newPane = {
        title: formatMessage({ id: e.item.props.locale }),
        key: e.key,
      };
      const newPanes = [...panes, newPane];
      this.setState({ panes: newPanes, activeKey: e.key });
    }

    this.setState({ activeKey: e.key });
    // 三、点击tab出现真实内容
  };

  openStoreGainOrderNotification = result => {
    let description = '';
    if (result.type === 0) {
      description = '你有一个新的配送订单待处理';
      this.deliveryOrdering.play();
    } else if (result.type === 1) {
      description = '你有一个新的自取订单待处理';
      this.selfOrdering.play();
    } else if (result.type === 2) {
      description = '你有一个新的酒库订单';
    }
    // this.audio.play();
    notification.open({
      message: 'APP下单通知',
      description: description,
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
      duration: 10,
      onClick: () => {
        let destination = '';
        switch (result.type) {
          case 0:
            destination = '/app-management/order-management/store-delivery'; //同页面刷新已实现
            break;
          case 1:
            destination = '/app-management/order-management/store-gain'; //同页面刷新已实现
            break;
          case 2:
            destination = '/app-management/order-management/keep-wine-management'; //同页面刷新已实现
            break;
        }
        const pathname = this.props.location.pathname;
        router.push({
          pathname: destination,
          query: {
            type: pathname === destination ? '同页面跳转' : '不同页面跳转',
          },
        });
      },
    });
  };

  openReturnOrderNotification = () => {
    // this.audio.play();
    notification.open({
      message: '退货通知',
      description: '你有一个新的退货订单',
      icon: <Icon type="frown" style={{ color: '#108ee9' }} />,
      duration: 10,
      onClick: () => {
        let destination = '/app-management/order-management/return-management'; //同页面刷新已实现
        // router.push(destination);
        const pathname = this.props.location.pathname;
        router.push({
          pathname: destination,
          query: {
            type: pathname === destination ? '同页面跳转' : '不同页面跳转',
          },
        });
      },
    });
  };

  openCancelOrderNotification = result => {
    let description = '';
    if (result.type === 0) {
      description = '您有一个配送订单被取消';
      if (this.deliveryToCancel) this.deliveryToCancel.play();
    } else if (result.type === 1) {
      description = '您有一个自取订单被取消';
      if (this.selfGainToCancel) this.selfGainToCancel.play();
    } else if (result.type === 2) {
      description = '您有一个酒库订单被取消';
    }
    // this.audio.play();
    notification.open({
      message: '订单取消通知',
      description: description,
      icon: <Icon type="frown" style={{ color: '#108ee9' }} />,
      duration: 20,
      onClick: () => {
        let destination = '';
        switch (result.type) {
          case 0:
            destination = '/app-management/order-management/store-delivery'; //同页面刷新已实现
            break;
          case 1:
            destination = '/app-management/order-management/store-gain'; //同页面刷新已实现
            break;
          case 2:
            destination = '/app-management/order-management/keep-wine-management'; //移动端无取消操作
            break;
        }
        // router.push(destination);
        const pathname = this.props.location.pathname;
        router.push({
          pathname: destination,
          query: {
            type: pathname === destination ? '同页面跳转' : '不同页面跳转',
            orderNumber: result.orderNumber,
          },
        });
      },
    });
  };

  getPickWineOrderNotification = () => {
    // this.audio.play();
    notification.open({
      message: '提酒申请通知',
      description: '你有一个新的提酒申请订单',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
      duration: 10,
      onClick: () => {
        let destination = '/app-management/order-management/pick-wine-apply'; //同页面刷新已实现
        // router.push(destination);
        const pathname = this.props.location.pathname;
        router.push({
          pathname: destination,
          query: {
            type: pathname === destination ? '同页面跳转' : '不同页面跳转',
          },
        });
      },
    });
  };

  getApprovalOrderNotification = () => {
    // this.audio.play();
    notification.open({
      message: '审批通知',
      description: '你有一个新的审批待处理',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
      duration: 10,
      onClick: () => {
        let destination = '/approval-management/approval-logs'; //同页面刷新已实现
        // router.push(destination);
        const pathname = this.props.location.pathname;
        router.push({
          pathname: destination,
          query: {
            type: pathname === destination ? '同页面跳转' : '不同页面跳转',
          },
        });
      },
    });
  };

  getDeliveryApplyOrderOrderNotification = () => {
    // this.audio.play();
    notification.open({
      message: '调货申请通知',
      description: '你有一个新的调货申请单待处理',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
      duration: 10,
      onClick: () => {
        let destination = '/delivery-management/delivery-apply/delivery-apply-order-list'; //同页面刷新已实现
        // router.push(destination);
        const pathname = this.props.location.pathname;
        router.push({
          pathname: destination,
          query: {
            type: pathname === destination ? '同页面跳转' : '不同页面跳转',
          },
        });
      },
    });
  };

  handleData = data => {
    if (!localStorage.getItem('token')) return;

    console.log('data: ', data);
    let result = JSON.parse(data);
    console.log('result: ', result);
    if (result.code === 0) {
      //门店收到属于该门店的APP下单通知
      this.openStoreGainOrderNotification(result);
    } else if (result.code === 1) {
      //退货订单
      this.openReturnOrderNotification();
    } else if (result.code === 2) {
      //付款后，取消订单的通知
      this.openCancelOrderNotification(result);
    } else if (result.code === 3) {
      //提酒申请：没有该类通知
      this.getPickWineOrderNotification(result);
    } else if (result.code === 4) {
      //审批通知
      this.getApprovalOrderNotification();
    } else if (result.code === 5) {
      //调货申请单提交后，给申请机构(调货接收方)发送的通知： 申请单提交时后台会对在线的被申请机构员工发送通知
      this.getDeliveryApplyOrderOrderNotification();
    }
  };

  beforeunload = e => {
    if (this.int) clearInterval(this.int);
  };

  render() {
    const {
      navTheme,
      layout: PropsLayout,
      children,
      location: { pathname },
      user: { currentUser },
      currentUserloading,
    } = this.props;
    const { isMobile, menuData } = this.state;
    const isTop = PropsLayout === 'topmenu';
    const routerConfig = this.matchParamsPath(pathname);

    const layout = (
      <Layout>
        {isTop && !isMobile ? null : (
          <SiderMenu
            logo={logo}
            Authorized={Authorized}
            theme={navTheme}
            onCollapse={this.handleMenuCollapse}
            menuData={menuData}
            isMobile={isMobile}
            onMenuItemClick={this.onMenuItemClick}
            {...this.props}
          />
        )}
        <Layout
          style={{
            ...this.getLayoutStyle(),
            minHeight: '100vh',
          }}
        >
          <Header
            menuData={menuData}
            handleMenuCollapse={this.handleMenuCollapse}
            logo={logo}
            isMobile={isMobile}
            {...this.props}
          />

          <Content style={this.getContentStyle()}>
            <Authorized authority={routerConfig.authority} noMatch={<Exception403 />}>
              {children}
            </Authorized>
          </Content>

          {/*自定义tab标签打开页*/}
          {/*<Tabs*/}
          {/*hideAdd*/}
          {/*onChange={this.onChange}*/}
          {/*// onTabClick={this.onTabClick}*/}
          {/*activeKey={this.state.activeKey}*/}
          {/*type="editable-card"*/}
          {/*onEdit={this.onEdit}*/}
          {/*style={{ margin: '16px 16px 0' }}*/}
          {/*className={styles.customTabBar}*/}
          {/*>*/}
          {/*{this.state.panes.map(pane => (//activeKey*/}
          {/*<TabPane*/}
          {/*tab={*/}
          {/*<Link to={pane.key}>{pane.title}</Link>*/}
          {/*// <ContextMenuProvider id="menu_id">*/}
          {/*//   <Link to={pane.key}>{pane.title}</Link>*/}
          {/*// </ContextMenuProvider>*/}
          {/*}*/}
          {/*// tab={<span onClick={() => this.setState({ activeKey: pane.key })}>{pane.title}</span>}*/}
          {/*// tab={pane.title}*/}
          {/*key={pane.key}*/}
          {/*closable={pane.closable}*/}
          {/*>*/}
          {/*<Content style={this.getContentStyle()}>*/}
          {/*<Authorized authority={routerConfig.authority} noMatch={<Exception403 />}>*/}
          {/*{children}*/}
          {/*</Authorized>*/}
          {/*</Content>*/}
          {/*</TabPane>*/}
          {/*))}*/}
          {/*</Tabs>*/}
          {/*-------------------------*/}
          <Footer />
          <MyAwesomeMenu />
        </Layout>
      </Layout>
    );
    return (
      <React.Fragment>
        <DocumentTitle title={this.getPageTitle(pathname)}>
          <ContainerQuery query={query}>
            {params => (
              <Context.Provider value={this.getContext()}>
                <div className={classNames(params)}>
                  {currentUserloading ? (
                    <div style={{ textAlign: 'center' }}>
                      <Spin size="large" style={{ marginTop: 100 }} />
                    </div>
                  ) : (
                    layout
                  )}
                </div>
              </Context.Provider>
            )}
          </ContainerQuery>
        </DocumentTitle>
        {this.renderSettingDrawer()}
        {currentUser.staffId ? (
          <Websocket
            url={`${socketDomainName}/websocket/${currentUser.staffId}`}
            onMessage={this.handleData.bind(this)}
            ref={websocket => {
              this.websocket = websocket;
            }}
          />
        ) : (
          ''
        )}
        {/*<audio*/}
        {/*style={{ display: 'none' }}*/}
        {/*ref={audio => {*/}
        {/*this.audio = audio;*/}
        {/*}}*/}
        {/*src="http://zjlt.sc.chinaz.com/Files/DownLoad/sound1/201808/10467.mp3"*/}
        {/*controls="controls"*/}
        {/*>*/}
        {/*Your browser does not support the audio element.*/}
        {/*</audio>*/}
        <audio
          style={{ display: 'none' }}
          ref={audio => {
            this.selfOrdering = audio;
          }}
          src="https://36519.oss-cn-beijing.aliyuncs.com/orderAudio/%E8%87%AA%E5%8F%96%E8%AE%A2%E5%8D%95%E9%80%9A%E7%9F%A5.m4a"
          controls="controls"
          preload="auto"
        >
          Your browser does not support the audio element.
        </audio>
        <audio
          style={{ display: 'none' }}
          ref={audio => {
            this.deliveryOrdering = audio;
          }}
          src="https://36519.oss-cn-beijing.aliyuncs.com/orderAudio/%E9%85%8D%E9%80%81%E8%AE%A2%E5%8D%95%E9%80%9A%E7%9F%A5.m4a"
          controls="controls"
          preload="auto"
        >
          Your browser does not support the audio element.
        </audio>
        <audio
          style={{ display: 'none' }}
          ref={audio => {
            this.selfGainToOut = audio;
          }}
          src="https://36519.oss-cn-beijing.aliyuncs.com/orderAudio/%E8%87%AA%E5%8F%96%E5%BE%85%E5%87%BA%E5%BA%93.m4a"
          controls="controls"
          preload="auto"
        >
          Your browser does not support the audio element.
        </audio>
        <audio
          style={{ display: 'none' }}
          ref={audio => {
            this.deliveryToOut = audio;
          }}
          src="https://36519.oss-cn-beijing.aliyuncs.com/orderAudio/%E9%85%8D%E9%80%81%E5%BE%85%E5%87%BA%E5%BA%93.m4a"
          controls="controls"
          preload="auto"
        >
          Your browser does not support the audio element.
        </audio>
        <audio
          style={{ display: 'none' }}
          ref={audio => {
            this.deliveryToDelivery = audio;
          }}
          src="https://36519.oss-cn-beijing.aliyuncs.com/orderAudio/%E9%85%8D%E9%80%81%E5%BE%85%E9%85%8D%E9%80%81.m4a"
          controls="controls"
          preload="auto"
        >
          Your browser does not support the audio element.
        </audio>
        <audio
          style={{ display: 'none' }}
          ref={audio => {
            this.deliveryToCancel = audio;
          }}
          src="https://36519.oss-cn-beijing.aliyuncs.com/orderAudio/%E9%85%8D%E9%80%81%E5%8F%96%E6%B6%88.m4a"
          controls="controls"
          preload="auto"
        >
          Your browser does not support the audio element.
        </audio>
        <audio
          style={{ display: 'none' }}
          ref={audio => {
            this.selfGainToCancel = audio;
          }}
          src="https://36519.oss-cn-beijing.aliyuncs.com/orderAudio/%E8%87%AA%E5%8F%96%E5%8F%96%E6%B6%88.m4a"
          controls="controls"
          preload="auto"
        >
          Your browser does not support the audio element.
        </audio>
      </React.Fragment>
    );
  }
}

export default connect(({ global, user, setting, loading }) => ({
  collapsed: global.collapsed,
  layout: setting.layout,
  user,
  currentUserloading: loading.effects['user/fetchCurrent'],
  ...setting,
}))(BasicLayout);
