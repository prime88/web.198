import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchVipLevelList(params) {
  return request(`/server/memberLevel/list?${stringify(params)}`);
}

export async function fetchElectronicWalletList(params) {
  return request(`/server/eWallet/list?${stringify(params)}`);
}

export async function fetchEleWallRechargeRecordList(params) {
  return request(`/server/eWallet/rechargeList?${stringify(params)}`);
}

export async function fetchBuyingGivingList(params) {
  return request(`/server/buyGive/list?${stringify(params)}`);
}

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function fetchBuyingGivingGoodsList(params) {
  return request(`/server/buyGive/goods/list?${stringify(params)}`);
}

export async function fetchBuyingGivingRechargeRecordList(params) {
  return request(`/server/buyGive/rechargeList?${stringify(params)}`);
}

export async function addVipLevel(params) {
  return request('/server/memberLevel/create', {
    method: 'POST',
    body: params,
  });
}

export async function addElectronicWallet(params) {
  return request('/server/eWallet/create', {
    method: 'POST',
    body: params,
  });
}

export async function addBuyingGiving(params) {
  return request('/server/buyGive/create', {
    method: 'POST',
    body: params,
  });
}

export async function addBuyingGivingGoods(params) {
  return request('/server/buyGive/goods/create', {
    method: 'POST',
    body: params,
  });
}

export async function isVipLevelNameRepeated(params) {
  return request(`/server/memberLevel/getJudgeByName?${stringify(params)}`);
}

export async function removeVipLevel(params) {
  return request(`/server/memberLevel/${params.memberLevelIds}`, {
    method: 'DELETE',
  });
}

export async function removeElectronicWallet(params) {
  return request(`/server/eWallet/${params.eWalletIds}`, {
    method: 'DELETE',
  });
}

export async function removeBuyingGiving(params) {
  return request(`/server/buyGive/${params.buyGiveIds}`, {
    method: 'DELETE',
  });
}

export async function removeBuyingGivingGoods(params) {
  return request(`/server/buyGive/goods/${params.goodsIds}`, {
    method: 'DELETE',
  });
}

export async function updateVipLevel(params) {
  return request(`/server/memberLevel/${params.memberLevelId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateElectronicWallet(params) {
  return request(`/server/eWallet/${params.eWalletId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateBuyingGiving(params) {
  return request(`/server/buyGive/${params.buyGiveId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateBuyingGivingGoods(params) {
  return request(`/server/buyGive/goods/${params.goodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeElectronicWalletStatus(params) {
  return request(`/server/eWallet/setEnable/${params.eWalletId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeBuyingGivingStatus(params) {
  return request(`/server/buyGive/setEnable/${params.buyGiveId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeBuyingGivingGoodsStatus(params) {
  return request(`/server/buyGive/goods/setEnable/${params.goodsId}`, {
    method: 'PUT',
    body: params,
  });
}
