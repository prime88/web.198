import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchInvoiceList(params) {
  return request(`/server/invoice/list?${stringify(params)}`);
}
