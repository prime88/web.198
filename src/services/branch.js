import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchBranchList(params) {
  return request(`/server/branch/treeList?${stringify(params)}`);
}

export async function fetchChildBranchList(params) {
  return request(`/server/branch/list?${stringify(params)}`);
}

export async function fetchStaffList(params) {
  return request(`/server/personnel/list?${stringify(params)}`);
}

export async function addBranch(params) {
  let url = '/server/branch/create';
  if (params.type === 1) {
    url = '/server/branch/createStore';
  }
  return request(url, {
    method: 'POST',
    body: params,
  });
}

export async function addBranchStaff(params) {
  return request('/server/personnel/create', {
    method: 'POST',
    body: params,
  });
}

export async function isBranchNameRepeated(params) {
  return request(`/server/branch/getJudgeByName?${stringify(params)}`);
}

export async function removeBranch(params) {
  return request(`/server/branch/${params.branchId}`, {
    method: 'DELETE',
  });
}

export async function removeStaff(params) {
  return request(`/server/personnel/${params.personnelIds}`, {
    method: 'DELETE',
  });
}

export async function updateBranch(params) {
  return request(`/server/branch/${params.branchId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateBranchStaff(params) {
  return request(`/server/personnel/${params.personnelId}`, {
    method: 'PUT',
    body: params,
  });
}
