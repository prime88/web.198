import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchPhoneOrderDeliveryList(params) {
  return request(`/server/salesOrder/list?${stringify(params)}`);
}

export async function fetchSaleOrderDetails(params) {
  return request(`/server/salesOrder/goodList?${stringify(params)}`);
}

export async function delivery(params) {
  return request(`/server/salesOrder/distribution/${params.salesOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function receivables(params) {
  return request(`/server/salesOrder/signFor/${params.salesOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function cancelOrder(params) {
  return request(`/server/salesOrder/cancel/${params.salesOrderId}`, {
    method: 'PUT',
    body: params,
  });
}
