import { stringify } from 'qs';
import request from '@/utils/request';
import { key } from '../constants/index'

//数据字典查询
export async function queryDictionary(params) {
  return request(`/server/dictionary/list?${stringify(params)}`);
}

// 高德地图
export async function queryGadMapInputtipsList(params, options) {
  params.key = key;
  params.datatype = 'all';
  return request(`/server/amap/list?${stringify(params)}`, options)
}

export async function upload(params) {
  return request('/image/upload', {
    method: 'POST',
    body: params,
  });
}

//获取仓库批次列表
export async function fetchWarehouseStockBatchList(params) {
  return request(`/server/warehouseStock/batchNumberList?${stringify(params)}`);
}
