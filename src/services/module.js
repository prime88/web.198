import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchModuleTree(params) {
  return request(`/server/permission/treeList?${stringify(params)}`);
}

export async function fetchModuleList(params) {
  return request(`/server/permission/list?${stringify(params)}`);
}

export async function addModule(params) {
  return request('/server/permission/create', {
    method: 'POST',
    body: params,
  });
}

export async function isBranchNameRepeated(params) {
  return request(`/server/branch/getJudgeByName?${stringify(params)}`);
}

export async function removeModule(params) {
  return request(`/server/permission/${params.permissionIds}`, {
    method: 'DELETE',
  });
}

export async function updateModule(params) {
  return request(`/server/permission/${params.permissionId}`, {
    method: 'PUT',
    body: params,
  });
}
