import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/distributionPutOut/getOrderNumber?${stringify(params)}`);
}

export async function fetchDeliveryOutOrderInventoryList(params) {
  return request(`/server/distributionPutOut/stockList?${stringify(params)}`);
}

export async function fetchDeliveryApplyOrderList(params) {
  return request(`/server/distributionApply/list?${stringify(params)}`);
}

export async function fetchDeliveryOutOrderList(params) {
  return request(`/server/distributionPutOut/list?${stringify(params)}`);
}

export async function addDeliveryOutOrder(params) {
  return request('/server/distributionPutOut/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeDeliveryOutOrder(params) {
  return request(`/server/distributionPutOut/${params.distributionPutOutId}`, {
    method: 'DELETE',
  });
}

export async function updateDeliveryOutOrder(params) {
  return request(`/server/distributionPutOut/${params.distributionPutOutId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitDeliveryOutOrders(params) {
  return request(`/server/distributionPutOut/submit/${params.distributionPutOutIds}`, {
    method: 'PUT',
    body: params,
  });
}
