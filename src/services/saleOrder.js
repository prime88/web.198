import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/salesPutOut/getOrderNumber?${stringify(params)}`);
}

export async function fetchSaleOrderInventoryList(params) {
  return request(`/server/salesPutOut/stockList?${stringify(params)}`);
}

export async function fetchDeliveryApplyOrderList(params) {
  return request(`/server/distributionApply/list?${stringify(params)}`);
}

export async function fetchSaleOrderList(params) {
  return request(`/server/salesPutOut/list?${stringify(params)}`);
}

export async function fetchOrderDetailList(params) {
  return request(`/server/wineCellarOrder/${params.wineCellarOrderId}`);
}

export async function addSaleOrder(params) {
  return request('/server/salesPutOut/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeSaleOrder(params) {
  return request(`/server/salesPutOut/${params.salesPutOutId}`, {
    method: 'DELETE',
  });
}

export async function updateSaleOrder(params) {
  return request(`/server/salesPutOut/${params.salesPutOutId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitSaleOrders(params) {
  return request(`/server/salesPutOut/submit/${params.salesPutOutIds}`, {
    method: 'PUT',
    body: params,
  });
}
