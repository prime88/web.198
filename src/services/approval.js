import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchFlowList(params) {
  return request(`/server/flow/list?${stringify(params)}`);
}

export async function fetchBranchs(params) {
  return request(`/server/branch/list?${stringify(params)}`);
}

export async function fetchFormList(params) {
  return request(`/server/flow/formList?${stringify(params)}`);
}

export async function fetchSales(params) {
  return request(`/server/statistics/sales?${stringify(params)}`);
}

export async function fetchFlowNodeList(params) {
  return request(`/server/flowNode/list?${stringify(params)}`);
}

export async function fetchFlowNodePersonnelList(params) {
  return request(`/server/personnel/${params.personnelIds}`);
}

export async function fetchApprovalLogList(params) {
  return request(`/server/approvalLog/list?${stringify(params)}`);
}

export async function fetchPurchaseingPlanApprovalLogDetail(params) {
  return request(`/server/approvalLog/${params.approvalLogId}`);
}

export async function fetchPurchaseingInventoryList(params) {
  return request(`/server/purchaseRequisition/${params.purchaseRequisitionIds}`);
}

export async function fetchProcurementInventoryList(params) {
  return request(`/server/purchasePlan/${params.purchasePlanIds}`);
}

export async function fetchProcurementOrderInventoryList(params) {
  return request(`/server/purchaseOrder/${params.purchaseOrderIds}`);
}

export async function fetchArrivalOrderInventoryList(params) {
  return request(`/server/purchaseArrival/${params.purchaseArrivalIds}`);
}

export async function fetchWarehousingOrderInventoryList(params) {
  return request(`/server/purchaseGodownEntry/${params.purchaseGodownEntryIds}`);
}

export async function fetchDeliveryApplyOrderInventoryList(params) {
  return request(`/server/distributionApply/${params.distributionApplyIds}`);
}

export async function fetchDeliveryOrderInventoryList(params) {
  return request(`/server/distributionOrder/${params.distributionOrderIds}`);
}

export async function fetchDeliveryOutOrderInventoryList(params) {
  return request(`/server/distributionPutOut/${params.distributionPutOutIds}`);
}

export async function fetchDeliveryInOrderInventoryList(params) {
  return request(`/server/distributionPutIn/${params.distributionPutInIds}`);
}

export async function fetchOtherInInventoryList(params) {
  return request(`/server/otherPutIn/${params.otherPutInId}`);
}

export async function fetchOtherOutOrderInventoryList(params) {
  return request(`/server/otherPutOut/${params.otherPutOutId}`);
}

export async function addFlow(params) {
  return request('/server/flow/create', {
    method: 'POST',
    body: params,
  });
}

export async function addFlowNode(params) {
  return request('/server/flowNode/create', {
    method: 'POST',
    body: params,
  });
}

export async function isBrandNameRepeated(params) {
  return request(`/server/brand/getJudgeByName?${stringify(params)}`);
}

export async function removeFlow(params) {
  return request(`/server/flow/${params.flowIds}`, {
    method: 'DELETE',
  });
}

export async function removeFlowNode(params) {
  return request(`/server/flowNode/${params.flowNodeId}`, {
    method: 'DELETE',
  });
}

export async function updateFlow(params) {
  return request(`/server/flow/${params.flowId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateFlowNode(params) {
  return request(`/server/flowNode/${params.flowNodeId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function approval(params) {
  return request(`/server/approvalLog/${params.approvalLogId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeFlowStatus(params) {
  return request(`/server/flow/setEnable/${params.flowId}`, {
    method: 'PUT',
    body: params,
  });
}
