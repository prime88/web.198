import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/checkList/getOrderNumber?${stringify(params)}`);
}

export async function fetchLogList(params) {
  return request(`/server/warehouseStock/logList?${stringify(params)}`);
}

export async function fetchCheckInventoryList(params) {
  return request(`/server/checkList/stockList?${stringify(params)}`);
}

export async function fetchDeliveryApplyOrderList(params) {
  return request(`/server/distributionApply/list?${stringify(params)}`);
}

export async function fetchStockList(params) {
  return request(`/server/warehouseStock/batchList?${stringify(params)}`);
}

export async function fetchCheckOrderList(params) {
  return request(`/server/checkList/list?${stringify(params)}`);
}

export async function addCheckOrder(params) {
  return request('/server/checkList/create', {
    method: 'POST',
    body: params,
  });
}

export async function putInCheckOrder(params) {
  return request(`/server/checkList/putIn/${params.checkListId}`, {
    method: 'POST',
    body: params,
  });
}

export async function removeCheckOrder(params) {
  return request(`/server/checkList/${params.checkListId}`, {
    method: 'DELETE',
  });
}

export async function updateCheckOrder(params) {
  return request(`/server/checkList/${params.checkListId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitCheckOrders(params) {
  return request(`/server/checkList/submit/${params.checkListIds}`, {
    method: 'PUT',
    body: params,
  });
}
