import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function fetchBranchList(params) {
  return request(`/server/branch/list?${stringify(params)}`);
}

export async function fetchPurchaseingPlanList(params) {
  return request(`/server/purchaseRequisition/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/purchaseOrder/getOrderNumber?${stringify(params)}`);
}

export async function fetchPurchaseingInventoryList(params) {
  return request(`/server/purchaseRequisition/stockList?${stringify(params)}`);
}

export async function fetchProcurementOrderList(params) {
  return request(`/server/purchaseOrder/list?${stringify(params)}`);
}

export async function fetchProcurementInventoryList(params) {
  return request(`/server/purchaseOrder/stockList?${stringify(params)}`);
}

export async function addProcurementOrder(params) {
  return request('/server/purchaseOrder/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeInventory(params) {
  return request(`/server/purchaseRequisition/deleteStocks/${params.prStockId}`, {
    method: 'DELETE',
  });
}

export async function removePurchaseOrder(params) {
  return request(`/server/purchaseRequisition/${params.purchaseRequisitionId}`, {
    method: 'DELETE',
  });
}

export async function removeProcurementOrder(params) {
  return request(`/server/purchaseOrder/${params.purchaseOrderId}`, {
    method: 'DELETE',
  });
}

export async function updatePurchase(params) {
  return request(`/server/purchaseRequisition/${params.purchaseRequisitionId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateProcurementOrder(params) {
  return request(`/server/purchaseOrder/${params.purchaseOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitProcurementOrders(params) {
  return request(`/server/purchaseOrder/submit/${params.purchaseOrderIds}`, {
    method: 'PUT',
    body: params,
  });
}
//
// export async function changeInventoryClassificationStatus(params) {
//   return request(`/server/stock_classify/setEnable/${params.stockClassifyId}`, {
//     method: 'PUT',
//     body: params,
//   });
// }
