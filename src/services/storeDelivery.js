import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchOrderList(params) {
  return request(`/server/order/list?${stringify(params)}`);
}

export async function fetchOrderDetailList(params) {
  return request(`/server/order/${params.orderId}`);
}

// export async function addInventoryClassification(params) {
//   return request('/server/stock_classify/create', {
//     method: 'POST',
//     body: params,
//   });
// }
//
// export async function isInventoryClassificationNameRepeated(params) {
//   return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
// }
//
// export async function removeInventoryClassification(params) {
//   return request(`/server/stock_classify/${params.stockClassifyIds}`, {
//     method: 'DELETE',
//   });
// }
//
// export async function updateInventoryClassification(params) {
//   return request(`/server/stock_classify/${params.stockClassifyId}`, {
//     method: 'PUT',
//     body: params,
//   });
// }
//

export async function confirmOrder(params) {
  return request(`/server/order/confirm/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}


export async function outOrder(params) {
  return request(`/server/order/putOut/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function deliveryOrder(params) {
  return request(`/server/order/distribution/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeDistributionStatus(params) {
  return request(`/server/order/setDistributionStatus/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function confirmReceipt(params) {
  return request(`/server/order/confirmReceipt/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function cancelOrder(params) {
  return request(`/server/order/cancel/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}
