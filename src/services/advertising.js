import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchSoftTextList(params) {
  return request(`/server/softText/list?${stringify(params)}`);
}

export async function fetchAdvertisingList(params) {
  return request(`/server/banner/list?${stringify(params)}`);
}

export async function addSoftText(params) {
  return request('/server/softText/create', {
    method: 'POST',
    body: params,
  });
}

export async function addAdvertising(params) {
  return request('/server/banner/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeSoftText(params) {
  return request(`/server/softText/${params.softTextIds}`, {
    method: 'DELETE',
  });
}

export async function removeAdvertising(params) {
  return request(`/server/banner/${params.advertisingIds}`, {
    method: 'DELETE',
  });
}

export async function updateSoftText(params) {
  return request(`/server/softText/${params.softTextId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateAdvertising(params) {
  return request(`/server/banner/${params.bannerId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeAdvertisingStatus(params) {
  return request(`/server/banner/setEnable/${params.bannerId}`, {
    method: 'PUT',
    body: params,
  });
}
