import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryMeasurementUnitClassificationList(params) {
  return request(`/server/units_classify/list?${stringify(params)}`);
}

export async function queryMeasurementUnitList(params) {
  return request(`/server/units/list?${stringify(params)}`);
}

export async function addMeasurementUnitClassification(params) {
  return request('/server/units_classify/create', {
    method: 'POST',
    body: params,
  });
}

export async function addMeasurementUnit(params) {
  return request('/server/units/create', {
    method: 'POST',
    body: params,
  });
}

export async function isMeasurementUnitClassificationNameRepeated(params) {
  return request(`/server/units_classify/getJudgeByName?${stringify(params)}`);
}

export async function isMeasurementUnitNameRepeated(params) {
  return request(`/server/units/getJudgeByName?${stringify(params)}`);
}

export async function isMeasurementUnitClassificationCodeRepeated(params) {
  return request(`/server/units_classify/getJudgeByCode?${stringify(params)}`);
}

export async function isMeasurementUnitCodeRepeated(params) {
  return request(`/server/units/getJudgeByCode?${stringify(params)}`);
}

export async function removeMeasurementUnitClassification(params) {
  return request(`/server/units_classify/${params.unitsClassifyIds}`, {
    method: 'DELETE',
  });
}

export async function removeMeasurementUnit(params) {
  return request(`/server/units/${params.unitsIds}`, {
    method: 'DELETE',
  });
}

export async function updateMeasurementUnitClassification(params) {
  return request(`/server/units_classify/${params.unitsClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateMeasurementUnit(params) {
  return request(`/server/units/${params.unitsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeMeasurementUnitClassificationStatus(params) {
  return request(`/server/units_classify/setEnable/${params.unitsClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeMeasurementUnitStatus(params) {
  return request(`/server/units/setEnable/${params.unitsId}`, {
    method: 'PUT',
    body: params,
  });
}
