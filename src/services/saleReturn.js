import { stringify } from 'qs';
import request from '@/utils/request';

export async function addStoreSaleOrder(params) {
  return request('/server/salesOrder/create', {
    method: 'POST',
    body: params,
  });
}

export async function returnGoods(params) {
  return request('/server/salesReturnGoods/create', {
    method: 'POST',
    body: params,
  });
}

export async function fetchSaleOrderList(params) {
  return request(`/server/salesOrder/list?${stringify(params)}`);
}

export async function fetchSaleOrderDetails(params) {
  return request(`/server/salesOrder/goodList?${stringify(params)}`);
}

export async function fetchSaleReturnGoodsList(params) {
  return request(`/server/salesReturnGoods/list?${stringify(params)}`);
}

export async function fetchsalesReturnGoodsDetails(params) {
  return request(`/server/salesReturnGoods/goodList?${stringify(params)}`);
}
