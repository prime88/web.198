import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchOrderList(params) {
  return request(`/server/returnGoods/list?${stringify(params)}`);
}

export async function fetchOrderDetailList(params) {
  return request(`/server/order/${params.orderId}`);
}

// export async function addInventoryClassification(params) {
//   return request('/server/stock_classify/create', {
//     method: 'POST',
//     body: params,
//   });
// }
//
// export async function isInventoryClassificationNameRepeated(params) {
//   return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
// }
//
// export async function removeInventoryClassification(params) {
//   return request(`/server/stock_classify/${params.stockClassifyIds}`, {
//     method: 'DELETE',
//   });
// }
//
// export async function updateInventoryClassification(params) {
//   return request(`/server/stock_classify/${params.stockClassifyId}`, {
//     method: 'PUT',
//     body: params,
//   });
// }
//

export async function confirmOrder(params) {
  return request(`/server/returnGoods/confirm/${params.returnGoodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function refund(params) {
  return request(`/server/returnGoods/refund/${params.returnGoodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function outOrder(params) {
  return request(`/server/order/putOut/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function confirmReceipt(params) {
  return request(`/server/returnGoods/confirmReceipt/${params.returnGoodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function cancelOrder(params) {
  return request(`/server/order/cancel/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}
