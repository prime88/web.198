import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryStoreList(params) {
  return request(`/server/store/list?${stringify(params)}`);
}

export async function addStore(params) {
  return request('/server/store/create', {
    method: 'POST',
    body: params,
  });
}

export async function isStoreNameRepeated(params) {
  return request(`/server/store/getJudgeByName?${stringify(params)}`);
}

export async function removeStore(params) {
  return request(`/server/store/${params.storeIds}`, {
    method: 'DELETE',
  });
}

export async function updateStore(params) {
  return request(`/server/store/${params.storeId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeStoreStatus(params) {
  return request(`/server/store/setEnable/${params.storeId}`, {
    method: 'PUT',
    body: params,
  });
}
