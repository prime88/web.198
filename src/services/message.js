import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchMessageList(params) {
  return request(`/server/messageTemplate/list?${stringify(params)}`);
}

export async function fetchMessageRecordList(params) {
  return request(`/server/messageLog/list?${stringify(params)}`);
}

export async function addMessage(params) {
  return request('/server/messageTemplate/create', {
    method: 'POST',
    body: params,
  });
}

export async function sendMessage(params) {
  return request('/server/messageTemplate/sendToAll', {
    method: 'POST',
    body: params,
  });
}

export async function removeMessage(params) {
  return request(`/server/messageTemplate/${params.messageTemplateIds}`, {
    method: 'DELETE',
  });
}

export async function removeMessageRecord(params) {
  return request(`/server/messageLog/${params.messageLogIds}`, {
    method: 'DELETE',
  });
}

export async function updateMessage(params) {
  return request(`/server/messageTemplate/${params.messageTemplateId}`, {
    method: 'PUT',
    body: params,
  });
}
