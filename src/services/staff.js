import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchStaffList(params) {
  return request(`/server/staff/list?${stringify(params)}`);
}

export async function addStaff(params) {
  return request('/server/staff/create', {
    method: 'POST',
    body: params,
  });
}

export async function isInventoryClassificationNameRepeated(params) {
  return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
}

export async function removeStaff(params) {
  return request(`/server/staff/${params.staffIds}`, {
    method: 'DELETE',
  });
}

export async function updateStaff(params) {
  return request(`/server/staff/${params.staffId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function setStaffRole(params) {
  return request(`/server/staff/setRole`, {
    method: 'PUT',
    body: params,
  });
}

export async function resetPassword(params) {
  return request(`/server/staff/resetPassword/${params.staffId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeInventoryClassificationStatus(params) {
  return request(`/server/stock_classify/setEnable/${params.stockClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}
