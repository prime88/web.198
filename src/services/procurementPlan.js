import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}


export async function fetchProcurementPlanList(params) {
  return request(`/server/purchasePlan/list?${stringify(params)}`);
}

export async function fetchPlanNumber(params) {
  return request(`/server/purchasePlan/getOrderNumber?${stringify(params)}`);
}

export async function fetchProcurementInventoryList(params) {
  return request(`/server/purchasePlan/stockList?${stringify(params)}`);
}

export async function fetchPurchaseingInventoryList(params) {
  return request(`/server/purchaseRequisition/stockList?${stringify(params)}`);
}

export async function fetchPurchaseingPlanList(params) {
  return request(`/server/purchaseRequisition/list?${stringify(params)}`);
}

export async function addProcurementPlan(params) {
  return request('/server/purchasePlan/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeInventory(params) {
  return request(`/server/purchasePlan/deleteStocks/${params.ppStockId}`, {
    method: 'DELETE',
  });
}

export async function removeProcurementOrder(params) {
  return request(`/server/purchasePlan/${params.purchasePlanId}`, {
    method: 'DELETE',
  });
}

export async function updateProcurement(params) {
  return request(`/server/purchasePlan/${params.purchasePlanId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitProcurementOrders(params) {
  return request(`/server/purchasePlan/submit/${params.purchasePlanIds}`, {
    method: 'PUT',
    body: params,
  });
}
//
// export async function changeInventoryClassificationStatus(params) {
//   return request(`/server/stock_classify/setEnable/${params.stockClassifyId}`, {
//     method: 'PUT',
//     body: params,
//   });
// }
