import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}


export async function fetchPurchaseingPlanList(params) {
  return request(`/server/purchaseRequisition/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/returnGoodsPutIn/getOrderNumber?${stringify(params)}`);
}

export async function fetchPurchaseingInventoryList(params) {
  return request(`/server/purchaseRequisition/stockList?${stringify(params)}`);
}

export async function fetchProcurementOrderList(params) {
  return request(`/server/purchaseOrder/list?${stringify(params)}`);
}

export async function fetchReturnInInventoryList(params) {
  return request(`/server/returnGoodsPutIn/stockList?${stringify(params)}`);
}

export async function fetchArrivalOrderList(params) {
  return request(`/server/purchaseArrival/list?${stringify(params)}`);
}

export async function fetchReturnInOrderList(params) {
  return request(`/server/returnGoodsPutIn/list?${stringify(params)}`);
}

export async function queryWarehouseList(params) {
  return request(`/server/warehouse/list?${stringify(params)}`);
}

export async function addProcurementOrder(params) {
  return request('/server/purchaseOrder/create', {
    method: 'POST',
    body: params,
  });
}

export async function addArrivalOrder(params) {
  return request('/server/purchaseArrival/create', {
    method: 'POST',
    body: params,
  });
}

export async function addReturnInOrder(params) {
  return request('/server/returnGoodsPutIn/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeInventory(params) {
  return request(`/server/purchaseRequisition/deleteStocks/${params.prStockId}`, {
    method: 'DELETE',
  });
}

export async function removePurchaseOrder(params) {
  return request(`/server/purchaseRequisition/${params.purchaseRequisitionId}`, {
    method: 'DELETE',
  });
}

export async function removeProcurementOrder(params) {
  return request(`/server/purchaseOrder/${params.purchaseOrderId}`, {
    method: 'DELETE',
  });
}

export async function removeArrivalOrder(params) {
  return request(`/server/purchaseArrival/${params.purchaseArrivalId}`, {
    method: 'DELETE',
  });
}

export async function removeReturnInOrder(params) {
  return request(`/server/returnGoodsPutIn/${params.returnGoodsPutInId}`, {
    method: 'DELETE',
  });
}

export async function updatePurchase(params) {
  return request(`/server/purchaseRequisition/${params.purchaseRequisitionId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateProcurementOrder(params) {
  return request(`/server/purchaseOrder/${params.purchaseOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateArrivalOrder(params) {
  return request(`/server/purchaseArrival/${params.purchaseArrivalId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateReturnInOrder(params) {
  return request(`/server/returnGoodsPutIn/${params.returnGoodsPutInId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitReturnInOrders(params) {
  return request(`/server/returnGoodsPutIn/submit/${params.returnGoodsPutInIds}`, {
    method: 'PUT',
    body: params,
  });
}

export async function fetchOrderDetailList(params) {
  return request(`/server/order/${params.orderId}`);
}
//
// export async function changeInventoryClassificationStatus(params) {
//   return request(`/server/stock_classify/setEnable/${params.stockClassifyId}`, {
//     method: 'PUT',
//     body: params,
//   });
// }
