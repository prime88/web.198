import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/distributionOrder/getOrderNumber?${stringify(params)}`);
}

export async function fetchDeliveryOrderInventoryList(params) {
  return request(`/server/distributionOrder/stockList?${stringify(params)}`);
}

export async function fetchDeliveryApplyOrderList(params) {
  return request(`/server/distributionApply/list?${stringify(params)}`);
}

export async function fetchDeliveryOrderList(params) {
  return request(`/server/distributionOrder/list?${stringify(params)}`);
}

export async function addDeliveryOrder(params) {
  return request('/server/distributionOrder/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeApplyOrder(params) {
  return request(`/server/distributionOrder/${params.distributionOrderId}`, {
    method: 'DELETE',
  });
}

export async function updateDeliveryOrder(params) {
  return request(`/server/distributionOrder/${params.distributionOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitApplyOrders(params) {
  return request(`/server/distributionOrder/submit/${params.distributionOrderIds}`, {
    method: 'PUT',
    body: params,
  });
}
