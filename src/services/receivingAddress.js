import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchReceivingAddressList(params) {
  return request(`/server/address/list?${stringify(params)}`);
}
