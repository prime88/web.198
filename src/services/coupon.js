import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchCouponList(params) {
  return request(`/server/coupon/list?${stringify(params)}`);
}

export async function addCoupon(params) {
  return request('/server/coupon/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeCoupon(params) {
  return request(`/server/coupon/${params.couponIds}`, {
    method: 'DELETE',
  });
}

export async function updateCoupon(params) {
  return request(`/server/coupon/${params.couponId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeCouponStatus(params) {
  return request(`/server/coupon/setEnable/${params.couponId}`, {
    method: 'PUT',
    body: params,
  });
}
