import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchSaleOrderList(params) {
  return request(`/server/salesOrder/list?${stringify(params)}`);
}

export async function fetchSaleOrderDetails(params) {
  return request(`/server/salesOrder/goodList?${stringify(params)}`);
}

export async function receivables(params) {
  return request(`/server/salesOrder/receivables/${params.salesOrderId}`, {
    method: 'PUT',
    body: params,
  });
}
