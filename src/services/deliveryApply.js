import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/distributionApply/getOrderNumber?${stringify(params)}`);
}

export async function fetchApplyInventoryList(params) {
  return request(`/server/distributionApply/stockList?${stringify(params)}`);
}

export async function fetchDeliveryApplyOrderList(params) {
  return request(`/server/distributionApply/list?${stringify(params)}`);
}

export async function addDeliveryApplyOrder(params) {
  return request('/server/distributionApply/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeApplyOrder(params) {
  return request(`/server/distributionApply/${params.distributionApplyId}`, {
    method: 'DELETE',
  });
}

export async function updateApplyOrder(params) {
  return request(`/server/distributionApply/${params.distributionApplyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitApplyOrders(params) {
  return request(`/server/distributionApply/submit/${params.distributionApplyIds}`, {
    method: 'PUT',
    body: params,
  });
}
