import { stringify } from 'qs';
import request from '@/utils/request';

export async function querySupplierClassificationList(params) {
  return request(`/server/supplier_classify/list?${stringify(params)}`);
}

export async function querySupplierList(params) {
  return request(`/server/supplier/list?${stringify(params)}`);
}

export async function addSupplierClassification(params) {
  return request('/server/supplier_classify/create', {
    method: 'POST',
    body: params,
  });
}

export async function addSupplier(params) {
  return request('/server/supplier/create', {
    method: 'POST',
    body: params,
  });
}

export async function isSupplierClassificationNameRepeated(params) {
  return request(`/server/supplier_classify/getJudgeByName?${stringify(params)}`);
}

export async function isSupplierNameRepeated(params) {
  return request(`/server/supplier/getJudgeByName?${stringify(params)}`);
}

export async function isSupplierClassificationCodeRepeated(params) {
  return request(`/server/supplier_classify/getJudgeByCode?${stringify(params)}`);
}

export async function isSupplierCodeRepeated(params) {
  return request(`/server/supplier/getJudgeByCode?${stringify(params)}`);
}

export async function removeSupplierClassification(params) {
  return request(`/server/supplier_classify/${params.supplierClassifyIds}`, {
    method: 'DELETE',
  });
}

export async function removeSupplier(params) {
  return request(`/server/supplier/${params.supplierIds}`, {
    method: 'DELETE',
  });
}

export async function updateSupplierClassification(params) {
  return request(`/server/supplier_classify/${params.supplierClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateSupplier(params) {
  return request(`/server/supplier/${params.supplierId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeSupplierClassificationStatus(params) {
  return request(`/server/supplier_classify/setEnable/${params.supplierClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeSupplierStatus(params) {
  return request(`/server/supplier/setEnable/${params.supplierId}`, {
    method: 'PUT',
    body: params,
  });
}
