import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchRoleList(params) {
  return request(`/server/role/list?${stringify(params)}`);
}

export async function addRole(params) {
  return request('/server/role/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeRole(params) {
  return request(`/server/role/${params.roleIds}`, {
    method: 'DELETE',
  });
}

export async function updateRole(params) {
  return request(`/server/role/${params.roleId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function setRolePermissions(params) {
  return request(`/server/role/setPermission`, {
    method: 'PUT',
    body: params,
  });
}
