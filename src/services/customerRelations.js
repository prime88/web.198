import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchVipList(params) {
  return request(`/server/member/list?${stringify(params)}`);
}

export async function fetchConsumptionDetails(params) {
  return request(`/server/member/memberConsumptionList?${stringify(params)}`);
}

export async function addVip(params) {
  return request('/server/member/create', {
    method: 'POST',
    body: params,
  });
}

export async function isBrandNameRepeated(params) {
  return request(`/server/brand/getJudgeByName?${stringify(params)}`);
}

export async function removeVip(params) {
  return request(`/server/member/${params.memberIds}`, {
    method: 'DELETE',
  });
}

export async function updateVip(params) {
  return request(`/server/member/${params.memberId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeBrandStatus(params) {
  return request(`/server/brand/setEnable/${params.brandId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function isVipNameRepeated(params) {
  return request(`/server/member/getJudgeByPhone?${stringify(params)}`);
}
