import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchTagList(params) {
  return request(`/server/storeLabel/list?${stringify(params)}`);
}

export async function addTag(params) {
  return request('/server/storeLabel/create', {
    method: 'POST',
    body: params,
  });
}

export async function isTagNameRepeated(params) {
  return request(`/server/storeLabel/getJudgeByName?${stringify(params)}`);
}

export async function removeTag(params) {
  return request(`/server/storeLabel/${params.storeLabelIds}`, {
    method: 'DELETE',
  });
}

export async function updateTag(params) {
  return request(`/server/storeLabel/${params.storeLabelId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeTagStatus(params) {
  return request(`/server/storeLabel/setDefault/${params.storeLabelId}`, {
    method: 'PUT',
    body: params,
  });
}
