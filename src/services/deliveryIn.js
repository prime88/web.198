import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function fetchOrderNumber(params) {
  return request(`/server/distributionPutIn/getOrderNumber?${stringify(params)}`);
}

export async function fetchDeliveryInOrderInventoryList(params) {
  return request(`/server/distributionPutIn/stockList?${stringify(params)}`);
}

export async function fetchDeliveryApplyOrderList(params) {
  return request(`/server/distributionApply/list?${stringify(params)}`);
}

export async function fetchDeliveryOrderList(params) {
  return request(`/server/distributionOrder/list?${stringify(params)}`);
}

export async function fetchDeliveryInOrderList(params) {
  return request(`/server/distributionPutIn/list?${stringify(params)}`);
}

export async function addDeliveryInOrder(params) {
  return request('/server/distributionPutIn/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeDeliveryInOrder(params) {
  return request(`/server/distributionPutIn/${params.distributionPutInId}`, {
    method: 'DELETE',
  });
}

export async function updateDeliveryInOrder(params) {
  return request(`/server/distributionPutIn/${params.distributionPutInId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitDeliveryInOrders(params) {
  return request(`/server/distributionPutIn/submit/${params.distributionPutInIds}`, {
    method: 'PUT',
    body: params,
  });
}
