import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchOrderList(params) {
  return request(`/server/order/list?${stringify(params)}`);
}

export async function fetchWineApplyOrderDetailList(params) {
  return request(`/server/wineCellarOrder/${params.wineCellarOrderId}`);
}

export async function fetchOrderDetailList(params) {
  return request(`/server/order/${params.orderId}`);
}

export async function fetchPickWineApplyList(params) {
  return request(`/server/wineCellarOrder/list?${stringify(params)}`);
}

// export async function addInventoryClassification(params) {
//   return request('/server/stock_classify/create', {
//     method: 'POST',
//     body: params,
//   });
// }
//
// export async function isInventoryClassificationNameRepeated(params) {
//   return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
// }
//
// export async function removeInventoryClassification(params) {
//   return request(`/server/stock_classify/${params.stockClassifyIds}`, {
//     method: 'DELETE',
//   });
// }
//
// export async function updateInventoryClassification(params) {
//   return request(`/server/stock_classify/${params.stockClassifyId}`, {
//     method: 'PUT',
//     body: params,
//   });
// }
//

export async function confirmApplyOrder(params) {
  return request(`/server/wineCellarOrder/confirm/${params.wineCellarOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function confirmOrder(params) {
  return request(`/server/order/confirm/${params.orderId}`, {
    method: 'PUT',
    body: params,
  });
}


export async function outOrder(params) {
  return request(`/server/order/putOut/${params.wineCellarOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function confirmReceipt(params) {
  return request(`/server/wineCellarOrder/confirmReceipt/${params.wineCellarOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function cancelOrder(params) {
  return request(`/server/wineCellarOrder/cancel/${params.wineCellarOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function deliveryOrder(params) {
  return request(`/server/wineCellarOrder/distribution/${params.wineCellarOrderId}`, {
    method: 'PUT',
    body: params,
  });
}
