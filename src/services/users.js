import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchUserList(params) {
  return request(`/server/user/list?${stringify(params)}`);
}

export async function fetchVipLevelList(params) {
  return request(`/server/memberLevel/list?${stringify(params)}`);
}

export async function addInventoryClassification(params) {
  return request('/server/stock_classify/create', {
    method: 'POST',
    body: params,
  });
}

export async function sendMessage(params) {
  return request('/server/messageTemplate/sendToUser', {
    method: 'POST',
    body: params,
  });
}

export async function isInventoryClassificationNameRepeated(params) {
  return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
}

export async function removeInventoryClassification(params) {
  return request(`/server/stock_classify/${params.stockClassifyIds}`, {
    method: 'DELETE',
  });
}

export async function updateInventoryClassification(params) {
  return request(`/server/stock_classify/${params.stockClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeInventoryClassificationStatus(params) {
  return request(`/server/stock_classify/setEnable/${params.stockClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}
