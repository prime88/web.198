import { stringify } from 'qs';
import request from '@/utils/request';

export async function addStoreSaleOrder(params) {
  return request('/server/salesOrder/create', {
    method: 'POST',
    body: params,
  });
}
