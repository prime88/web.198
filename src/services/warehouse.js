import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryWarehouseList(params) {
  return request(`/server/warehouse/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}

export async function addInventoryClassification(params) {
  return request('/server/stock_classify/create', {
    method: 'POST',
    body: params,
  });
}

export async function addWarehouse(params) {
  return request('/server/warehouse/create', {
    method: 'POST',
    body: params,
  });
}

export async function addInventory(params) {
  return request('/server/stock/create', {
    method: 'POST',
    body: params,
  });
}

export async function isInventoryClassificationNameRepeated(params) {
  return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
}

export async function isWarehouseNameRepeated(params) {
  return request(`/server/warehouse/getJudgeByName?${stringify(params)}`);
}

export async function isInventoryNameRepeated(params) {
  return request(`/server/stock/getJudgeByName?${stringify(params)}`);
}

export async function isInventoryNumRepeated(params) {
  return request(`/server/stock/getJudgeByStockNum?${stringify(params)}`);
}

export async function isInventoryClassificationCodeRepeated(params) {
  return request(`/server/stock_classify/getJudgeByCode?${stringify(params)}`);
}

export async function isWarehouseCodeRepeated(params) {
  return request(`/server/warehouse/getJudgeByCode?${stringify(params)}`);
}

export async function removeInventoryClassification(params) {
  return request(`/server/stock_classify/${params.stockClassifyIds}`, {
    method: 'DELETE',
  });
}

export async function removeWarehouse(params) {
  return request(`/server/warehouse/${params.warehouseIds}`, {
    method: 'DELETE',
  });
}

export async function removeInventory(params) {
  return request(`/server/stock/${params.stockIds}`, {
    method: 'DELETE',
  });
}

export async function updateInventoryClassification(params) {
  return request(`/server/stock_classify/${params.stockClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateWarehouse(params) {
  return request(`/server/warehouse/${params.warehouseId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateInventory(params) {
  return request(`/server/stock/${params.stockId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeInventoryClassificationStatus(params) {
  return request(`/server/stock_classify/setEnable/${params.stockClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeWarehouseStatus(params) {
  return request(`/server/warehouse/setEnable/${params.warehouseId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeInventory(params) {
  return request(`/server/stock/setEnable/${params.stockId}`, {
    method: 'PUT',
    body: params,
  });
}
