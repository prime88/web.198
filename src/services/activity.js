import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function fetchGoodsList(params) {
  return request(`/server/goods/list?${stringify(params)}`);
}

export async function fetchPromotionGoodsList(params) {
  return request(`/server/promotionGoods/list?${stringify(params)}`);
}

export async function fetchActivityList(params) {
  return request(`/server/promotion/list?${stringify(params)}`);
}

export async function addInventoryClassification(params) {
  return request('/server/stock_classify/create', {
    method: 'POST',
    body: params,
  });
}

export async function addGoods(params) {
  return request('/server/goods/create', {
    method: 'POST',
    body: params,
  });
}

export async function addActivityGoods(params) {
  return request('/server/promotionGoods/create', {
    method: 'POST',
    body: params,
  });
}

export async function addActivity(params) {
  return request('/server/promotion/create', {
    method: 'POST',
    body: params,
  });
}

export async function isInventoryClassificationNameRepeated(params) {
  return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
}

export async function removeGoods(params) {
  return request(`/server/promotionGoods/${params.promotionGoodsIds}`, {
    method: 'DELETE',
  });
}

export async function removeUpperGoods(params) {
  return request(`/server/storeGoods/${params.storeGoodsIds}`, {
    method: 'DELETE',
  });
}

export async function removeActivity(params) {
  return request(`/server/promotion/${params.promotionIds}`, {
    method: 'DELETE',
  });
}

export async function updateGoods(params) {
  return request(`/server/goods/${params.goodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateActivity(params) {
  return request(`/server/promotion/${params.promotionId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateActivityGoodsPrice(params) {
  return request(`/server/promotionGoods/${params.promotionGoodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeGoodsStatus(params) {
  return request(`/server/goods/setEnable/${params.goodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeUpperGoodsStatus(params) {
  return request(`/server/storeGoods/${params.storeGoodsIds}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeActivityStatus(params) {
  return request(`/server/promotion/setEnable/${params.promotionId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function batchChangeUpperGoodsStatus(params) {
  return request(`/server/storeGoods/updateStatus`, {
    method: 'PUT',
    body: params,
  });
}
