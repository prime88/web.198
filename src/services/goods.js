import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function fetchGoodsList(params) {
  return request(`/server/goods/list?${stringify(params)}`);
}

export async function fetchUpperGoodsList(params) {
  return request(`/server/storeGoods/list?${stringify(params)}`);
}

export async function addInventoryClassification(params) {
  return request('/server/stock_classify/create', {
    method: 'POST',
    body: params,
  });
}

export async function addGoods(params) {
  return request('/server/goods/create', {
    method: 'POST',
    body: params,
  });
}

export async function addUpperGoods(params) {
  return request('/server/storeGoods/create', {
    method: 'POST',
    body: params,
  });
}

export async function isInventoryClassificationNameRepeated(params) {
  return request(`/server/stock_classify/getJudgeByName?${stringify(params)}`);
}

export async function removeGoods(params) {
  return request(`/server/goods/${params.goodsIds}`, {
    method: 'DELETE',
  });
}

export async function removeUpperGoods(params) {
  return request(`/server/storeGoods/${params.storeGoodsIds}`, {
    method: 'DELETE',
  });
}

export async function updateGoods(params) {
  return request(`/server/goods/${params.goodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeGoodsStatus(params) {
  return request(`/server/goods/setEnable/${params.goodsId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeUpperGoodsStatus(params) {
  return request(`/server/storeGoods/${params.storeGoodsIds}`, {
    method: 'PUT',
    body: params,
  });
}

export async function batchChangeUpperGoodsStatus(params) {
  return request(`/server/storeGoods/updateStatus`, {
    method: 'PUT',
    body: params,
  });
}
