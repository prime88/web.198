import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchUserCommentList(params) {
  return request(`/server/evaluate/list?${stringify(params)}`);
}

export async function removeUserComment(params) {
  return request(`/server/evaluate/${params.evaluateIds}`, {
    method: 'DELETE',
  });
}
