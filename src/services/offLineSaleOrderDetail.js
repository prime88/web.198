import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchOrderDetailList(params) {
  return request(`/server/salesOrder/goodList?${stringify(params)}`);
}
