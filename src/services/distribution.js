import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchDistributionList(params) {
  return request(`/server/delivery/list?${stringify(params)}`);
}

export async function fetchProblemList(params) {
  return request(`/server/problem/list?${stringify(params)}`);
}

export async function addDistributionService(params) {
  return request('/server/delivery/create', {
    method: 'POST',
    body: params,
  });
}

export async function addProblem(params) {
  return request('/server/problem/create', {
    method: 'POST',
    body: params,
  });
}

export async function isProblemClassificationNameRepeated(params) {
  return request(`/server/problem_classify/getJudgeByName?${stringify(params)}`);
}

export async function removeDistributionService(params) {
  return request(`/server/delivery/${params.deliveryIds}`, {
    method: 'DELETE',
  });
}

export async function removeProblem(params) {
  return request(`/server/problem/${params.problemIds}`, {
    method: 'DELETE',
  });
}

export async function updateDistributionService(params) {
  return request(`/server/delivery/${params.deliveryId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateProblem(params) {
  return request(`/server/problem/${params.problemId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeDistributionServiceStatus(params) {
  return request(`/server/delivery/setEnable/${params.deliveryId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeProblemStatus(params) {
  return request(`/server/problem/setEnable/${params.problemId}`, {
    method: 'PUT',
    body: params,
  });
}
