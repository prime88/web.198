import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchProblemClassificationList(params) {
  return request(`/server/problem_classify/list?${stringify(params)}`);
}

export async function fetchProblemList(params) {
  return request(`/server/problem/list?${stringify(params)}`);
}

export async function addProblemClassification(params) {
  return request('/server/problem_classify/create', {
    method: 'POST',
    body: params,
  });
}

export async function addProblem(params) {
  return request('/server/problem/create', {
    method: 'POST',
    body: params,
  });
}

export async function isProblemClassificationNameRepeated(params) {
  return request(`/server/problem_classify/getJudgeByName?${stringify(params)}`);
}

export async function removeProblemClassification(params) {
  return request(`/server/problem_classify/${params.problemClassifyIds}`, {
    method: 'DELETE',
  });
}

export async function removeProblem(params) {
  return request(`/server/problem/${params.problemIds}`, {
    method: 'DELETE',
  });
}

export async function updateProblemClassification(params) {
  return request(`/server/problem_classify/${params.problemClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateProblem(params) {
  return request(`/server/problem/${params.problemId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeProblemClassificationStatus(params) {
  return request(`/server/problem_classify/setEnable/${params.problemClassifyId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeProblemStatus(params) {
  return request(`/server/problem/setEnable/${params.problemId}`, {
    method: 'PUT',
    body: params,
  });
}
