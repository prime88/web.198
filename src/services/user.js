import request from '@/utils/request';

export async function query() {
  return request('/api/users');
}

export async function queryCurrent() {
  return request('/server/staff/get');
}

export async function changePassword(params) {
  return request(`/server/staff/updatePassword`, {
    method: 'PUT',
    body: params,
  });
}
