import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryInventoryClassificationList(params) {
  return request(`/server/stock_classify/list?${stringify(params)}`);
}

export async function queryInventoryList(params) {
  return request(`/server/stock/list?${stringify(params)}`);
}


export async function fetchPurchaseingPlanList(params) {
  return request(`/server/purchaseRequisition/list?${stringify(params)}`);
}

export async function fetchInOrderNumber(params) {
  return request(`/server/otherPutIn/getOrderNumber?${stringify(params)}`);
}

export async function fetchOutOrderNumber(params) {
  return request(`/server/otherPutOut/getOrderNumber?${stringify(params)}`);
}

export async function fetchPurchaseingInventoryList(params) {
  return request(`/server/purchaseRequisition/stockList?${stringify(params)}`);
}

export async function fetchProcurementOrderList(params) {
  return request(`/server/purchaseOrder/list?${stringify(params)}`);
}

export async function fetchOtherInInventoryList(params) {
  return request(`/server/otherPutIn/stockList?${stringify(params)}`);
}

export async function fetchOtherOutOrderInventoryList(params) {
  return request(`/server/otherPutOut/stockList?${stringify(params)}`);
}

export async function fetchArrivalOrderList(params) {
  return request(`/server/purchaseArrival/list?${stringify(params)}`);
}

export async function fetchOtherInOrderList(params) {
  return request(`/server/otherPutIn/list?${stringify(params)}`);
}

export async function fetchOtherOutOrderList(params) {
  return request(`/server/otherPutOut/list?${stringify(params)}`);
}

export async function queryWarehouseList(params) {
  return request(`/server/warehouse/list?${stringify(params)}`);
}

export async function addProcurementOrder(params) {
  return request('/server/purchaseOrder/create', {
    method: 'POST',
    body: params,
  });
}

export async function addArrivalOrder(params) {
  return request('/server/purchaseArrival/create', {
    method: 'POST',
    body: params,
  });
}

export async function addOtherInOrder(params) {
  return request('/server/otherPutIn/create', {
    method: 'POST',
    body: params,
  });
}

export async function addOtherOutOrder(params) {
  return request('/server/otherPutOut/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeInventory(params) {
  return request(`/server/purchaseRequisition/deleteStocks/${params.prStockId}`, {
    method: 'DELETE',
  });
}

export async function removePurchaseOrder(params) {
  return request(`/server/purchaseRequisition/${params.purchaseRequisitionId}`, {
    method: 'DELETE',
  });
}

export async function removeProcurementOrder(params) {
  return request(`/server/purchaseOrder/${params.purchaseOrderId}`, {
    method: 'DELETE',
  });
}

export async function removeArrivalOrder(params) {
  return request(`/server/purchaseArrival/${params.purchaseArrivalId}`, {
    method: 'DELETE',
  });
}

export async function removeOtherInOrder(params) {
  return request(`/server/otherPutIn/${params.otherPutInId}`, {
    method: 'DELETE',
  });
}

export async function removeOtherOutOrder(params) {
  return request(`/server/otherPutOut/${params.otherPutOutId}`, {
    method: 'DELETE',
  });
}

export async function updatePurchase(params) {
  return request(`/server/purchaseRequisition/${params.purchaseRequisitionId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateProcurementOrder(params) {
  return request(`/server/purchaseOrder/${params.purchaseOrderId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateArrivalOrder(params) {
  return request(`/server/purchaseArrival/${params.purchaseArrivalId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateOtherInOrder(params) {
  return request(`/server/otherPutIn/${params.otherPutInId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateOtherOutOrder(params) {
  return request(`/server/otherPutOut/${params.otherPutOutId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitOtherInOrders(params) {
  return request(`/server/otherPutIn/submit/${params.otherPutInIds}`, {
    method: 'PUT',
    body: params,
  });
}

export async function submitOtherOutOrders(params) {
  return request(`/server/otherPutOut/submit/${params.otherPutOutIds}`, {
    method: 'PUT',
    body: params,
  });
}
//
// export async function changeInventoryClassificationStatus(params) {
//   return request(`/server/stock_classify/setEnable/${params.stockClassifyId}`, {
//     method: 'PUT',
//     body: params,
//   });
// }
