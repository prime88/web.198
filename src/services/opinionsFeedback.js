import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchOpinionsFeedbackList(params) {
  return request(`/server/feedback/list?${stringify(params)}`);
}

export async function removeOpinionsFeedback(params) {
  return request(`/server/feedback/${params.feedbackIds}`, {
    method: 'DELETE',
  });
}

export async function changeReadStatus(params) {
  return request(`/server/feedback/setRead/${params.feedbackIds}`, {
    method: 'PUT',
    body: params,
  });
}

export async function reply(params) {
  return request(`/server/feedback/reply/${params.feedbackId}`, {
    method: 'PUT',
    body: params,
  });
}
