import { stringify } from 'qs';
import request from '@/utils/request';

export async function fetchDeliveryTimeList(params) {
  return request(`/server/deliveryTime/list?${stringify(params)}`);
}

export async function fetchBaseSetting(params) {
  return request(`/server/setting/get?${stringify(params)}`);
}

export async function addDeliveryTime(params) {
  return request('/server/deliveryTime/create', {
    method: 'POST',
    body: params,
  });
}

export async function removeDeliveryTime(params) {
  return request(`/server/deliveryTime/${params.deliveryTimeIds}`, {
    method: 'DELETE',
  });
}

export async function updateDeliveryTime(params) {
  return request(`/server/deliveryTime/${params.deliveryTimeId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function updateBaseSetting(params) {
  return request(`/server/setting/update`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeDeliveryTimeStatus(params) {
  return request(`/server/deliveryTime/setEnable/${params.deliveryTimeId}`, {
    method: 'PUT',
    body: params,
  });
}
