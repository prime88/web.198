import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryBrandList(params) {
  return request(`/server/brand/list?${stringify(params)}`);
}

export async function addBrand(params) {
  return request('/server/brand/create', {
    method: 'POST',
    body: params,
  });
}

export async function isBrandNameRepeated(params) {
  return request(`/server/brand/getJudgeByName?${stringify(params)}`);
}

export async function isBrandCodeRepeated(params) {
  return request(`/server/brand/getJudgeByCode?${stringify(params)}`);
}

export async function removeBrand(params) {
  return request(`/server/brand/${params.brandIds}`, {
    method: 'DELETE',
  });
}

export async function updateBrand(params) {
  return request(`/server/brand/${params.brandId}`, {
    method: 'PUT',
    body: params,
  });
}

export async function changeBrandStatus(params) {
  return request(`/server/brand/setEnable/${params.brandId}`, {
    method: 'PUT',
    body: params,
  });
}
