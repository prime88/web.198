import React, { PureComponent } from 'react';
import {imgDomainName} from "../../constants";
import {
  Row,
  Col,
  Modal,
} from 'antd';

class ImagePreviewModal extends PureComponent {
  render() {
    const {
      previewVisible,
      imageList,
      handleCancel,
      imgWidth,
      imgHeight,
      colSpan,
      width,
      title,
    } = this.props;

    return (
      <Modal title={title || ''} width={width || 520} visible={previewVisible} footer={null} onCancel={() => handleCancel()}>
        <Row type='flex' justify='center' gutter={16}>
          {imageList.map((item, index) => (
            <Col key={index} span={colSpan || 8} style={{marginTop: 16, textAlign: 'center'}}>
              <img style={{width: imgWidth, height: imgHeight}} alt='' src={`${imgDomainName}${item}`} />
            </Col>
          ))}
        </Row>
      </Modal>
    );
  }
}

export default ImagePreviewModal;
