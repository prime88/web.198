import React, { PureComponent, Fragment } from 'react';
import { Table, Alert } from 'antd';
import styles from './index.less';

function initTotalList(columns) {
  const totalList = [];
  columns.forEach(column => {
    if (column.needTotal) {
      totalList.push({ ...column, total: 0 });
    }
  });
  return totalList;
}

class StandardTable extends PureComponent {
  constructor(props) {
    super(props);
    const { columns } = props;
    const needTotalList = initTotalList(columns);

    this.state = {
      selectedRowKeys: [],
      needTotalList,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // clean state
    // if (nextProps.selectedRows.length === 0) {
    if (nextProps.selectedRows && nextProps.selectedRows.length === 0) {
      const needTotalList = initTotalList(nextProps.columns);
      return {
        selectedRowKeys: [],
        needTotalList,
      };
    }
    else if (nextProps.selectedRows && nextProps.selectedRows.length > 0) {
    // else if (nextProps.selectedRows && nextProps.selectedRows.length > 0) {
      return {
        selectedRowKeys: nextProps.selectedRows.map(item => item[nextProps.rowKey]),
      };
    }
    return null;
  }

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    let { needTotalList } = this.state;
    needTotalList = needTotalList.map(item => ({
      ...item,
      total: selectedRows.reduce((sum, val) => sum + parseFloat(val[item.dataIndex], 10), 0),
    }));
    const { onSelectRow } = this.props;
    if (onSelectRow) {
      onSelectRow(selectedRows);
    }
    this.setState({ selectedRowKeys, needTotalList });
  };

  handleTableChange = (pagination, filters, sorter) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(pagination, filters, sorter);
    }
  };

  cleanSelectedKeys = () => {
    this.handleRowSelectChange([], []);
  };

  onExpandedRowsChange = () => {

  }

  defaultTitle = () => '';

  render() {
    const { selectedRowKeys, needTotalList } = this.state;
    const {
      data: { rows, pagination },
      loading,
      columns,
      rowKey,
      expandedRowRender,
      onExpand,
      footer,
      scroll,
      onRow,
      components,
      className,
      size,
      selectedRows,
      rowClassName,
      paginationOfTable,
      type,
      onExpandedRowsChange,
      expandedRowKeys,
      title,
      onSelect,
      checkboxProps,
      hideDefaultSelections,
      onSelectAll,
    } = this.props;

    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      ...pagination,
      ...paginationOfTable,
    };

    const rowSelection = {
      hideDefaultSelections: true,
      selectedRowKeys,
      onChange: this.handleRowSelectChange,
      onSelect: onSelect || null,
      // getCheckboxProps: record => ({
      //   disabled: record.disabled,
      // }),
      getCheckboxProps: checkboxProps || '',
      type: type || 'checkbox',
      onSelectAll: onSelectAll ? (selected, selectedRows, changeRows) => onSelectAll(selected, selectedRows, changeRows, rows) : null
    };

    return (
      <div className={styles.standardTable}>
        {/*<div className={styles.tableAlert}>*/}
        {/*<Alert*/}
        {/*message={*/}
        {/*<Fragment>*/}
        {/*已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;*/}
        {/*{needTotalList.map(item => (*/}
        {/*<span style={{ marginLeft: 8 }} key={item.dataIndex}>*/}
        {/*{item.title}*/}
        {/*总计&nbsp;*/}
        {/*<span style={{ fontWeight: 600 }}>*/}
        {/*{item.render ? item.render(item.total) : item.total}*/}
        {/*</span>*/}
        {/*</span>*/}
        {/*))}*/}
        {/*<a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>*/}
        {/*清空*/}
        {/*</a>*/}
        {/*</Fragment>*/}
        {/*}*/}
        {/*type="info"*/}
        {/*showIcon*/}
        {/*/>*/}
        {/*</div>*/}
        <Table
          className={className || null}
          loading={loading}
          rowKey={rowKey || 'key'}
          expandedRowRender={expandedRowRender || null}
          onExpand={onExpand || null}
          rowSelection={selectedRows ? rowSelection : null}
          // dataSource={list}
          dataSource={rows}
          columns={columns}
          pagination={paginationProps}
          onChange={this.handleTableChange}
          footer={footer || null}
          scroll={scroll || {}}
          onRow={onRow || null}
          components={components || { body: {} }}
          size={size || null}
          rowClassName={rowClassName || ''}
          onExpandedRowsChange={onExpandedRowsChange || this.onExpandedRowsChange}
          expandedRowKeys={expandedRowKeys || []}
          title={title || null}
        />
      </div>
    );
  }
}

export default StandardTable;
