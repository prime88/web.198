# web.198

#### 项目介绍
{**以下是码云平台说明，您可以替换为您的项目简介**
码云是开源中国推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


#### 自定义组件customComponents

一、StockList: 库存列表
1、属性
（1）warehouseId：仓库Id

二、InventoryList：存货列表
1、属性：无

三、OffLineSaleOrderDetail：线下销售订单打印详情，分为普通订单详情 和 配送订单详情
1、属性
（1）record：订单数据
（2）isDeliveryOrder：是否是配送单

四、EditableCell：单元格编辑
1、属性（都可不传）
（1）editable：列是否进行单元格编辑
（2）isInputNumber：单元格是否用InputNumber组件进行编辑
（3）inputNumberPrecision：InputNumber值保留的小数点个数
（4）inputNumberMin：InputNumber的最小值
（5）handleInputNumberChange：InputNumber的onChange事件
（6）isSelect：单元格是否用Select组件进行编辑
（7）handleSelectChange：Select的onChange事件
（8）selectOptions：Select的下拉列表------没用了
（9）selectFocus：Select focus时调用，用于获取下拉列表数据
（10）selectChange：Select onChange时调用
