export default [
  //移动端二维码跳转
  {
    path: '/mobile',
    component: '../pages/Mobile/Mobile',
  },
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' },
      { path: '/user/register', component: './User/Register' },
      { path: '/user/register-result', component: './User/RegisterResult' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    authority: ['admin', 'user'],
    routes: [
      //HomePage
      { path: '/', redirect: '/homepage' },
      {
        path: '/homepage',
        name: 'homepage',
        icon: 'dashboard',
        component: './HomePage/HomePage',
      },
      {
        path: '/basic-data',
        name: 'basic-data',
        icon: 'database',
        routes: [
          {
            path: '/basic-data/inventory-classification',
            name: 'inventory-classification',
            component: './BasicData/InventoryClassification',
          },
          {
            path: '/basic-data/inventory-management',
            name: 'inventory-management',
            component: './BasicData/InventoryManagement',
          },
          {
            path: '/basic-data/Warehouse-management',
            name: 'Warehouse-management',
            component: './BasicData/WarehouseManagement',
          },
          {
            path: '/basic-data/brand-management',
            name: 'brand-management',
            component: './BasicData/BrandManagement',
          },
          {
            path: '/basic-data/supplier-classification',
            name: 'supplier-classification',
            component: './BasicData/SupplierClassification',
          },
          {
            path: '/basic-data/supplier',
            name: 'supplier',
            component: './BasicData/Supplier',
          },
          {
            path: '/basic-data/measurement-unit-classification',
            name: 'measurement-unit-classification',
            component: './BasicData/MeasurementUnitClassification',
          },
          {
            path: '/basic-data/measurement-unit',
            name: 'measurement-unit',
            component: './BasicData/MeasurementUnit',
          },
        ],
      },
      {
        path: '/approval-management',
        name: 'approval-management',
        icon: 'check-square',
        routes: [
          {
            path: '/approval-management/approval-settings',
            name: 'approval-settings',
            component: './ApprovalManagement/ApprovalSettings',
          },
          {
            path: '/approval-management/approval-logs',
            name: 'approval-logs',
            component: './ApprovalManagement/ApprovalLogs',
          },
        ],
      },
      {
        path: '/procurement-management',
        name: 'procurement-management',
        icon: 'shopping-cart',
        routes: [
          {
            path: '/procurement-management/store-purchaseing',
            name: 'store-purchaseing',
            routes: [
              {
                path: '/procurement-management/store-purchaseing/purchase-plan',
                name: 'purchase-plan',
                component: './Procurement/StorePurchaseing/PurchaseingPlan',
              },
              {
                path: '/procurement-management/store-purchaseing/purchase-plan-list',
                name: 'purchase-plan-list',
                component: './Procurement/StorePurchaseing/PurchaseingPlanList',
              },
            ],
          },
          {
            path: '/procurement-management/procurement-plan',
            name: 'procurement-plan',
            routes: [
              {
                path: '/procurement-management/procurement-plan/procurement-plan-order',
                name: 'procurement-plan-order',
                component: './Procurement/ProcurementPlan/ProcurementPlanOrder',
                // component: './Dashboard/Analysis',
              },
              {
                path: '/procurement-management/procurement-plan/procurement-plan-list',
                name: 'procurement-plan-list',
                component: './Procurement/ProcurementPlan/ProcurementPlanList',
              },
            ],
          },
          {
            path: '/procurement-management/procurement-order',
            name: 'procurement-order',
            routes: [
              {
                path: '/procurement-management/procurement-order/procurement-order-add',
                name: 'procurement-order-add',
                component: './Procurement/ProcurementOrder/ProcurementOrder',
              },
              {
                path: '/procurement-management/procurement-order/procurement-order-list',
                name: 'procurement-order-list',
                component: './Procurement/ProcurementOrder/ProcurementOrderList',
              },
            ],
          },
          {
            path: '/procurement-management/procurement-arrival',
            name: 'procurement-arrival',
            // component: './Procurement',
            routes: [
              {
                path: '/procurement-management/procurement-arrival/arrival-order',
                name: 'arrival-order',
                component: './Procurement/ProcurementArrival/ArrivalOrder',
              },
              {
                path: '/procurement-management/procurement-arrival/arrival-order-list',
                name: 'arrival-order-list',
                component: './Procurement/ProcurementArrival/ArrivalOrderList',
              },
            ],
          },
          {
            path: '/procurement-management/procurement-warehousing',
            name: 'procurement-warehousing',
            // component: './Procurement',
            routes: [
              {
                path: '/procurement-management/procurement-warehousing/warehousing-order',
                name: 'warehousing-order',
                component: './Procurement/ProcurementWarehousing/WarehousingOrder',
              },
              {
                path: '/procurement-management/procurement-warehousing/warehousing-order-list',
                name: 'warehousing-order-list',
                component: './Procurement/ProcurementWarehousing/WarehousingOrderList',
              },
            ],
          },
        ],
      },
      {
        path: '/delivery-management',
        name: 'delivery-management',
        icon: 'stock',
        routes: [
          {
            path: '/delivery-management/delivery-apply',
            name: 'delivery-apply',
            routes: [
              {
                path: '/delivery-management/delivery-apply/delivery-apply-order',
                name: 'delivery-apply-order',
                component: './Delivery/DeliveryApply/DeliveryApplyOrder',
              },
              {
                path: '/delivery-management/delivery-apply/delivery-apply-order-list',
                name: 'delivery-apply-order-list',
                component: './Delivery/DeliveryApply/DeliveryApplyOrderList',
              },
            ],
          },
          {
            path: '/delivery-management/delivery-order',
            name: 'delivery-order',
            routes: [
              {
                path: '/delivery-management/delivery-order/delivery-order',
                name: 'delivery-order',
                component: './Delivery/DeliveryOrder/DeliveryOrder',
              },
              {
                path: '/delivery-management/delivery-order/delivery-order-list',
                name: 'delivery-order-list',
                component: './Delivery/DeliveryOrder/DeliveryOrderList',
              },
            ],
          },
          {
            path: '/delivery-management/delivery-out',
            name: 'delivery-out',
            routes: [
              {
                path: '/delivery-management/delivery-out/delivery-out-order',
                name: 'delivery-out-order',
                component: './Delivery/DeliveryOut/DeliveryOutOrder',
              },
              {
                path: '/delivery-management/delivery-out/delivery-out-order-list',
                name: 'delivery-out-order-list',
                component: './Delivery/DeliveryOut/DeliveryOutOrderList',
              },
            ],
          },
          {
            path: '/delivery-management/delivery-in',
            name: 'delivery-in',
            routes: [
              {
                path: '/delivery-management/delivery-in/delivery-in-order',
                name: 'delivery-in-order',
                component: './Delivery/DeliveryIn/DeliveryInOrder',
              },
              {
                path: '/delivery-management/delivery-in/delivery-in-order-list',
                name: 'delivery-in-order-list',
                component: './Delivery/DeliveryIn/DeliveryInOrderList',
              },
            ],
          },
        ],
      },
      {
        path: '/stock-management',
        name: 'stock-management',
        icon: 'inbox',
        routes: [
          {
            path: '/stock-management/check-order',
            name: 'check-order',
            component: './StockManagement/CheckOrder',
          },
          {
            path: '/stock-management/check-order-list',
            name: 'check-order-list',
            component: './StockManagement/CheckOrderList',
          },
          {
            path: '/stock-management/stock-list',
            name: 'stock-list',
            component: './StockManagement/StockList',
          },
          {
            path: '/stock-management/other-out-in',
            name: 'other-out-in',
            routes: [
              {
                path: '/stock-management/other-out-in/other-in',
                name: 'other-in-order',
                component: './StockManagement/OtherIn',
              },
              {
                path: '/stock-management/other-out-in/other-in-list',
                name: 'other-in-order-list',
                component: './StockManagement/OtherInList',
              },
              {
                path: '/stock-management/other-out-in/other-out',
                name: 'other-out-order',
                component: './StockManagement/OtherOut',
              },
              {
                path: '/stock-management/other-out-in/other-out-list',
                name: 'other-out-order-list',
                component: './StockManagement/OtherOutList',
              },
            ],
          }
        ],
      },
      {
        path: '/sales-management',
        name: 'sales-management',
        icon: 'shop',
        routes: [
          {
            path: '/sales-management/store-sales',
            name: 'store-sales',
            component: './SalesManagement/StoreSales',
          },
          {
            path: '/sales-management/phone-sales',
            name: 'phone-sales',
            component: './SalesManagement/PhoneSales',
          },
          {
            path: '/sales-management/delivery-management',
            name: 'delivery-management',
            component: './SalesManagement/DeliveryManagement',
          },
          {
            path: '/sales-management/whole-sales',
            name: 'whole-sales',
            component: './SalesManagement/WholeSales',
          },
          {
            path: '/sales-management/sale-order-list',
            name: 'sale-order-list',
            component: './SalesManagement/SaleOrderList',
          },
          {
            path: '/sales-management/sale-return',
            name: 'sale-return',
            component: './SalesManagement/SaleReturn',
          },
          {
            path: '/sales-management/sale-return-list',
            name: 'sale-return-list',
            component: './SalesManagement/SaleReturnList',
          },
        ],
      },
      {
        path: '/customer-relations',
        name: 'customer-relations',
        icon: 'swap',
        routes: [
          {
            path: '/customer-relations/vip-management',
            name: 'vip-management',
            component: './CustomerRelations/VipManagement',
          },
          {
            path: '/customer-relations/consumption-details',
            name: 'consumption-details',
            component: './CustomerRelations/ConsumptionDetails',
          },
        ],
      },
      {
        path: '/system-management',
        name: 'system-management',
        icon: 'setting',
        routes: [
          {
            path: '/system-management/organizational-structure',
            name: 'organizational-structure',
            component: './SystemManagement/OrganizationalStructure',
          },
          {
            path: '/system-management/personnel-files',
            name: 'personnel-files',
            component: './SystemManagement/PersonnelFiles',
          },
          {
            path: '/system-management/staff-management',
            name: 'staff-management',
            component: './SystemManagement/StaffManagement',
          },
          {
            path: '/system-management/role-management',
            name: 'role-management',
            component: './SystemManagement/RoleManagement',
          },
          {
            path: '/system-management/module-management',
            name: 'module-management',
            component: './SystemManagement/ModuleManagement',
          },
        ],
      },
      {
        path: '/app-management',
        name: 'app-management',
        icon: 'appstore',
        routes: [
          {
            path: '/app-management/order-management',
            name: 'order-management',
            routes: [
              // {
              //   path: '/app-management/order-management/express-distribution',
              //   name: 'express-distribution',
              //   component: './AppManagement/StoreManagement/StoreList',
              // },
              {
                path: '/app-management/order-management/store-gain',
                name: 'store-gain',
                component: './AppManagement/OrderManagement/StoreGain',
              },
              {
                path: '/app-management/order-management/store-delivery',
                name: 'store-delivery',
                component: './AppManagement/OrderManagement/StoreDelivery',
              },
              {
                path: '/app-management/order-management/keep-wine-management',
                name: 'keep-wine-management',
                component: './AppManagement/OrderManagement/KeepWineManagement',
              },
              {
                path: '/app-management/order-management/pick-wine-apply',
                name: 'pick-wine-apply',
                component: './AppManagement/OrderManagement/PickWineApply',
              },
              {
                path: '/app-management/order-management/sale-out-order',
                name: 'sale-out-order',
                component: './AppManagement/OrderManagement/SaleOrder',
              },
              {
                path: '/app-management/order-management/sale-out-order-list',
                name: 'sale-out-order-list',
                component: './AppManagement/OrderManagement/SaleOrderList',
              },
              {
                path: '/app-management/order-management/return-management',
                name: 'return-management',
                component: './AppManagement/OrderManagement/ReturnManagement',
              },
              {
                path: '/app-management/order-management/return-in',
                name: 'return-in',
                component: './AppManagement/OrderManagement/ReturnIn',
              },
              {
                path: '/app-management/order-management/return-in-list',
                name: 'return-in-list',
                component: './AppManagement/OrderManagement/ReturnInList',
              },
            ],
          },
          {
            path: '/app-management/store-management',
            name: 'store-management',
            routes: [
              {
                path: '/app-management/store-management/store-list',
                name: 'store-list',
                component: './AppManagement/StoreManagement/StoreList',
              },
            ],
          },
          {
            path: '/app-management/goods-management',
            name: 'goods-management',
            routes: [
              {
                path: '/app-management/goods-management/goods-list',
                name: 'goods-list',
                component: './AppManagement/GoodsManagement/GoodsList',
              },
              {
                path: '/app-management/goods-management/goods-upper',
                name: 'goods-upper',
                component: './AppManagement/GoodsManagement/GoodsUpper',
              },
              {
                path: '/app-management/goods-management/user-comment',
                name: 'user-comment',
                component: './AppManagement/GoodsManagement/UserComment',
              },
              // {
              //   path: '/app-management/goods-management/inventory-query',
              //   name: 'inventory-query',
              //   component: './AppManagement/GoodsManagement/InventoryQuery',
              // },
            ],
          },
          {
            path: '/app-management/vip-management',
            name: 'vip-management',
            routes: [
              {
                path: '/app-management/vip-management/electronic-wallet',
                name: 'electronic-wallet',
                component: './AppManagement/VipManagement/ElectronicWallet',
              },
              {
                path: '/app-management/vip-management/ele-wall-recharge-record',
                name: 'ele-wall-recharge-record',
                component: './AppManagement/VipManagement/EleWallRechargeRecord',
              },
              {
                path: '/app-management/vip-management/vip-level-settings',
                name: 'vip-level-settings',
                component: './AppManagement/VipManagement/VipLevelSettings',
              },
              {
                path: '/app-management/vip-management/buying-giving-settings',
                name: 'buying-giving-settings',
                component: './AppManagement/VipManagement/BuyingGivingSettings',
              },
              {
                path: '/app-management/vip-management/buying-giving-recharge-record',
                name: 'buying-giving-recharge-record',
                component: './AppManagement/VipManagement/BuyingGivingRechargeRecord',
              },
            ],
          },
          {
            path: '/app-management/advertising-management',
            name: 'advertising-management',
            routes: [
              {
                path: '/app-management/advertising-management/soft-text-management',
                name: 'soft-text-management',
                component: './AppManagement/AdvertisingManagement/SoftTextManagement',
              },
              {
                path: '/app-management/advertising-management/advertising-list',
                name: 'advertising-list',
                component: './AppManagement/AdvertisingManagement/AdvertisingList',
              },
            ],
          },
          {
            path: '/app-management/message-management',
            name: 'message-management',
            routes: [
              {
                path: '/app-management/message-management/message-list',
                name: 'message-list',
                component: './AppManagement/MessageManagement/MessageList',
              },
              {
                path: '/app-management/message-management/send-record',
                name: 'send-record',
                component: './AppManagement/MessageManagement/SendRecord',
              },
            ],
          },
          {
            path: '/app-management/activity-management',
            name: 'activity-management',
            routes: [
              {
                path: '/app-management/activity-management/coupon-management',
                name: 'coupon-management',
                component: './AppManagement/ActivityManagement/CouponManagement',
              },
              {
                path: '/app-management/activity-management/cash-coupon-management',
                name: 'cash-coupon-management',
                component: './AppManagement/ActivityManagement/CashCouponManagement',
              },
              {
                path: '/app-management/activity-management/activity-list',
                name: 'activity-list',
                component: './AppManagement/ActivityManagement/ActivityList',
              },
            ],
          },
          {
            path: '/app-management/other-management',
            name: 'other-management',
            routes: [
              {
                path: '/app-management/other-management/user-management',
                name: 'user-management',
                component: './AppManagement/OtherManagement/UserManagement',
              },
              {
                path: '/app-management/other-management/opinions-feedback',
                name: 'opinions-feedback',
                component: './AppManagement/OtherManagement/OpinionsFeedback',
              },
              {
                path: '/app-management/other-management/receiving-address',
                name: 'receiving-address',
                component: './AppManagement/OtherManagement/ReceivingAddress',
              },
              {
                path: '/app-management/other-management/invoice-management',
                name: 'invoice-management',
                component: './AppManagement/OtherManagement/InvoiceManagement',
              },
              {
                path: '/app-management/other-management/common-problem-classification',
                name: 'common-problem-classification',
                component: './AppManagement/OtherManagement/CommonProblemClassification',
              },
              {
                path: '/app-management/other-management/common-problem',
                name: 'common-problem',
                component: './AppManagement/OtherManagement/CommonProblem',
              },
              {
                path: '/app-management/other-management/tags-management',
                name: 'tags-management',
                component: './AppManagement/OtherManagement/StoreTags',
              },
              {
                path: '/app-management/other-management/distribution-service',
                name: 'distribution-service',
                component: './AppManagement/OtherManagement/DistributionService',
              },
              {
                path: '/app-management/other-management/other-settings',
                name: 'other-settings',
                component: './AppManagement/OtherManagement/OtherSettings',
              },
            ],
          },
        ],
      },
      {
        path: 'change-logs',
        name: 'change-logs',
        icon: 'menu',
        component: './Logs/ChangeLogs',
      },
      {
        name: 'exception',
        icon: 'warning',
        path: '/exception',
        hideInMenu: true,
        routes: [
          // exception
          {
            path: '/exception/403',
            name: 'not-permission',
            component: './Exception/403',
          },
          {
            path: '/exception/404',
            name: 'not-find',
            component: './Exception/404',
          },
          {
            path: '/exception/500',
            name: 'server-error',
            component: './Exception/500',
          },
          {
            path: '/exception/trigger',
            name: 'trigger',
            hideInMenu: true,
            component: './Exception/TriggerException',
          },
        ],
      },

      // dashboard
      // { path: '/', redirect: '/dashboard/analysis' },
      /*
      {
        path: '/dashboard',
        name: 'dashboard',
        icon: 'dashboard',
        routes: [
          {
            path: '/dashboard/analysis',
            name: 'analysis',
            component: './Dashboard/Analysis',
          },
          {
            path: '/dashboard/monitor',
            name: 'monitor',
            component: './Dashboard/Monitor',
          },
          {
            path: '/dashboard/workplace',
            name: 'workplace',
            component: './Dashboard/Workplace',
          },
        ],
      },
      // forms
      {
        path: '/form',
        icon: 'form',
        name: 'form',
        routes: [
          {
            path: '/form/basic-form',
            name: 'basicform',
            component: './Forms/BasicForm',
          },
          {
            path: '/form/step-form',
            name: 'stepform',
            component: './Forms/StepForm',
            hideChildrenInMenu: true,
            routes: [
              {
                path: '/form/step-form',
                name: 'stepform',
                redirect: '/form/step-form/info',
              },
              {
                path: '/form/step-form/info',
                name: 'info',
                component: './Forms/StepForm/Step1',
              },
              {
                path: '/form/step-form/confirm',
                name: 'confirm',
                component: './Forms/StepForm/Step2',
              },
              {
                path: '/form/step-form/result',
                name: 'result',
                component: './Forms/StepForm/Step3',
              },
            ],
          },
          {
            path: '/form/advanced-form',
            name: 'advancedform',
            authority: ['admin'],
            component: './Forms/AdvancedForm',
          },
        ],
      },
      // list
      {
        path: '/list',
        icon: 'table',
        name: 'list',
        routes: [
          {
            path: '/list/table-list',
            name: 'searchtable',
            component: './List/TableList',
          },
          {
            path: '/list/basic-list',
            name: 'basiclist',
            component: './List/BasicList',
          },
          {
            path: '/list/card-list',
            name: 'cardlist',
            component: './List/CardList',
          },
          {
            path: '/list/search',
            name: 'searchlist',
            component: './List/List',
            routes: [
              {
                path: '/list/search',
                redirect: '/list/search/articles',
              },
              {
                path: '/list/search/articles',
                name: 'articles',
                component: './List/Articles',
              },
              {
                path: '/list/search/projects',
                name: 'projects',
                component: './List/Projects',
              },
              {
                path: '/list/search/applications',
                name: 'applications',
                component: './List/Applications',
              },
            ],
          },
        ],
      },
      {
        path: '/profile',
        name: 'profile',
        icon: 'profile',
        routes: [
          // profile
          {
            path: '/profile/basic',
            name: 'basic',
            component: './Profile/BasicProfile',
          },
          {
            path: '/profile/advanced',
            name: 'advanced',
            authority: ['admin'],
            component: './Profile/AdvancedProfile',
          },
        ],
      },
      {
        name: 'result',
        icon: 'check-circle-o',
        path: '/result',
        routes: [
          // result
          {
            path: '/result/success',
            name: 'success',
            component: './Result/Success',
          },
          { path: '/result/fail', name: 'fail', component: './Result/Error' },
        ],
      },
      {
        name: 'exception',
        icon: 'warning',
        path: '/exception',
        routes: [
          // exception
          {
            path: '/exception/403',
            name: 'not-permission',
            component: './Exception/403',
          },
          {
            path: '/exception/404',
            name: 'not-find',
            component: './Exception/404',
          },
          {
            path: '/exception/500',
            name: 'server-error',
            component: './Exception/500',
          },
          {
            path: '/exception/trigger',
            name: 'trigger',
            hideInMenu: true,
            component: './Exception/TriggerException',
          },
        ],
      },
      {
        name: 'account',
        icon: 'user',
        path: '/account',
        routes: [
          {
            path: '/account/center',
            name: 'center',
            component: './Account/Center/Center',
            routes: [
              {
                path: '/account/center',
                redirect: '/account/center/articles',
              },
              {
                path: '/account/center/articles',
                component: './Account/Center/Articles',
              },
              {
                path: '/account/center/applications',
                component: './Account/Center/Applications',
              },
              {
                path: '/account/center/projects',
                component: './Account/Center/Projects',
              },
            ],
          },
          {
            path: '/account/settings',
            name: 'settings',
            component: './Account/Settings/Info',
            routes: [
              {
                path: '/account/settings',
                redirect: '/account/settings/base',
              },
              {
                path: '/account/settings/base',
                component: './Account/Settings/BaseView',
              },
              {
                path: '/account/settings/security',
                component: './Account/Settings/SecurityView',
              },
              {
                path: '/account/settings/binding',
                component: './Account/Settings/BindingView',
              },
              {
                path: '/account/settings/notification',
                component: './Account/Settings/NotificationView',
              },
            ],
          },
        ],
      },
      {
        component: '404',
      },
      */
    ],
  },
];
